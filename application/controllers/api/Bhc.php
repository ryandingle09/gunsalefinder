<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/******************************************************************
*
*	Title		: 	About Us
*	Author		:   JaneDoe
*	Filename 	: 	Bhc.php
*	Date 		: 	September 2018
*
******************************************************************/

class Bhc extends CI_Controller 
{

	function __construct()
	{
        parent::__construct();
        $this->load->model('api/bhc_model');
        ini_set('max_execution_time', 5000);
	}

    public function parseCsv( ) {
        $filename = $this->bhc_model->getCsvFilename();
        
        /* 
        **    [0] => product_name
        **    [1] => universal_product_code
        **    [2] => short_description
        **    [3] => long_description
        **    [4] => category_code
        **    [5] => category_description
        **    [6] => product_price
        **    [7] => small_image_path
        **    [8] => Large_image_path
        **    [9] => product_weight
        **    [10] => marp
        **    [11] => msrp
        */

        // Inactvie rerords       
        //$this->bhc_model->inactiveProducts();

        $report = fopen( base_url() . "assets/csv/".$filename,"r");
        $row = 1;
        $insertBatch = [];
        $updateBatch = [];
        while (($line = fgets($report)) !== false) {

            if($row == 1){ $row++; continue; }
            $line = str_replace('"', '', preg_replace('/\r\n/', '', $line));
            $line = explode(',', $line);

            $productcode = isset($line[0]) ? $line[0]  : '';
            $list = $this->bhc_model->getCategoryCode($line[4]);
            $category = isset($list['category']) ? $list['category'] : '';
            $subcategory = isset($list['subcategory']) ? $list['subcategory'] : '';
            //$shipping_fee = $this->bhc_model->getShippingfee($category);
            $upc = isset($line[1]) ? $line[1] : '';
            $standardUpc = $this->common->makeUpcStandard($upc);
            //$description = isset($line[3]) ? $line[3]  : '';
            //$price = isset($line[11]) ? $line[11]  : '';
            //$image_url = ($productcode !='') ? base_url('assets/product-image/bhc/').$productcode.'.jpg' : $default;

            #product Category
           /* $productData = [
                'code' => trim($productcode)
                ,'title' => trim($title)
                ,'category_code' => trim($category)
                ,'sub_category_code' => trim($subcategory)
                ,'upc' => trim($upc)
                ,'price' => trim($price)
                ,'shipping_fee' => trim($shipping_fee)
                ,'image_url' => trim($image_url)
                ,'status' => 'A'
                ,'dealer_id' => BHC
            ];*/
            
            $countRecord = $this->bhc_model->ifProductCategoryExists($standardUpc);
            $brand = explode(' ',trim($productcode));
            $bhcbrand = isset($brand[0]) ? $brand[0] : '';
            $brandid = $this->bhc_model->getBrandID($bhcbrand);

            if ($countRecord[0]['counts'] == '0' ) {
                $productInsert = [
                    'distributor_id' => BHC
                    ,'product_upc' => trim($standardUpc)
                    ,'category_code' => trim($category)
                    ,'sub_category_code' => trim($subcategory)
                    ,'brand_id' => trim($brandid)
                ];
                $insertBatch[] = $productInsert;
            }
            else {
                $productUpdate = [
                    'distributor_id' => BHC
                    ,'product_upc' => trim($standardUpc)
                    ,'category_code' => trim($category)
                    ,'sub_category_code' => trim($subcategory)
                    ,'brand_id' => trim($brandid)
                    ,'date_updated' => date('Y-m-d H:i:s')
                ];
                // unset($productData['upc']);
                $productData['product_upc'] = $countRecord[0]['product_upc'];
                $updateBatch[] = $productUpdate;
            }
        }
		if (is_array($insertBatch)) {
            $limitInserteBatch = array_chunk($insertBatch, 3000);
            foreach ($limitInserteBatch as $parsedArray) {
                $this->bhc_model->productCategoryInsert($parsedArray);
            }
        }

        if (is_array($updateBatch)) {
            $limitUpdateBatch = array_chunk($updateBatch, 3000);
            foreach ($limitUpdateBatch as $parsedArray) {
                $this->bhc_model->productUpdaBatch($parsedArray);
            }
        }
        die('Done');
    #Description
       /* $_report = fopen( base_url() . "assets/csv/".$filename,"r");
        $row = 1;
        while (($_line = fgets($_report)) !== false) {
            
            if($row == 1){ $row++; continue; }
            $_line = str_replace('"', '', preg_replace('/\r\n/', '', $_line));
            $_line = explode(',', $_line);

            $_upc =  isset($_line[1]) ? $_line[1] : '';
            $_long_description =  isset($_line[3]) ? $_line[3] : '';

            $_details = $this->bhc_model->getIdUpc( $_upc );
            var_dump($_details);
            var_dump('UPDATE INSERT '.$_upc);
            echo $this->bhc_model->productDescription($_details['id'], $_long_description );
        }*/
    }

    public function parseCsvDesc( ) 
    {

        $filename = $this->bhc_model->getCsvFilename();
        $_report = fopen( base_url() . "assets/csv/".$filename,"r");
        $row = 1;
        while (($_line = fgets($_report)) !== false) {
            
            if($row == 1){ $row++; continue; }
            $_line = str_replace('"', '', preg_replace('/\r\n/', '', $_line));
            $_line = explode(',', $_line);

            $_upc =  isset($_line[1]) ? $_line[1] : '';
            $description =  isset($_line[3]) ? $_line[3] : '';

            $upc = $this->common->makeUpcStandard($_upc);          
            $data = array(
                'distributor_id' => BHC,
                'product_upc' => $upc,
                'descriptions' => $description,
            );

            $result = $this->bhc_model->productDescExist($upc, BHC);

            if( $result !== false )
            {
                $this->bhc_model->updateProductDesc($upc,BHC, $data);
            }
            else 
            {
                $this->bhc_model->insertproductDesc($data);
            }
        }
        die('Done');

    }

    public function parseCsvImages( ) 
    {

        $filename = $this->bhc_model->getCsvFilename();
        $default = base_url('assets/product-image/').'default.jpg';
        $_report = fopen( base_url() . "assets/csv/".$filename,"r");
        $row = 1;
        while (($_line = fgets($_report)) !== false) {
            
            if($row == 1){ $row++; continue; }
            $_line = str_replace('"', '', preg_replace('/\r\n/', '', $_line));
            $_line = explode(',', $_line);
            $productcode =  isset($_line[0]) ? $_line[0] : '';
            $_upc =  isset($_line[1]) ? $_line[1] : '';
            //$image_url = ($productcode !='') ? base_url('assets/product-image/bhc/').$productcode.'.jpg' : $default;
            $image_url = ($productcode !='') ? 'http://gunsalefinder.com/assets/product-image/bhc/'.$productcode.'.jpg' : $default;

            $upc = $this->common->makeUpcStandard($_upc);          
            $data = array(
                'distributor_id' => BHC,
                'product_upc' => $upc,
                'image_url' => $image_url,
            );

            $result = $this->bhc_model->productImgExist($upc, BHC);

            if( $result !== false )
            {
                $this->bhc_model->updateProductImg($upc,BHC, $data);
            }
            else 
            {
                $this->bhc_model->insertproductImg($data);
            }
        }
        die('Done');

    }
	
}
