<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Utility extends CI_Controller 
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('api/Utility_model','product',true);
		#ini_set('max_execution_time', 5000);
    	#ini_set('mysql.connect_timeout', 30000);
        #ini_set('default_socket_timeout', 30000);
	}

	function index()
	{
		die('Welcome for Utility tools');
	}

	function migrateimages()
	{
		$result = $this->product->getProducts();

    	foreach($result as $deal)
		{
			$image = null;

			if($deal->api_created == 1)
			{
				$image = $this->product->getProductImagebyUPC($deal->upc);
			}
			else
			{
				$image = $this->product->getDealImagebyID($deal->id);
			}
		   //echo 'image is not null <br>';
		   $this->product->updateProduct($deal->id, ['product_image' => $image]);
		  
		}

		echo json_encode($result);

		die('done');
	
	}

	function migratecategories()
	{
		$result = $this->product->getCategoryProducts();
    	foreach($result as $deal)
		{
		    
		   $category = $this->product->getProductCategory($deal->category_code);
		  
		   if($category != '')
		   {
			   	$data = array(
					'category_info' => json_encode($category),
				);
			    $this->product->updateProduct($deal->id, $data);
		   }
		  
		}
		die('done');
	
	}

	function migratesubcategories()
	{
		$result = $this->product->getSubCategoryProducts();

    	foreach($result as $deal)
		{
		    
		   $subcategory = $this->product->getProductSubCategory($deal->category_code, $deal->sub_category_code);
		  
		   if($subcategory != '')
		   {
			   	$data = array(
					'sub_category_info' => json_encode($subcategory),
				);
			    $this->product->updateProduct($deal->id, $data);
		   }
		  
		}
		die('done');
	
	}

	function migratebrands()
	{
		$result = $this->product->getBrandProducts();
    	foreach($result as $deal)
		{
		    
		   $brand = $this->product->getProductBrand($deal->brand_id);
		   if($brand != '')
		   {
			    $this->product->updateProduct($deal->id, ['brand_name' => $brand]);
		   }
		  
		}
		die('done');
	
	}


	function migratedealers()
	{
		$result = $this->product->getDealerProducts();
    	foreach($result as $deal)
		{
		    
		   $dealer = $this->product->getProductDealer($deal->dealer_id);
		  
		   if($dealer != '')
		   {
			   	$data = array(
					'dealer_info' => json_encode($dealer),
				);
			    $this->product->updateProduct($deal->id, $data);
		   }
		  
		}
		die('done');
	
	}

	function migratelikecount()
	{
		$result = $this->product->getProductLike();
    	foreach($result as $deal)
		{
		    
		   $count = $this->product->countProductLike($deal->product_id);
		  
		   if($count > 0)
		   {
			   	$data = array(
					'like_count' => $count,
				);
			    $this->product->updateProduct($deal->product_id, $data);
		   }
		  
		}
		die('done');
	
	}

	function migratedislikecount()
	{
		$result = $this->product->getProductdisLike();
    	foreach($result as $deal)
		{
		    
		   $count = $this->product->countProductdisLike($deal->product_id);
		  
		   if($count > 0)
		   {
			   	$data = array(
					'dislike_count' => $count,
				);
			    $this->product->updateProduct($deal->product_id, $data);
		   }
		  
		}
		die('done');
	
	}

	function migratecommentcount()
	{
		$result = $this->product->getProductComment();
    	foreach($result as $deal)
		{
		    
		   $count = $this->product->countProductComment($deal->product_id);
		  
		   if($count > 0)
		   {
			   	$data = array(
					'comment_count' => $count,
				);
			    $this->product->updateProduct($deal->product_id, $data);
		   }
		  
		}
		die('done');
	
	}

	function migratesubcribecount()
	{
		$result = $this->product->getProductSubcribe();
    	foreach($result as $deal)
		{
		    
		   $count = $this->product->countProductSubcribe($deal->product_id);
		  
		   if($count > 0)
		   {
			   	$data = array(
					'subscriber_count' => $count,
				);
			    $this->product->updateProduct($deal->product_id, $data);
		   }
		  
		}
		die('done');
	
	}

	function migratepricesubcribecount()
	{
		$result = $this->product->getProductdropSubcribe();
    	foreach($result as $deal)
		{
		    
		   $count = $this->product->countProductSubcribe($deal->product_id);
		  
		   if($count > 0)
		   {
			    $this->product->updatedropSubcribe($deal->product_id, $count);
		   }
		  
		}
		die('done');
	
	}

	function migrateratecount()
	{
		$result = $this->product->getProductRated();
    	foreach($result as $deal)
		{
		    
		   $count = $this->product->countProductRate($deal->product_id);
		  
		   if($count > 0)
		   {
			   	$data = array(
					'rate_count' => $count,
				);
			    $this->product->updateProduct($deal->product_id, $data);
		   }
		  
		}
		die('done');
	
	}




	function convertssl()
	{
		$result = $this->product->getAllImages(G4GUSA);
    	foreach($result as $deal)
		{
		   $value = $this->product->getCorrectCategory($deal->upc);
		   $category = isset($value[0]['category_code']) ? $value[0]['category_code'] : null;
		   $subcategory = isset($value[0]['sub_category_code']) ? $value[0]['sub_category_code'] : null;
		   $brand = isset($value[0]['brand_id']) ? $value[0]['brand_id'] : null;
		   $data = array(
				'category_code' => $category,
				'sub_category_code' => $subcategory,
				'brand_id' => $brand,
			);
		    $this->product->updateProduct($deal->id, $data);
		  
		}
		die('done');
	
	}
     
}
