<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ads extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
				
		$this->load->model('dealer/ads_model','adsdb',TRUE);
		$this->load->model('dealer/dealer_model','dealerdb',TRUE);
	}
	
	function index()
	{
		if($this->session->userdata('dealer_login'))
		{
			$id = $this->session->userdata('id');
			$this->load->library('pagination');
			$result_per_page = PERPAGE;  // the number of result per page
			$config['base_url'] = base_url() . '/dealer/ads/index/';
			$config['total_rows'] = $this->adsdb->countMyAds($id);
			$config['per_page'] = $result_per_page;
			//*for boostrap pagination
			$config['full_tag_open'] = '<ul class="pagination pagination-lg">';
			$config['full_tag_close'] = '</ul>';            
			$config['prev_link'] = '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
			//end boostrap pagination
			$this->pagination->initialize($config);
			$offset = ($this->uri->segment(4) != '')?$this->uri->segment(4):0;
			$data['countrow'] = $config['total_rows'];
			$data['results'] = $this->adsdb->getAllMyAds($id, $result_per_page, $offset);
			$data['info'] = $this->dealerdb->getUserinfobyid($id);

			if($this->input->get('id'))
			{
				$add_id = $this->input->get('id');

				$this->form_validation->set_rules('url', 'Ads Link Url', 'required|trim|valid_url');
				
				$ads 			= $this->adsdb->getAdsByid($add_id)[0];
				$add_location 	= $this->adsdb->getAdsLocationByid($ads->location_id)[0];
				$data['ads_location'] = $add_location;

				$title = ($this->input->post('title')) ? $this->input->post('title') : $ads->title;
				$description = ($this->input->post('description')) ? $this->input->post('description') : $ads->description;

				if(in_array($add_location->location_code, ['VPMF','VPRMF','SLMF','HMF']) == TRUE)
				{
					$this->form_validation->set_rules('title', 'Ads Feature New Title', 'required|trim');
				}

				$real_dimentions = str_replace(' ','', $add_location->size);
				$real_dimentions = str_replace('*',',', $real_dimentions);
				$real_dimentions = str_replace('x',',', $real_dimentions);

				$b_height 	= $real_dimentions[0];
				$b_width 	= $real_dimentions[1];
				
				if($this->input->post())
				{
					$config['upload_path']          = './assets/uploads/ads/';
					$config['allowed_types']        = 'gif|jpg|png|svg';
					$config['max_size']             = 100;
					//$config['max_width']            = $b_height;
					//$config['max_height']           = $b_width;
					$config['encrypt_name']         = TRUE;
					$config['file_ext_tolower']     = TRUE;

					$this->load->library('upload', $config);
					
					$message = '';

					if ( ! $this->upload->do_upload('ad_photo'))
					{
						$message = '<div class="alert alert-danger"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$this->upload->display_errors().'</strong></div>';
						$this->session->set_flashdata('message', $message);
					}
					else
					{
						if ($this->form_validation->run() == FALSE)
						{
							$message = '<div class="alert alert-danger"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.validation_errors().'</strong></div>';
							$this->session->set_flashdata('message', $message);
						}
						else
						{
							$full_path 	= $this->upload->data('full_path');
							$image_url 	= 'assets/uploads/ads/'.$this->upload->data('file_name');
							$status 	= $this->input->post('status');
							$url 		= $this->input->post('url');

							if(!is_null($ads->image_full_path))
							{
								if(file_exists($ads->image_full_path)) unlink($ads->image_full_path);
							}

							# delete old image on admin source via api curl method
							$params=['image_url' => $ads->image_url, 'image_full_path' => $ads->image_full_path];
							$ch = curl_init(ADMINURL.'/pbanner/remove_image?image_full_path='.$ads->image_full_path);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
							$response = curl_exec($ch);
							curl_close($ch);
							# end

							$items = [
								'image_full_path' 	=> $full_path,
								'image_url' 		=> $image_url,
								'link_url' 			=> $url,
								'title' 			=> $title,
								'description' 		=> $description,
								'status' 			=> $status,
								'updated_by' 		=> $id,
								'updated_at' 		=> date('Y-m-d H:i:s')
							];

							$this->adsdb->updatetAds($items, $add_id);

							$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>You ads has been successfully updated.</strong></div>';
							$this->session->set_flashdata('message', $message);

							redirect('/'.$this->input->get('redirect_to'), 'refresh');
						}
					}
				}
			}

			$this->load->view('dealers/myads', $data);
			
		} else {
			redirect('dealer-login');
		}


	}

	function pending()
	{
		if($this->session->userdata('dealer_login'))
		{
			$id = $this->session->userdata('id');
			$this->load->library('pagination');
			$active=FALSE;
			$result_per_page = PERPAGE;  // the number of result per page
			$config['base_url'] = base_url() . '/dealer/ads/pending/index/';
			$config['total_rows'] = $this->adsdb->countMyAds($id, $active);
			$config['per_page'] = $result_per_page;
			//*for boostrap pagination
			$config['full_tag_open'] = '<ul class="pagination pagination-lg">';
			$config['full_tag_close'] = '</ul>';            
			$config['prev_link'] = '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
			//end boostrap pagination
			$this->pagination->initialize($config);
			$offset = ($this->uri->segment(5) != '')?$this->uri->segment(5):0;
			$data['countrow'] = $config['total_rows'];
			$data['results'] = $this->adsdb->getAllMyAds($id, $result_per_page, $offset, $active);
			$data['info'] = $this->dealerdb->getUserinfobyid($id);
			$data['title'] = 'Pending Ads';

			if($this->input->get('id'))
			{
				$add_id = $this->input->get('id');

				$this->form_validation->set_rules('url', 'Ads Link Url', 'required|trim|valid_url');
				
				$ads 			= $this->adsdb->getAdsByid($add_id)[0];
				$add_location 	= $this->adsdb->getAdsLocationByid($ads->location_id)[0];
				$data['ads_location'] = $add_location;

				$title = ($this->input->post('title')) ? $this->input->post('title') : $ads->title;
				$description = ($this->input->post('description')) ? $this->input->post('description') : $ads->description;

				if(in_array($add_location->location_code, ['VPMF','VPRMF','SLMF','HMF']) == TRUE)
				{
					$this->form_validation->set_rules('title', 'Ads Feature New Title', 'required|trim');
				}
				
				$real_dimentions = str_replace(' ','', $add_location->size);
				$real_dimentions = str_replace('*',',', $real_dimentions);
				$real_dimentions = str_replace('x',',', $real_dimentions);

				$b_height 	= $real_dimentions[0];
				$b_width 	= $real_dimentions[1];
				
				if($this->input->post())
				{
					$config['upload_path']          = './assets/uploads/ads/';
					$config['allowed_types']        = 'gif|jpg|png|svg';
					$config['max_size']             = 100;
					//$config['max_width']            = $b_height;
					//$config['max_height']           = $b_width;
					$config['encrypt_name']         = TRUE;
					$config['file_ext_tolower']     = TRUE;

					$this->load->library('upload', $config);
					
					$message = '';

					if ( ! $this->upload->do_upload('ad_photo'))
					{
						$message = '<div class="alert alert-danger"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$this->upload->display_errors().'</strong></div>';
						$this->session->set_flashdata('message', $message);
					}
					else
					{
						if ($this->form_validation->run() == FALSE)
						{
							$message = '<div class="alert alert-danger"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.validation_errors().'</strong></div>';
							$this->session->set_flashdata('message', $message);
						}
						else
						{
							$full_path 	= $this->upload->data('full_path');
							$image_url 	= 'assets/uploads/ads/'.$this->upload->data('file_name');
							$status 	= $this->input->post('status');
							$url 		= $this->input->post('url');

							if(!is_null($ads->image_full_path))
							{
								if(file_exists($ads->image_full_path)) unlink($ads->image_full_path);
							}

							# delete old image on admin source via api curl method
							$params=['image_url' => $ads->image_url, 'image_full_path' => $ads->image_full_path];
							$ch = curl_init(ADMINURL.'/pbanner/remove_image?image_full_path='.$ads->image_full_path);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
							$response = curl_exec($ch);
							curl_close($ch);
							# end

							$items = [
								'image_full_path' 	=> $full_path,
								'image_url' 		=> $image_url,
								'link_url' 			=> $url,
								'status' 			=> $status,
								'title' 			=> $title,
								'description' 		=> $description,
								'updated_by' 		=> $id,
								'updated_at' 		=> date('Y-m-d H:i:s')
							];

							$this->adsdb->updatetAds($items, $add_id);

							$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>You ads has been successfully updated.</strong></div>';
							$this->session->set_flashdata('message', $message);

							redirect('/'.$this->input->get('redirect_to'), 'refresh');
						}
					}
				}
			}

			$this->load->view('dealers/myads', $data);
			
		} else {
			redirect('dealer-login');
		}
	}

	function select( $id = false )
	{
		$id = base64_decode($id);
		if( $this->common->checkDealerExists($id) )
		{
			$data['subscriptions'] = $this->common->getallSubscriptions();
			$this->load->view('dealers/subcriptions');
		} else {
			redirect('dealer-login');
		}
	}

	public function remove_image()
	{
		$path = $this->input->get('image_full_path');
		
		if(file_exists($path))
		{
			unlink($path);
		}

		echo 'Image successfull removed.';
	}
	
}
