<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('dealer/post_model','postdb',TRUE);
		$this->load->model('dealer/dealer_model','dealerdb',TRUE);
		$this->load->model('dealer/ads_model','adsdb',TRUE);
		$this->load->helper('string');
		$this->load->library('image_lib');
		$this->load->helper('text');
		$this->load->library('pagination');
		$this->load->helper(array('my_url_helper', 'payment_helper'));
	}

	function index()
    {
    	$data['pagetitle'] = 'Post ad';

		if($this->session->userdata('dealer_login'))
		{
			$data['results'] = $this->postdb->getAllAdslocation();
			$data['myads'] = $this->postdb->getmyads();

			$this->load->view('ads/postad', $data);
		} else {
			$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Please login as a dealer to continue</strong></div>';
			$this->session->set_flashdata('dealermessage', $message);
			redirect('dealer-login');
		}
	}

	function addpackage($id)
	{
		#use_ssl();
        if($this->session->userdata('dealer_login'))
		{
			$ads = $this->postdb->getAllAdslocationbyid($id);
			foreach ($ads as $item):
		        $data = array(
		            'id' =>$id,
		            'qty' => 1,
		            'name' => $item->title,
		            'size' => $item->size,
		            'price' => $item->price,  
		        
		        );
		        $this->cart->insert($data);
	    	endforeach;
	        redirect('dealer/post/mycart'); 
		} else {
			redirect('dealer-login');
		}
	}

	function mycart()
	{
		$data['pagetitle'] = 'Cart';

		if($this->session->userdata('dealer_login'))
		{
			$id = $this->session->userdata('id');
			$data['userid'] = $id ;
			$this->load->view('ads/mycart', $data);
		} else {
			redirect('dealer-login');
		}
	}
	
	function remove($rowid) 
    {

		if($this->session->userdata('dealer_login'))
		{
			if ($rowid==="all"){
				$this->cart->destroy();
			}else{
				$data = array(
				'rowid' => $rowid,
				'qty' => 0
				);
				$this->cart->update($data);
			}
			redirect('dealer/post/mycart');
		} else {
			redirect('dealer-login');
		}

	}
	
	function purchasenow()
	{

		if($this->session->userdata('dealer_login'))
		{
			$user = $this->dealerdb->getUserinfobyid($this->session->userdata('id'))[0];
			$country = $this->dealerdb->getUserCountry($user->country)[0];
			$name = ''; # $this->security->xss_clean($this->input->post('name'));
			$type = 'ALL'; # $this->security->xss_clean($this->input->post('type'));
			$cardno = $this->security->xss_clean($this->input->post('cardno'));
			$month = $this->security->xss_clean($this->input->post('month'));
			$year = $this->security->xss_clean($this->input->post('year'));
			$cvv = $this->security->xss_clean($this->input->post('cvv'));
			$total = $this->security->xss_clean($this->input->post('total'));
			# $pmethod = $this->security->xss_clean($this->input->post('pmethod'));
			$ip = $this->input->ip_address();

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
			//$this->form_validation->set_rules('name', 'Card Name', 'trim|required');
			//$this->form_validation->set_rules('type', 'Credit Card Type', 'trim|required');
			$this->form_validation->set_rules('cardno', 'Card Number', 'trim|required');
			$this->form_validation->set_rules('month', 'Expiration Month', 'trim|required');
			$this->form_validation->set_rules('year', 'Expiration Year', 'trim|required');
			$this->form_validation->set_rules('cvv', 'Card Verification Number', 'trim|required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', validation_errors());
				redirect('dealer/post/mycart');
			}
			
			$order = array(
				'total' => $total,
				'payment_method' => CARD,
				'status' => 0,
				'created_by' => $this->session->userdata('id'),
				'created_at' => date("Y-m-d H:i:s")
			);

			$this->postdb->insertOrder($order);
			$ord_id = $this->db->insert_id();

			$order_ads = [];
			if ($cart = $this->cart->contents()):
				foreach ($cart as $item):
					$order_item = array(
						'order_id' => $ord_id,
						'ads_id' => $item['id'],
						'qty' => $item['qty'],
						'price' => $item['price'],
						'status' => 'P'
					);
					$this->postdb->insertOrderAds($order_item);
					$id = $this->db->insert_id();

					$arr = [
						'order_id' => $ord_id,
						'ads_order_id' => $id,
						'ads_name' => $item['name'],
						'ads_id' => $item['id'],
						'qty' => $item['qty'],
						'price' => $item['price'],
						'status' => 'P'
					];
					array_push($order_ads, $arr);
				endforeach;
			endif;

			$gw = new Payment_helper;
			$gw->setLogin(USERNAME_PAYMENT, PASSWORD_PAYMENT);
			// $gw->setLogin("demo", "password");
			$gw->setBilling($user->first_name, $user->last_name, $user->company_name, $user->address, $user->address, $user->city, $user->state, $user->zip_code, $country->name, $user->contact, '', $user->email, $user->web_url);
			$gw->setShipping($user->first_name, $user->last_name, $user->company_name, $user->address,$user->address, $user->city, $user->state, $user->zip_code, $country->name, $user->email);
			
			foreach ($order_ads as $item):
				$gw->setOrder($ord_id, $item['ads_name'],'0', $user->address, $item['ads_order_id'], $ip);
			endforeach;

			$r = $gw->doSale($total, $cardno, ''.$month.'/'.$year.'', $cvv);

			$response_code = $r['response_code'];
			$responsetext = $r['responsetext'];

			$payment_info = [
				'user_info' => [
					'user_id' 		=> $user->id,
					'first_name' 	=> $user->first_name,
					'last_name' 	=> $user->last_name, 
					'company_name' 	=> $user->company_name, 
					'address' 		=> $user->address, 
					'address' 		=> $user->address, 
					'city' 			=> $user->city, 
					'state' 		=> $user->state, 
					'zip_code' 		=> $user->zip_code, 
					'country' 		=> $country->name, 
					'contact' 		=> $user->contact, 
					'email' 		=> $user->email, 
					'web_url' 		=> $user->web_url
				],
				'order_info' => $order_ads,
				'payment_info' => [
					'country' 	=> $country,
					//'name' 		=> $name,
					//'type' 		=> $type,
					'cardno' 	=> $cardno,
					'month' 	=> $month,
					'year' 		=> $year,
					'cvv' 		=> $cvv,
					'total' 	=> $total,
					//'pmethod' 	=> $pmethod,
					'ip' 		=> $ip
				],
				'payment_gateway_response' => [
					'response' 		=> $r['response'],
					'response_code' => $r['response_code'],
					'responsetext' 	=> $r['responsetext'],
					'authcode' 		=> $r['authcode'],
					'transactionid' => $r['transactionid'],
					'avsresponse' 	=> $r['avsresponse'],
					'cvvresponse' 	=> $r['cvvresponse'],
					'orderid' 		=> $r['orderid'],
					'type' 			=> $r['type'],
				]
			];

			$this->postdb->updatetOrder(['payment_info' => json_encode($payment_info)], $ord_id);

			if($response_code !== '100')
			{
				$message = '<div class="alert alert-danger"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$responsetext.'.</strong></div>';
				$this->session->set_flashdata('message', $message);
				redirect('dealer/post/mycart');
			}

			foreach ($order_ads as $item):
				$this->adsdb->insertAds([
					'dealer_id' 	=> $user->id, 
					'location_id' 	=> $item['ads_id'],
					'price' 		=> $item['price'],
					'title' 		=> $item['ads_name'],
					'state' 		=> $user->state,
					'created_at' 	=> date('Y-m-d H:i:s'),
					'updated_at' 	=> date('Y-m-d H:i:s'),
				]);
			endforeach;

			$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Your Order has been placed. Please update your ads photo and set your ads status to active.</strong></div>';
			$this->session->set_flashdata('message', $message);
			$this->cart->destroy();

			redirect('/pending-ads');

		} else {
			redirect('dealer-login');
		}
		
	}
	
	function saveads() 
	{
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->form_validation->set_rules('category', 'Category', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('subcategory', 'Sub category', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[300]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[12]');
		$this->form_validation->set_rules('location', 'location', 'trim|max_length[300]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['pagetitle'] = "Post ad";
			$data['category'] = $this->common->getallcategory();
			$data['location'] = $this->common->getallcity();
			$data['code'] = random_string('alnum',6);
			$this->load->view('postads', $data);
		}
		else
		{
			$code = $this->security->xss_clean($this->input->post('code'));
			$title = $this->security->xss_clean($this->input->post('title'));
			$category = $this->security->xss_clean($this->input->post('category'));
			$subcategory = $this->security->xss_clean($this->input->post('subcategory'));
			$location = $this->security->xss_clean($this->input->post('location'));
			$price = $this->security->xss_clean($this->input->post('price'));
			$description = $this->security->xss_clean($this->input->post('editor1'));
		
			
			if($this->ads->checkadsexist($title)=== FALSE)
			{
				$data = array(
					'title' => $title,
					'category' => $category,
					'subcategory' => $subcategory,
					'location' => $location,
					'price' => $price,
					'description' => $description,
					'enabled'=> 0,
					'created_date' => date('Y-m-d H:i:s'),
					'created_by' => $this->session->userdata('id')
				);
				
				if($this->ads->save_post($data) !== FALSE)
				{
					$id = $this->db->insert_id(); 
					$this->ads->insert_images($id, $code);
					$this->ads->delete_tempimages($code);
					$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Your listing has been submitted for approval.</strong></div>';
					$this->session->set_flashdata('message', $message);
					redirect("ads/pendingads");
				}
				else{
					$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Error Occurs. Please reports to administrator.</div>';
					$this->session->set_flashdata('message', $message);
					$data['category'] = $this->common->getallcategory();
					$data['location'] = $this->common->getallcity();
					$data['code'] = random_string('alnum',6);
					$this->load->view('postads', $data);
				}
			}
			else{
				$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Error Occurs. Please reports to administrator.</div>';
				$this->session->set_flashdata('message', $message);
				redirect("ads/post");
			}
		
		}
	}
	
	function editpost($id)
	{
		if($this->session->userdata('dealer_login'))
		{
			$data['pagetitle'] = 'Edit ads';
			$data['category'] = $this->common->getallcategory();
			$data['location'] = $this->common->getallcity();
			$data['ads'] = $this->ads->getadsbyid($id);
			$data['adsimages'] = $this->ads->getadsimages($id);
			$data['code'] = random_string('alnum',6);
			$this->load->view('editads', $data);
		} else {
			redirect('login');
		}
		
	}

	function deleteimages($adsid = 0, $id = 0)
	{
		if($this->ads->delete_images($id) !== FALSE)
		{
			redirect("ads/editpost/".$adsid);
		}
		else{
			$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Error Occurs. Please reports to administrator.</div>';
			$this->session->set_flashdata('message', $message);
			redirect('ads/pendingads');
		}
	}

	function updateads()
	{
		
		$adsid = $this->input->post('adsid');
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->form_validation->set_rules('category', 'Category', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('subcategory', 'Sub category', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[300]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[12]');
		$this->form_validation->set_rules('location', 'location', 'trim|max_length[300]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['pagetitle'] = 'Edit ads';
			$data['category'] = $this->common->getallcategory();
			$data['location'] = $this->common->getallcity();
			$data['ads'] = $this->ads->getadsbyid($adsid);
			$data['adsimages'] = $this->ads->getadsimages($id);
			$data['code'] = random_string('alnum',6);
			$this->load->view('editads/'.$adsid, $data);
		}
		else
		{
			$code = $this->input->post('code');
			$data = array(
				'title' => $this->input->post('title'),
				'category' => $this->input->post('category'),
				'subcategory' => $this->input->post('subcategory'),
				'location' => $this->input->post('location'),
				'price' => $this->input->post('price'),
				'description' => $this->input->post('editor1'),
				'enabled'=> 1,
				'created_date' => date('Y-m-d H:i:s'),
				'created_by' => $this->session->userdata('id')
			);
			
			if($this->ads->updateads($data, $adsid) !== FALSE)
			{
				$this->ads->insert_images($adsid, $code);
				$this->ads->delete_tempimages($code);
				$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Your item has been updated.</strong></div>';
				$this->session->set_flashdata('message', $message);
				redirect("ads/pendingads");
			}
			else
			{
				$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Item has been added to watchlist.</strong></div>';
				$this->session->set_flashdata('message', $message);
				redirect("ads/pendingads");
			}
		}
	}

	function addwatchlist($id = 0, $title = 0)
	{
		$data = array(
			'ads_id' => $id,
			'created_date' => date('Y-m-d H:i:s'),
			'user_id' =>$this->session->userdata('id')
		);
		
		if($this->ads->save_watchlist($data, $id) !== FALSE)
		{
			$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Item has been added to watchlist.</strong></div>';
			$this->session->set_flashdata('message', $message);
			redirect('ads/viewpost/'.$id.'/'.$title);
			
		}
		else{
			$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Error Occurs. Please reports to administrator.</div>';
			$this->session->set_flashdata('message', $message);
			redirect('ads/viewpost/'.$id.'/'.$title);
		}
		
	}
	
	function adslist($id = 0)
	{

		$data['pagetitle'] = 'List ads';
		/*PAGING*/
		$result_per_page = PERPAGE;  // the number of result per page
		$config['base_url'] = base_url() . '/ads/adslist/'.$id.'/';
		$config['total_rows'] = $this->ads->countmyads($id);
		$config['per_page'] = $result_per_page;
		//*for boostrap pagination
		$config['full_tag_open'] = '<ul class="pagination pagination-lg">';
		$config['full_tag_close'] = '</ul>';            
		$config['prev_link'] = '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
		//end boostrap pagination
		$this->pagination->initialize($config);
		$offset = ($this->uri->segment(3) != '')?$this->uri->segment(3):0;
		$data['myads'] = $this->ads->getmyads($id, $result_per_page, $offset);
		$data['countrow'] = $config['total_rows'];
		/*PAGING*/
		$data['info'] = $this->register->getuserinfobyid($id);
		//$data['myads'] = $this->ads->getmyads($id);
		$data['city'] = $this->common->getallcity();
		$this->load->view('adslist', $data);
	}
	
	# REPORT ADS
	#-------------------------------------------------------------
	function reportsads($id = 0, $title = 0)
	{
		if($this->session->userdata('dealer_login'))
		{
			$reason = $this->input->post('reason');
			$comment = $this->input->post('comment');
			$data = array(
				'ads_id' => $id,
				'reason' => $reason,
				'comments' => $comment,
				'created_date' => date('Y-m-d H:i:s'),
				'user_id' =>$this->session->userdata('id')
			);
			
			if($this->ads->save_reported($data, $id) !== FALSE)
			{
				$this->session->set_flashdata('message', '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Item has been reported to administrator.</strong></div>');
				redirect('ads/viewpost/'.$id.'/'.$title);
				
			}
			else{
				$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Error Occurs. Please reports to administrator.</div>';
				$this->session->set_flashdata('message', $message);
				redirect('ads/viewpost/'.$id.'/'.$title);
			}
		}else {
			redirect('login');
		}
		
	}
	# SEND EMAIL
	#-------------------------------------------------------------
	function sendemail($id = 0, $title = 0)
	{
		
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$contact = $this->input->post('contact');
		$message = $this->input->post('message');
		$data = array(
			'name' => $name,
			'email' => $email,
			'contact' => $contact,
			'message' => $message,
			'created_date' => date('Y-m-d H:i:s'),
			'created_by' =>$this->session->userdata('id')
		);
		
		if($this->ads->save_sendemail($data) !== FALSE)
		{
			$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Your message has been sent to seller.</strong></div>';
			$this->session->set_flashdata('message', $message);
			redirect('ads/viewpost/'.$id.'/'.$title);
			
		}
		else{
			$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Error Occurs. Please reports to administrator.</div>';
			$this->session->set_flashdata('message', $message);
			redirect('ads/viewpost/'.$id.'/'.$title);
		}
		
	}
	
}
