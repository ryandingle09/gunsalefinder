<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportedad extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('dealer/product_model','productdb',TRUE);
		$this->load->model('dealer/dealer_model','dealerdb',TRUE);
		$this->load->model('dealer/Reportdeal_model','reportdealdb',TRUE);
	}
	
	function index()
	{
		if($this->session->userdata('dealer_login'))
		{
			
			$id = $this->session->userdata('id');
			$this->load->library('pagination');
			$result_per_page = PERPAGE;  // the number of result per page
			$config['base_url'] = base_url() . '/dealer/product/index/';
			$config['total_rows'] = $this->reportdealdb->countMyReportedProducts($id);
			$config['per_page'] = $result_per_page;
			//*for boostrap pagination
			$config['full_tag_open'] = '<ul class="pagination pagination-lg">';
			$config['full_tag_close'] = '</ul>';            
			$config['prev_link'] = '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
			//end boostrap pagination
			$this->pagination->initialize($config);
			$offset = ($this->uri->segment(4) != '')?$this->uri->segment(4):0;
			$data['countrow'] = $config['total_rows'];
			$products = $this->reportdealdb->getDealerProducts();
			$in_products = array();
			foreach($products as $product)
			{
				array_push($in_products, $product->id);
			}
			$data['results'] = $this->reportdealdb->getAllMyReportedProducts($in_products, $result_per_page, $offset);
			//debug($data, 1);
			$data['info'] = $this->dealerdb->getUserinfobyid($id);
			$data['pagetitle'] = 'Reported ads';
			$this->load->view('dealers/reportedads', $data);
			
		} else {
			redirect('dealer-login');
		}


	}


	
}
