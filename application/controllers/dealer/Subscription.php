<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscription extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('dealer/Subscription_model','subscriptiondb',TRUE);
		$this->load->model('dealer/dealer_model','dealerdb',TRUE);
		$this->load->helper(array('my_url_helper', 'payment_helper'));
	}
	
	function index()
	{
		
		if($this->session->userdata('dealer_login'))
		{
			$data['subscriptions'] = $this->subscriptiondb->getallSubscriptions();
			$data['pagetitle'] = 'Subscription';
			$this->load->view('dealers/subcriptions');
		} else {
			redirect('dealer-login');
		}
	}

	function select( $id = false )
	{
		$id = base64_decode($id);
		$result = $this->subscriptiondb->getDealerDetails($id);

		if( $result )
		{
			$sess_array = array();
			 foreach($result as $row)
			 {
			   if($row->status == 1){
				    $newdata = array(
					   'id'  => $row->id,
					   'name'     => $row->first_name.' '.$row->last_name,
					   'dealer_login' => true
					);
					$this->session->set_userdata($newdata);
			   }else{
				   $status = false;
			   }
			  
			 }
			$data['subscriptions'] = $this->common->getallSubscriptions();
			$this->load->view('dealers/subcriptions', $data);
		} else {
			redirect('dealer-login');
		}
	}

	public function payment_form() 
	{
		if($this->session->userdata('dealer_login'))
		{
			$id 				= $this->session->userdata('id');
			$subscription 		= ($this->input->get('subscription')) ? $this->input->get('subscription') : $this->security->xss_clean($this->input->post('subscription'));
			$subscription_data 	= $this->dealerdb->getSubscription( $subscription )[0];

			$data['data'] 		= $subscription_data;

			$this->load->view('dealers/mysubscription', $data);
		}
		else
		{
			redirect('dealer-login');
		}
	}

	// function selectpackage( $id = false )
	// {
	// 	if($this->session->userdata('dealer_login'))
	// 	{
	// 		$subscriptions = $this->subscriptiondb->getAllSubscriptionbyid($id);
	// 		foreach ($subscriptions as $item):
	// 	        $data = array(
	// 	            'id' =>$id,
	// 	            'qty' => 1,
	// 	            'name' => $item->name,
	// 	            'description' => $item->description,
	// 	            'price' => $item->price,  
		        
	// 	        );
	// 	        $this->cart->insert($data);
	//     	endforeach;
	//         redirect('dealer/subscription/mysubscription'); 
	// 	} else {
	// 		redirect('dealer-login');
	// 	}
	// }

	function mysubscription()
	{
		if($this->session->userdata('dealer_login'))
		{
			$id = $this->session->userdata('id');
			$data['userid'] = base64_encode($id);
			$this->load->view('dealers/mysubscription', $data);
		} else {
			redirect('dealer-login');
		}
	}
	
		
	function subscribenow()
	{
		if($this->session->userdata('dealer_login'))
		{
			$user		= $this->dealerdb->getUserinfobyid($this->session->userdata('id'))[0];
			$country 	= $this->dealerdb->getUserCountry($user->country)[0];
			$name 		= '';#$this->security->xss_clean($this->input->post('name'));
			$type 		= 'ALL';#$this->security->xss_clean($this->input->post('type'));
			$cardno 	= $this->security->xss_clean($this->input->post('cardno'));
			$month 		= $this->security->xss_clean($this->input->post('month'));
			$year 		= $this->security->xss_clean($this->input->post('year'));
			$cvv 		= $this->security->xss_clean($this->input->post('cvv'));
			$total 		= $this->security->xss_clean($this->input->post('total'));
			//$pmethod 	= $this->security->xss_clean($this->input->post('pmethod'));
			$ip 		= $this->input->ip_address();

			$id 				= $this->session->userdata('id');
			$user_subscription 	= $this->dealerdb->getCurrentSubscription( $id )[0];
			$subscription 		= $this->security->xss_clean($this->input->post('subscription'));
			$order 				= $this->dealerdb->getSubscription( $subscription )[0];
			$order_data 		= $this->dealerdb->getSubscription( $subscription );

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
			//$this->form_validation->set_rules('name', 'Card Name', 'trim|required');
			//$this->form_validation->set_rules('type', 'Credit Card Type', 'trim|required');
			$this->form_validation->set_rules('cardno', 'Card Number', 'trim|required');
			$this->form_validation->set_rules('month', 'Expiration Month', 'trim|required');
			$this->form_validation->set_rules('year', 'Expiration Year', 'trim|required');
			$this->form_validation->set_rules('cvv', 'Card Verification Number', 'trim|required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', validation_errors());
				redirect('dealer/subscription/payment_form?subscription='.$subscription.'&user_id='.base64_encode($user->id).'');
			}

			$gw = new Payment_helper;
			$gw->setLogin(USERNAME_PAYMENT, PASSWORD_PAYMENT);
			//$gw->setLogin("demo", "password");
			$gw->setBilling($user->first_name, $user->last_name, $user->company_name, $user->address, $user->address, $user->city, $user->state, $user->zip_code, $country->name, $user->contact, '', $user->email, $user->web_url);
			$gw->setShipping($user->first_name, $user->last_name, $user->company_name, $user->address,$user->address, $user->city, $user->state, $user->zip_code, $country->name, $user->email);
			
			$gw->setOrder($order->id, $order->name,'0', $user->address, $user_subscription->id, $ip);

			$r = $gw->doSale($total, $cardno, ''.$month.'/'.$year.'', $cvv);

			$response_code = $r['response_code'];
			$responsetext = $r['responsetext'];

			$payment_info = [
				'user_info' => [
					'user_id' 		=> $user->id,
					'first_name' 	=> $user->first_name,
					'last_name' 	=> $user->last_name, 
					'company_name' 	=> $user->company_name, 
					'address' 		=> $user->address, 
					'address' 		=> $user->address, 
					'city' 			=> $user->city, 
					'state' 		=> $user->state, 
					'zip_code' 		=> $user->zip_code, 
					'country' 		=> $country->name, 
					'contact' 		=> $user->contact, 
					'email' 		=> $user->email, 
					'web_url' 		=> $user->web_url
				],
				'order_info' => $order_data,
				'payment_info' => [
					'country' 	=> $country,
					//'name' 		=> $name,
					//'type' 		=> $type,
					'cardno' 	=> $cardno,
					'month' 	=> $month,
					'year' 		=> $year,
					'cvv' 		=> $cvv,
					'total' 	=> $total,
					//'pmethod' 	=> $pmethod,
					'ip' 		=> $ip
				],
				'payment_gateway_response' => [
					'response' 		=> $r['response'],
					'response_code' => $r['response_code'],
					'responsetext' 	=> $r['responsetext'],
					'authcode' 		=> $r['authcode'],
					'transactionid' => $r['transactionid'],
					'avsresponse' 	=> $r['avsresponse'],
					'cvvresponse' 	=> $r['cvvresponse'],
					'orderid' 		=> $r['orderid'],
					'type' 			=> $r['type'],
				]
			];

			if($response_code !== '100')
			{
				$message = '<div class="alert alert-danger"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$responsetext.'.</strong></div>';
				$this->session->set_flashdata('message', $message);

				redirect('dealer/subscription/payment_form?subscription='.$subcription.'&user_id='.base64_encode($user->id).'');
			}

			$this->dealerdb->subscriptionPayment([
				'payment_info' => json_encode($payment_info),
				'dealer_id' => $id,
				'subscription_id' => $subscription
			]);

			$this->dealerdb->addSubscription([
				'subscription_id' => $subscription,
				'dealer_id' => $id,
				'is_paid' => 1
			]);

			$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Awesome ! You have been successfully purchased your subscription plan. Thank you!</strong></div>';
			$this->session->set_flashdata('message', $message);

			redirect('dealer/profile?tab=subscription');

		} 
		else 
		{
			redirect('dealer-login');
		}
		
	}
	
}
