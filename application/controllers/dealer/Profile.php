<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('registration_model','register',TRUE);
		$this->load->model('dealer/dealer_model','dealerdb',TRUE);
		$this->load->library('pagination');
		$this->load->helper(array('my_url_helper', 'payment_helper'));
	}

	public function index()
	{
		if($this->session->userdata('dealer_login'))
		{
			$id = $this->session->userdata('id');
			$current_subscription_data = (count($this->dealerdb->getCurrentSubscription( $id )) !== 0) ? $this->dealerdb->getCurrentSubscription( $id ) : [];
			$subscription_data = $this->dealerdb->getSubscription( (isset($current_subscription_data[0]->subscription_id)) ? $current_subscription_data[0]->subscription_id : 0 );
			
			$data['info'] 						= $this->dealerdb->getUserinfobyid($id);
			$data['states'] 					= $this->common->getallStates();
			$data['countries'] 					= $this->common->getallCountries();
			$data['subscriptions'] 				= $this->common->getallSubscriptions();
			$data['productcount'] 				= $this->dealerdb->countmyProduct( $id );
			$data['current_subscription_data'] 	= $current_subscription_data;
			$data['subscription_data'] 			= $subscription_data;

			$data['userid'] = base64_encode($id);
			$data['pagetitle'] = 'Profile';
			$this->load->view('dealers/profile', $data);
		} else {
			redirect('dealer-login');
		}
	}

	function updateinformation()
	{
	   $this->load->library('form_validation');
	   $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>', '</div>');
	   $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
	   $this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');
	   $this->form_validation->set_rules('email', 'Email', 'trim|required');
	   $this->form_validation->set_rules('zipcode', 'Zip code', 'trim|required');
	   $this->form_validation->set_rules('country', 'Country', 'trim|required');
	   $this->form_validation->set_rules('webpage', 'Webpage', 'trim|required');
	
	   if($this->form_validation->run() == FALSE)
	   {
			$id = $this->session->userdata('id');
			$data['info'] = $this->dealerdb->getUserinfobyid($id);
			$data['states'] = $this->common->getallStates();
			$data['countries'] = $this->common->getallCountries();
			$data['userid'] = base64_encode($id);
			$data['pagetitle'] = 'Profile';
			$this->load->view('dealers/profile', $data);
	   }
	   else{
		   
		    $id = $this->session->userdata('id');
		    $title = $this->input->post('title');
			$firstname = $this->input->post('firstname');
			$middlename = $this->input->post('middlename');
			$lastname = $this->input->post('lastname');
			$suffix = $this->input->post('suffix');
			$email = $this->input->post('email');
			$address = $this->input->post('address');
			$city = $this->input->post('city');
			$state = $this->input->post('state');
			$zipcode = $this->input->post('zipcode');
			$country = $this->input->post('country');
			$jobtitle = $this->input->post('jobtitle');
			$contactnumber = $this->input->post('contact');
			$webpage = $this->input->post('webpage');
	
			if($_FILES['profile']['name'] == '') {
				
				$data = array(
					'title' => $title,
					'first_name' => $firstname,
					'middle_name' => $middlename,
					'last_name' => $lastname,
					'suffix' => $suffix,
					'email' => $email,
					'address' => $address,
					'city' => $city,
					'state' => $state,
					'zip_code' => $zipcode,
					'country' => $country,
					'job_title' => $jobtitle,
					'contact' => $contactnumber,
					'web_url' => $webpage,
					'updated_at' => date("Y-m-d H:i:s"),
					'updated_by' => $this->session->userdata('id')
				);

				$this->dealerdb->updateuserprofile($id,$data);
				$this->session->set_flashdata('message', '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Information successfully updated.</strong></div>');
				redirect('dealer/profile');
			   
		    } else {
					$config['upload_path']          = './assets/pictures/';
					$config['allowed_types']        = 'gif|jpg|png';
					$config['max_width']            = 360;
					$config['max_height']           = 362;
					$new_name = time().$_FILES['profile']['name'];
					$new_name = preg_replace('/[^a-zA-Z0-9_.]/', '', $new_name);
					$config['file_name'] = $new_name;

					$this->load->library('upload', $config);

					if ( ! $this->upload->do_upload('profile'))
					{
						$data['info'] = $this->register->getuserinfobyid($id);
						$data['states'] = $this->common->getallStates();
						$data['countries'] = $this->common->getallCountries();
						$data['userid'] = base64_encode($id);
						$data['error'] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$this->upload->display_errors().'</div>';
						$this->load->view('dealers/profile', $data);
					}
					else
					{
						$data = array('profile' => $this->upload->data());
						//Image Resizing
						$config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
						$config['maintain_ratio'] = FALSE;
						$config['width'] = 360;
						$config['height'] = 362;

						$this->load->library('image_lib', $config);
						#IMAGE RESIZE
						if ( ! $this->image_lib->resize()){
							$this->session->set_flashdata('message', $this->image_lib->display_errors('<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>', '</strong></div>'));
							redirect('dealer/profile');
						}else{
							// UPLOAD
							$data = array(
								'title' => $title,
								'first_name' => $firstname,
								'middle_name' => $middlename,
								'last_name' => $lastname,
								'suffix' => $suffix,
								'email' => $email,
								'address' => $address,
								'city' => $city,
								'state' => $state,
								'zip_code' => $zipcode,
								'country' => $country,
								'job_title' => $jobtitle,
								'contact' => $contactnumber,
								'web_url' => $webpage,
								'picture' => $new_name,
								'updated_at' => date("Y-m-d H:i:s"),
								'updated_by' => $this->session->userdata('id')
							);
							$this->dealerdb->updateuserprofile($id,$data);
							$this->session->set_flashdata('message', '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Information successfully updated.</strong></div>');
							redirect('dealer/profile');
						}
					}
		    }
		   
	   }
		
	}

	function updatesubscription()
	{
		if($this->session->userdata('dealer_login'))
		{
			$user		= $this->dealerdb->getUserinfobyid($this->session->userdata('id'))[0];
			$country 	= $this->dealerdb->getUserCountry($user->country)[0];
			$name 		= ''; # $this->security->xss_clean($this->input->post('name'));
			$type 		= 'ALL'; # $this->security->xss_clean($this->input->post('type'));
			$cardno 	= $this->security->xss_clean($this->input->post('cardno'));
			$month 		= $this->security->xss_clean($this->input->post('month'));
			$year 		= $this->security->xss_clean($this->input->post('year'));
			$cvv 		= $this->security->xss_clean($this->input->post('cvv'));
			$total 		= $this->security->xss_clean($this->input->post('total'));
			//$pmethod 	= $this->security->xss_clean($this->input->post('pmethod'));
			$ip 		= $this->input->ip_address();

			$id 				= $this->session->userdata('id');
			$subscription 		= $this->security->xss_clean($this->input->post('subscription'));
			$order 				= $this->dealerdb->getSubscription( $subscription )[0];
			$order_data 		= $this->dealerdb->getSubscription( $subscription );

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
			//$this->form_validation->set_rules('name', 'Card Name', 'trim|required');
			//$this->form_validation->set_rules('type', 'Credit Card Type', 'trim|required');
			$this->form_validation->set_rules('cardno', 'Card Number', 'trim|required');
			$this->form_validation->set_rules('month', 'Expiration Month', 'trim|required');
			$this->form_validation->set_rules('year', 'Expiration Year', 'trim|required');
			$this->form_validation->set_rules('cvv', 'Card Verification Number', 'trim|required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('message', validation_errors());
				redirect('dealer/profile/payment_form?subscription='.$subscription.'');
			}
			
			if ($this->dealerdb->subscriptionExists($id) == FALSE) 
			{
				$this->dealerdb->addSubscription([
					'dealer_id' => $id,
					'subscription_id' => $subscription
				]);
			}

			$user_subscription 	= $this->dealerdb->getCurrentSubscription( $id )[0];

			$gw = new Payment_helper;
			$gw->setLogin(USERNAME_PAYMENT, PASSWORD_PAYMENT);
			$gw->setBilling($user->first_name, $user->last_name, $user->company_name, $user->address, $user->address, $user->city, $user->state, $user->zip_code, $country->name, $user->contact, '', $user->email, $user->web_url);
			$gw->setShipping($user->first_name, $user->last_name, $user->company_name, $user->address,$user->address, $user->city, $user->state, $user->zip_code, $country->name, $user->email);
			
			$gw->setOrder($order->id, $order->name,'0', $user->address, $user_subscription->id, $ip);

			$r = $gw->doSale($total, $cardno, ''.$month.'/'.$year.'', $cvv);

			$response_code = $r['response_code'];
			$responsetext = $r['responsetext'];

			$payment_info = [
				'user_info' => [
					'user_id' 		=> $user->id,
					'first_name' 	=> $user->first_name,
					'last_name' 	=> $user->last_name, 
					'company_name' 	=> $user->company_name, 
					'address' 		=> $user->address, 
					'address' 		=> $user->address, 
					'city' 			=> $user->city, 
					'state' 		=> $user->state, 
					'zip_code' 		=> $user->zip_code, 
					'country' 		=> $country->name, 
					'contact' 		=> $user->contact, 
					'email' 		=> $user->email, 
					'web_url' 		=> $user->web_url
				],
				'order_info' => $order_data,
				'payment_info' => [
					'country' 	=> $country,
					//'name' 		=> $name,
					//'type' 		=> $type,
					'cardno' 	=> $cardno,
					'month' 	=> $month,
					'year' 		=> $year,
					'cvv' 		=> $cvv,
					'total' 	=> $total,
					//'pmethod' 	=> $pmethod,
					'ip' 		=> $ip
				],
				'payment_gateway_response' => [
					'response' 		=> $r['response'],
					'response_code' => $r['response_code'],
					'responsetext' 	=> $r['responsetext'],
					'authcode' 		=> $r['authcode'],
					'transactionid' => $r['transactionid'],
					'avsresponse' 	=> $r['avsresponse'],
					'cvvresponse' 	=> $r['cvvresponse'],
					'orderid' 		=> $r['orderid'],
					'type' 			=> $r['type'],
				]
			];

			if($response_code !== '100')
			{
				$message = '<div class="alert alert-danger"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>'.$responsetext.'.</strong></div>';
				$this->session->set_flashdata('message', $message);

				$delete = $this->dealerdb->deleteSubscription([
					'dealer_id' => $id,
					'subscription_id' => $subscription,
					'is_paid' => 0
				]);

				redirect('dealer/profile/payment_form?subscription='.$subscription.'');
			}

			$this->dealerdb->subscriptionPayment([
				'payment_info' => json_encode($payment_info),
				'dealer_id' => $id,
				'subscription_id' => $subscription
			]);

			
			$data['subscription_id'] = $subscription;
			$data['is_paid'] = 1;
			$this->dealerdb->updateSubscription( $id, $data );

			$this->dealerdb->updateSubscription( $id, ['subscription_id' => $subscription]);

			$this->session->set_flashdata('message', '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Subscription successfully changed. </strong></div>');

			redirect('dealer/profile?tab=subscription');

		} 
		else 
		{
			redirect('dealer-login');
		}
	}

	public function payment_form() 
	{
		if($this->session->userdata('dealer_login'))
		{
			$id 				= $this->session->userdata('id');
			$subscription 		= ($this->input->get('subscription')) ? $this->input->get('subscription') : $this->security->xss_clean($this->input->post('subscription'));
			$subscription_data 	= $this->dealerdb->getSubscription( $subscription )[0];

			$data['data'] 		= $subscription_data;
			$data['pagetitle'] = 'Payment Form';
			$this->load->view('dealers/payment_form', $data);
		}
		else
		{
			redirect('dealer-login');
		}
	}

}
