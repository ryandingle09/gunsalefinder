<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/******************************************************************
*
*	Title		: 	Promotions Ads
*	Author		: 	Ronald Duque | ronaldduque24@gmail.com
*	Filename 	: 	Postdeal.php
*	Date 		: 	Sept 2018
*
******************************************************************/
class Postdeal extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('dealer/postdeal_model','postdb',TRUE);
		$this->load->helper('string');
		$this->load->library('image_lib');
		$this->load->helper('text');
		$this->load->helper('date');
		$this->load->library('pagination');
		$this->load->helper('countryblock');
		$this->load->helper('mailgun');
		if(!checkcountry()){
			redirect('/out-of-region');
		}
	}
	function index()
	{
		if($this->session->userdata('dealer_login'))
		{
			
			$id = $this->session->userdata('id');
			$createdAt = $this->postdb->getDealerCreatedDate($id);
			
			if(strtotime($createdAt) < strtotime('-30 days')) {
				
				$subscription = $this->postdb->getSubsription($id);
				$userid = base64_encode($id);

				if($subscription == false){
					$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Please Select subscription to continue</strong></div>';
					$this->session->set_flashdata('dealermessage', $message);
					redirect('dealer/subscription/select/'.$userid);
				}else{

					$checkSubcription = $this->checkSubs($subscription);
					if(!$checkSubcription){
						$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Please upgrade your subscription to continue</strong></div>';
						$this->session->set_flashdata('dealermessage', $message);
						redirect('dealer/subscription/select/'.$userid);
					}

				}
			}
			$data['pagetitle'] = 'Post a deal';
			$data['category'] = $this->common->getallcategory();
			$data['states'] = $this->common->getallStates();
			$data['brands'] = $this->common->getallBrand();
			$data['code'] = random_string('alnum',6);
			$this->load->view('dealers/postdeal', $data);
		} else {
			
			$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Please login as a dealer to continue</strong></div>';
			$this->session->set_flashdata('dealermessage', $message);
			redirect('dealer-login');
		}
	}

		function checkSubs($subscription = 0) 
	{
		$user_id = $this->session->userdata('id');

		$canpost = array('1', '2', '3', '6');

		if (in_array($subscription, $canpost)) {
		   if($subscription == '6'){
		   		return true;
		   }else{
		   		$getLastPostDeal = $this->postdb->getLastPostDealbyDealer($user_id);
		   		#if no created deal yet
		   		if($getLastPostDeal == ''){
		   			return true;
		   		}else{
		   			#1 Deal Every 2 Days
		   			if($subscription == '1'){
		   				if(strtotime($getLastPostDeal) < strtotime('-2 days')) {
		   					return true;
		   				}
		   			}
		   			
		   			#7 Deals Every 7 Days,Sunday through Saturday
		   			if($subscription == '2'){
		   				$countDeal = $this->postdb->countSevenDaysDeal($user_id);
		   				if($countDeal < 8) {
		   					return true;
		   				}
		   			}

		   			#1 Deal Every 3 Days
		   			if($subscription == '3'){
		   				if(strtotime($getLastPostDeal) < strtotime('-3 days')) {
		   					return true;
		   				}
		   			}

		   			return false;


		   		}
		   }
		   
		}else{
			return false;
		}
	}


	function getImageOfAds(){
		$img_id = $this->input->post("id");
		$ads = $this->postdb->getAdsLocationbyid($img_id);
		echo json_encode($ads);
	}


	function daysOfTheWeek($date) {
	    // Assuming $date is in format DD-MM-YYYY
	    list($year, $month, $day) = explode("-", $date);

	    // Get the weekday of the given date
	    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

	    switch($wkday) {
	        case 'Monday': $numDaysToSun = 1; break;
	        case 'Tuesday': $numDaysToSun = 2; break;
	        case 'Wednesday': $numDaysToSun = 3; break;
	        case 'Thursday': $numDaysToSun = 4; break;
	        case 'Friday': $numDaysToSun = 5; break;
	        case 'Saturday': $numDaysToSun = 6; break;
	        case 'Sunday': $numDaysToSun = 0; break;   
	    }

	    // Timestamp of the monday for that week
	    $sunday = mktime('0','0','0', $month, $day-$numDaysToSun, $year);

	    $seconds_in_a_day = 86400;

	    // Get date for 7 days from Monday (inclusive)
	    for($i=0; $i<7; $i++)
	    {
	        $dates[$i] = date('Y-m-d',$sunday+($seconds_in_a_day*($i+1)));
	    }

	    return $dates;
	}

	#DROPZONE UPLOADING
	#-------------------------------------------------------------
	function dealsupload($code){
		if (!empty($_FILES)) {
			$tempFile = $_FILES['file']['tmp_name'];
			$file = str_replace(' ', '', $_FILES['file']['name']);
			$fileName = date('Y-m-dH-i-s').'-'.$file;
			$targetPath = 'uploads/deals/';
			if (!file_exists($targetPath)) {
			    mkdir($targetPath, 0775, true);
			}
			$targetFile = $targetPath . $fileName ;
			move_uploaded_file($tempFile, $targetFile);
			$this->resize($fileName);
			$fileName = base_url('uploads/deals/'.$fileName);
			$this->db->insert('upload_temp',array('code' => $code, 'filename' => $fileName, 'created_at' => date('Y-m-d H:i:s')));
		}else {                                                           
		    $result  = array();
		 
		    $files = scandir($storeFolder);                 //1
		    if ( false!==$files ) {
		        foreach ( $files as $file ) {
		            if ( '.'!=$file && '..'!=$file) {       //2
		                $obj['name'] = $file;
		                $obj['size'] = filesize($storeFolder.$ds.$file);
		                $result[] = $obj;
		            }
		        }
		    }
		     
		    header('Content-type: text/json');              //3
		    header('Content-type: application/json');
		    echo json_encode($result);
		}
	}
	
	#RESIZING IMAGE
	#-------------------------------------------------------------
	function resize($fileName){
	
		$config['image_library'] = 'gd2';
		$config['source_image'] = 'uploads/'.$fileName;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 313;
		$config['height'] = 234;
		$this->image_lib->initialize($config);
		$this->image_lib->resize();
		$this->image_lib->clear();
		
	}
	
	#SAVE ADS
	#-------------------------------------------------------------
	function savedeals() {
		
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->form_validation->set_rules('upc', 'UPC', 'trim|required|max_length[12]');
		$this->form_validation->set_rules('category', 'Category', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[300]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[12]');
		$this->form_validation->set_rules('brand', 'Brand', 'trim|max_length[10]');
		$this->form_validation->set_rules('itemurl', 'Item URL', 'trim|required|callback_valid_url');
		$code = $this->security->xss_clean($this->input->post('code'));
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['category'] = $this->common->getallcategory();
			$data['states'] = $this->common->getallStates();
			$data['brands'] = $this->common->getallBrand();
			$data['code'] = isset($code)? $code:random_string('alnum',6);
			$data['uploadimages'] = $this->postdb->getallUploaded($code);
			$this->load->view('dealers/postdeal', $data);
		}
		else
		{
			$title = $this->security->xss_clean($this->input->post('title'));
			$brand = $this->security->xss_clean($this->input->post('brand'));
			$category = $this->security->xss_clean($this->input->post('category'));
			$subcategory = $this->security->xss_clean($this->input->post('subcategory'));
			$upc = $this->security->xss_clean($this->input->post('upc'));
			$sku = $this->security->xss_clean($this->input->post('sku'));
			$price = $this->security->xss_clean($this->input->post('price'));
			$shipping = $this->security->xss_clean($this->input->post('shipping'));
			$url = $this->security->xss_clean($this->input->post('itemurl'));
			$notify = $this->security->xss_clean($this->input->post('notify'));
			
			if($this->postdb->checkDealsExist($upc)=== FALSE)
			{
				$dealer = $this->session->userdata('id');
				$upc = $this->common->makeUpcStandard($upc);
				$productImage = $this->postdb->getDealImagebyCode($code);
				$dealerInfo = $this->postdb->getDealerInfo($dealer);
				$categoryInfo = $this->postdb->getCategoryInfo($category);
				$subcategoryInfo = $this->postdb->getsubCategoryInfo($category, $subcategory);
				$brandInfo = $this->postdb->getBrandInfo($brand);
				$brandname = isset($brandInfo['0']['name'])? $brandInfo['0']['name'] : null;

				$data = array(
					'code' => $code,
					'dealer_id' => $dealer,
					'category_code' => $category,
					'sub_category_code' => $subcategory,
					'brand_id' => $brand,
					'brand_name' => $brandname,
					'title' => $title,
					'upc' => $upc,
					'sku' => $sku,
					'price' => $price,
					'shipping_fee' => $shipping,
					'item_url' => $url,
					'status'=> 'A',
					'notify_dealer' => $notify,
					'product_image' => $productImage,
					'dealer_info' => json_encode($dealerInfo),
					'category_info' => json_encode($categoryInfo),
					'sub_category_info' => json_encode($subcategoryInfo),
					'brand_info' => json_encode($brandInfo),
					'api_created' => 0,
					'created_at' => date('Y-m-d H:i:s')
				);
				
				if($this->postdb->insertProducts($data) !== false)
				{
					$id = $this->db->insert_id(); 
					$descriptions = $this->security->xss_clean($this->input->post('editor1'));
					if($descriptions != '') {
						
						$desc = array(
							'deal_id' => $id,
							'descriptions' => $descriptions,
							'status' => "A"
						);
						$this->postdb->insertProductDescription($desc);
					}

					$caliber = $this->security->xss_clean($this->input->post('caliber'));
					$framedesc = $this->security->xss_clean($this->input->post('framedesc'));
					$stock = $this->security->xss_clean($this->input->post('stock'));
					$type = $this->security->xss_clean($this->input->post('type'));
					$barrel = $this->security->xss_clean($this->input->post('barrel'));
					$capacity = $this->security->xss_clean($this->input->post('capacity'));
					$weight = $this->security->xss_clean($this->input->post('weight'));
					
					if($caliber != '' || $framedesc != '' || $stock != '' || $type != '' || $barrel != '' || $capacity != '' || $weight != '') {
						
						$info = array(
							'deal_id' => $id,
							'caliber' => $caliber,
							'type' => $type,
							'stock_description' => $stock,
							'frame_description' => $framedesc,
							'barrel' => $barrel,
							'capacity' => $capacity,
							'weight' => $weight
						);
						$this->postdb->insertDealInfo($info);
					}

					$this->postdb->insertImages($id, $code);
					$this->postdb->deleteTempimages($code);
					$this->postdb->clearRedisCache();
					$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Your deal has been listed.</strong></div>';
					$this->session->set_flashdata('message', $message);
					redirect("dealer-deals/");
				}
				else{
					$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Error Occurs. Please reports to administrator.</div>';
					$this->session->set_flashdata('message', $message);
					$data['category'] = $this->common->getallcategory();
					$data['states'] = $this->common->getallStates();
					$data['brands'] = $this->common->getallBrand();
					$data['code'] = random_string('alnum',6);
					$this->load->view('dealers/postdeal', $data);
				}
			}
			else{
				$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Deals with the same UPC already exists.</div>';
				$this->session->set_flashdata('message', $message);
				redirect("dealer/postdeal/");
			}
		
		}
	}

	function getsubcategory(){
		$catid = $this->input->post('category');
		$return_data = $this->postdb->getSubcategory($catid);
		header('Content-Type: application/json');
		echo json_encode($return_data);
		exit();
	}

	function managedeal($id = 0)
	{
		$data['deals'] = $this->postdb->getDealsbyid($id);
		$data['dealImages'] = $this->postdb->getDealImages($id);
		$data['dealDescriptions'] = $this->postdb->getDealsDescriptionbyid($id);
		$data['dealOtherinfo'] = $this->postdb->getOtherInfobyid($id);
		$data['ratings'] = $this->postdb->getRatings($id);
		$data['comments'] = $this->postdb->getComments($id);
		$data['issues'] = $this->postdb->getIssues($id);


		$data['code'] = random_string('alnum',6);
		$data['id'] = $id;
		$data['category'] = $this->common->getallcategory();
		$data['states'] = $this->common->getallStates();
		$data['brands'] = $this->common->getallBrand();
		$data['pagetitle'] = "Manage deal";
		$this->load->view('dealers/managedeal', $data);
	}

	function managedealsupload($id, $code){
		if (!empty($_FILES)) {
			$tempFile = $_FILES['file']['tmp_name'];
			$file = str_replace(' ', '', $_FILES['file']['name']);
			$fileName = date('Y-m-dH-i-s').'-'.$file;
			$targetPath = 'uploads/deals/';
			if (!file_exists($targetPath)) {
			    mkdir($targetPath, 0775, true);
			}
			$targetFile = $targetPath . $fileName ;
			move_uploaded_file($tempFile, $targetFile);
			$this->resize($fileName);
			$fileName = base_url('uploads/deals/'.$fileName);
			$this->db->insert('upload_temp',array('code' => $code, 'filename' => $fileName, 'created_at' => date('Y-m-d H:i:s')));

			$this->postdb->insertImages($id, $code);
			$this->postdb->deleteTempimages($code);
		}
	}

	function deletedealsupload($id){
		$dealsImages = $this->postdb->getDealImages($id);

		foreach ($dealsImages as $key => $value) {
			if (strpos($value->image_url, $_POST['file_name']) !== false) {
			    //$fileName = parse_url($value->image_url,PHP_URL_PATH);
			    $fileName = getcwd()."/uploads/".explode('uploads/', $value->image_url)[1];
			    echo $fileName;
			    if (file_exists($fileName)) {
				    unlink($fileName);
				}
			    $this->postdb->deleteDealImages($value->id);
			}
		}
	}

	function getdealsupload($id){
		$dealsImages = $this->postdb->getDealImages($id);

		foreach($dealsImages as $file){ 
			$fileName = getcwd()."/uploads/".explode('uploads/', $file->image_url)[1];
	        $obj['name'] = explode('-',explode('uploads/', $file->image_url)[1])[5]; 
	        $obj['size'] = filesize($fileName); 
	        $obj['url'] = $file->image_url; 
	        $result[] = $obj; 
	    }
       	echo json_encode(@$result);
	}

	function updateinformation(){
		
		$dealid = $this->input->post('dealid');
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->form_validation->set_rules('itemcode', 'Code', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('category', 'Category', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[300]');
		$this->form_validation->set_rules('price', 'Price', 'trim|required|max_length[12]');
		$this->form_validation->set_rules('brand', 'Brand', 'trim|max_length[10]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['deals'] = $this->postdb->getDealsbyid($id);
			$data['dealImages'] = $this->postdb->getDealImages($id);
			$data['dealDescriptions'] = $this->postdb->getDealsDescriptionbyid($id);
			$data['dealOtherinfo'] = $this->postdb->getOtherInfobyid($id);
			$data['code'] = random_string('alnum',6);
			$data['category'] = $this->common->getallcategory();
			$data['states'] = $this->common->getallStates();
			$data['brands'] = $this->common->getallBrand();
			$this->load->view('dealers/managedeal', $data);
		}
		else
		{
			$code = $this->security->xss_clean($this->input->post('code'));
			$itemcode = $this->security->xss_clean($this->input->post('itemcode'));
			$title = $this->security->xss_clean($this->input->post('title'));
			$brand = $this->security->xss_clean($this->input->post('brand'));
			$category = $this->security->xss_clean($this->input->post('category'));
			$subcategory = $this->security->xss_clean($this->input->post('subcategory'));
			$upc = $this->security->xss_clean($this->input->post('upc'));
			$sku = $this->security->xss_clean($this->input->post('sku'));
			$price = $this->security->xss_clean($this->input->post('price'));
			$shipping = $this->security->xss_clean($this->input->post('shipping'));
			$item_url = $this->security->xss_clean($this->input->post('item_url'));
			$status = $this->security->xss_clean($this->input->post('status'));
			$qty = ($status == 'A') ? 1:0;
			$notify = $this->security->xss_clean($this->input->post('notify'));
			$categoryInfo = $this->postdb->getCategoryInfo($category);
			$subcategoryInfo = $this->postdb->getsubCategoryInfo($category, $subcategory);
			$brandInfo = $this->postdb->getBrandInfo($brand);
			$brandname = isset($brandInfo['0']['name'])? $brandInfo['0']['name'] : null;

			$data = array(
					'code' => $itemcode,
					'dealer_id' => $this->session->userdata('id'),
					'category_code' => $category,
					'sub_category_code' => $subcategory,
					'brand_id' => $brand,
					'brand_name' => $brandname,
					'title' => $title,
					'upc' => $upc,
					'sku' => $sku,
					'price' => $price,
					'shipping_fee' => $shipping,
					'qty' => $qty,
					'status' => $status,
					'item_url' => $item_url,
					'notify_dealer' => $notify,
					'category_info' => json_encode($categoryInfo),
					'sub_category_info' => json_encode($subcategoryInfo),
					'brand_info' => json_encode($brandInfo),
					'updated_by' => $this->session->userdata('id'),
					'updated_at' => date('Y-m-d H:i:s')
			);
			
			if($this->postdb->updateDeals($dealid, $data) !== FALSE)
			{
				#Notify price drop
				$oldprice = $this->security->xss_clean($this->input->post('oldprice'));
				if($price < $oldprice){
					$userToNotify = $this->postdb->getUserToNotify($dealid);
					$producturl = $this->common->formatUrl($title);
					if($userToNotify)
		   			{
		   				foreach($userToNotify as $row)
				 		{
				 			//send email
				 			$userid = $row->user_id;
				 			if($row->user_type == 'dealer'){
				 				$name = $this->common->getDealerusername($userid);
				 				$email = $this->common->getDealeremail($userid);
				 			}else{
				 				$name = $this->common->getusername($userid);
				 				$email = $this->common->getUseremail($userid);
				 			}
							
							
							$subject = 'Price Reduction';
							$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
										<html xmlns="http://www.w3.org/1999/xhtml">
										<head>
											<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
											<title>Gunsalefinder - Price Reduction Notification</title>
										</head>
										<body style="margin:0px;">
											<font face="Segoe UI Semibold, Segoe UI Bold, Helvetica Neue Medium, Arial, sans-serif ">
												<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
													<tr><td colspan="2"><br />Hello '.$name.',</td></tr>
													<tr>
														<td colspan="2"><br />
															We are happy to inform you that '.$title.'  price drop down from '.$oldprice.' to '.$price.'
															<br />
															<br />
															Item URL: <i><a href="https://gunsalefinder.com/deals/view/'.$dealid.'/'.$producturl.'">https://gunsalefinder.com/deals/view/'.$dealid.'/'.$producturl.'</a></i><br />
															<br />
															Cheers, <br />
															Gun Sale Finder 
														</td>
													</tr>
												</table>
											</font>
										</body>
										</html>';
							sendmailgun($email, '', EMAILBCC, $subject, $message);

				 		}
		   			}

				}
				#update description
				$descriptions = $this->security->xss_clean($this->input->post('editor1'));
				$desc = array(
					'descriptions' => $descriptions,
				);
				$this->postdb->updateDealDescription($dealid, $desc);


				$caliber = $this->security->xss_clean($this->input->post('caliber'));
				$framedesc = $this->security->xss_clean($this->input->post('framedesc'));
				$stock = $this->security->xss_clean($this->input->post('stock'));
				$type = $this->security->xss_clean($this->input->post('type'));
				$barrel = $this->security->xss_clean($this->input->post('barrel'));
				$capacity = $this->security->xss_clean($this->input->post('capacity'));
				$weight = $this->security->xss_clean($this->input->post('weight'));
				
				$info = array(
					'caliber' => $caliber,
					'type' => $type,
					'stock_description' => $stock,
					'frame_description' => $framedesc,
					'barrel' => $barrel,
					'capacity' => $capacity,
					'weight' => $weight
				);
				$this->postdb->updateDealInformation($dealid, $info);
				$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Your deal has been updated.</strong></div>';
				$this->session->set_flashdata('message', $message);
				redirect("dealer/postdeal/managedeal/".$dealid);
			}
			else
			{
				$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Error Occurs. Please reports to administrator.</div>';
				$this->session->set_flashdata('message', $message);
				redirect("dealer/postdeal/managedeal/".$dealid);
			}
		}
	}

	function valid_url($url)
	{       

	  $pattern = '/' . '^(https?:\/\/)[^\s]+$' . '/';
	  preg_match($pattern, $url, $matches);
	  $this->form_validation->set_message('valid_url', "The Item url must start with 'https:// or https://' and contain no spaces");
	  return (empty($matches)) ? false : true;

	}

	function deletedeal($id=0){
		
		if($this->session->userdata('dealer_login'))
		{
			$this->postdb->deleteDeal($id);
			$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Deal deleted successfully.</div>';
			$this->session->set_flashdata('message', $message);
			redirect('dealer-deals');
			
		} else {
			
			$message = '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Please login as a dealer to continue</strong></div>';
			$this->session->set_flashdata('dealermessage', $message);
			redirect('dealer-login');
		}
		
	}
}
