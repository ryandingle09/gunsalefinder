<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration extends CI_Controller 
{
    function __construct()
    {
        parent::__construct();
        ini_set('mysql.connect_timeout', 30000);
        ini_set('default_socket_timeout', 30000);
        ini_set('max_execution_time', 5000);
        $this->load->model('migration/migration_model','product',true);
    }

    function index()
    {
        die("Welcome Migration API");
    }


    function migratecategory()
    {
        $result = $this->product->getDealerProductsCategory();
        foreach($result as $deal)
        {
           $value = $this->product->getCorrectCategory($deal->upc);

            if(count($value) > 0)
            {
                $this->product->updateProduct($deal->id, ['category_code' => $value]);
            }
           
        }
        die('done');

    }

    function migratesubcategory()
    {
        $result = $this->product->getDealerProductsSubCategory();
        foreach($result as $deal)
        {
           $value = $this->product->getCorrectSubCategory($deal->upc);
           if(count($value) > 0)
           {
                $this->product->updateProduct($deal->id, ['sub_category_code' => $value[0]['sub_category_code']]);
            }
          
        }
        die('done');

    }

    function migratebrand()
    {
        $result = $this->product->getDealerProductsBrand();
        foreach($result as $deal)
        {
           $value = $this->product->getCorrectBrand($deal->upc);
           if(count($value) > 0)
           {
                $this->product->updateProduct($deal->id, ['brand_id' => $value[0]['brand_id']]);
           }
        }
        die('done');

    }

}
