<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->driver('cache');
		
		$this->load->model('product_model','product',TRUE);
		$this->load->model('category_model','category',TRUE);
		$this->load->model('home_model','homedb',TRUE);
		$this->load->helper('date');
		$this->load->helper('countryblock');
		if(!checkcountry()){
			redirect('/out-of-region');
		}
	}

	public function index()
	{
		$listMenu 		= $this->cache->redis->get('home_listMenu');
		
		if(!$listMenu)
		{
			$listMenu = $this->category->listMenuHome( array( 'a.features' => '1'));
			$status = $this->cache->redis->save('home_listMenu', $listMenu, 3600);
		}

		foreach ($listMenu as $parseMenuKey => $parseListMenu) 
		{
			$whereFeature = [
				'a.category_code' => $parseListMenu['category_code'] 
			];

			$listSubMenu = $this->category->listSubMenu( $whereFeature  );

			foreach ($listSubMenu as $parseListSubMenu) 
			{
				$listMenu[$parseMenuKey]['subMenu'][] = $parseListSubMenu;
			}
		}
		
		$result = [
			'pagetitle' 	=> "Home",
			'menu' 			=> $listMenu,
		];
		
		$this->load->view('home/index', $result);
	}

	public function get_content()
	{
		$page 			= $this->input->get('page');
		$offset 		= ($this->input->get('offset')) ? $this->input->get('offset') : 0;

		$data = [];

		if($page == 'feature')
		{
			$feature = $this->cache->redis->get('home_feature'); 

			if(!$feature)
			{
				$matic = $this->cache_homepage('matic');
				
				if($matic)
				{
					$feature = $this->cache->redis->get('home_feature');
				}
			}

			$data 		= $feature; 

			echo json_encode($data);
		}

		if($page == 'popular_deals')
		{ 
			$popular_deals 	= $this->product->getProducts(TRUE, FALSE, $offset);

			foreach ($popular_deals as $parseKey => $parseProduct) 
			{
				$dealer 	= is_null($parseProduct['dealer_info']) ? null : json_decode($parseProduct['dealer_info'], true);
				$category 	= is_null($parseProduct['category_info']) ? null : json_decode($parseProduct['category_info'], true);

				$popular_deals[$parseKey]['isProductLike'] 		= $this->common->isProductLike($parseProduct['product_id']);	
				$popular_deals[$parseKey]['isProductDisLike'] 	= $this->common->isProductDisLike($parseProduct['product_id']);	
				$popular_deals[$parseKey]['isProductSubs'] 		= $this->common->isProductSubs($parseProduct['product_id']);	

				$popular_deals[$parseKey]['dealer'] 	= is_null($dealer) ? '' : $dealer[0]['company_name'];
				$popular_deals[$parseKey]['category'] 	= is_null($category) ? '' : $category[0]['name'];

				$popular_deals[$parseKey]['images'] = ($parseProduct['product_image'] == null) ? $this->product->update_image($parseProduct['product_id'], $parseProduct['upc'], $parseProduct['api_created']) : $parseProduct['images'];
			}

			$data = $popular_deals; 

			$this->load->view('home/popularList', ['data' => $data, 'offset' => $offset]);
		}

		if($page == 'today_deals')
		{ 
			$today_deals 	= $this->product->getTodaysDeals(FALSE, 12, $offset);

			foreach ($today_deals as $parseKey => $parseProduct) 
			{
				$dealer 	= is_null($parseProduct['dealer_info']) ? null : json_decode($parseProduct['dealer_info'], true);
				$category 	= is_null($parseProduct['category_info']) ? null : json_decode($parseProduct['category_info'], true);

				$today_deals[$parseKey]['isProductLike'] 	= $this->common->isProductLike($parseProduct['product_id']);	
				$today_deals[$parseKey]['isProductDisLike'] = $this->common->isProductDisLike($parseProduct['product_id']);	
				$today_deals[$parseKey]['isProductSubs'] 	= $this->common->isProductSubs($parseProduct['product_id']);	
				
				$today_deals[$parseKey]['dealer'] 	= is_null($dealer) ? '' : $dealer[0]['company_name'];
				$today_deals[$parseKey]['category'] 	= is_null($category) ? '' : $category[0]['name'];

				$today_deals[$parseKey]['images'] = ($parseProduct['product_image'] == null) ? $this->product->update_image($parseProduct['product_id'], $parseProduct['upc'], $parseProduct['api_created']) : $parseProduct['images'];
			}

			$data = $today_deals; 
			
			$this->load->view('home/todayList', ['data' => $data, 'offset' => $offset]);
		}
	}

	public function getTopBanners()
	{
		$result = $this->cache->redis->get('getTopBanners');

		if(!$result)
		{
			$result = $this->homedb->getBannersArray();
			$this->cache->redis->save('getTopBanners', $result, 7200);
		}

		$arr = [];

		foreach($result as $path)
		{
			$url = ADMINURL.$path['image_url'];

			if(file_exists($path['image_full_path']) || $path['image_full_path'] !== null)
			{
				$url = '/'.$path['image_url'];
			}

			$new = [
				'link_url' 	=> $path['link_url'],
				'image_url' => $url
			];

			array_push($arr, $new);
		}

		echo json_encode($arr);
	}

	public function getCenterSlider(){

		$result = $this->cache->redis->get('getCenterSlider');

		if(!$result)
		{
			$result = $this->homedb->getBannersArray('33');
			$this->cache->redis->save('getCenterSlider', $result, 7200);
		}

		$arr = [];

		foreach($result as $path)
		{
			$url = ADMINURL.$path['image_url'];

			if(file_exists($path['image_full_path']) || $path['image_full_path'] !== null)
			{
				$url = '/'.$path['image_url'];
			}

			$new = [
				'link_url' 	=> $path['link_url'],
				'image_url' => $url
			];

			array_push($arr, $new);
		}

		echo json_encode($arr);
	}

	public function getRightCenterSlider(){
		$result = $this->cache->redis->get('getRightCenterSlider');

		if(!$result)
		{
			$result = $this->homedb->getBannersArray('33');
			$this->cache->redis->save('getRightCenterSlider', $result, 7200);
		}

		$arr = [];

		foreach($result as $path)
		{
			$url = ADMINURL.$path['image_url'];

			if(file_exists($path['image_full_path']) || $path['image_full_path'] !== null)
			{
				$url = '/'.$path['image_url'];
			}

			$new = [
				'link_url' 	=> $path['link_url'],
				'image_url' => $url
			];

			array_push($arr, $new);
		}

		echo json_encode($arr);
	}

	public function getLeftSlider(){
		$result = $this->cache->redis->get('getLeftSlider');

		if(!$result)
		{
			$result = $this->homedb->getBannersArray('33');
			$this->cache->redis->save('getLeftSlider', $result, 7200);
		}

		$arr = [];

		foreach($result as $path)
		{
			$url = ADMINURL.$path['image_url'];

			if(file_exists($path['image_full_path']) || $path['image_full_path'] !== null)
			{
				$url = '/'.$path['image_url'];
			}

			$new = [
				'link_url' 	=> $path['link_url'],
				'image_url' => $url
			];

			array_push($arr, $new);
		}

		echo json_encode($arr);
	}

	public function getFooterSlider(){
		$result = $this->cache->redis->get('getFooterSlider');

		if(!$result)
		{
			$result = $this->homedb->getBannersArray('6');
			$this->cache->redis->save('getFooterSlider', $result, 7200);
		}

		$arr = [];

		foreach($result as $path)
		{
			$url = ADMINURL.$path['image_url'];

			if(file_exists($path['image_full_path']) || $path['image_full_path'] !== null)
			{
				$url = '/'.$path['image_url'];
			}

			$new = [
				'link_url' 	=> $path['link_url'],
				'image_url' => $url
			];

			array_push($arr, $new);
		}

		echo json_encode($arr);
	}

	public function getSearchSlider(){
		$result = $this->cache->redis->get('getSearchSlider');

		if(!$result)
		{
			$result = $this->homedb->getBannersArray('34');
			$this->cache->redis->save('getSearchSlider', $result, 7200);
		}
		$arr = [];

		foreach($result as $path)
		{
			$url = ADMINURL.$path['image_url'];

			if(file_exists($path['image_full_path']) || $path['image_full_path'] !== null)
			{
				$url = '/'.$path['image_url'];
			}

			$new = [
				'link_url' 	=> $path['link_url'],
				'image_url' => $url
			];

			array_push($arr, $new);
		}

		echo json_encode($arr);
	}

	public function getCompareSlider(){
		$result = $this->cache->redis->get('getCompareSlider');

		if(!$result)
		{
			$result = $this->homedb->getBannersArray('35');
			$this->cache->redis->save('getCompareSlider', $result, 7200);
		}
		$arr = [];

		foreach($result as $path)
		{
			$url = ADMINURL.$path['image_url'];

			if(file_exists($path['image_full_path']) || $path['image_full_path'] !== null)
			{
				$url = '/'.$path['image_url'];
			}

			$new = [
				'link_url' 	=> $path['link_url'],
				'image_url' => $url
			];

			array_push($arr, $new);
		}

		echo json_encode($arr);
	}

	public function getBanners(){

		$location_code 	= ($this->input->get('location_code')) ? $this->input->get('location_code') : 'HTC';
		$result 		= $this->cache->redis->get('getBanners_'.$location_code.'');

		if(!$result)
		{
			$result = $this->homedb->getBannersByLocationCode($location_code);

			$this->cache->redis->save('getBanners_'.$location_code.'', $result, 7200);
		}

		$arr = [];

		foreach($result as $path)
		{
			$url = ADMINURL.$path['image_url'];

			if(file_exists($path['image_full_path']) || $path['image_full_path'] !== null)
			{
				$url = base_url().$path['image_url'];
			}

			$new = [
				'link_url' 		=> $path['link_url'],
				'image_url' 	=> $url,
				'title' 		=> $path['title'],
				'description' 	=> $path['description'],
				'category' 		=> $path['category'],
				'state' 		=> $path['state'],
				'duration' 		=> isset($path['duration']) ? $path['duration'] : '5000',
				'created_at' 	=> $path['created_at'],
				'updated_at' 	=> $path['updated_at'],
			];

			array_push($arr, $new);
		}

		echo json_encode($arr);
	}

	public function refresh_all_cache()
	{
		$this->cache->redis->clean();
		echo json_encode($this->cache->redis->cache_info());
	}

	public function cache_homepage($call = 'cron')
	{
		$this->cache->redis->delete('home_feature');

		$feature 		= $this->homedb->getFeatureByLocationCode('HMF');

		/** CACHE FEATURE */
		$arr = [];
		foreach($feature as $path)
		{
			$url = ADMINURL.$path['image_url'];

			if(is_null($path['image_url']))
				$url = ADMINURL.'images/banners/1539619369GSFOnlineMarketplaceBlack313x234.jpg';
			else
			{
				if(file_exists($path['image_full_path']))
					$url = base_url().$path['image_url'];
				else
					$url = ADMINURL.$path['image_url'];
			}

			$new = [
				'link_url' 			=> is_null($path['link_url']) ? '#' : $path['link_url'],
				'image_url' 		=> $url,
				'title' 			=> is_null($path['title']) ? '': $path['title'],
				'description' 		=> is_null($path['description']) ? '' : $path['description'],
				'category' 			=> $path['category'],
				'state' 			=> $path['state'],
				'price' 			=> is_null($path['price']) ? '0.00' : $path['price'],
				'duration' 			=> isset($path['duration']) ? $path['duration'] : '5000',
				'category_name' 	=> $this->common->getCategoryname($path['category']),
				'dealer' 			=> $this->common->getDealerCompanyName($path['dealer_id']),
				'created_at' 		=> $path['created_at'],
				'updated_at' 		=> $path['updated_at']
			];

			array_push($arr, $new);
		}

		$this->cache->redis->save('home_feature', $arr, 7200);
		
		$feature 		= $this->cache->redis->get('home_feature');

		$data = [
			'home_feature' => $feature,
		];

		if($call == 'cron')
		{
			if(isset($_GET['result']))
				return true;
			else
				echo json_encode($data);
		}
		else return true;
	}

	public function cache_banner()
	{
		$location_code 	= ($this->input->get('location_code')) ? $this->input->get('location_code') : 'HTC';
		$result 		= $this->homedb->getBannersByLocationCode($location_code);

		$this->cache->redis->delete('getBanners_'.$location_code.'');
		$this->cache->redis->save('getBanners_'.$location_code.'', $result, 7200);

		return true;
	}

	public function array_sort_by_column(&$arr, $col, $dir = SORT_DESC) {
		$sort_col = array();
		foreach ($arr as $key=> $row) {
			$sort_col[$key] = $row[$col];
		}
		array_multisort($sort_col, $dir, $arr);
	}

	public function test_cache_homepage_curl()
	{
		$ch = curl_init($_SERVER['SERVER_NAME'].'/home/cache_homepage?result=false');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['cron' => 'matic']);
		$response = curl_exec($ch);
		echo json_encode($response);
        curl_close($ch);
	}

	public function test_cache_banner_curl()
	{
		$ch = curl_init($_SERVER['SERVER_NAME'].'/home/cache_banner?location=HTC');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['cron' => 'matic']);
		$response = curl_exec($ch);
		echo json_encode($response);
        curl_close($ch);
	}
}
