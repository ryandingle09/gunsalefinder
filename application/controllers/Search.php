<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller 
{
    private $sorting = '';

	function __construct() {

		parent::__construct();

		$this->load->model('search_model','search',TRUE);
		$this->load->model('category_model','category',TRUE);
		$this->load->model('product_model','product',TRUE);
		$this->load->model('home_model','homedb',TRUE);

		$this->load->library('form_validation');
		$this->load->library('pagination');
		$this->load->helper('date');
		$this->load->library('cart');
		$this->load->library('session');
		$this->load->helper('countryblock');

		if(!checkcountry())
		{
			redirect('/out-of-region');
		}
	}

	public function filtering( $deals ){

	    switch ($deals) {
            case 'latest':

                break;
            case 'highest':

                break;
            case 'rate':

                break;
            case 'commented':

				break;
			case 'like':

                break;

            default:
        }
    }

    public function sort( $sort, $sort_type ){

        $sorting = array();

        switch ( $sort ) {
            case 'price':
                $this->sorting      = 'price';
                $sorting['by']      = 'price';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'desc';
                break;
            case 'rating':
                $this->sorting      = 'rating';
                $sorting['by']      = 'p.rate_count';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'desc';
                break;
            case 'commented':
                $this->sorting      = 'commented';
                $sorting['by']      = 'p.comment_count';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'desc';
				break;
			case 'like':
                $this->sorting      = 'like';
                $sorting['by']      = 'p.like_count';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'desc';
                break;
            case 'views_count':
                $this->sorting      = 'views_count';
                $sorting['by']      = 'p.view_count';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'desc';
                break;
            case 'title':
                $this->sorting      = 'relevance';
                $sorting['by']      = 'p.title';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'asc';
                break;
            case 'relevance':
                $this->sorting      = 'relevance';
                $sorting['by']      = 'p.title';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'asc';
                break;
            case 'default':
            	$this->sorting      = 'default';
                $sorting['by']      = 'p.created_at';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'desc';
                break;
            case 'date':
                $this->sorting      = 'title';
                $sorting['by']      = 'p.created_at';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'desc';
				break;
			case 'categories':
                $this->sorting      = 'categories';
                $sorting['by']      = 'p.category_code';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'desc';
                break;

            default:
                $sorting['by']      = 'p.created_at';
                $sorting['sort']    = ($sort_type != '') ? $sort_type : 'asc';
                $this->sorting = '';
                break;
        }
        return $sorting;
    }

	public function index( $offset = 0 ) 
	{
	    //GET QUERY STRING
        $category_code          = ($this->input->get('category') || $this->input->get('category') != 'undefined') ? $this->input->get('category') : false;
		$sub_category_code      = ($this->input->get('sub_category')) ? $this->input->get('sub_category') : false;
        $dealer                 = ($this->input->get('dealer')) ? $this->input->get('dealer') : false;
        $brand                  = ($this->input->get('brand')) ? $this->input->get('brand') : false;
        $sort                   = $this->sort($this->input->get('sort'), $this->input->get('sort_type'));
		$order                  = $sort['sort'];

		if(strtoupper($category_code) == 'ALL')
            $category_code = false;

		/* Get list menu */
		$category 	= $this->category->listMenuSearch($dealer);
		$brandList 	= $this->search->brands();

		/*  */
		foreach ($category as $parseMenuKey => $parseListMenu) 
		{
			$whereCategory = array( 'a.category_code' => $parseListMenu['category_code'] );

			if ($dealer != '0') $where['a.dealer_id'] = $dealer;

			$listSubMenu = $this->category->listSubMenu( $whereCategory );

			foreach ($listSubMenu as $parseListSubMenu) 
			{
				$category[$parseMenuKey]['subMenu'][] = $parseListSubMenu;
			}
		}

		/*  Get dealer details */
		$dealerDetails = ($dealer != 0) ? $this->search->dealers( $dealer ) : '';
		
		$pMin = ($this->input->get('pMin') != null) ? $this->input->get('pMin') : '0';
		$pMax = ($this->input->get('pMax') != null) ? $this->input->get('pMax') : '10000.00';

		$where_or = array();

		/**/
		if($this->input->get('search') != '')
		{
            $search = $this->input->get('search');

			//With or Without zero UPC1
			$upc               				= $search;
			if (substr($upc,0,1)=="0") $upc = substr($upc,1);
			$where_or['p.upc'] 				= $upc;

			#REDIRECTED TO PRICE COMPARISON IF UPC SEARCH
			if(is_numeric($upc) && (strlen($upc) == 12 || strlen($upc) == 11) ) 
			{
				redirect('compare/prices/'.$upc);
			}

		}

        $full_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		$data = array(
			'features' => $this->homedb->getBannersByLocationCode('SLMF'),
		    'pagetitle' => 'Search deals',
			'category' => $category,
			'category_code' => $category_code,
			'brand' => $brandList,
			'postData' => $this->input->post(),
			'currLink' => $full_url,
			'currOrder' => $order,
			'currOffset' => $offset,
			'catCode' => $category_code,
			'subCatCode' => $sub_category_code,
			'sortCode' => $this->sorting,
			'compareList' => $this->session->userdata('compare_components'),
			'compareIdList' => $this->session->userdata('compareId_components') ?: 0,
			'dealer' => $dealerDetails,
			'dealerId' => $dealer,
			'brandId' => $brand,
			'priceMin' => $pMin,
			'priceMax' => $pMax,
        );

		$this->load->view("common/loading_modal");
		$this->load->view('search/index', $data);
		$this->load->view('common/globalJS');
	}

	public function render_deals($offset = 0 )
	{
		//GET QUERY STRING
        $category_code          = ($this->input->get('category') || $this->input->get('category') != 'undefined') ? $this->input->get('category') : false;
        $sub_category_code      = ($this->input->get('sub_category')) ? $this->input->get('sub_category') : false;
        $dealer                 = ($this->input->get('dealer')) ? $this->input->get('dealer') : false;
        $brand                  = ($this->input->get('brand')) ? $this->input->get('brand') : false;
        $sort                   = $this->sort($this->input->get('sort'), $this->input->get('sort_type'));
		$order                  = $sort['sort'];

		if(strtoupper($category_code) == 'ALL')
            $category_code = false;
		
		/*  Get dealer details */
		$dealerDetails = ($dealer != 0 || $dealer != null) ? $this->search->dealers( $dealer ) : '';
		
		$pMin = ($this->input->get('pMin') != null) ? $this->input->get('pMin') : '0';
		$pMax = ($this->input->get('pMax') != null) ? $this->input->get('pMax') : '10000.00';

		$where_or = array();

		/**/
		// if($this->input->get('search') != '')
		// {
        //     $search = $this->input->get('search');

        //     $where_or['p.title']                = $search;
		// 	$where_or['p.brand_info']           = $search;
		// 	$where_or['p.category_code']        = $search;
		// 	$where_or['p.sub_category_code']    = $search;

		// 	//With or Without zero UPC1
		// 	$upc                    		= $search;
		// 	if (substr($upc,0,1)=="0") $upc = substr($upc,1);
		// 	$where_or['p.upc'] 				= $upc;
		// }

		$price['p.price >= '] = $pMin;
        $price['p.price <= '] = $pMax;

        $limit = 12;

        $product	 = $this->search->filter_products($where_or, $sort, $category_code, $sub_category_code, $price, $dealer, $brand, $limit, $offset);
        $searchCount = $this->search->count_filter_products($where_or, $sort, $category_code, $sub_category_code, $price, $dealer, $brand);

		$config = [
			'base_url' 				=> base_url().'search/render_deals/',
			'total_rows' 			=> $searchCount,
			'per_page' 				=> $limit,
			'full_tag_open' 		=> "<ul class='pagination pagination-lg'>",
			'full_tag_close' 		=> "</ul>",
			'num_tag_open' 			=> '<li>',
			'num_tag_close' 		=> '</li>',
			'next_tag_open' 		=> "<li >",
			'next_tag_close' 		=> "</li>",
			'prev_tag_open' 		=> "<li>",
			'prev_tag_close' 		=> "</li>",
			'first_tag_open' 		=> "<li>",
			'first_tag_close' 		=> "</li>",
			'last_tag_open' 		=> "<li>",
			'last_tag_close' 		=> "</li>",
			'cur_tag_open' 			=> '<li id="currentPage" class="active" ><a>',
			'cur_tag_close' 		=> '</a></li>',
			'prev_link' 			=> "<span class='fa fa-chevron-left'> </span>",
			'next_link' 			=> "<span class='fa fa-chevron-right'> </span>",
			'use_page_numbers' 		=> FALSE,
			'num_links' 			=> 4,
			'reuse_query_string' 	=> TRUE,
		];

		$this->pagination->initialize($config); 

		$pageLinks 	= $this->pagination->create_links();
        $full_url 	= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

		$data = [
			'product' 	=> $product,
			'paginate' 	=> [
				'pageLinks' => $pageLinks,
				'offset' 	=> ($searchCount > $offset) ? $offset + 1 : $searchCount + 1,
				'limit' 	=> ($searchCount > $limit && ($limit + $offset) < $searchCount) ? $limit + $offset : $searchCount,
				'count' 	=> $searchCount
			],
			'catCode' 		=> $category_code,
			'subCatCode' 	=> $sub_category_code,
			'sortCode' 		=> $this->sorting,
			'dealer' 		=> $dealerDetails,
			'dealerId' 		=> $dealer,
			'brandId' 		=> $brand,
			'priceMin' 		=> $pMin,
			'priceMax' 		=> $pMax,
			'currLink' 		=> $full_url
		];
		
		$this->load->view('search/listItem', $data);
	}

	/*Search Item by title*/
	public function searchFilter() {
		$result = $this->search->productListFilter( $this->input->get('filter') );
		echo json_encode($result);
	}

	public function update_images()
	{
		$order = $this->input->get('order_by');
		$products = $this->product->get_all_null_images($order);

		foreach($products as $value)
		{
			$this->product->update_image($value['id'], $value['upc'], $value['api_created']);
			echo $value['id'].'<br>';
		}

		if(count($products) !== 0)
			echo 'Images of products successfully updated.';
		else
			echo 'No product has null image found.';
	}

	public function delete_deals_cache()
	{
		//run this every hour via cron
		$this->search->delete_deals_cache();
	}

}
