<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller 
{

	function __construct() {
		parent::__construct();

		$this->load->model('product_model','product',TRUE);
		$this->load->model('promotion_model','promotiondb',TRUE);
		$this->load->helper('date');
		$this->load->model('home_model','homedb',TRUE);
		$this->load->library('form_validation');
		$this->load->helper('mailgun');
		$this->load->helper('countryblock');
		if(!checkcountry()){
			redirect('/out-of-region');
		}
	}

	public function index() {
		$this->load->view('home/index');
	}

	public function category() {
		$result = $this->product->catgeory();
		
	}

	public function getTopBanners(){
		$result = $this->homedb->getBannersArray('32');
		echo json_encode($result);
	}

	/* View product details and picture */
	public function viewProduct() {

		$id = $this->input->get('id');
        /* Details of product */
        $result = $this->product->productDetails($id);
        $title_url = '';
        if(isset($result['title'])){
            $title_url = str_replace(' ', '-', strtolower(trim($result['title'])));
            $title_url = str_replace('/', '-', $title_url);
            $title_url = urlencode($title_url);
        }

		redirect(base_url().'deals/view/'.$id.'/'.$title_url);
		die;

		// /* Details of product */
		// $result = $this->product->productDetails($id);
		// $this->product->updateDealViews($id);
		// /* Get Dealer Details */
		// $dealer_id = $result['dealer_id'];
		// $dealer = $this->product->dealer($result['dealer_id']);
		// $result['dealer'] = count($dealer) > 0 ? $dealer : '';

		// /* commets */
		// $comments = $this->product->comments($id);
		// $result['comments'] = count($comments) > 0 ? $comments : '';
		// //debug($result['comments'], 1);
		// /* SCALE RATE */
		// $result['rateScale'] = $this->product->rateProductWithBreakDown($id);

		// /* Get ruser rate product */
		// $where = [
		// 	'product_id' => $id
		// 	,'user_id' => $this->session->userdata('id')
		// ];
		// $result['rateDetails'] = json_encode($this->product->getUserRateProduct( $where ));

		// $upc = $this->common->getProductUPC($id);
		// $result['related'] = $this->product->getRelatedProducts($id, $upc);
		// $apiCreated = isset($result['api_created']) ? $result['api_created'] : 1; 
		// if($apiCreated == 1){
		// 	/* Links of images */
		// 	$images = $this->product->imagesLink($upc);
		// 	$result['images'] = isset($images) > 0 ? $images : '';
		// 	/* Item Descriptions */
		// 	$descriptions = $this->product->productDescriptions($upc);
		// 	$result['pdescriptions'] = count($descriptions) > 0 ? $descriptions : '';
		// 	$result['productinfo'] = $this->product->getDealAPIInformation($upc);
		// 	$result['thumbnails'] = $this->product->getAllThumbs($upc);
		// } else {
		// 	/* Links of images */
		// 	$images = $this->product->dealImagesLink($id);
		// 	$result['images'] = isset($images) > 0 ? $images : '';
		// 	/* Item Descriptions */
		// 	$descriptions = $this->product->productWebsiteDesc($id);
		// 	$result['pdescriptions'] = count($descriptions) > 0 ? $descriptions : '';
		// 	$result['productinfo'] = $this->product->getDealWebsiteInformation($upc);
		// 	$result['thumbnails'] = $this->product->getWebThumbs($id);
		// }
		
		// $this->load->view('product/index', $result);
		// $this->load->view('product/modal', $result);
		// $this->load->view('product/rateModal', $result);
		// $this->load->view('product/reportModal', $result);
		// $this->load->view('common/alert');
		// $this->load->view('common/globalJS');
		// $this->load->view('common/loading_modal');
		// $this->load->view('product/script', $result);
	}

    public function viewDeal()
    {
		$id 			= $this->uri->segment(3);
		$segment_title 	= $this->uri->segment(4);

        /* Details of product */
		$result = $this->product->productDetails($id);
		if( !$segment_title || $segment_title == '')
		{
			$title_url = str_replace(' ', '-', strtolower(trim($result['title'])));
			$title_url = str_replace('/', '-', $title_url);
			$title_url = urlencode($title_url);

			redirect(base_url().'deals/view/'.$id.'/'.$title_url);
		}

		$page_title = $result['title'];
		
		$this->product->updateDealViews($id);

        /* commets */
        $comments 					= $this->product->comments($id);
        $result['comments'] 		= count($comments) > 0 ? $comments : '';
        $result['comment_count'] 	= count($comments);
        $result['like_count'] 		= $this->product->get_count('like', $id);
        $result['dislike_count'] 	= $this->product->get_count('dislike', $id);
        $result['rateScale'] 		= $this->product->rateProductWithBreakDown($id);

        /* Get ruser rate product */
        $where = [
            'product_id' 	=> $id,
            'user_id' 		=> $this->session->userdata('id')
		];
		
        $result['rateDetails'] = json_encode($this->product->getUserRateProduct( $where ));

        $upc 				= $this->common->getProductUPC($id);
        $result['related'] 	= $this->product->getRelatedProducts($id, $upc);
		$apiCreated 		= isset($result['api_created']) ? $result['api_created'] : 1;
		
		if($apiCreated == 1)
		{
            /* Links of images */
            $result['thumbnails'] 		= $this->product->getAllThumbs($upc);
			
            /* Item Descriptions */
            $descriptions 				= $this->product->productDescriptions($upc);
            $result['pdescriptions'] 	= count($descriptions) > 0 ? $descriptions : '';
            $result['productinfo'] 		= $this->product->getDealAPIInformation($upc);
		} 
		else 
		{
            /* Links of images */
            $result['thumbnails'] 		= $this->product->getWebThumbs($id);
			
            /* Item Descriptions */
            $descriptions 				= $this->product->productWebsiteDesc($id);
            $result['pdescriptions'] 	= count($descriptions) > 0 ? $descriptions : '';
            $result['productinfo'] 		= $this->product->getDealWebsiteInformation($upc);
        }

        $result['comment_rates'] 		= $this->product->getRates( $id );
        $result['pagetitle'] 			= ucwords(strtolower($page_title));

        $this->load->view('product/index', $result);
        $this->load->view('product/modal', $result);
        $this->load->view('product/rateModal', $result);
        $this->load->view('product/reportModal', $result);
        $this->load->view('common/alert');
        $this->load->view('common/globalJS');
        $this->load->view('common/loading_modal');
        $this->load->view('product/script', $result);
    }

	/* LoadMore comments */
	public function loadMoreComments() 
	{
		$product_id = $this->input->post('product_id');
		$start 		= $this->input->post('start');
		
		$comments 	= $this->product->comments($product_id, $start);

		if (is_array($comments['data']) && count($comments['data']) > 0) 
		{
			$views = '';

			foreach ($comments['data'] as $key => $value) 
			{
				$views .=  $this->load->view('product/comments', $value, TRUE); 
			}

			$result = [
				'views' 	=> $views,
				'next' 		=> $comments['next'],
				'start' 	=> $comments['start'],
			];

			echo json_encode($result);
		}
			
	}

	public function insert_commentv2()
	{
		$txtComment = $this->input->post('message');
		$product_id = $this->input->post('product_id');
		$userid 	= $this->session->userdata('id');
		$usertype 	= (($this->session->userdata('logged_in'))) ? 'user' : 'dealer';

		$data = [
			'product_id' 	=> $product_id,
			'message' 		=> $txtComment,
			'created_at' 	=> date("Y-m-d H:i:s"),
			'user_id' 		=> $userid,
			'status' 		=> 'A',
			'user_type' 	=> $usertype
		];

		$comment_id = $this->product->insert_comments($data);

		if($comment_id != 0)
		{
			$id 			= 0;
			$name 			= $this->common->getNameLoggedInV2();
			$result['html'] = '<li class="comment" id="comment'.$comment_id.'">
			<div class="comment-info">
				<img class="pull-left hidden-xs img-circle" src="'.base_url().'assets/pictures/userprofile90x90.png" alt="author">
					<div class="author-desc">
						<div class="author-title">
							<strong> '.$name.'</strong>
							<ul class="list-inline pull-right">
								<li>
									<a href="#">'.timespan( strtotime(date("Y-m-d H:i:s")) , time(), 2). ' ago</a>
								</li>
							</ul>
						</div>
						<span id="message'.$comment_id.'">'.$txtComment.'</span>
						<span style="display:none;" id="edit'.$comment_id.'">
							<div class="input-group">
								<textarea class="form-control custom-control" rows="3" style="resize:none"  id="textMsg'.$comment_id.'">'.$txtComment.'</textarea>     
								<span class="input-group-addon btn btn-primary" onClick="updateComment('.$comment_id.')"><i class="fa fa-save"></i></span>
							</div>
						</span>
						<ul class="list-inline pull-right" id="util'.$comment_id.'">
							<li>
								<a id="editBtn'.$comment_id.'" onClick="toggleEdit('.$comment_id.')">
									<i class="fa fa-pencil"></i>
								</a>    
								<a id="deleteBtn'.$comment_id.'" onClick="deleteComment('.$comment_id.')">
									<i class="fa fa-trash"></i>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</li>';

			$result['total'] = $this->product->commentCount($product_id);
			$this->notifydealer($product_id);

			echo json_encode($result);
		}
		else
		{
			echo 'error';
		}
	}

	public function notifydealer($product_id){

		$product = $this->common->getProductDetails($product_id);

		if($product->notify_dealer == 1)
		{
			$email 		= $this->common->getDealeremail($product->dealer_id);
			$producturl = $this->common->formatUrl($product->title);
			$message 	= '<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
				<tr><td colspan="2"><br />Hi there,</td></tr>
				<tr>
					<td colspan="2"><br />
						Someone has commented on your product. <br />
						Product Name: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">'.$product->title.'</a></i><br />
						Item URL: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'</a></i><br />
						User Name: <i>'.$this->common->getNameLoggedIn().'</i>
						<br /><br />
						Cheers, <br />
						Gunsalefinder
					</td>
				</tr>
			</table>';

			$subject 	= 'User commented on your product.';
			$output 	= sendmailgun($email, '', EMAILBCC, $subject, $message);
			$result 	= json_decode($output);
		}
	}

	public function update_comments()
	{
		$comment_id 		= $this->input->post('comment_id');
		$data['message'] 	= $this->input->post('new_comment');
		$result 			= $this->product->update_comment($data, $comment_id);

		echo $result;
	}

	public function delete_comment()
	{
		$id 		= $this->input->post('comment_id');
		$data 		= $this->product->get_comment($id);
		$result 	= $this->product->delete_comment($id);

		if($result)
		{
			$total = $this->product->commentCount($data->product_id);

			echo $total;
		}
	}

	public function submit_commentreport()
	{
		$data['comment_id'] = $this->input->post('comment_id');
		$data['details'] 	= $this->input->post('details');
		$data['user_id'] 	= $this->session->userdata('id');
		$data['user_type'] 	= (($this->session->userdata('dealer_login'))) ? 'dealer' : 'user';
		$result 			= $this->product->reportComment($data);
		$name 				= $this->common->getNameLoggedIn();

		$this->emailreport($data['comment_id'], $name, $data['details']);

		echo $result;
	}

	public function emailreport($comment_id, $reporter, $details)
	{
		$data['details'] = '<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
			<tr><td colspan="2"><br />Hi there Admin,</td></tr>
			<tr>
				<td colspan="2"><br />
					Comment has been reported. <br />
					Comment ID: <i>'.$comment_id.'</i><br />
					Reporter Name: <i>'.$reporter.'</i><br />
					Details: <i>'.$details.'</i>
					<br /><br />
					Cheers, <br />
					Gunsalefinder
				</td>
			</tr>
		</table>';
		$subject = 'GUNSALEFINDER comment reported.';
		$message = $this->load->view('product/email', $data, TRUE);

		$this->load->library('email');

		sendmailgun(EMAILFROM, '', EMAILBCC, $subject, $message);
		
	}

	public function insert_comments() {

		$this->form_validation->set_rules('txtComment', 'Comment', 'trim|required');
		if ($this->form_validation->run() === FALSE)  
		{
			echo json_encode(array("errCode"=>"R09090", "errMsg"=>validation_errors())); 

			return;
		}

		// Check session
		if ( !$this->session->userdata('logged_in') ) 
		{
            // Echo error msg if ajax request
			if ($this->input->is_ajax_request())
			{
                echo json_encode(array("errCode"=>"R00002", "errMsg"=>R00002));
                exit;
            }

            redirect(base_url()."home");
            exit;
        }

		$userid = $this->session->userdata('id');
		
       	$data 	= array(
			'user_id'	 	=> $userid,
			'promo_id' 		=> 1,
			'promo_type' 	=> 'comments',
			'points' 		=> 1,
			'ipaddress'	 	=> isset($ip) ? $ip : '' ,
			'created_by' 	=> $this->session->userdata('id'),
			'created_at' 	=> date("Y-m-d H:i:s")
		);

		$this->promotiondb->addPointsHistory($data);

		if ($this->promotiondb->userPointExists($userid)) 
		{
			$this->promotiondb->incresePoints($userid);
		} 
		else 
		{
			$data = array(
				'user_id' 	=> $userid,
				'points' 	=> 1
			);

			$this->promotiondb->addUserPoints($data);
		}
		
		$txtEmail 	= $this->input->post('txtEmail');
		$txtComment = $this->input->post('txtComment');
		$product_id = $this->input->post('product_id');

		$data = [
			'product_id' 	=> $product_id,
			'message' 		=> $txtComment,
			'created_at' 	=> date("Y-m-d H:i:s"),
			// 'created_by' => ,
			'user_id' 		=> $userid,
			'status' 		=> 'A'
		];

		$this->product->insert_comments($data);

		echo json_encode(array( 'errCode' => 'R00003','errMsg' => R00003));

		return;
	}
	
	public function subsPDC()
	{
		$product_id = $this->input->post('product_id');
		$user_id 	= $this->session->userdata('id');
		$user_type 	= $this->common->getUserType();

		$where_sub['user_id'] 		= $user_id;
		$where_sub['product_id'] 	= $product_id;
		$where_sub['user_type'] 	= $user_type;
		$exist 						= $this->product->subsPDCExist($where_sub);

		if($exist == true)
		{
			$result = $this->product->unsubsPDC($where_sub);

			if($result)
			{	
				echo 'unsubs';
			}
		}
		else
		{
			$save_sub['user_id'] 	= $user_id;
			$save_sub['product_id'] = $product_id;
			$save_sub['user_type'] 	= $user_type;

			$this->product->subsPDC($save_sub);

			$product = $this->common->getProductDetails($product_id);
			if($product->notify_dealer == 1)
			{
				$email 		= $this->common->getDealeremail($product->dealer_id);
				$producturl = $this->common->formatUrl($product->title);
				$data 		= '<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
					<tr><td colspan="2"><br />Hi there,</td></tr>
					<tr>
						<td colspan="2"><br />
							Someone has subscribed to your deal!. <br />
							Product Name: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">'.$product->title.'</a></i><br />
							Item URL: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'</a></i><br />
							User Name: <i>'.$this->common->getNameLoggedIn().'</i>
							<br /><br />
							Cheers, <br />
							Gunsalefinder
						</td>
					</tr>
				</table>';
				$subject = 'GUNSALEFINDER Subscribe on your product.';
				
				$this->emailDealer($email, $subject, $data);
			}

			//$this->product->updateSubscriberCount($product_id);

			echo 'subs';
		}
	}

	public function subsComment()
	{
		$product_id = $this->input->post('product_id');
		$user_id 	= $this->session->userdata('id');
		$user_type 	= $this->common->getUserType();

		$where_sub['user_id'] 		= $user_id;
		$where_sub['product_id'] 	= $product_id;
		$where_sub['user_type'] 	= $user_type;

		$exist = $this->product->subsCommentExist($where_sub);

		if($exist == true)
		{
			$result = $this->product->unsubsComment($where_sub);

			if($result)
			{	
				echo 'unsubs';
			}
		}
		else
		{
			$save_sub['user_id'] 	= $user_id;
			$save_sub['product_id'] = $product_id;
			$save_sub['user_type'] 	= $user_type;

			$this->product->subsComment($save_sub);

			$product = $this->common->getProductDetails($product_id);

			if($product->notify_dealer == 1)
			{
				$email 		= $this->common->getDealeremail($product->dealer_id);
				$producturl = $this->common->formatUrl($product->title);
				$data 		= '<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
					<tr><td colspan="2"><br />Hi there,</td></tr>
					<tr>
						<td colspan="2"><br />
							Subscribe on your product for comments. <br />
							Product Name: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">'.$product->title.'</a></i><br />
							Item URL: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'</a></i><br />
							User Name: <i>'.$this->common->getNameLoggedIn().'</i>
							<br /><br />
							Cheers, <br />
							Gunsalefinder
						</td>
					</tr>
				</table>';
				$subject = 'GUNSALEFINDER Subscribe on your product.';

				$this->emailDealer($email, $subject, $data);
			}

			$this->product->updatepriceDropSubscriberCount($product_id);

			echo 'subs';
		}
	}

	public function subsPriceDrop()
	{
		$product_id = $this->input->post('product_id');
		$user_id 	= $this->session->userdata('id');
		$user_type 	= $this->common->getUserType();

		$where_sub['user_id'] 		= $user_id;
		$where_sub['product_id'] 	= $product_id;
		$where_sub['user_type'] 	= $user_type;

		$exist = $this->product->subsPricedropExist($where_sub);

		if($exist == true)
		{
			$result = $this->product->unsubsPricedrop($where_sub);

			if($result)
			{	
				echo 'unsubs';
			}
		}
		else
		{
			$save_sub['user_id'] 	= $user_id;
			$save_sub['product_id'] = $product_id;
			$save_sub['user_type'] 	= $user_type;

			$this->product->subsPricedrop($save_sub);

			$product = $this->common->getProductDetails($product_id);

			if($product->notify_dealer == 1)
			{
				$email 		= $this->common->getDealeremail($product->dealer_id);
				$producturl = $this->common->formatUrl($product->title);
				$data 		= '<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
					<tr><td colspan="2"><br />Hi there,</td></tr>
					<tr>
						<td colspan="2"><br />
							Subscribe on your product for price drop. <br />
							Product Name: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">'.$product->title.'</a></i><br />
							Item URL: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'</a></i><br />
							User Name: <i>'.$this->common->getNameLoggedIn().'</i>
							<br /><br />
							Cheers, <br />
							Gunsalefinder
						</td>
					</tr>
				</table>';
				$subject = 'GUNSALEFINDER Subscribe on your product.';

				$this->emailDealer($email, $subject, $data);
			}

			$this->product->updatepriceDropSubscriberCount($product_id);

			echo 'subs';
		}
	}

	public function likeProduct()
	{
		$product_id = $this->input->post('product_id');
		$user_id 	= $this->session->userdata('id');
		$user_type 	= $this->common->getUserType();

		$where_like['user_id'] 		= $user_id;
		$where_like['product_id'] 	= $product_id;
		$where_like['user_type'] 	= $user_type;
		$where_like['type'] 		= 'like';

		$exist = $this->product->likeExist($where_like);

		if(count($exist) !== 0)
		{
			$result = $this->product->unlikeProduct($where_like);

			if($result)
			{	
				echo 'unlike';
			}
		}
		else
		{
			$save_like['user_id'] 		= $user_id;
			$save_like['product_id'] 	= $product_id;
			$save_like['user_type'] 	= $user_type;
			$save_like['type'] 			= 'like';
			$save_like['created_at'] 	= date("Y-m-d H:i:s");

			$this->product->likeProduct($save_like);

			$where_dislike['user_id'] 		= $user_id;
			$where_dislike['product_id'] 	= $product_id;
			$where_dislike['user_type'] 	= $user_type;
			$where_dislike['type'] 			= 'dislike';

			$this->product->undislikeProduct($where_dislike);

			$product = $this->common->getProductDetails($product_id);

			if($product->notify_dealer == 1)
			{
				$email 		= $this->common->getDealeremail($product->dealer_id);
				$producturl = $this->common->formatUrl($product->title);
				$data 		= '<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
					<tr><td colspan="2"><br />Hi there,</td></tr>
					<tr>
						<td colspan="2"><br />
							A User liked Your Deal! <br />
							Product Name: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">'.$product->title.'</a></i><br />
							Item URL: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'</a></i><br />
							User Name: <i>'.$this->common->getNameLoggedIn().'</i>
							<br /><br />
							Cheers, <br />
							Gunsalefinder
						</td>
					</tr>
				</table>';
				$subject = 'GUNSALEFINDER Product has been liked.';
				$this->emailDealer($email, $subject, $data);
			}

			//$this->product->updateLikeCount($product_id);

			echo 'like';
		}
	}

	public function checkifdislikeexist()
	{
		$product_id = $this->input->post('product_id');
		$user_id 	= $this->session->userdata('id');
		$user_type 	= $this->common->getUserType();

		$where_dislike['user_id'] 		= $user_id;
		$where_dislike['product_id'] 	= $product_id;
		$where_dislike['user_type'] 	= $user_type;
		$where_dislike['type'] 			= 'dislike';

		$exist = $this->product->dislikeExist($where_dislike);
		
		if(count($exist) !== 0)
		{
			$result = $this->product->undislikeProduct($where_dislike);

			if($result)
			{	
				echo 'dislike';
			}
		}
		else
		{
			echo 'undislike';
		}
	}

	public function dislikeProduct()
	{
		$product_id = $this->input->post('product_id');
		$user_id 	= $this->session->userdata('id');
		$user_type 	= $this->common->getUserType();

		$where_dislike['user_id'] 		= $user_id;
		$where_dislike['product_id'] 	= $product_id;
		$where_dislike['user_type'] 	= $user_type;
		$where_dislike['type'] 			= 'dislike';

		$exist = $this->product->dislikeExist($where_dislike);

		if(count($exist) !== 0)
		{
			$result = $this->product->undislikeProduct($where_dislike);

			if($result)
			{	
				echo 'undislike';
			}
		}
		else
		{
			$save_dislike['user_id'] 			= $user_id;
			$save_dislike['product_id'] 		= $product_id;
			$save_dislike['user_type'] 			= $user_type;
			$save_dislike['type'] 				= 'dislike';
			$save_dislike['dislike_type'] 		= $this->input->post('dislike_type');
			$save_dislike['dislike_details'] 	= $this->input->post('dislike_details');
			$save_dislike['created_at'] 		= date("Y-m-d H:i:s");

			$this->product->dislikeProduct($save_dislike);

			$where_like['user_id'] 		= $user_id;
			$where_like['product_id'] 	= $product_id;
			$where_like['user_type'] 	= $user_type;
			$where_like['type'] 		= 'like';

			$this->product->unlikeProduct($where_like);

			$product = $this->common->getProductDetails($product_id);

			if($product->notify_dealer == 1)
			{
				$email 		= $this->common->getDealeremail($product->dealer_id);
				$producturl = $this->common->formatUrl($product->title);
				$data 		= '<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
					<tr><td colspan="2"><br />Hi there,</td></tr>
					<tr>
						<td colspan="2"><br />
							Someone has Disliked Your Deal! <br />
							Product Name: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">'.$product->title.'</a></i>
							<br />
							Item URL: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'</a></i><br />
							User Name: <i>'.$this->common->getNameLoggedIn().'</i><br />
							Details: <i>['.$this->input->post('dislike_type').'] '.$this->input->post('dislike_details').'</i>
							<br /><br />
							Cheers, <br />
							Gunsalefinder
						</td>
					</tr>
				</table>';
				$subject = 'GUNSALEFINDER Product has been Disliked.';

				$this->emailDealer($email, $subject, $data);
			}

			//$this->product->updatedisLikeCount($product_id);

			echo 'dislike';
		}
	}

	public function rateProduct() 
	{

		if ( !$this->session->userdata('logged_in') && !$this->session->userdata('dealer_login')) 
		{
            // Echo error msg if ajax request
			if ($this->input->is_ajax_request()) 
			{
				echo json_encode(array("errCode"=>"R00002", "errMsg"=>R00002));
				
                exit;
            }

			redirect(base_url()."home");
			
            exit;
        }

        /* Form Validation */
		$this->form_validation->set_rules('rate', 'Rate', 'trim|required');
		$this->form_validation->set_rules('remarks', 'Remarks', 'trim|required');

		if ($this->form_validation->run() === FALSE)  
		{
			echo json_encode(array("errCode"=>"R09090", "errMsg"=>validation_errors())); 

			return;
		}

		$product_id = $this->input->post('product_id');
		
        $data = [
        	'post_date' 	=> date("Y-m-d"),
			'post_time' 	=> date("H:i:s"),
			'product_id' 	=> $product_id,
			'rate' 			=> $this->input->post('rate'),
			'remarks' 		=> $this->input->post('remarks'),
			'status' 		=> 'I',
			'user_id' 		=> $this->session->userdata('id'),
			'is_dealer' 	=> ($this->session->userdata('dealer_login')) ? 'YES' : null
        ];

        $this->product->insertRateProduct($data);
		$this->product->updateRateCount($product_id, $this->input->post('rate'));
		$this->notifyDealeronRate($product_id);
		
		echo json_encode(array( 'errCode' => 'R00004','errMsg' => R00004));
		
		return;
	}

	public function notifyDealeronRate($product_id)
	{
		$product = $this->common->getProductDetails($product_id);

		if($product->notify_dealer == 1)
		{
			$email 		= $this->common->getDealeremail($product->dealer_id);
			$producturl = $this->common->formatUrl($product->title);
			$message 	= '<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
				<tr><td colspan="2"><br />Hi there,</td></tr>
				<tr>
					<td colspan="2"><br />
						Someone has Rated Your Deal! <br />
						Product Name: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">'.$product->title.'</a></i><br />
						Item URL: <i><a href="https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'">https://gunsalefinder.com/deals/view/'.$product_id.'/'.$producturl.'</a></i><br />
						User Name: <i>'.$this->common->getNameLoggedIn().'</i>
						<br /><br />
						Cheers, <br />
						Gunsalefinder
					</td>
				</tr>
			</table>';
			$subject 	= 'User rate on your product.';
			$output 	= sendmailgun($email, '', EMAILBCC, $subject, $message);
			$result 	= json_decode($output);
		}
	}

	function emailDealer($emailto, $subject, $data)
	{
		
		//$message = $this->load->view('product/email', $data, TRUE);
		$output = sendmailgun($emailto, '', EMAILBCC, $subject, $data);
		$result = json_decode($output);
		$send 	= false;

		if($result->message == 'Queued. Thank you.')
		{
			$send =  true;
		}

		return $send;
	}

}
