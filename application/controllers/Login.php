<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('login_model','',TRUE);
		$this->load->helper('countryblock');
		$this->load->helper('mailgun');
		if(!checkcountry()){
			redirect('/out-of-region');
		}
	}
	
	public function index()
	{
	    $data['pagetitle'] = 'Sign in';
		$this->load->view('login',$data);
	}

	function checklogin()
	{
       $data['pagetitle'] = 'Sign in';

	   $this->load->library('form_validation');
	   $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
	   $this->form_validation->set_rules('username', 'Username', 'trim|required');
	   $this->form_validation->set_rules('password', 'Password', 'trim|required');

	   if($this->form_validation->run() == false)
	   {
	        $this->load->view('login');
	   }
	   else
	   {
		   $username = $this->input->post('username');
		   $password = $this->input->post('password');
		   $result = $this->login_model->login($username, $password);
		   $isconfirm = true;
		 
		   if($result)
		   {
				 $sess_array = array();
				 foreach($result as $row)
				 {
				   if($row->isconfirm == 1){
					    $newdata = array(
						   'id'  => $row->id,
						   'name'     => $row->name,
						   'logged_in' => true
						);
						$this->session->set_userdata($newdata);
				   }else{
					   $isconfirm = false;
				   }
				  
				 }

				if($isconfirm == false){
					$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Email Confirmation required before login.</div>';
					$this->session->set_flashdata('message', $message);
					redirect('login'); 
				 }else{
					redirect('profile'); 
				 }
		   }
		   else
		   {
				$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Invalid username or password.</div>';
				$this->session->set_flashdata('message', $message);
				redirect('login');
		   }
			
	   }
	  
	}

	#LOGOUT
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('login');
	}
	
	function forgotpassword()
	{
		$email = $this->input->post("email");
		$userid = $this->login_model->loginemailcheck($email);
		# if login success
		if( $userid !== false )
		{		
			//send email
			$name = $this->common->getusername($userid);
			$userid = base64_encode($userid);
			$userid = urlencode($userid);
			
			$subject = 'Gunsalefinder - Password Reset';
			
			$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
								<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
								<title>gunsalefinder.com - Find Gun in America</title>
							</head>
							<body style="margin:0px;">';
		   $message .='<font face="Segoe UI Semibold, Segoe UI Bold, Helvetica Neue Medium, Arial, sans-serif ">
								<table cellspacing="0" cellpadding="0" style="width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
									<tr>
										<td style="background:#ea8916;height:60px;padding:0;background:#fff;">
											<img src="'.base_url().'assets/images/header/gunpro-header.png" style="width: 100px;left:10px;height:77px;"/>
										</td>
										<!--<td style="width:100%;">
											<img src="'.base_url().'assets/img/EmailLogo-BG.png" style="height: 77px; max-height: 77px;width: 100%;" />
										</td>-->
									</tr>
								</table>
								<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
									<tr><td colspan="2"><br />Hi '.$name.',</td></tr>
									<tr>
										<td colspan="2"><br />
											We received a request to change your password. To reset your password, please click this link:<br /><br />
											<a href="'.base_url('login/recoverpassword/'.$userid).'" style="color: #FFF;text-decoration: none;background-color: #08c;border-radius: 5px;padding: 3px 10px;">Reset Now</a> &nbsp; &nbsp; or copy the following link: '.base_url('login/recoverpassword/'.$userid).'<br /><br />
											If you did not request for a password reset, please ignore this message. If you think your account has been compromised, please contact our support team through <a href="mailto:'.EMAILFROM.'" style="color:#08c;text-decoration:none;">'.EMAILFROM.'</a>.
											<br /><br />
											Cheers, <br />
											Gun Sale Finder 
										</td>
									</tr>
								</table>
							</font>';
			$message .= "</body></html>";


			$output = sendmailgun($email, '', EMAILBCC, $subject, $message);
			$result = json_decode($output);

			if($result->message == 'Queued. Thank you.'){				
				$this->session->set_flashdata('message', '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>We have sent you an email with the instructions to reset your password.</strong></div>');
				redirect('login');
			}
			else{
				$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Email not sent, Please contact our support through <a href="mailto:'.EMAILFROM.'" style="color:#08c;text-decoration:none;">'.EMAILFROM.'</a></div>';
				$this->session->set_flashdata('message', $message);
				redirect('login');
			}
		}
		else {
			$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Invalid email address.</div>';
			$this->session->set_flashdata('message', $message);
			redirect('login');

		}
		
	}

	#RECOVER NEW PASSWORD
	#-------------------------------------------------------------
	function recoverpassword($userid = 0)
	{
		$id = base64_decode(urldecode($userid));
		$data['userid'] = $id;
		$data['usertype'] = 'user';
		$this->load->view('resetpassword', $data);
	}
	#RECOVER NEW PASSWORD
	#-------------------------------------------------------------
	function saverecoverpassword() {
		$this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>', '</div>');
		$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|min_length[5]|max_length[100]|matches[repeat]');
        $this->form_validation->set_rules('repeat', 'Reenter Password', 'trim|required|min_length[5]|max_length[100]');
		$id = $this->input->post('userid');
		
		if ($this->form_validation->run() == false){
			$data['userid'] = $id;
			$this->load->view('resetpassword', $data);
		}
		else
		{
			if(abs($id) > 0){
				
				$pwd = $this->input->post('newpassword');
				$password = md5($pwd);
	
				$data = array(
					'password' => $password,
					'updated_at' => date('Y-m-d H:i:s'),
					'updated_by' => $id
				);
				
				if($this->login_model->updatepassword($id, $data) !== false){
					$this->session->set_flashdata('message', '<div class="alert alert-success"><strong><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Password successfully changed, You can now login.</strong></div>');
					redirect('login');
				}
				else{
					$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Error in  recover password. Please report this to administrator.</div>';
					$this->session->set_flashdata('message', $message);
					redirect("login/recoverpassword");
				}
				
			}
			else {
				
				$message = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> This link already expired.</div>';
				$this->session->set_flashdata('message', $message);
				redirect("login/recoverpassword");
				
			}
			
			
		}
	}
}
