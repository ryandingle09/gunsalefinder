<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportproduct extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('registration_model','register',TRUE);
		$this->load->model('wishlist_model','wishlistdb',TRUE);
		$this->load->model('Reportproduct_model', 'reportproduct', TRUE);
		$this->load->model('product_model','product',TRUE);
		$this->load->helper('countryblock');
		$this->load->helper('mailgun');
		if(!checkcountry()){
			redirect('/out-of-region');
		}
	}
	
	function index()
	{
		if($this->session->userdata('logged_in'))
		{
			
			$id = $this->session->userdata('id');
			$this->load->library('pagination');
			$result_per_page = PERPAGE;  // the number of result per page
			$config['base_url'] = base_url() . '/dealer/product/index/';
			$countwish = $this->wishlistdb->countWishlist($id);
			$config['total_rows'] = $countwish;
			$config['per_page'] = $result_per_page;
			//*for boostrap pagination
			$config['full_tag_open'] = '<ul class="pagination pagination-lg">';
			$config['full_tag_close'] = '</ul>';            
			$config['prev_link'] = '<i class="fa fa-chevron-left" aria-hidden="true"></i>';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '</li>';
			$config['next_link'] = '<i class="fa fa-chevron-right" aria-hidden="true"></i>';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config["num_links"] = round( $config["total_rows"] / $config["per_page"] );
			//end boostrap pagination
			$this->pagination->initialize($config);
			$offset = ($this->uri->segment(4) != '')?$this->uri->segment(4):0;
			$data['countrow'] = $config['total_rows'];
			$data['results'] = $this->wishlistdb->getAllWishlist($id, $result_per_page, $offset);
			$data['info'] = $this->register->getuserinfobyid($id);
			$this->load->view('dashboard/wishlists', $data);
			
		} else {
			redirect('login');
		}


	}

	function reportItem(){
		if($this->session->userdata('logged_in') || $this->session->userdata('dealer_login'))
		{
			$user_type = ($this->session->userdata('dealer_login')) ? 'dealer' : 'customer';
			$data = array(
				'product_id' => $this->input->post('id'),
				'details' => $this->input->post('details'),
				'report_type' => $this->input->post('type'),
				'user_id' => $this->session->userdata('id'),
				'user_type' => $user_type,
				'date_reported' => date("Y-m-d H:i:s")
			);

			if($this->reportproduct->insertReportProduct($data)){
				$product_id = $this->input->post('id');
				$notify_dealer = $this->reportproduct->notifyDealer($product_id);
				
				$sendToAdmin = $this->emailReportProductToAdmin($data);
				$status = false;
				if($sendToAdmin)
				{
					$status = true;
					if($notify_dealer->notify_dealer == 1)
					{
						$sendToDealer = $this->emailReportProductToDealer($data);
						if($sendToDealer)
						{
							$status = true;
						}
						else
						{
							$status = false;
						}
					}
				}
				else
				{
					$status = false;
				}

				echo ($status == true) ? '1' : 'error email'; 
			}else{
				echo "2";
			}
			
		} else {
			echo "0";
		}

	}

	function emailReportProductToDealer($data)
	{ 
		$product_id = $data['product_id'];		
		$user_id = $this->session->userdata('id');

		$product = $this->reportproduct->productDetails($product_id);

		//Dealer Info
		$dealer_id = $product->dealer_id;
		$dealer = $this->reportproduct->getDealerDetailsByID($dealer_id);
		$dealer_email = $dealer->email;
		$dealer_name = $dealer->title.' '.$dealer->first_name.' '.$dealer->last_name;

		//Reporter Info
		$reporter_name = '';
		if($data['user_type'] == 'dealer')
		{
			$reporter = $this->reportproduct->getDealerDetailsByID($user_id);
			$reporter_name = $reporter->title.' '.$reporter->first_name.' '.$reporter->last_name;
		}
		else
		{
			$reporter = $this->reportproduct->getUserDetailsByID($user_id);
			$reporter_name = $reporter->name;
		}
		
		
		$subject = 'Gun Sale Finder - Your item has been reported';
		$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<title>gunsalefinder.com - Find Gun in America</title>
					</head>
					<body style="margin:0px;">';
		$message .='<font face="Segoe UI Semibold, Segoe UI Bold, Helvetica Neue Medium, Arial, sans-serif ">
						<table cellspacing="0" cellpadding="0" style="width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
							<tr>
								<td style="background:#ea8916;height:60px;padding:0;background:#fff;">
									<img src="'.base_url().'assets/images/header/gunpro-header.png" style="width: 100px;left:10px;height:77px;"/>
								</td>
								<!--<td style="width:100%;">
									<img src="'.base_url().'assets/img/EmailLogo-BG.png" style="height: 77px; max-height: 77px;width: 100%;" />
								</td>-->
							</tr>
						</table>
						<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
							<tr><td colspan="2"><br />Hi there '.$dealer_name.',</td></tr>
							<tr>
								<td colspan="2"><br />
									Your item has been reported. <br />
									Product Name: <i>'.$product->title.'</i>
									Details: <i>'.$data['details'].'</i>
									Reported by: <i>'.$reporter_name.'</i>
									<br /><br />
									Cheers, <br />
									Gunsalefinder
								</td>
							</tr>
						</table>
					</font>';
		$message .= "</body></html>";
		$output = sendmailgun($dealer_email, '', EMAILBCC, $subject, $message);
		$result = json_decode($output);
		$issent = false;
		if($result->message == 'Queued. Thank you.'){
			$issent = true;
		}

		return $issent;
	}

	function emailReportProductToAdmin($data)
	{
		$product_id = $data['product_id'];
		$user_id = $this->session->userdata('id');
		
		$product = $this->reportproduct->productDetails($product_id);

		//Dealer Info
		$dealer_id = $product->dealer_id;
		$dealer = $this->reportproduct->getDealerDetailsByID($dealer_id);
		$dealer_email = $dealer->email;
		$dealer_name = $dealer->title.' '.$dealer->first_name.' '.$dealer->last_name;

		//Reporter Info
		$reporter_name = '';
		if($data['user_type'] == 'dealer')
		{
			$reporter = $this->reportproduct->getDealerDetailsByID($user_id);
			$reporter_email = $reporter->email;
			$reporter_name = $reporter->title.' '.$reporter->first_name.' '.$reporter->last_name;
		}
		else
		{
			$reporter = $this->reportproduct->getUserDetailsByID($user_id);
			$reporter_email = $reporter->email;
			$reporter_name = $reporter->name;
		}

		$subject = 'Gun Sale Finder - Item has been reported';
		$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
					<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<title>gunsalefinder.com - Find Gun in America</title>
					</head>
					<body style="margin:0px;">';
		$message .='<font face="Segoe UI Semibold, Segoe UI Bold, Helvetica Neue Medium, Arial, sans-serif ">
						<table cellspacing="0" cellpadding="0" style="width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
							<tr>
								<td style="background:#ea8916;height:60px;padding:0;background:#fff;">
									<img src="'.base_url().'assets/images/header/gunpro-header.png" style="width: 100px;left:10px;height:77px;"/>
								</td>
								<!--<td style="width:100%;">
									<img src="'.base_url().'assets/img/EmailLogo-BG.png" style="height: 77px; max-height: 77px;width: 100%;" />
								</td>-->
							</tr>
						</table>
						<table cellspacing="0" cellpadding="0" style="padding:0 20px;width: 100%; font-size: 15px;color: #383838;  line-height: 26px;">
							<tr><td colspan="2"><br />Dear Admin,</td></tr>
							<tr>
								<td colspan="2"><br />
									Item has been reported. <br />
									Product Name: <i>'.$product->title.'</i><br />
									Report Details: <i>'.$data['details'].'</i><br />
									Reported By: <i>'.$reporter_name.'</i><br />
									<br /><br />
									Cheers, <br />
									Gunsalefinder
								</td>
							</tr>
						</table>
					</font>';
		$message .= "</body></html>";
		$from = $reporter_name.' <'.$reporter_email.'>';
		$output = sendmailgun(EMAILFROM, '', EMAILBCC, $subject, $message, $from);
		$result = json_decode($output);
		$issent = false;
		if($result->message == 'Queued. Thank you.'){
			$issent = true;
		}
		return $issent;
	}
	
}
