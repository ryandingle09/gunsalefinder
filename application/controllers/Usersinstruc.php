<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/******************************************************************
*
*	Title		: 	Users instruction
*	Author		: 	Ronald Duque | ronaldduque24@gmail.com
*	Filename 	: 	Usersinstruc.php
*	Date 		: 	August 2018
*
******************************************************************/

class Usersinstruc extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper('countryblock');
		if(!checkcountry()){
			redirect('/out-of-region');
		}
	}
	
	function index()
	{
		$this->load->view('pages/usersinstruction');
	}

	
}
