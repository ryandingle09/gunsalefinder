<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Memcached settings
| -------------------------------------------------------------------------
| Your Memcached servers can be specified below.
|
|	See: https://codeigniter.com/user_guide/libraries/caching.html#memcached
|
*/
$host = '127.0.0.1';
$port = '11211';
$weight = '1';

if($_SERVER['SERVER_NAME'] == 'gunsalefinder.com' || $_SERVER['SERVER_NAME'] == 'www.gunsalefinder.com'){ 
	$host = '127.0.0.1';
	$port = '11211';
	$weight = '1';
}

$config = array(
	'default' => array(
		'hostname' => $host,
		'port'     => $port,
		'weight'   => $weight,
	),
);
