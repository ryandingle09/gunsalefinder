<?php defined('BASEPATH') OR exit('No direct script access allowed.');


$host = '127.0.0.1';
$port = '6379';
$timeout = 0;

if($_SERVER['SERVER_NAME'] == 'gunsalefinder.com' || $_SERVER['SERVER_NAME'] == 'www.gunsalefinder.com'){ 
	$host = '127.0.0.1';
    $port = '6379';
    $timeout = 0;
}

$config['socket_type'] = 'tcp'; //`tcp` or `unix`
$config['socket'] = '/var/run/redis.sock'; // in case of `unix` socket type
$config['host'] = $host;
$config['password'] = NULL;
$config['port'] = $port;
$config['timeout'] = $timeout;