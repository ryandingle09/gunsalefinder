<?php

function sendmailgun($send_to, $cc = '', $bcc = '', $subject, $content, $from='') {

	    $api_key = '54827534cb6a86e1fd7f1f0bf9041beb-1053eade-5100c003';
	    $api_domain = 'https://api.mailgun.net/v3/mail.gunsalefinder.com';
		$name = "Gunsalefinder Team";
		
		$email = $from;

	    if($from == ''){
	    	$email = 'Gunsalefinder Team <support@gunsalefinder.com>';
	    }
	    
	    $messageBody = $content;
	    $config = array();
	    $config['api_key'] = $api_key;
	    $config['api_url'] = 'https://api.mailgun.net/v3/mail.gunsalefinder.com/messages';
	    $message = array();
	    $message['from'] = $email;
	    $message['to'] = $send_to;
	    $message['h:Reply-To'] = $email;
	    $message['subject'] = $subject;
	    $message['html'] = $messageBody;
	    if($cc != ''){
			$message['cc'] = $cc;
		}
		if($bcc != ''){
			$message['bcc'] = $bcc;
		}
	    $curl = curl_init();
	    curl_setopt($curl, CURLOPT_URL, $config['api_url']);
	    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	    curl_setopt($curl, CURLOPT_USERPWD, "api:{$config['api_key']}");
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	    curl_setopt($curl, CURLOPT_POST, true); 
	    curl_setopt($curl, CURLOPT_POSTFIELDS,$message);
	    $result = curl_exec($curl);
	    curl_close($curl);
	    return $result;
}



?>