<?php

class Homepage_helper {
    
    public function cache_homepage()
    {
        $ch = curl_init($_SERVER['SERVER_NAME'].'/home/cache_homepage');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['cron' => 'matic']);
        $response = curl_exec($ch);
        curl_close($ch);
    }

}

?>