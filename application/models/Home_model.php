<?php
Class Home_model extends CI_Model
{
	function getBannersArray($id = '1') {
		$this->db->from('ads');
		$this->db->join('ad_locations', 'ad_locations.id = ads.location_id');
		$this->db->where('ad_locations.id', $id);
		$this->db->where('ad_locations.status', 'A');
		$this->db->where('ads.status', 'A');
		$this->db->select('ads.image_url,ads.link_url,ads.image_full_path');
		$result = $this->db->get()->result_array();
		return $result;
	}

	// this is for dynamic calling of banners rather than calling ids
	function getBannersByLocationCode($location_code = 'HTC') 
	{
		$this->db->from('ads');
		$this->db->join('ad_locations', 'ad_locations.id = ads.location_id');
		$this->db->where('ad_locations.location_code', $location_code);
		$this->db->where('ad_locations.status', 'A');
		$this->db->where('ads.status', 'A');
		$this->db->select('ads.*');
		$result = $this->db->get()->result_array();
		return $result;
	}

	// this is for dynamic calling of feature rather than calling ids
	function getFeatureByLocationCode($location_code = 'HMF') 
	{
		$this->db->select('ads.*');
		$this->db->from('ads');
		$this->db->join('ad_locations', 'ad_locations.id = ads.location_id');
		$this->db->where('ad_locations.location_code', $location_code);
		$this->db->where('ad_locations.status', 'A');
		$this->db->where('ads.status', 'A');
		$result = $this->db->get()->result_array();
		return $result;
	}

}
?>