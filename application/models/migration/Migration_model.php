<?php
Class Migration_model extends CI_Model
{
	
	public $tblProducts = "products";
	public $tblProductCategories = "product_categories";
	public $tblProductImg = "product_images";


	function updateProduct($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update($this->tblProducts, $data); 
		return;
	}

	function checkUPCexist( $upc = ''){
		$this->db->where('upc', $upc);
		$this->db->where('dealer_id', KINGSFARM);
		$this->db->where('api_created', 1);
		$this->db->select('id, price, item_url');    
		$this->db->from($this->tblProducts);
		$query = $this->db->get();
		$ret = $query->row();
		return ( $query->num_rows() >= 1 ) ? $ret : false;  
	}

	function checkUPCImagesexist( $upc = ''){
		$this->db->where('upc', $upc);
		$this->db->where('dealer_id', KINGSFARM);
		$this->db->select('id');    
		$this->db->from($this->tblProductImg);
		$query = $this->db->get();
		$ret = $query->row();
		return ( $query->num_rows() >= 1 ) ? $ret->id : false;  
	}

	function getDealerProducts($id = 0)
	{
	   $this->db->select('upc'); 
       $this->db->from($this->tblProducts);   
	   $this->db->where('dealer_id', $id);
	   $query = $this->db->get();
	   return $query->result();
	}

	function getDealerProductsCategory($id = 0)
	{
	   $this->db->select('id, upc'); 
       $this->db->from($this->tblProducts);   
	   $this->db->where('category_code IS NULL', null, false);
	   $query = $this->db->get();
	   return $query->result();
	}

	function getCorrectCategory($upc = 0)
	{
	   $this->db->select('category_code'); 
       $this->db->from($this->tblProductCategories);
       $this->db->where('category_code IS NOT NULL', null, false);
       $this->db->where('product_upc', $upc); 
	   $this->db->order_by('distributor_id', 'asc');
	   $this->db->limit(1);
	   $query = $this->db->get();
	   return $query->result_array(); 
	}

	function getDealerProductsSubCategory($id = 0)
	{
	   $this->db->select('id, upc'); 
       $this->db->from($this->tblProducts);   
	   $this->db->where('dealer_id', $id);
	   $this->db->where('sub_category_code IS NULL', null, false);
	   $query = $this->db->get();
	   return $query->result();
	}


	function getCorrectSubCategory($upc = 0)
	{
	   $this->db->select('sub_category_code'); 
       $this->db->from($this->tblProductCategories);
       $this->db->where('sub_category_code IS NOT NULL', null, false);  
	   $this->db->order_by('distributor_id', 'asc');
	   $this->db->where('product_upc', $upc);
	   $this->db->limit(1);
	   $query = $this->db->get();
	   return $query->result_array(); 
	}

	function getDealerProductsBrand($id = 0)
	{
	   $this->db->select('id, upc'); 
       $this->db->from($this->tblProducts);   
	   $this->db->where('brand_id IS NULL', null, false);
	   $query = $this->db->get();
	   return $query->result();
	}


	function getCorrectBrand($upc = 0)
	{
	   $this->db->select('brand_id'); 
       $this->db->from($this->tblProductCategories);
       $this->db->where('brand_id IS NOT NULL', null, false); 
       $this->db->where('product_upc', $upc);
	   $this->db->order_by('distributor_id', 'asc');
	   $this->db->limit(1);
	   $query = $this->db->get();
	   return $query->result_array(); 
	}
	
}
?>