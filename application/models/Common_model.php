<?php
Class Common_model extends CI_Model
{
	function getusername($id='')
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('users');
	   $ret = $query->row();
	   return isset($ret)? $ret->username: '';
	}

	function getName($id='')
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('users');
	   $ret = $query->row();
	   return isset($ret)? $ret->name: '';
	}


	function getUseremail($id='')
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('users');
	   $ret = $query->row();
	   return isset($ret)? $ret->email: '';
	}

	function getDealerusername($id='')
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('dealers');
	   $ret = $query->row();
	   return isset($ret)? $ret->username: '';
	}

	function getDealeremail($id='')
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('dealers');
	   $ret = $query->row();
	   return isset($ret)? $ret->email: '';
	}

	function getDealerName($id='')
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('dealers');
	   $ret = $query->row();
	   return isset($ret)? $ret->first_name.' '.$ret->last_name: '';
	}


	function getDealerCompanyName($id='')
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('dealers');
	   $ret = $query->row();
	   return isset($ret)? $ret->company_name: '';
	}

	function getDealerCompanyUrl($id='')
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('dealers');
	   $ret = $query->row();
	   return isset($ret)? $ret->web_url: '';
	}

	function getProductTitlebyUPC($upc='')
	{
	   $this->db->limit('1');
	   $this->db->where('upc', $upc);
	   $query = $this->db->get('products');
	   $ret = $query->row();
	   return isset($ret)? $ret->title: '';
	}

	function getProductIDbyUPC($upc='')
	{
	   $this->db->limit('1');
	   $this->db->where('upc', $upc);
	   $this->db->order_by('price', 'asc');
	   $query = $this->db->get('products');
	   $ret = $query->row();
	   return isset($ret)? $ret->id: '';
	}

	function getProductImagebyID($product_id='')
	{
	   $this->db->limit('1');
	   $this->db->where('product_id', $product_id);
	   $query = $this->db->get('product_images');
	   $ret = $query->row();
	   return isset($ret)? $ret->image_url : '';
	}

	function getProductImagebyUPC($upc='')
	{
	   $this->db->limit('1');
	   $this->db->where('product_upc', $upc);
	   $this->db->order_by('distributor_id', 'asc');
	   $query = $this->db->get('product_images');
	   $ret = $query->row();
	   return isset($ret)? $ret->image_url : '';
	}

	function getProductImage($upc='', $api = 1, $id = 0 )
	{
	   	if($api == 1)
	   	{
		  	$this->db->where('product_upc', $upc);
		  	$this->db->order_by('distributor_id', 'asc');
		  	$this->db->limit('1');
		  	$query = $this->db->get('product_images');
		}
		else
	   	{
	   	  	$this->db->where('deal_id', $id);
		  	$this->db->order_by('deal_id', 'asc');
		  	$this->db->limit('1');
		  	$query = $this->db->get('deal_images');
	   	}

	   $ret = $query->row();
	   return isset($ret)? $ret->image_url : '';
	}


	function getDealImagebyUPC($upc='')
	{
	   $this->db->limit('1');
	   $this->db->where('deal_id', $upc);
	   $query = $this->db->get('deal_images');
	   $ret = $query->row();
	   return isset($ret)? $ret->image_url : '';
	}

	function getProductUPC($id)
	{
	   $this->db->limit('1');
	   $this->db->where('id', $id);
	   $query = $this->db->get('products');
	   $ret = $query->row();
	   return isset($ret)? $ret->upc: '';
	}

	function checkDealerExists($id){
	   $this->db->where('id', $id);
	   $query = $this->db->get('dealers');
	   return (count($query->result()) > 0)? true:false;
	}
	 
	function getallcategory()
	{
	   $this->db->order_by('category_code', 'asc');
	   $query = $this->db->get('categories');
	   return $query->result();
	}

	function getAllSubcategory()
	{
	   $this->db->order_by('sub_category_code', 'asc');
	   $query = $this->db->get('sub_categories');
	   return $query->result();
	}

	function getSubcategoryByCode($code)
	{
	   $this->db->where('category_code', $code);
	   $this->db->order_by('sub_category_code', 'asc');
	   $query = $this->db->get('sub_categories');
	   return $query->result();
	}

	function getCategoryname($id=0)
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('categories');
	   $ret = $query->row();
	   return isset($ret)? $ret->name: '';
	}

	function getallStates()
	{
	   $this->db->order_by('name', 'asc');
	   $this->db->where('status', '1');
	   $query = $this->db->get('states');
	   return $query->result();
	}

	function getallBrand()
	{
	   $this->db->order_by('name', 'asc');
	   $this->db->where('status', '1');
	   $query = $this->db->get('brands');
	   return $query->result();
	}


	function getStatename($id=0)
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('states');
	   $ret = $query->row();
	   return isset($ret)? $ret->name: '';
	}

	function getStatebyDealer($id=0)
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('dealers');
	   $ret = $query->row();
	   return isset($ret)? $this->getStatename($ret->state): '';
	}
	
	
	function getallCountries()
	{
	   $this->db->order_by('name', 'asc');
	   $query = $this->db->get('countries');
	   return $query->result();
	}

	function getallSubscriptions()
	{
	   $this->db->order_by('price', 'asc');
	   $query = $this->db->get('subscriptions');
	   return $query->result();
	}

	function getBanner($location) {

		$this->db->select('image_url');
    	$this->db->from('ads');
    	$this->db->where('location_id', $location);
    	$this->db->order_by('created_at', 'DESC');
    	$result = $this->db->get();

		return $result->row();
	}
	
	function getPurchasedBanner($id) {
		$this->db->from('ads');
		$this->db->join('ad_locations', 'ad_locations.id = ads.location_id');
		$this->db->where('ad_locations.id', $id);
		$this->db->where('ad_locations.status', 'A');
		$this->db->where('ads.status', 'A');
		$this->db->select('ads.status, ads.image_url, ad_locations.status as l_status');
		$result = $this->db->get()->result_array();
		if($result){
			return $result[0]['image_url'];
		}else{
			return '';
		}
	}

	function timetoago($datetime, $full = false) {
	     $today = time();    
		 $createdday= strtotime($datetime); 
		 $datediff = abs($today - $createdday);  
		 $difftext="";  
		 $years = floor($datediff / (365*60*60*24));  
		 $months = floor(($datediff - $years * 365*60*60*24) / (30*60*60*24));  
		 $days = floor(($datediff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));  
		 $hours= floor($datediff/3600);  
		 $minutes= floor($datediff/60);  
		 $seconds= floor($datediff);  
		 //year checker  
		 if($difftext=="")  
		 {  
		   if($years>1)  
			$difftext=$years." years ago";  
		   elseif($years==1)  
			$difftext=$years." year ago";  
		 }  
		 //month checker  
		 if($difftext=="")  
		 {  
			if($months>1)  
			$difftext=$months." months ago";  
			elseif($months==1)  
			$difftext=$months." month ago";  
		 }  
		 //month checker  
		 if($difftext=="")  
		 {  
			if($days>1)  
			$difftext=$days." days ago";  
			elseif($days==1)  
			$difftext=$days." day ago";  
		 }  
		 //hour checker  
		 if($difftext=="")  
		 {  
			if($hours>1)  
			$difftext=$hours." hours ago";  
			elseif($hours==1)  
			$difftext=$hours." hour ago";  
		 }  
		 //minutes checker  
		 if($difftext=="")  
		 {  
			if($minutes>1)  
			$difftext=$minutes." minutes ago";  
			elseif($minutes==1)  
			$difftext=$minutes." minute ago";  
		 }  
		 //seconds checker  
		 if($difftext=="")  
		 {  
			if($seconds>1)  
			$difftext=$seconds." seconds ago";  
			elseif($seconds==1)  
			$difftext=$seconds." second ago";  
		 }  
		 return $difftext;  
	}
	
	function formatdate($date =''){
		 if($date !=''){
			 $formatdate = date('M d, Y', strtotime($date));
		 }else{
			 $formatdate = '';
		 }
		 return $formatdate;
	}
	
	function cleanurl($string) {
	    $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
	    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		$string = str_replace('--', '-', $string); // Replaces double spaces with hyphens.
		return $string;
	}

	function checkSession(){
		 $loginshow = true;
		 if($this->session->userdata('logged_in')){
			 $loginshow = false;
		 }

		 if($this->session->userdata('dealer_login')){
			 $loginshow = false;
		 }
		 return $loginshow;
	}

	function getIpAddress() 
	{
		
		$local = isset($_SERVER['LOCAL_ADDR'])? $_SERVER['LOCAL_ADDR'] : '';
		$port = isset($_SERVER['LOCAL_PORT'])? $_SERVER['LOCAL_PORT'] : '';
		$ip = $_SERVER['HTTP_USER_AGENT'].$local.$port.$_SERVER['REMOTE_ADDR'];
		return $ip;
	}

	function countMyWishlist()
	{
	   $id = $this->session->userdata('id');

	   if($this->session->userdata('logged_in')){
           $this->db->where('created_by', $id);
       }

	   if($this->session->userdata('dealer_login')){
           $this->db->where('dealer_id', $id);
       }

	   $q = $this->db->get('wishlists');
	   return $q->num_rows();
	}

	function countmyProduct()
	{
	 	$id = $this->session->userdata('id');
	 	$this->db->where('api_created !=', 1);
	 	$this->db->where('dealer_id', $id);
	    $q = $this->db->get('products');
	    return $q->num_rows();
	}

	function countmyAds($active=True)
	{
	 	$id = $this->session->userdata('id');
	 	if($active==False)
		{
			$this->db->where('status', 'I');
		}
	 	$this->db->where('dealer_id', $id);
	    $q = $this->db->get('ads');
	    return $q->num_rows();
	}

	function countmyPendingAds()
	{
	 	$id = $this->session->userdata('id');
	 	$this->db->where('dealer_id', $id);
	 	$this->db->where('status', 'I');
	    $q = $this->db->get('ads');
	    return $q->num_rows();
	}

	function countmyReportDeals()
	{
	 	$id = $this->session->userdata('id');
	    $this->db->where('products.dealer_id', $id);
		$this->db->join('products', 'reported_products.product_id = products.id');
		$q = $this->db->get('reported_products');
	    return $q->num_rows();
	}

	function getBannersArray($id = '1') {
		$this->db->from('ads');
		$this->db->join('ad_locations', 'ad_locations.id = ads.location_id');
		$this->db->where('ad_locations.id', $id);
		$this->db->where('ad_locations.status', 'A');
		$this->db->where('ads.status', 'A');
		$this->db->select('ads.image_url,ads.link_url');
		$result = $this->db->get()->result_array();
		return $result;
	}
	
	function getNameLoggedIn()
	{
		$id = $this->session->userdata('id');
		$this->db->where('id', $id);
		if($this->session->userdata('logged_in'))
		{
			$q = $this->db->get('users')->row();
	    	return $q->name;
		}
		if($this->session->userdata('dealer_login'))
		{
			$q = $this->db->get('dealers')->row();
	    	return $q->first_name.' '.$q->last_name;
		}
	    
	}

	function getNameLoggedInV2()
	{
		$id = $this->session->userdata('id');
		$this->db->where('id', $id);
		if($this->session->userdata('logged_in'))
		{
			$q = $this->db->get('users')->row();
	    	return $q->username;
		}
		if($this->session->userdata('dealer_login'))
		{
			$q = $this->db->get('dealers')->row();
	    	return $q->username;
		}
	    
	}
	
	function getUserType()
	{
		if($this->session->userdata('logged_in'))
		{
	    	return 'user';
		}
		if($this->session->userdata('dealer_login'))
		{
	    	return 'dealer';
		}
	}

	function isProductLike($id)
	{
		$user_id = $this->session->userdata('id');
		$this->db->where('user_id', $user_id);
		$this->db->where('product_id', $id);
		$this->db->where('type','like');
		$result = $this->db->get('product_likes')->row();
		if(!empty($result))
		{
			return 'liked';
		}
		
	}

	function thisProductLike($id)
	{
		$user_id = $this->session->userdata('id');
		$this->db->where('user_id', $user_id);
		$this->db->where('product_id', $id);
		$this->db->where('type','like');
		$result = $this->db->get('product_likes')->row();
		if(!empty($result))
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
	
	function isProductDislike($id)
	{
		$user_id = $this->session->userdata('id');
		$this->db->where('user_id', $user_id);
		$this->db->where('product_id', $id);
		$this->db->where('type','dislike');
		$result = $this->db->get('product_likes')->row();
		if(!empty($result))
		{
			return 'liked';
		}
		
	}

	function isProductSubs($id)
	{
		$user_id = $this->session->userdata('id');
		$this->db->where('user_id', $user_id);
		$this->db->where('product_id', $id);
		$comments = $this->db->get('comment_subscriptions')->result();
		$this->db->where('user_id', $user_id);
		$this->db->where('product_id', $id);
		$price_drops = $this->db->get('pricedrop_subscriptions')->result();

		if(count($comments) > 0 || count($price_drops) > 0)
		{
			return 'liked';
		}
		
	}

	function isProductSubsComment($id)
	{
		$user_id = $this->session->userdata('id');
		$this->db->where('user_id', $user_id);
		$this->db->where('product_id', $id);
		$comments = $this->db->get('comment_subscriptions')->result();

		return (count($comments) > 0) ? true : false;
	}

	function isProductSubsPricedrop($id)
	{
		$user_id = $this->session->userdata('id');
		$this->db->where('user_id', $user_id);
		$this->db->where('product_id', $id);
		$price_drops = $this->db->get('pricedrop_subscriptions')->result();

		return (count($price_drops) > 0) ? true : false;
	}

	function thisProductDislike($id)
	{
		$user_id = $this->session->userdata('id');
		$this->db->where('user_id', $user_id);
		$this->db->where('product_id', $id);
		$this->db->where('type','dislike');
		$result = $this->db->get('product_likes')->row();
		if(!empty($result))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	function getProductDetails($id)
	{
		$this->db->where('id', $id);
		return $this->db->get('products')->row();
	}

	function makeUpcStandard($upc='')
	{
	   $count = strlen($upc);
	   $standardUPC = $upc;
	   if($count == 11){
	   		$standardUPC = '0'.$upc;
	   }
	   return $standardUPC;
	}

	function dealImagesLink($id) {

		$this->db->select('image_url');
		$this->db->from('deal_images');
		$this->db->where('deal_id', $id);
		$this->db->limit('1');
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		$ret = $query->row();
	    return isset($ret)? $ret->image_url: '';
	}

	function formatUrl($string) 
	{
	   $string = str_replace(' ', '-', $string); // Replaces  spaces with hyphens.
	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}

	public function update_image($product_id, $product_upc, $api_created)
	{
		$this->db->select('image_url');
		
	   	if($api_created == 1)
	   	{
		  	$this->db->where('product_upc', $product_upc);
		  	$this->db->order_by('distributor_id', 'asc');
		  	$this->db->limit('1');
		  	$query = $this->db->get('product_images');
		}
		else
	   	{
	   	  	$this->db->where('deal_id', $product_id);
		  	$this->db->order_by('deal_id', 'asc');
		  	$this->db->limit('1');
		  	$query = $this->db->get('deal_images');
	   	}

	   	$ret 	= $query->row();
		$image 	= isset($ret) ? $ret->image_url : null;
		
		if($image != null)
		{
			$this->db->where('id', $product_id);
			$this->db->update('products', ['product_image' => $image]); 
		}

		return $image;
	}
}
?>