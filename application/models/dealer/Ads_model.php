<?php
Class Ads_model extends CI_Model
{
	function getAllMyAds($id=0, $limit = 0, $offset=0, $active=True)
	{
	   $this->db->limit($limit, $offset);
	   if($active==False)
			$this->db->where('status', 'I');
		else
			$this->db->where('status', 'A');
	   $this->db->where('dealer_id', $id);
	   #$this->db->order_by('title', 'asc');
	   $this->db->order_by('id', 'desc');
	   $query = $this->db->get('ads');
	   return $query->result();
	}

	function countMyAds($id=0, $active=True){
		if($active==False)
		{
			$this->db->where('status', 'I');
		}
		$this->db->where('dealer_id', $id);
	   	$query = $this->db->get('ads');
	   	return $query->num_rows();
	}

	function getAdsByid($id=0)
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('ads');
	   return $query->result();
	}

	function getAdsLocationByid($id=0)
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('ad_locations');
	   return $query->result();
	}

	function insertAds($data){
		$this->db->insert('ads', $data);
		return ($this->db->affected_rows() == '1') ? true : false;
	}

	function updatetAds($data, $id){
		$this->db->where('id', $id);
		$this->db->update('ads', $data);
		return ($this->db->affected_rows() == '1') ? true : false;
	}
}
?>