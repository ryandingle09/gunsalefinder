<?php
Class Postdeal_model extends CI_Model
{
	public $tblAdsLocation = "ad_locations";
	public $tblBrands = "brands";
	public $tblDeal = "products";
	public $tblDealDesc = "deal_descriptions";
	public $tblDealInfo = "deal_informations";
	public $tblDealImages = "deal_images";
	public $tblNotify = "pricedrop_subscriptions";
	public $tblRatings = "rate_product";
	public $tblUsers = "users";
	public $tblComments = "comments";
	public $tblIssues = "reported_products";
	public $tblDealerSubs = "dealer_subscriptions";
	public $tblImageTemp = "upload_temp";
	public $tblDealer = "dealers";
	public $tblCategories = "categories";
	public $tblSubCategories = "sub_categories";

	function getAllAdslocationbyid($id=0)
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get($this->tblDeal);
	   return $query->result();
	}

	function getAdsLocationbyid($id=0)
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get($this->tblAdsLocation);
	   return $query->result();
	}

	function insertProducts($data){
		$this->db->insert($this->tblDeal, $data);
		return ($this->db->affected_rows() == '1') ? true : false;
	}

	function insertProductDescription($data){
		$this->db->insert($this->tblDealDesc, $data);
		return ($this->db->affected_rows() == '1') ? true : false;
	}

	function insertDealInfo($data){
		$this->db->insert($this->tblDealInfo, $data);
		return ($this->db->affected_rows() == '1') ? true : false;
	}

	function delete_tempimages($code =''){
		$this->db->where('code', $code);
		$this->db->delete('upload_temp');
		return ($this->db->affected_rows()=='1')? true : false ;
	}

	function countmyads($id=0){
		$this->db->where('created_by', $id);
		return $this->db->count_all($this->tblDeal);
	}
	
	function getSubcategory($code=0)
	{
		$this->db->where('category_code', $code);
		$query = $this->db->get('sub_categories');
		$x = array();
		if($query->result())
		{
			foreach ($query->result() as $row)
			{
				$x[$row->sub_category_code] = $row->list_name;
			}
			return $x;
		}
		else
		{
			return false;
		}
	}

	function checkDealsExist($upc=''){	
		$this->db->select('id');  
		$this->db->from($this->tblDeal);
		$this->db->where('upc', $upc);
		$this->db->where('dealer_id', $this->session->userdata('id'));
		$query = $this->db->get();
		$result = $query->result();
		return ( count($result) > 0 ) ? TRUE : FALSE;  
	}

	function insertImages($id = 0, $code =''){
		$sql = "INSERT INTO `deal_images` (`deal_id`,image_url) SELECT '$id', filename FROM `upload_temp` WHERE `code` ='$code';";
		$query = $this->db->query($sql);
		return ($this->db->affected_rows() > 0)? TRUE : FALSE ;
	}
	
	function getProductURL($code='')
	{
	   $this->db->order_by('id', 'ASC');
	   $this->db->where('code', $code);
	   $this->db->limit(1);
	   $query = $this->db->get('upload_temp');
	   $ret = $query->row();
	   return isset($ret)? $ret->filename: '';
	}

	function deleteTempimages($code =''){
		$this->db->where('code', $code);
		$this->db->delete('upload_temp');
		return ($this->db->affected_rows()=='1')? TRUE : FALSE ;
	}

	function getDealsbyid($id=0)
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get($this->tblDeal);
	   return $query->result();
	}

	function getDealsDescriptionbyid($id=0)
	{
	   $this->db->where('deal_id', $id);
	   $query = $this->db->get($this->tblDealDesc);
	   return $query->result();
	}

	function getOtherInfobyid($id=0)
	{
	   $this->db->where('deal_id', $id);
	   $query = $this->db->get($this->tblDealInfo);
	   return $query->result();
	}

	function getUPCbyid($id='')
	{
	   $this->db->where('id', $id);
	   $this->db->limit(1);
	   $query = $this->db->get($this->tblDeal);
	   $ret = $query->row();
	   return isset($ret)? $ret->upc: '';
	}

	function getDealImages($id = 0)
	{	
		$this->db->from($this->tblDealImages);
		$this->db->where('deal_id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	function deleteDealImages($id =''){
		$this->db->where('id', $id);
		$this->db->delete($this->tblDealImages);
		return ($this->db->affected_rows()=='1')? TRUE : FALSE ;
	}

	function updateDeals($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update($this->tblDeal, $data); 
		return($this->db->affected_rows() > 0)? true:false;
	}

	function updateDealDescription($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update($this->tblDealDesc, $data); 
		return($this->db->affected_rows() > 0)? true:false;
	}

	function updateDealInformation($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update($this->tblDealInfo, $data); 
		return($this->db->affected_rows() > 0)? true:false;
	}

	function getUserToNotify($id = 0)
	{	
		$this->db->from($this->tblNotify);
		$this->db->where('product_id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	function getRatings($id = 0)
	{
	    $this->db->select('r.product_id, r.rate, r.remarks, r.post_date, r.post_time, r.user_id, u.name');
        $this->db->from($this->tblRatings." r");
        $this->db->join($this->tblUsers." u", 'u.id = r.user_id');
        $this->db->where('r.product_id', $id);
        $query = $this->db->get();
	   	return $query->result();

	   	// echo $this->db->last_query(); die();
	}

	function getComments($id = 0){
		$this->db->select('c.id, c.product_id, c.user_id, c.message, c.created_at, u.name, u.picture');
		$this->db->from($this->tblComments." c");
        $this->db->join($this->tblUsers." u", 'u.id = c.user_id');
        $this->db->where('c.product_id', $id);
        $this->db->order_by('c.created_at', 'DESC');
        $query = $this->db->get();
	   	return $query->result();
	}

	function getIssues($id = 0){
		$this->db->select('c.*, u.name');
		$this->db->from($this->tblIssues." c");
        $this->db->join($this->tblUsers." u", 'u.id = c.user_id');
        $this->db->where('c.product_id', $id);
        $this->db->order_by('c.date_reported', 'DESC');
        $query = $this->db->get();
	   	return $query->result();
	}

	function getSubsription($id = 0){
		$this->db->select('*');
		$this->db->from($this->tblDealerSubs);
        $this->db->where('dealer_id', $id);
        $this->db->limit(1);
        $query = $this->db->get();
        $ret = $query->row();
	   	return ($query->num_rows() > 0)? $ret->subscription_id : false;
	}

	function getDealsbyDealer($id=0)
	{
	   $this->db->where('dealer_id', $id);
	   $query = $this->db->get($this->tblDeal);
	   return $query->result();
	}

	function getLastPostDealbyDealer($id=0)
	{
	   $this->db->select('created_at');
	   $this->db->where('dealer_id', $id);
	   $this->db->order_by('created_at',"desc")->limit(1);
	   $query = $this->db->get($this->tblDeal);
	   $ret = $query->row();
	   return isset($ret)? $ret->created_at: '';
	}

	function countSevenDaysDeal($id=0)
	{
	    $this->db->select('id');
		$this->db->where('created_at BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW()');
		$this->db->where('dealer_id', $id);
		$q = $this->db->get($this->tblDeal);
	   return $q->num_rows();
	}

	function getDealsByDate($id=0,$date)
	{
	   $this->db->where('dealer_id', $id);
	   $this->db->where('DATE(created_at)', $date);
	   $query = $this->db->get($this->tblDeal);
	   return $query->result();
	}

	function getDealerCreatedDate($id='')
	{
	   $this->db->where('id', $id);
	   $query = $this->db->get('dealers');
	   $ret = $query->row();
	   return isset($ret)? $ret->created_at: '';
	}

	function deleteDeal($id)
	{
	    $this->db->where('id', $id);
		$this->db->delete('products');
		$this->deleteDealDesc($id);
		$this->deleteDealInfo($id);
		$this->deleteDealImages($id);
		return;
	}

	function deleteDealDesc($id){
		$this->db->where('id', $id);
		$this->db->delete($this->tblDealDesc);
		return;
	}

	function deleteDealInfo($id){
		$this->db->where('id', $id);
		$this->db->delete($this->tblDealInfo);
		return;
	}

	function getDealImagebyCode($code='')
	{
	   $this->db->limit('1');
	   $this->db->where('code', $code);
	   $this->db->order_by('id', 'asc');
	   $query = $this->db->get($this->tblImageTemp);
	   $ret = $query->row();
	   return isset($ret)? $ret->filename : null;
	}

	function getDealerInfo($id='')
	{
	   $this->db->limit('1');
	   $this->db->where('id', $id);
	   $query = $this->db->get($this->tblDealer);
	   $ret = $query->row();
	   return isset($ret)? $query->result_array() : null;
	}

	function getCategoryInfo($code='')
	{
	   $this->db->limit('1');
	   $this->db->where('category_code', $code);
	   $query = $this->db->get($this->tblCategories);
	   $ret = $query->row();
	   return isset($ret)? $query->result_array() : '';
	}

	function getsubCategoryInfo($code='', $subcode='')
	{
	   $this->db->limit('1');
	   $this->db->where('category_code', $code);
	   $this->db->where('sub_category_code', $subcode);
	   $query = $this->db->get($this->tblSubCategories);
	   $ret = $query->row();
	   return isset($ret)? $query->result_array() : '';
	}

	function getBrandInfo($id='')
	{
	   $this->db->limit('1');
	   $this->db->where('id', $id);
	   $query = $this->db->get($this->tblBrands);
	   $ret = $query->row();
	   return isset($ret)? $query->result_array() : '';
	}

	function clearRedisCache($id='')
	{
	    $ch = curl_init("https://gunsalefinder.com/home/cache_homepage?result=false");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
	    return;
	}

	function getallUploaded($code)
	{
	   $this->db->where('code', $code);
	   $query = $this->db->get($this->tblImageTemp);
	   return $query->result();
	}


}
?>