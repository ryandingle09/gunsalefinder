<?php
Class Reportdeal_model extends CI_Model
{
	function getAllMyReportedProducts($array=['1','2'], $limit = 0, $offset=1000)
	{
		$this->db->limit($limit, $offset);

		if(count($array) !== 0)
		{
			$this->db->where_in('reported_products.product_id', $array);
		} else {
			$this->db->where('products.dealer_id', $this->session->userdata('id'));
		}

		$this->db->join('products', 'reported_products.product_id = products.id');
		$this->db->order_by('reported_products.date_reported', 'DESC');
		return $this->db->get('reported_products')->result();

	}	

	function countMyReportedProducts()
	{
		$id = $this->session->userdata('id');
	    $this->db->where('products.dealer_id', $id);
		$this->db->join('products', 'reported_products.product_id = products.id');
		$q = $this->db->get('reported_products');
	    return $q->num_rows();
	}

	function getDealerProducts()
	{
		$this->db->where('dealer_id', $this->session->userdata('id'));
		return $this->db->get('products')->result();
	}
}
?>