<?php
Class Compare_model extends CI_Model
{
	public $table = "products";

	function getProductImagebyUPC($upc='')
	{
	   $this->db->limit('1');
	   $this->db->where('product_upc', $upc);
	   $this->db->order_by('distributor_id', 'asc');
	   $query = $this->db->get('product_images');
	   $ret = $query->row();
	   return isset($ret)? $ret->image_url : '';
	}

	function compareProductbyUPC($upc = 0)
	{

	   // Query #1
		$this->db->select('id,title, price, shipping_fee, dealer_id, qty, item_url, product_image, dealer_info');
		$this->db->from($this->table);
		if (strlen($upc) == 11){
	   		$this->db->like('upc', $upc);
	    }else{
	   		$this->db->where('upc', $upc);
	    }
		$this->db->where('qty >', 0);
		$this->db->where('status', 'A');
	    $this->db->order_by('price', 'ASC');
		$query1 = $this->db->get()->result();
	
		// Query #2
		$this->db->select('id,title, price, shipping_fee, dealer_id, qty, item_url, product_image, dealer_info');
		$this->db->from($this->table);
		if (strlen($upc) == 11){
	   		$this->db->like('upc', $upc);
	    }else{
	   		$this->db->where('upc', $upc);
	    }
		$this->db->where('qty', 0);
		$this->db->where('status', 'A');
		$this->db->order_by('price', 'ASC');
		$query2 = $this->db->get()->result();

		$query = array_merge($query1, $query2);
		return $query;
	}
}
?>