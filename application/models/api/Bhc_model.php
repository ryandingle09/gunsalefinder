<?php
Class Bhc_model extends CI_Model
{
	public $table = 'products';
	public $table2 = 'dealers';
    public $table3 = 'brands';
    public $tblDesc = 'product_descriptions';
    public $tblCategory = 'product_categories';
    public $tblImg = 'product_images';

    function getAllBhcproducts($id = 0)
    {
       $this->db->where('id', BHC);
       $query = $this->db->get($this->table);
       return $query->result();
    }

    function getCsvFilename($id='5')
    {
       $this->db->where('dealer_id', $id);
       $this->db->limit(1);
       $query = $this->db->get('dealer_files');
       $ret = $query->row();
       return isset($ret)? $ret->filename: '';
    }

    #All product will be inactive
	public function inactiveProducts( $where = false ){

		$data = [
            'status' => 'I'
        ];

        $this->db->where( 'dealer_id', BHC );
        $this->db->update($this->table, $data); 
    }

    public function ifProductCategoryExists( $upc) {

        $this->db->select('product_upc,count(id) as counts');
        $this->db->where('product_upc', $upc );
        $this->db->where('distributor_id', BHC );
        $this->db->from($this->tblCategory);
        $result = $this->db->get();
		return $result->result_array(); 
    }

    public function productInsertBatch( $data ) {
        $this->db->insert_batch($this->table, $data);
        return;
    }

    public function productUpdaBatch( $data ) {
        echo $this->db->last_query();
        $this->db->update_batch($this->table, $data, 'id'); 
        return;
    }

    public function productCategoryInsert( $data ) {
        $this->db->insert_batch($this->tblCategory, $data);
        return;
    }

    public function productCategoryUpdate( $data ) {
        $this->db->update_batch($this->tblCategory, $data, 'product_upc'); 
        return;
    }

    function updatefilename($id, $data)
    {
        $this->db->where('dealer_id', $id);
        $this->db->update('dealer_files', $data); 
        return($this->db->affected_rows() > 0)? true:false;
    }

    function getCategoryCode($category)
    {
        switch ($category) {
            case ("H600"):
                $list['category'] = 'FIREARM';
                $list['subcategory'] = 'RIFLES';
                return $list;
                break;
            case ("H601"):
                $list['category'] = 'FIREARM';
                $list['subcategory'] = 'SHOTGUN';
                return $list;
                break;
            case ("H602"):
                $list['category'] = 'FIREARM';
                $list['subcategory'] = 'PISTOLS';
                return $list;
                break;
            case ("H603"):
                $list['category'] = 'FIREARM';
                $list['subcategory'] = 'REVOLVERS';
                return $list;
                break;
            case ("H605"):
                $list['category'] = 'GUNPRTS';
                $list['subcategory'] = 'ACTION_KITS';
                return $list;
                break;
            case ("H606"):
                $list['category'] = 'NFAITEM';
                $list['subcategory'] = 'SUPPRESSORS';
                return $list;
                break;
            case ("H607"):
                $list['category'] = 'NFAITEM';
                $list['subcategory'] = 'SHORT_BBL_RIFLES_SOT';
                return $list;
                break;
            case ("H608"):
                $list['category'] = 'NFAITEM';
                $list['subcategory'] = 'SHORT_BBL_SHOTGUNS_SOT';
                return $list;
                break;
            case ("H610"):
                $list['category'] = 'AMMO';
                $list['subcategory'] = 'CENTERFIRE_ROUNDS';
                return $list;
                break;
            case ("H619"):
                $list['category'] = 'MAGAZIN';
                $list['subcategory'] = 'MAGAZINES';
                return $list;
                break;
            case ("H631"):
                $list['category'] = 'GUNPRTS';
                $list['subcategory'] = 'HANDGUN_BARRELS';
                return $list;
                break;
            case ("H645"):
                $list['category'] = 'OPTICSS';
                $list['subcategory'] = 'RIFLE_SCOPES';
                return $list;
                break;
            case ("H646"):
                $list['category'] = 'OPTICSS';
                $list['subcategory'] = 'SCOPE_ACCESSORIES';
                return $list;
                break;
            case ("H647"):
                $list['category'] = 'AMMO';
                $list['subcategory'] = 'RIFLE_SCOPES';
                return $list;
                break;
            case ("H648"):
                $list['category'] = 'FIREARM';
                $list['subcategory'] = 'AIR_GUN_RIFLES';
                return $list;
                break;
            case ("H649"):
                $list['category'] = 'ACSRIES';
                $list['subcategory'] = 'BLACK_POWDER_KITS';
                return $list;
                break;
            case ("H650"):
                $list['category'] = 'GEARS';
                $list['subcategory'] = 'CLEANING_SOLVENTS_LUBRICANTS';
                return $list;
                break;
            case ("H655"):
                $list['category'] = 'RELOAD';
                $list['subcategory'] = 'RELOADING_BULLETS';
                return $list;
                break;
            case ("H656"):
                $list['category'] = 'RELOAD';
                $list['subcategory'] = 'RELOADING_ACCESSORIES';
                return $list;
                break;
            case ("H659"):
                $list['category'] = 'GEARS';
                $list['subcategory'] = 'GUN_CASES';
                return $list;
                break;
            case ("H660"):
                $list['category'] = 'ACSRIES';
                $list['subcategory'] = 'AIR_GUN_ACCESSORIES';
                return $list;
                break;
            case ("H661"):
                $list['category'] = 'KNIVES';
                $list['subcategory'] = 'KNIVES_AND_TOOLS';
                return $list;
                break;
            case ("H665"):
                $list['category'] = 'GEARS';
                $list['subcategory'] = 'GAME_CALLS';
                return $list;
                break;
            case ("H680"):
                $list['category'] = 'ARCHERY';
                $list['subcategory'] = 'ARCHERY_ACCESSORIES';
                return $list;
                break;
            case ("H699"):
                $list['category'] = 'GEARS';
                $list['subcategory'] = 'SAFES_VAULTS';
                return $list;
                break;
            case ("H700"):
                $list['category'] = 'GEARS';
                $list['subcategory'] = 'APPAREL';
                return $list;
                break;
            case ("H701"):
                $list['category'] = 'GEARS';
                $list['subcategory'] = 'HUNTING_ELECTRONICS';
                return $list;
                break;
            case ("H702"):
                $list['category'] = 'GEARS';
                $list['subcategory'] = 'DECOYS';
                return $list;
                break;
            case ("H999"):
                $list['category'] = 'ACSRIES';
                $list['subcategory'] = 'DISPLAYS';
                return $list;
                break;
            case ("MISC"):
                $list['category'] = 'ACSRIES';
                $list['subcategory'] = '';
                return $list;
                break;
            default:
               return [];
        }
    }

    function getShippingfee($categorycode)
    {
        switch ($categorycode) {
            case ("ACSRIES" || "ARCHERY" || "FISHING" || "GUNPRTS" || "GEARS" || "KNIVESS" || "MAGAZINES" || "OPTICSS"):
                return '8.95';
                break;
            case ("AMMO" || "RELOAD"):
                return '10.95';
                break;
            case ("FIREARM" || "NFAITEM"):
                return '14.95';
                break;
            default:
               return '0';
        }
    }

    public function getIdUpc( $upc ) {
        $this->db->select('id,upc');
        $this->db->where('upc', $upc );
        $this->db->where('dealer_id', BHC );
        $this->db->from($this->table);
        $result = $this->db->get();
		return $result->row_array(); 
    }

    function productDescExist( $upc = '', $distributor = '')
    {
        $this->db->where('product_upc', $upc);
        $this->db->where('distributor_id', $distributor);
        $this->db->select('id');    
        $this->db->from($this->tblDesc);
        $query = $this->db->get();
        return ($query->num_rows() == 1)? true: false;
    }

    public function insertproductDesc( $data ) {

        $this->db->insert($this->tblDesc, $data);
         return;
        
    }

    function updateProductDesc($upc, $distributor, $data)
    {
        $this->db->where('product_upc', $upc);
        $this->db->where('distributor_id', $distributor);
        $this->db->update($this->tblDesc, $data); 
        return;
    }

    function productImgExist( $upc = '', $distributor = '')
    {
        $this->db->where('product_upc', $upc);
        $this->db->where('distributor_id', $distributor);
        $this->db->select('id');    
        $this->db->from($this->tblImg);
        $query = $this->db->get();
        return ($query->num_rows() == 1)? true: false;
    }

    public function insertproductImg( $data ) {

        $this->db->insert($this->tblImg, $data);
         return;
        
    }

    function updateProductImg($upc, $distributor, $data)
    {
        $this->db->where('product_upc', $upc);
        $this->db->where('distributor_id', $distributor);
        $this->db->update($this->tblImg, $data); 
        return;
    }

    function getBrandID($brand)
    {
        switch ($brand) {
            case ("2A"):
                return 2;
                break;
            case ("AAC"):
                return 18;
                break;
            case ("AAE"):
                return 6;
                break;
            case ("ADAMS"):
                return 12;
                break;
            case ("ADTAC"):
                return 16;
                break;
            case ("AGU"):
                return 23;
                break;
            case ("AI"):
                return 46;
                break;
            case ("ALLEN"):
                return 31;
                break;
            case ("AM"):
                return 50;
                break;
            case ("AMEND2"):
                return 33;
                break;
            case ("AMK"):
                return 45;
                break;
            case ("AO"):
                return 69;
                break;
            case ("APF"):
                return 29;
                break;
            case ("APT"):
                return 13;
                break;
            case ("ARMA"):
                return 59;
                break;
            case ("ARS"):
                return 62;
                break;
            case ("AT"):
                return 19;
                break;
            case ("ATI"):
                return 42;
                break;
            case ("ATN"):
                return 66;
                break;
            case ("AUA"):
                return 60;
                break;
            case ("AZOOM"):
                return 73;
                break;
            case ("BAR"):
                return 77;
                break;
            case ("BARN"):
                return 78;
                break;
            case ("BARR"):
                return 79;
                break;
            case ("BARS"):
                return 82;
                break;
            case ("BB"):
                return 124;
                break;
            case ("BC"):
                return 113;
                break;
            case ("BD"):
                return 170;
                break;
            case ("BEAR"):
                return 92;
                break;
            case ("BEN"):
                return 102;
                break;
            case ("BER"):
                return 104;
                break;
            case ("BF"):
                return 175;
                break;
            case ("BFG"):
                return 125;
                break;
            case ("BGA"):
                return 105;
                break;
            case ("BH"):
                return 118;
                break;
            case ("BHF"):
                return 119;
                break;
            case ("BIA"):
                return 111;
                break;
            case ("BM"):
                return 108;
                break;
            case ("BOB"):
                return 127;
                break;
            case ("BOG"):
                return 128;
                break;
            case ("BOND"):
                return 134;
                break;
            case ("BPT"):
                return 121;
                break;
            case ("BR"):
                return 115;
                break;
            case ("BREAK"):
                return 145;
                break;
            case ("BRO"):
                return 152;
                break;
            case ("BSA"):
                return 155;
                break;
            case ("BSON"):
                return 90;
                break;
            case ("BSQ"):
                return 156;
                break;
            case ("BTI"):
                return 157;
                break;
            case ("BUG"):
                return 167;
                break;
            case ("BUR"):
                return 173;
                break;
            case ("BUS"):
                return 176;
                break;
            case ("BUT"):
                return 178;
                break;
            case ("BXB"):
                return 93;
                break;
            case ("C15"):
                return 239;
                break;
            case ("CA"):
                return 202;
                break;
            case ("CAA"):
                return 229;
                break;
            case ("CARL"):
                return 190;
                break;
            case ("CCI"):
                return 193;
                break;
            case ("CDLY"):
                return 201;
                break;
            case ("CE"):
                return 189;
                break;
            case ("CENT"):
                return 189;
                break;
            case ("CF"):
                return 203;
                break;
            case ("CHAMP"):
                return 200;
                break;
            case ("CHIP"):
                return 242;
                break;
            case ("CIM"):
                return 206;
                break;
            case ("CLT"):
                return 224;
                break;
            case ("CMC"):
                return 212;
                break;
            case ("CMMG"):
                return 213;
                break;
            case ("COA"):
                return 234;
                break;
            case ("COV"):
                return 240;
                break;
            case ("CP"):
                return 194;
                break;
            case ("CR"):
                return 224;
                break;
            case ("CRKT"):
                return 244;
                break;
            case ("CROS"):
                return 245;
                break;
            case ("CTC"):
                return 243;
                break;
            case ("CVA"):
                return 251;
                break;
            case ("CW"):
                return 183;
                break;
            case ("CYC"):
                return 252;
                break;
            case ("CZ"):
                return 254;
                break;
            case ("DA"):
                return 254;
                break;
            case ("DAIR"):
                return 261;
                break;
            case ("DAISY"):
                return 256;
                break;
            case ("DAT"):
                return 256;
                break;
            case ("DBF"):
                return 271;
                break;
            case ("DDUP"):
                return 469;
                break;
            case ("DES"):
                return 269;
                break;
            case ("DH"):
                return 273;
                break;
            case ("DP"):
                return 890;
                break;
            case ("DRA"):
                return 286;
                break;
            case ("DSA"):
                return 290;
                break;
            case ("DSC"):
                return 282;
                break;
            case ("DTD"):
                return 284;
                break;
            case ("DTI"):
                return 268;
                break;
            case ("EAA"):
                return 296;
                break;
            case ("EI"):
                return 35;
                break;
            case ("EOT"):
                return 309;
                break;
            case ("ERGO"):
                return 310;
                break;
            case ("EST"):
                return 313;
                break;
            case ("ETS"):
                return 307;
                break;
            case ("FA"):
                return 355;
                break;
            case ("FED"):
                return 326;
                break;
            case ("FF"):
                return 333;
                break;
            case ("FIME"):
                return 329;
                break;
            case ("FIO"):
                return 330;
                break;
            case ("FLITZ"):
                return 342;
                break;
            case ("FMK"):
                return 346;
                break;
            case ("FNH"):
                return 348;
                break;
            case ("FO"):
                return 356;
                break;
            case ("FOBUS"):
                return 349;
                break;
            case ("FRANK"):
                return 354;
                break;
            case ("G2R"):
                return 363;
                break;
            case ("G96"):
                return 364;
                break;
            case ("GAL"):
                return 366;
                break;
            case ("GAMO"):
                return 368;
                break;
            case ("GATCO"):
                return 371;
                break;
            case ("GEM"):
                return 376;
                break;
            case ("GERBER"):
                return 379;
                break;
            case ("GLOCK"):
                return 385;
                break;
            case ("GM"):
                return 411;
                break;
            case ("GRAB"):
                return 392;
                break;
            case ("GRIF"):
                return 399;
                break;
            case ("GT"):
                return 404;
                break;
            case ("GUNS"):
                return 414;
                break;
            case ("GV"):
                return 416;
                break;
            case ("HAR"):
                return 420;
                break;
            case ("HEX"):
                return 439;
                break;
            case ("HK"):
                return 448;
                break;
            case ("HKS"):
                return 450;
                break;
            case ("HL"):
                return 468;
                break;
            case ("HM"):
                return 470;
                break;
            case ("HME"):
                return 451;
                break;
            case ("HOG"):
                return 453;
                break;
            case ("HOLDEN"):
                return 1066;
                break;
            case ("HOLO"):
                return 455;
                break;
            case ("HOOY"):
                return 460;
                break;
            case ("HOP"):
                return 461;
                break;
            case ("HORN"):
                return 462;
                break;
            case ("HORT"):
                return 464;
                break;
            case ("HRAC"):
                return 432;
                break;
            case ("HRACW"):
                return 433;
                break;
            case ("HSM"):
                return 469;
                break;
            case ("HSP"):
                return 476;
                break;
            case ("HTG"):
                return 436;
                break;
            case ("HUN"):
                return 473;
                break;
            case ("HUSK"):
                return 477;
                break;
            case ("IAC"):
                return 489;
                break;
            case ("IFC"):
                return 488;
                break;
            case ("IFG"):
                return 494;
                break;
            case ("IMG"):
                return 479;
                break;
            case ("ITH"):
                return 495;
                break;
            case ("IWI"):
                return 497;
                break;
            case ("JP"):
                return 505;
                break;
            case ("JRC"):
                return 507;
                break;
            case ("KA"):
                return 511;
                break;
            case ("KB"):
                return 527;
                break;
            case ("KEL"):
                return 515;
                break;
            case ("KER"):
                return 518;
                break;
            case ("KING"):
                return 524;
                break;
            case ("KON"):
                return 524;
                break;
            case ("KRISS"):
                return 532;
                break;
            case ("KSA"):
                return 542;
                break;
            case ("LANTAC"):
                return 541;
                break;
            case ("LAS"):
                return 1011;
                break;
            case ("LD"):
                return 560;
                break;
            case ("LEU"):
                return 552;
                break;
            case ("LIB"):
                return 556;
                break;
            case ("LL"):
                return 544;
                break;
            case ("LM"):
                return 545;
                break;
            case ("LMT"):
                return 554;
                break;
            case ("LS"):
                return 540;
                break;
            case ("LSI"):
                return 550;
                break;
            case ("LULA"):
                return 573;
                break;
            case ("LUTH"):
                return 550;
                break;
            case ("LWRC"):
                return 565;
                break;
            case ("LYM"):
                return 567;
                break;
            case ("MACE"):
                return 571;
                break;
            case ("MAKO"):
                return 583;
                break;
            case ("MAR"):
                return 589;
                break;
            case ("MAV"):
                return 596;
                break;
            case ("MECGAR"):
                return 602;
                break;
            case ("MEN"):
                return 604;
                break;
            case ("MFT"):
                return 614;
                break;
            case ("MI"):
                return 611;
                break;
            case ("MID"):
                return 609;
                break;
            case ("MILLET"):
                return 612;
                break;
            case ("MK"):
                return 618;
                break;
            case ("MKS"):
                return 445;
                break;
            case ("MM"):
                return 568;
                break;
            case ("MOSS"):
                return 625;
                break;
            case ("MOUL"):
                return 627;
                break;
            case ("MPA"):
                return 630;
                break;
            case ("MR"):
                return 575;
                break;
            case ("MTA"):
                return 580;
                break;
            case ("MTM"):
                return 633;
                break;
            case ("MXM"):
                return 598;
                break;
            case ("NAA"):
                return 639;
                break;
            case ("NAA"):
                return 641;
                break;
            case ("NEF"):
                return 417;
                break;
            case ("NEMO"):
                return 643;
                break;
            case ("NIK"):
                return 651;
                break;
            case ("NIKKO"):
                return 650;
                break;
            case ("NORD"):
                return 654;
                break;
            case ("NOREEN"):
                return 655;
                break;
            case ("NOS"):
                return 661;
                break;
            case ("NOVX"):
                return 663;
                break;
            case ("NS"):
                return 652;
                break;
            case ("OAI"):
                return 671;
                break;
            case ("OC"):
                return 681;
                break;
            case ("ODE"):
                return 681;
                break;
            case ("OEM"):
                return 1070;
                break;
            case ("OSS"):
                return 674;
                break;
            case ("OTIS"):
                return 679;
                break;
            case ("OUTERS"):
                return 684;
                break;
            case ("PAC"):
                return 686;
                break;
            case ("PEARCE"):
                return 693;
                break;
            case ("PEL"):
                return 697;
                break;
            case ("PELICAN"):
                return 696;
                break;
            case ("PLANO"):
                return 708;
                break;
            case ("PM"):
                return 726;
                break;
            case ("PMC"):
                return 711;
                break;
            case ("POF"):
                return 691;
                break;
            case ("PR"):
                return 699;
                break;
            case ("PRED"):
                return 722;
                break;
            case ("PRIMOS"):
                return 724;
                break;
            case ("PROOF"):
                return 728;
                break;
            case ("PT"):
                return 703;
                break;
            case ("PTR"):
                return 732;
                break;
            case ("PULSAR"):
                return 734;
                break;
            case ("PWS"):
                return 735;
                break;
            case ("PYRA"):
                return 736;
                break;
            case ("QUA"):
                return 739;
                break;
            case ("RAA"):
                return 1067;
                break;
            case ("RAD"):
                return 744;
                break;
            case ("RAM"):
                return 1012;
                break;
            case ("RAM"):
                return 1012;
                break;
            case ("RAVIN"):
                return 754;
                break;
            case ("RCBS"):
                return 756;
                break;
            case ("RDI"):
                return 785;
                break;
            case ("RED"):
                return 785;
                break;
            case ("REM"):
                return 773;
                break;
            case ("RETAY"):
                return 1068;
                break;
            case ("RIA"):
                return 791;
                break;
            case ("RISE"):
                return 788;
                break;
            case ("ROB"):
                return 1069;
                break;
            case ("ROG"):
                return 794;
                break;
            case ("ROS"):
                return 795;
                break;
            case ("RRA"):
                return 792;
                break;
            case ("RS"):
                return 800;
                break;
            case ("RUG"):
                return 797;
                break;
            case ("RWB"):
                return 767;
                break;
            case ("RWC"):
                return 801;
                break;
            case ("S9"):
                return 849;
                break;
            case ("SAB"):
                return 807;
                break;
            case ("SAF"):
                return 810;
                break;
            case ("SAGEN"):
                return 811;
                break;
            case ("SAR"):
                return 815;
                break;
            case ("SAU"):
                return 816;
                break;
            case ("SAV"):
                return 817;
                break;
            case ("SB"):
                return 819;
                break;
            case ("SCCY"):
                return 820;
                break;
            case ("SCEN"):
                return 822;
                break;
            case ("SDS"):
                return 828;
                break;
            case ("SF"):
                return 908;
                break;
            case ("SIE"):
                return 843;
                break;
            case ("SIG"):
                return 844;
                break;
            case ("SIL"):
                return 850;
                break;
            case ("SIM"):
                return 854;
                break;
            case ("SKULL"):
                return 857;
                break;
            case ("SL"):
                return 877;
                break;
            case ("SM"):
                return 847;
                break;
            case ("SME"):
                return 862;
                break;
            case ("SML"):
                return 890;
                break;
            case ("SOG"):
                return 868;
                break;
            case ("SONIC"):
                return 869;
                break;
            case ("SPEER"):
                return 873;
                break;
            case ("SPR"):
                return 879;
                break;
            case ("SPY"):
                return 884;
                break;
            case ("SRM"):
                return 885;
                break;
            case ("SSA"):
                return 853;
                break;
            case ("ST"):
                return 848;
                break;
            case ("STAG"):
                return 888;
                break;
            case ("STC"):
                return 892;
                break;
            case ("STEYR"):
                return 896;
                break;
            case ("STICKY"):
                return 897;
                break;
            case ("STOP"):
                return 893;
                break;
            case ("STORM"):
                return 696;
                break;
            case ("STREAM"):
                return 900;
                break;
            case ("STY"):
                return 903;
                break;
            case ("SUN"):
                return 905;
                break;
            case ("SUR"):
                return 907;
                break;
            case ("SW"):
                return 863;
                break;
            case ("SWAG"):
                return 911;
                break;
            case ("SWIFT"):
                return 913;
                break;
            case ("TA"):
                return 980;
                break;
            case ("TAC"):
                return 921;
                break;
            case ("TACSOL"):
                return 922;
                break;
            case ("TALO"):
                return 224;
                break;
            case ("TANN"):
                return 928;
                break;
            case ("TAPCO"):
                return 930;
                break;
            case ("TAS"):
                return 932;
                break;
            case ("TASER"):
                return 933;
                break;
            case ("TAU"):
                return 934;
                break;
            case ("TBF"):
                return 964;
                break;
            case ("TC"):
                return 917;
                break;
            case ("TCLIP"):
                return 937;
                break;
            case ("TF"):
                return 936;
                break;
            case ("TG"):
                return 978;
                break;
            case ("TGM"):
                return 938;
                break;
            case ("TIF"):
                return 981;
                break;
            case ("TIK"):
                return 952;
                break;
            case ("TINKS"):
                return 955;
                break;
            case ("TIP"):
                return 956;
                break;
            case ("TP"):
                return 941;
                break;
            case ("TPD"):
                return 959;
                break;
            case ("TRAD"):
                return 963;
                break;
            case ("TRI"):
                return 967;
                break;
            case ("TRIUS"):
                return 972;
                break;
            case ("TSA"):
                return 970;
                break;
            case ("UD"):
                return 987;
                break;
            case ("UM"):
                return 993;
                break;
            case ("UMAREX"):
                return 990;
                break;
            case ("UMAX"):
                return 988;
                break;
            case ("UTAS"):
                return 1001;
                break;
            case ("UTG"):
                return 548;
                break;
            case ("VC"):
                return 1007;
                break;
            case ("WAL"):
                return 1019;
                break;
            case ("WALK"):
                return 1018;
                break;
            case ("WAR"):
                return 1020;
                break;
            case ("WBY"):
                return 1024;
                break;
            case ("WEAVER"):
                return 1026;
                break;
            case ("WH"):
                return 1030;
                break;
            case ("WILL"):
                return 1035;
                break;
            case ("WISE"):
                return 1049;
                break;
            case ("WMD"):
                return 1050;
                break;
            case ("WR"):
                return 1033;
                break;
            case ("WR"):
                return 1045;
                break;
            case ("XP"):
                return 1055;
                break;
            case ("YHM"):
                return 1058;
                break;
            case ("ZEV"):
                return 1064;
                break;
            case ("ZEN"):
                return 1063;
                break;
            case ("ZS"):
                return 1062;
                break;
            default:
               return null;
        }
    }
}