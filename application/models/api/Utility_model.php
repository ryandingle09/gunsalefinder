<?php
Class Utility_model extends CI_Model
{
	public $tblBrands = "brands";
	public $tblCategories = "categories";
	public $tblSubCategories = "sub_categories";
	public $tblDealers = "dealers";
	public $tblDealImg = "deal_images";
	public $tblProductImg = "product_images";
	public $tblProducts = "products";

	function getAllImages($id = 0)
	{
	   $this->db->select('id, upc'); 
       $this->db->from($this->tblProductImg);
	   $query = $this->db->get();
	   return $query->result();
	}

	function updateProduct($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update($this->tblProducts, $data); 
	}

	function getProducts()
	{
	   $this->db->select('id, upc, api_created, product_image'); 
       $this->db->from($this->tblProducts);   
	   $this->db->where('product_image IS NULL', null, false); 
	   $this->db->order_by('id', 'asc');
	   $query = $this->db->get();
	   return $query->result();
	}

	function getProductImagebyUPC($upc='')
	{
	   $this->db->select('image_url'); 
       $this->db->from($this->tblProductImg); 
	   $this->db->where('product_upc', $upc);
	   $this->db->order_by('distributor_id', 'asc');
	   $this->db->limit('1');
	   $query = $this->db->get();
	   $ret = $query->row();
	   return isset($ret)? $ret->image_url : 'https://gunsalefinder.com/assets/pictures/default.jpg';
	}

	function getDealImagebyID($id='')
	{
	   $this->db->select('image_url'); 
       $this->db->from($this->tblDealImg); 
	   $this->db->where('deal_id', $id);
	   $this->db->order_by('id', 'asc');
	   $this->db->limit('1');
	   $query = $this->db->get();
	   $ret = $query->row();
	   return isset($ret)? $ret->image_url : 'https://gunsalefinder.com/assets/pictures/default.jpg';
	}

	#CATEGORY INFO
	function getCategoryProducts()
	{
	   $this->db->select('id, category_code'); 
       $this->db->from($this->tblProducts);
       $this->db->where('category_code IS NOT NULL', null, false); 
       $this->db->where('category_info IS NULL', null, false); 
	   $query = $this->db->get();
	   return $query->result();
	}

	function getProductCategory($code='')
	{
	   $this->db->limit('1');
	   $this->db->where('category_code', $code);
	   $query = $this->db->get($this->tblCategories);
	   $ret = $query->row();
	   return isset($ret)? $query->result_array() : '';
	}

	#SUBCATEGORY INFO
	function getSubCategoryProducts()
	{
	   $this->db->select('id, category_code, sub_category_code'); 
       $this->db->from($this->tblProducts);
       $this->db->where('category_code IS NOT NULL', null, false); 
       $this->db->where('sub_category_code IS NOT NULL', null, false);
       $this->db->where('sub_category_info IS NULL', null, false); 
	   $query = $this->db->get();
	   return $query->result();
	}

	function getProductSubCategory($code='', $subcode='')
	{
	   $this->db->limit('1');
	   $this->db->where('category_code', $code);
	   $this->db->where('sub_category_code', $subcode);
	   $query = $this->db->get($this->tblSubCategories);
	   $ret = $query->row();
	   return isset($ret)? $query->result_array() : '';
	}

	#BRAND INFO
	function getBrandProducts()
	{
	   $this->db->select('id, brand_id'); 
       $this->db->from($this->tblProducts);
       $this->db->where('brand_id IS NOT NULL', null, false);
       $this->db->where('brand_name IS NULL', null, false);  
	   $query = $this->db->get();
	   return $query->result();
	}

	function getProductBrand($id='')
	{
	   $this->db->limit('1');
	   $this->db->where('id', $id);
	   $query = $this->db->get($this->tblBrands);
	   $ret = $query->row();
	   return isset($ret)? $ret->name: '';
	}

	#DEALER INFO
	function getDealerProducts()
	{
	   $this->db->select('id, dealer_id'); 
       $this->db->from($this->tblProducts);
       $this->db->where('dealer_id IS NOT NULL', null, false); 
       $this->db->where('dealer_info IS NULL', null, false);  
	   $query = $this->db->get();
	   return $query->result();
	}

	function getProductDealer($id='')
	{
	   $this->db->limit('1');
	   $this->db->where('id', $id);
	   $query = $this->db->get($this->tblDealers);
	   $ret = $query->row();
	   return isset($ret)? $query->result_array() : '';
	}

	#Product LIKE
	function getProductLike()
	{
	   $this->db->select('id, product_id'); 
       $this->db->from('product_likes');
       $this->db->where('type', 'like');
       $this->db->group_by('product_id');
	   $query = $this->db->get();
	   return $query->result();
	}

	function countProductLike($id)
	{
	 	
	 	$this->db->where('product_id', $id);
	 	$this->db->where('type', 'like');
	    $q = $this->db->get('product_likes');
	    return $q->num_rows();
	}

	#Product DISLIKE
	function getProductdisLike()
	{
	   $this->db->select('id, product_id'); 
       $this->db->from('product_likes');
       $this->db->where('type', 'dislike');
       $this->db->group_by('product_id');
	   $query = $this->db->get();
	   return $query->result();
	}

	function countProductdisLike($id)
	{
	 	
	 	$this->db->where('product_id', $id);
	 	$this->db->where('type', 'dislike');
	    $q = $this->db->get('product_likes');
	    return $q->num_rows();
	}

	#Product Comment
	function getProductComment()
	{
	   $this->db->select('id, product_id'); 
       $this->db->from('comments');
       $this->db->group_by('product_id');
	   $query = $this->db->get();
	   return $query->result();
	}

	function countProductComment($id)
	{
	 	
	 	$this->db->where('product_id', $id);
	    $q = $this->db->get('comments');
	    return $q->num_rows();
	}

	#Product Subcribe
	function getProductSubcribe()
	{
	   $this->db->select('id, product_id'); 
       $this->db->from('comment_subscriptions');
       $this->db->group_by('product_id');
	   $query = $this->db->get();
	   return $query->result();
	}

	function countProductSubcribe($id)
	{
	 	
	 	$this->db->where('product_id', $id);
	    $q = $this->db->get('comment_subscriptions');
	    return $q->num_rows();
	}

	#Product DROP Subcribe
	function getProductdropSubcribe()
	{
	   $this->db->select('id, product_id'); 
       $this->db->from('pricedrop_subscriptions');
       $this->db->group_by('product_id');
	   $query = $this->db->get();
	   return $query->result();
	}

	function countdropSubcribe($id)
	{
	 	
	 	$this->db->where('product_id', $id);
	    $q = $this->db->get('pricedrop_subscriptions');
	    return $q->num_rows();
	}

	function updatedropSubcribe($id, $count){
		$sql= "UPDATE ".$this->tblProducts." SET subscriber_count = subscriber_count + '$count' WHERE id = '$id'";
		$query = $this->db->query($sql);
		return ($this->db->affected_rows() > 0)? TRUE : FALSE ;
	}

	#Rate Products
	function getProductRated()
	{
	   $this->db->select('id, product_id'); 
       $this->db->from('rate_product');
       $this->db->group_by('product_id');
	   $query = $this->db->get();
	   return $query->result();
	}

	function countProductRate($id)
	{
	 	
	 	$this->db->where('product_id', $id);
	 	$this->db->select('AVG(rate) average');
        $result = $this->db->get('rate_product')->row();
	    return $result->average;
	}
}
?>