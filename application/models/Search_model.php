<?php
Class Search_model extends CI_Model
{
    public $products = 'products';
    public $dealers = 'dealers';
    public $brands = 'brands';

	public $table = 'products';
	public $table2 = 'dealers';
	public $table3 = 'brands';

    public function count_filter_products( $where_or, $sort, $category_code, $sub_category_code, $price, $dealer, $brand )
    {
        $this->db->select('*');
        $this->db->from($this->products. " p");
        $this->db->where('p.status', "A");

        #By Category Code
        if($category_code) $this->db->where("p.category_code",$category_code);

        #By Sub Category Code
        if($sub_category_code) $this->db->where("p.sub_category_code",$sub_category_code);

        #By Brand
        if($brand) $this->db->where("p.brand_id",$brand);

        #By Dealer
        if($dealer) $this->db->where("p.dealer_id",$dealer);

        #By Price
        $this->db->where($price);
        
        # OR WHERE
        if($this->input->get('search') != '')
		{
            $search = $this->input->get('search');
            
            //$this->db->where("LOCATE('".$search."', p.brand_info) > 0");
            //$this->db->where("find_in_set('".$search."', p.brand_info) > 0");

            // $where_or['p.title']                = $search;
			// $where_or['p.brand_info']           = $search;
			// $where_or['p.category_code']        = $search;
            // $where_or['p.sub_category_code']    = $search;

			//With or Without zero UPC1
            $upc                            = $search;
            if (substr($upc,0,1)=="0") $upc = substr($upc,1);
            //$where_or['p.upc']              = $upc;
            
            $this->db->like('p.title', $search);
            $this->db->or_like('p.brand_name', $search);
            $this->db->or_like('p.category_code', $search);
            $this->db->or_like('p.sub_category_code', $search);
            $this->db->or_like('p.upc', $upc);

        }
        
        if($sort['by'] == 'p.created_at')
        {
            $this->db->where('p.qty >', '0');
        }

        //$this->db->or_like($where_or);
        return $this->db->count_all_results();
    }

    public function filter_products( $where_or, $sort, $category_code, $sub_category_code, $price, $dealer, $brand, $limit = 12, $offset = 0 )
    {
        $this->db->select('p.*');
        $this->db->from($this->products. " p");
        $this->db->where('p.status', "A");

        #By Category Code
        if($category_code) $this->db->where("p.category_code", $category_code);

        #By Sub Category Code
        if($sub_category_code) $this->db->where("p.sub_category_code", $sub_category_code);

        #By Brand
        if($brand) $this->db->where("p.brand_id", $brand);

        #By Dealer
        if($dealer) $this->db->where("p.dealer_id", $dealer);

        #By Price
        $this->db->where($price);

        # OR WHERE
        if($this->input->get('search') != '')
		{
            $search = $this->input->get('search');

            //$this->db->where("LOCATE('".$search."', p.brand_info) > 0");
            //$this->db->where("find_in_set('".$search."', p.brand_info) > 0");


            // $where_or['p.title']                = $search;
			// $where_or['p.brand_info']           = $search;
			// $where_or['p.category_code']        = $search;
            // $where_or['p.sub_category_code']    = $search;

			//With or Without zero UPC1
            $upc                            = $search;
            if (substr($upc,0,1)=="0") $upc = substr($upc,1);
            // $where_or['p.upc']              = $upc;
            
            $this->db->like('p.title', $search);
            $this->db->or_like('p.brand_name', $search);
            $this->db->or_like('p.category_code', $search);
            $this->db->or_like('p.sub_category_code', $search);
            $this->db->or_like('p.upc', $upc);

        }
        
        //$this->db->or_like($where_or);
        if($sort['by'] == 'p.created_at')
        {
            $this->db->where('p.qty >', '0');
        }

        $this->db->limit($limit, $offset);
        $this->db->order_by($sort['by'], $sort['sort']);

        $result = $this->db->get();
        return $result->result_array();

    }

	#GET ITEM NAME BY TITLE
	public function productListFilter($where){

		$this->db->select("p.id, p.title, p.sku, p.upc");
		$this->db->from("{$this->table} as p");
		$this->db->where('upc', $where);
		$upc = $this->db->get();

		if ($upc->num_rows() > 0) 
		{
			 $row = $upc->custom_result_object('CustomProductDetails');
			 return $row;
		}

#		$this->db->select("p.id, p.title, p.sku");
		$this->db->select("
		        p.id, p.title, p.sku, d.company_name
		        ");
	    $this->db->from("{$this->table} as p");
	    $this->db->join("{$this->table3} as b ", "p.brand_id = b.id");
	    $this->db->join("{$this->dealers} as d ", "p.dealer_id = d.id","LEFT");
	    $this->db->like('p.title', $where);
	    $this->db->or_like('p.sku', $where);
	    $this->db->or_like('b.name', $where);
	    $this->db->order_by("p.price", "desc");
	    $result = $this->db->get();
		return $row = $result->custom_result_object('CustomProductDetails');			
	 }


	# All products
	public function listProduct($where, $where_or, $limit, $offset, $sortfield, $order, $betweenPrice,$filterBrand = false) {
		$this->db->select('a.*,b.id as BRAND_ID, b.name as BRAND_NAME, (SELECT 
            ROUND(AVG(rate), 2)
        FROM
            rate_product
        WHERE
            product_id = a.id) AS rating,
         (SELECT 
            COUNT(*)
        FROM
            rate_product
        WHERE
            product_id = a.id) AS total_rates');
		$this->db->from($this->table. " a");
		$this->db->join($this->table3. " b", "a.brand_id = b.id", "LEFT");
        if($filterBrand) $this->db->where("b.id",$filterBrand);
		$this->db->like($where);
		$this->db->limit($limit, $offset);
		$this->db->where( $betweenPrice );
		$this->db->or_like($where_or);
		$this->db->order_by($sortfield, $order);
		$this->db->order_by('a.id', 'acs');
		$result = $this->db->get();
		return $result->result_array();
	}

	# Pagination of product
	public function countProduct($where = '', $betweenPrice, $filterBrand = false) {
		$this->db->from($this->table. " a");
		$this->db->like($where);
        if($filterBrand) $this->db->where("a.brand_id",$filterBrand);
		$this->db->where( $betweenPrice );
		return $this->db->count_all_results();
	}

	public function dealers( $id ) {
		$this->db->select("*");
		$this->db->from($this->table2);
		$this->db->where('id', $id);
		// $this->db->where('a.status', 'A');
		$result = $this->db->get();
		return $result->row_array(); 
	}

	public function brands() {
		$this->db->select('*');
		$this->db->from($this->brands);
		$result = $this->db->get();
		return $result->result_array();
    }
    
    public function delete_deals_cache()
    {
        $this->db->cache_delete_all();
    }

}
/* Custom Response for product Table*/
class CustomProductDetails {

	public $title;
	public $sku;
	public $description;
	public $price;
	public $image_url;
	public $productLink;
	// public $compareUpc;
	public $productId;
	public $upc;
	public $company_name;

    public function __set($title ,$value) {

        if ($title === 'id') {
            $this->productId = $value;
            $this->productLink = base_url().'product/viewProduct?id='.$value;
		}

		if ($title == 'COUNT_UPC_SAME' && $value > 1)  {
			$this->productLink = base_url().'compare/prices/'.$this->upc;
		}
	}
}
?>