<?php
Class Product_model extends CI_Model
{
	// private $db;
    public $table = "products";
    public $table2 = "product_images";
    public $table3 = "product_descriptions";
    public $table4 = "dealers";
    public $table5 = "comments";
    public $table6 = "users";
    public $table7 = "categories";
    public $table8 = "ads";
    public $table9 = "ad_locations";
    public $table10 = "rate_product";
    public $table11 = "categories";
    public $table12 = "sub_categories";
    public $table13 = "states";
    public $table14 = "brands";
    public $tableProdInfo = "product_informations";
    public $tableDealInfo = "deal_informations";
    public $tableDealDesc = "deal_descriptions";
    public $tableDealImg = "deal_images";
    public $dealer_table = "dealers";

  //   # GET LASTEST PRODUCT
  //   function lastestProducts() {
  //   	$this->db->select('*');
  //   	$this->db->from($this->table);
  //   	$this->db->where('status', 'A');
  //   	$this->db->order_by('created_at', 'DESC');
  //   	$this->db->limit(20);
  //   	$result = $this->db->get();

		// return $result->result_array(); 
  //   }

    # GET FEATURE PRODUCT
    function featureProducts($id = 'HMF') {
    	$this->db->select('A.*');
    	$this->db->from($this->table8. ' A');
    	$this->db->join($this->table9. ' B' , 'A.location_id = B.id');
		$this->db->where('B.location_code', $id);
		$this->db->where('A.status', 'A');
		$this->db->where('B.status', 'A');
		$this->db->order_by('id', 'asc');
    	$result = $this->db->get();
		return $result->result_array(); 
    }

    # GET PRODUCT
	function getProducts($latestItem = FALSE, $limit = FALSE, $offset = 0) 
	{
		$limit_set = '';
		$order = '';

		if ($limit != FALSE) 
		{
			$limit_set = $limit;
			$order = 'P.created_at DESC, P.id DESC';
		}
		else
		{
			$limit_set = 12;
			$order = 'order_display DESC';
		}

		$str = "
			SELECT 
			*, 
			id as product_id,
			product_image as images,
			(
				comment_count * .20 +
				like_count * .30 +
				view_count * .50
			) as order_display

			FROM products

			ORDER BY ".$order." 
			LIMIT ".$limit_set." 
			OFFSET ".$offset."
		";
		
		$result = $this->db->query($str);
		return $result->result_array(); 
    }

    # GET Todays Deal
    function getTodaysDeals($latestItem = FALSE, $limit = 12 , $offset = 0) {

		// $dealers = $this->db->get('dealers');
		// $new_data = [];

		// foreach($dealers->result_array() as $row)
		// {
		// 	$str = "
		// 		SELECT 
		// 		*, 
		// 		id as product_id,
		// 		product_image as images

		// 		FROM products

		// 		WHERE dealer_id = ".$row['id']." and status = 'A'

		// 		ORDER BY api_created ASC, created_at DESC
		// 		LIMIT 2
		// 	";

		// 	$result = $this->db->query($str);
		// 	$res = $result->result_array();

		// 	if(count($res) !== 0)
		// 	{
		// 		foreach($res as $key => $row2)
		// 		{
		// 			$res[$key]['first_name'] 	= $row['first_name'];
		// 			$res[$key]['last_name'] 	= $row['last_name'];
		// 			$res[$key]['dealer'] 		= $row['company_name'];

		// 			array_push($new_data, $res[$key]);
		// 		}
		// 	}
		// }
		$str = "
			SELECT 
			*, 
			id as product_id,
			product_image as images

			FROM products

			WHERE status = 'A' AND api_created = '0'

			ORDER BY created_at DESC
			LIMIT ".$limit." OFFSET ".$offset."
		";

		$result = $this->db->query($str);
		$res = $result->result_array();

		return $res;
    }

	#GET ITEM NAME BY ID
	function productDetails($id) {

		// $this->db->select("a.*,b.name AS CATEGORY_NAME ,c.list_name as SUB_CATEGORY_NAME, d.id as BRAND_ID, d.name as BRAND_NAME, f.name as STATE_NAME, i.image_url as IMG");
		// $this->db->from($this->table. " a");
		// $this->db->join($this->table2. " i", 'a.upc = i.product_upc', "LEFT");
		// $this->db->join($this->table11. " b", 'a.category_code = b.category_code', "LEFT");
		// $this->db->join($this->table12. " c", 'a.sub_category_code = c.sub_category_code', "LEFT");
		// $this->db->join($this->table14. ' d', 'a.brand_id = d.id', 'LEFT');
		// $this->db->join($this->table4. ' e', 'e.id = a.dealer_id', 'LEFT');
		// $this->db->join($this->table13. ' f', 'f.id = e.state', 'LEFT');

		$this->db->where('id', $id);
		$result = $this->db->get('products');
		return $result->row_array();
	}	

	/* Get product descriptions */
	function productDescriptions($upc) {
		$this->db->select('descriptions');
		$this->db->from($this->table3);
		$this->db->where('product_upc', $upc);
		$this->db->order_by('distributor_id', 'asc');
	   	$this->db->limit(1);
		$result = $this->db->get();
		return $result->result_array(); 
	}

	function productWebsiteDesc($id) {
		$this->db->select('descriptions');
		$this->db->from($this->tableDealDesc);
		$this->db->where('deal_id', $id);
	   	$this->db->limit(1);
		$result = $this->db->get();
		return $result->result_array(); 
	}

	# GET IMAGES LINK BY UPC
	function imagesLink($upc) {

		$this->db->select('image_url');
		$this->db->from($this->table2);
		$this->db->where('product_upc', $upc);
		$this->db->order_by('distributor_id', 'asc');
		$this->db->limit(1);
		$query = $this->db->get();
		$ret = $query->row();
	    return isset($ret)? $ret->image_url: '';
	}

	function imagesLinkDealer($upc, $id) {

		$this->db->select('image_url');
		$this->db->from($this->table2);
		$this->db->where('product_upc', $upc);
		$this->db->where('dealer_id', $id);
		$this->db->limit(1);
		$query = $this->db->get();
		$ret = $query->row();
	    return isset($ret)? $ret->image_url: '';
	}

	function dealImagesLink($id) {

		$this->db->select('image_url');
		$this->db->from($this->tableDealImg);
		$this->db->where('deal_id', $id);
		$this->db->limit(1);
		$query = $this->db->get();
		$ret = $query->row();
	    return isset($ret)? $ret->image_url: '';
	}

	/* Get dealer */
	function dealer($id) {
		$this->db->select('a.*, b.name AS STATE_NAME');
		$this->db->from($this->table4. " a");
		$this->db->join($this->table13. " b", "a.state = b.id", "LEFT");
		$this->db->where('a.id', $id);
		$this->db->where('a.status', '1');
		$result = $this->db->get();
		return $result->row_array(); 
	}

	/* Get comments */
	function comments($product_id, $start = '0', $limit = 3) {

		$this->db->select('A.*, B.id as USER_ID, B.name as USERNAME, B.username as USER_USERNAME, D.username as DEALER_USERNAME, CONCAT(D.first_name, " " ,D.last_name) as DEALER_NAME');
		$this->db->from($this->table5. ' A');
		$this->db->join($this->table6 . ' B', 'A.user_id = B.id and A.user_type = "user" ','LEFT');
		$this->db->join($this->dealer_table . ' D', 'A.user_id = D.id and A.user_type = "dealer" ','LEFT');
		$this->db->where('A.product_id', $product_id);
		$this->db->where('A.status', 'A');
		$this->db->limit($limit, $start);
		$this->db->order_by("A.created_at", "desc");
		$data = $this->db->get();
		$this->db->where('product_id', $product_id);
		$this->db->from($this->table5);
		$count = $this->db->count_all_results();

		return array( 
				'data' => $data->result_array(),
				'count' => $count,
				'next' => ($count > $start + $limit) ? 'Y' : 'N',
				'start' => $start + $limit
			); 
		
	}

	/* Insert comments */
	function insert_comments($data) {
		$this->db->insert($this->table5, $data);
		return $this->db->insert_id();
	}

	/* Update Comment */

	function update_comment($data, $id)
	{
		$this->db->where('id', $id);
		return $this->db->update('comments', $data);
	}	

	function commentCount($product_id)
	{	
		$this->db->where('product_id', $product_id);
		$this->db->where('status', 'A');
		$result = $this->db->get('comments')->result();
		return count($result);
	}

	function get_count($type, $id)
	{
		$table = 'product_likes';

		if($type == 'like')
			$this->db->where('type', $type);

		if($type == 'dislike')
			$this->db->where('type', $type);

		if($type == 'comment')
			$table = 'comments';

		if($type == 'subscriber')
			$table = 'pricedrop_subscriptions';
			
		$this->db->where('product_id', $id);
		$this->db->select('COUNT(*) as count');
		return $this->db->get($table)->result()[0]->count;
	}

	function get_comment($comment_id)
	{
		$this->db->where('id', $comment_id);
		return $this->db->get('comments')->row();
	}

	function delete_comment($comment_id)
	{	
		$this->db->where('id', $comment_id);
		return $this->db->delete('comments');
	}

	function reportComment($data)
	{
		return $this->db->insert('reported_comments', $data);
	}

	function subsPDCExist($where)
	{
		$this->db->where($where);
		$comments = $this->db->get('comment_subscriptions')->result();
		$this->db->where($where);
		$price_drops = $this->db->get('pricedrop_subscriptions')->result();

		if(count($comments) !== 0 && count($price_drops) !== 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function subsCommentExist($where)
	{
		$this->db->where($where);
		$comments = $this->db->get('comment_subscriptions')->result();

		if(count($comments) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function subsPricedropExist($where)
	{
		$this->db->where($where);
		$comments = $this->db->get('pricedrop_subscriptions')->result();

		if(count($comments) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function likeProduct($data)
	{
		$where_like 		= $data;
		$where_like['type'] = 'like';
		$dislikeExist 			= $this->likeExist($where_like);

		if(count($dislikeExist) !== 0)
		{
			$this->db->set('dislike_count', 'IF(dislike_count != 0, dislike_count - 1,  0)', FALSE);
			$this->db->where('id', $data['product_id']);
		}

		$this->db->insert('product_likes', $data);

		$this->db->set('like_count', 'like_count + 1', FALSE);
		$this->db->where('id', $data['product_id']);

		return $this->db->update('products');
	}
	
	function dislikeProduct($data)
	{
		$where_like 		= $data;
		$where_like['type'] = 'like';
		$likeExist 			= $this->likeExist($where_like);

		if(count($likeExist) !== 0)
		{
			$this->db->set('like_count', 'IF(like_count != 0, like_count - 1,  0)', FALSE);
			$this->db->where('id', $data['product_id']);
		}

		$this->db->insert('product_likes',$data);

		$this->db->set('dislike_count', 'dislike_count + 1', FALSE);
		$this->db->where('id', $data['product_id']);
		
		return $this->db->update('products');
	}

	function likeExist($where)
	{
		$this->db->where($where);
		return $this->db->get('product_likes')->result();
	}

	function dislikeExist($where)
	{
		$this->db->where($where);
		return $this->db->get('product_likes')->result();
	}

	function unlikeProduct($where)
	{
		$this->db->where($where);
		$this->db->delete('product_likes');

		$this->db->set('like_count', 'IF(like_count != 0, like_count - 1, 0)', FALSE);
		$this->db->where('id', $where['product_id']);

		return $this->db->update('products');
	}
	
	function undislikeProduct($where)
	{
		$this->db->where($where);
		$this->db->delete('product_likes');

		$this->db->set('dislike_count', 'IF(dislike_count != 0, dislike_count - 1,  0)', FALSE);
		$this->db->where('id', $where['product_id']);

		return $this->db->update('products');
	}

	function unsubsPDC($where)
	{
		$this->db->where($where);
		$comments =  $this->db->delete('comment_subscriptions');
		$this->db->where($where);
		$pricedrops =  $this->db->delete('pricedrop_subscriptions');

		if($comments == true && $pricedrops == true)
		{
			$this->db->set('subscriber_count', 'subscriber_count - 2', FALSE);
			$this->db->where('id', $where['product_id']);
			$this->db->update('products');

			return true;
		}
		else
		{
			return false;
		}
	}
	
	function unsubsComment($where)
	{
		$this->db->where($where);
		$this->db->delete('comment_subscriptions');

		$this->db->set('subscriber_count', 'subscriber_count - 1', FALSE);
		return $this->db->where('id', $where['product_id']);
		$this->db->update('products');
	}

	function unsubsPricedrop($where)
	{
		$this->db->where($where);
		$this->db->delete('pricedrop_subscriptions');

		$this->db->set('subscriber_count', 'subscriber_count - 1', FALSE);
		return $this->db->where('id', $where['product_id']);
		$this->db->update('products');
	}

	function subsPDC($data)
	{
		$this->db->insert('comment_subscriptions',$data);
		$this->db->insert('pricedrop_subscriptions',$data);

		$this->db->set('subscriber_count', 'subscriber_count + 2', FALSE);
		$this->db->where('id', $data['product_id']);
		$this->db->update('products');
	
	}

	function subsComment($data)
	{
		$this->db->insert('comment_subscriptions',$data);

		$this->db->set('subscriber_count', 'subscriber_count + 1', FALSE);
		$this->db->where('id', $data['product_id']);
		$this->db->update('products');
	}
	
	function subsPricedrop($data)
	{
		$this->db->insert('pricedrop_subscriptions',$data);

		$this->db->set('subscriber_count', 'subscriber_count + 1', FALSE);
		$this->db->where('id', $data['product_id']);
		$this->db->update('products');
	}

	function compareProductbyUPC($upc = 0)
	{
	   $this->db->where('status', 'A');
	   $this->db->where('upc', $upc);
	   $query = $this->db->get($this->table);
	   return $query->result();
	}

	/* Insert rate */
	function insertRateProduct($data) {
		return $result = $this->db->insert($this->table10, $data);
	}

    function getRateProduct( $id ) {
    	$this->db->select('COUNT(*) AS total_rates, ROUND(AVG(rate),2) AS rating');
    	$this->db->where( 'status', 'A' );
    	$this->db->where( 'product_id', $id );
    	$this->db->from($this->table10);
    	$result = $this->db->get();
    	return $result->row_array(); 
    }

    function rateProductWithBreakDown( $id ) {
       $query = $this->db->query('SELECT 
			(SELECT COUNT(*) FROM rate_product WHERE product_id = "'.$id.'" AND rate = 5 ) AS FIVE,
			(SELECT COUNT(*) FROM rate_product WHERE product_id = "'.$id.'" AND rate = 4 ) AS FOUR,
			(SELECT COUNT(*) FROM rate_product WHERE product_id = "'.$id.'" AND rate = 3 ) AS THREE,
			(SELECT COUNT(*) FROM rate_product WHERE product_id = "'.$id.'" AND rate = 2 ) AS TWO,
			(SELECT COUNT(*) FROM rate_product WHERE product_id = "'.$id.'" AND rate = 1 ) AS ONE,
			(SELECT COUNT(*) FROM rate_product WHERE product_id  = "'.$id.'" ) as TOTAL_RATES,
			(select ROUND(AVG(rate),2) from rate_product where product_id  = "'.$id.'" ) as ROUND  
		');
        return $query->row_array();
    }

    function getUserRateProduct( $where ) {
    	$this->db->select("*");
    	$this->db->where( $where );
    	$this->db->from($this->table10);
    	$result = $this->db->get();
    	return $result->row_array(); 
    }

    function getRelatedProducts($id , $upc)
    {
	   $this->db
	   		->select('products.id,products.dealer_id, products.title,products.price, products.product_image, products.upc, products.api_created, dealer_info')
         	->from($this->table);
       $this->db->where('products.status', 'A');
	   $this->db->where('products.upc', $upc);
	   $this->db->where('products.id !=', $id);
	   $this->db->group_by('products.dealer_id');
	   $this->db->limit(4);
	   $query = $this->db->get();
	   return $query->result();
    }

    function getDealAPIInformation($upc){
	   $this->db->where('product_upc', $upc);
	   $this->db->limit(1);
	   $query = $this->db->get($this->tableProdInfo);
	   return $query->row_array();
    }

    function getDealWebsiteInformation($id){
	   $this->db->where('deal_id', $id);
	   $this->db->limit(1);
	   $query = $this->db->get($this->tableDealInfo);
	   return $query->row_array();
    }

    function updateDealViews($id){
		$sql= "UPDATE ".$this->table." SET view_count = view_count + 1 WHERE id = '$id'";
		$query = $this->db->query($sql);
		return ($this->db->affected_rows() > 0)? TRUE : FALSE ;
	}

	function getAllThumbs($upc)
	{
	   $this->db->limit(4);
	   $this->db->where('product_upc', $upc);
	   $query = $this->db->get($this->table2);
	   return $query->result();
	}

	function getWebThumbs($id)
	{
	   $this->db->limit(4);
	   $this->db->where('deal_id', $id);
	   $query = $this->db->get($this->tableDealImg);
	   return $query->result();
	}

	#Elmer - get all rates per product
    function getRates( $id , $limit = 5, $offset = 0){

        $this->db->select('A.post_date, A.post_time, A.remarks , A.rate, B.username as user, D.username as dealer');
        $this->db->from($this->table10 . ' as A');
        $this->db->join($this->table6 . ' B', 'A.user_id = B.id and A.is_dealer != "YES" ','LEFT');
        $this->db->join($this->dealer_table . ' D', 'A.user_id = D.id and A.is_dealer = "YES" ','LEFT');
        $this->db->where('product_id', $id);
        $this->db->limit($limit, $offset);
		$this->db->order_by('A.id', 'DESC');
        $query = $this->db->get();

        return $query->result_array();

    }

    function updateLikeCount($id){
		$sql= "UPDATE ".$this->table." SET like_count = IFNULL(like_count, 0) + 1 WHERE id = '$id'";
		$query = $this->db->query($sql);
		return ($this->db->affected_rows() > 0)? TRUE : FALSE ;
	}

	function updatedisLikeCount($id){
		$sql= "UPDATE ".$this->table." SET dislike_count = IFNULL(dislike_count, 0) + 1 WHERE id = '$id'";
		$query = $this->db->query($sql);
		return ($this->db->affected_rows() > 0)? TRUE : FALSE ;
	}

	function updatepriceDropSubscriberCount($id){
		$sql= "UPDATE ".$this->table." SET subscriber_count = IFNULL(subscriber_count, 0) + 1 WHERE id = '$id'";
		$query = $this->db->query($sql);
		return ($this->db->affected_rows() > 0)? TRUE : FALSE ;
	}

	function updateSubscriberCount($id){
		$sql= "UPDATE ".$this->table." SET subscriber_count = IFNULL(subscriber_count, 0) + 2 WHERE id = '$id'";
		$query = $this->db->query($sql);
		return ($this->db->affected_rows() > 0)? TRUE : FALSE ;
	}

	function updateRateCount($id, $count){
		$sql= "UPDATE ".$this->table." SET rate_count = IFNULL(rate_count, 0) + $count WHERE id = '$id'";
		$query = $this->db->query($sql);
		return ($this->db->affected_rows() > 0)? TRUE : FALSE ;
	}

	public function get_all_null_images($order_by = 'DESC')
	{
		$this->db->select('id, upc, api_created');
		$this->db->where('product_image');
		$this->db->or_where("product_image = ''");
		$this->db->order_by('created_at', $order_by);
		$this->db->limit(20000);
        $query = $this->db->get('products');

        return $query->result_array();
	}
	
	public function update_image($product_id, $product_upc, $api_created)
	{
		$this->db->select('image_url');
		
	   	if($api_created == 1)
	   	{
		  	$this->db->where('product_upc', $product_upc);
		  	$this->db->order_by('distributor_id', 'asc');
		  	$this->db->limit('1');
		  	$query = $this->db->get('product_images');
		}
		else
	   	{
	   	  	$this->db->where('deal_id', $product_id);
		  	$this->db->order_by('deal_id', 'asc');
		  	$this->db->limit('1');
		  	$query = $this->db->get('deal_images');
	   	}

	   	$ret 	= $query->row();
		$image 	= isset($ret) ? $ret->image_url : null;
		
		if($image != null)
		{
			$this->db->where('id', $product_id);
			$this->db->update('products', ['product_image' => $image]); 
		}

		return $image;
	}
}
?>
