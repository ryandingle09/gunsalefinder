<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">Reported Deals</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-4 col-sm-12 col-xs-12 leftbar-stick blog-sidebar">
               <?php  foreach ($info as $users): ?>
                  <?php include_once('menu.php'); ?>
               <?php endforeach; ?>
                  </div>
                  <div class="col-md-8 col-sm-12 col-xs-12">
                     <!-- Row -->
                     <div class="row">
                        <!-- Sorting Filters -->
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 heading">
                           <!-- Advertisement Panel -->
                           <div class="panel panel-default">
                              <div class="panel-heading" >
                                 <div class="col-md-4 col-sm-4 col-xs-4">
                                    <h4 class="panel-title">
                                       <a>
                                       Reported Deals
                                       </a>
                                    </h4>
                                 </div>
                                 <div class="col-md-8 col-sm-4 col-xs-4">
                                    <!--<div class="search-widget pull-right">
                                       <input placeholder="search" type="text">
                                       <button type="submit"><i class="fa fa-search"></i></button>
                                    </div>-->
                                 </div>
                              </div>
                           </div>
                           <!-- Advertisement Panel End -->
                        </div>
                        <!-- Sorting Filters End-->
                        <div class="clearfix"></div>
                        <!-- Ads Archive -->
                        <div class="posts-masonry">
                           <div class="col-md-12 col-xs-12 col-sm-12 user-archives">
                              <!-- Ads Listing -->
                              <?php echo (count($results) == 0) ? '<h3>No Reported Deals Found.</h3>' : '';?>
                           <?php  foreach ($results as $product): ?>
                              <div class="ads-list-archive">
                                 <!-- Ads Listing -->    
                                 <div class="clearfix visible-xs-block"></div>
                                 <!-- Content Block -->     
                                 <div class="col-lg-12 col-md-12 col-sm-12 no-padding">
                                    <!-- Ad Desc -->     
                                    <div class="ad-archive-desc">
                                       <!-- Title -->    
                                       <a target="_blank" href="<?php echo base_url();?>product/viewProduct?id=<?php echo $product->id;?>">
                                       <h3><?php echo ucwords(strtolower($product->title)); ?></h3>
                                    </a>
                                       <!-- Category -->
                                       <div class="category-title"> 
                                          <span>
                                                <a href="#">
                                                      Report Message: 
                                                      <br>[<?php echo $product->report_type ?>]
                                                </a> 
                                                <br> 
                                                <br>
                                                <?php echo $product->details ?> 
                                          </span>

                                          <br>
                                          <br>
                                          <p class="text-right" style="color: #000">Reported by <strong><?php echo $this->common->getusername($product->user_id) ?></strong> <?php echo $this->common->timetoago($product->date_reported); ?> </p>
                                       </div>
                                       
                                       <!-- Short Description -->
                                       <div class="clearfix visible-xs-block"></div>
                                       <!-- Ad Features -->
                                       <!-- Ad History -->
                                       <!-- <div class="clearfix archive-history">
                                          <div class="last-updated">
                                             Reported by <strong><?php echo $this->common->getusername($product->user_id) ?></strong> <?php echo $this->common->timetoago($product->date_reported); ?> 
                                          </div>
                                       </div> -->
                                    </div>
                                    <!-- Ad Desc End -->     
                                 </div>
                                 <!-- Content Block End --> 
                              </div>
                           <?php endforeach; ?>
                              <!-- Ads Listing -->
                           </div>
                        </div>
                        <!-- Ads Archive End -->  
                        <div class="clearfix"></div>
                        <!-- Pagination -->  
                        <div class="col-md-12 col-xs-12 col-sm-12">
                           <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <!-- Pagination End -->   
                     </div>
                     <!-- Row End -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
    </body>
</html>