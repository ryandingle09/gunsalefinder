<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">My Ads</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding gray">
            <!-- Main Container -->
            <div class="container">
              <?php 
                echo $this->session->flashdata('message');  
                echo (isset($error))?$error:'';
              ?>
               <!-- Row -->
               <?php if(isset($_GET['update_ads'])): ?>
              <div class="row">
                <div class="col-md8 col-xs-8 col-sm-8 col-lg-8 col-md-offset-2 heading">
                  <!-- Advertisement Panel -->
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <h4 class="panel-title">
                        Update Your Ads <a href="/<?php echo $_GET['redirect_to'];?>" class="pull-right">< Back to ads</a><br><br>
                      </h4>
                      <form action="" enctype="multipart/form-data" action="<?php echo base_url('dealer/postdeal/savedeals'); ?>" method="post">
                        <!-- Image Upload  -->
                        <div class="form-group">
                            <label class="control-label">Photo for your ad <small>Please add image of your ad. (<?php echo str_replace('*','x', $ads_location->size);?>)</small></label>
                            <input type="file" name="ad_photo">
                        </div>
                        <?php if(in_array($ads_location->location_code, ['VPMF','VPRMF','SLMF','HMF']) == TRUE):?>
                        <div class="form-group">
                            <label class="control-label">Ads New Title <small>(Optional - set as default Ads Package Title)</small></label>
                            <input class="form-control" type="text" value="<?php echo (set_value('title') !== '') ? set_value('title') : $ads_location->title ;?>" name="title" placeholder="ex. My Ads Number 1">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Feature Ad Description <small>(Optional)</small></label>
                            <textarea class="form-control" name="description" placeholder="Description of you add."><?php echo set_value('description');?></textarea>
                        </div>
                        <?php endif;?>
                        <!-- Url Upload  -->
                        <div class="form-group">
                            <label class="control-label">Link Url <small>ex. https://mysite.com</small></label>
                            <input class="form-control" type="url" value="<?php echo set_value('url');?>" name="url" placeholder="eg. https://mysite.com">
                        </div>
                         <!-- status  -->
                         <div class="form-group">
                            <label>Status</label>
                            <select name="status">
                              <option value="A" <?php echo (set_value('status') == 'A') ? 'selected="selected"' : '';?> >Active</option>
                              <option value="I" <?php echo (set_value('status') == 'I') ? 'selected="selected"' : '';?> >Inactive</option>
                            </select>
                        </div>
                        <!-- end row -->
                        <button class="btn btn-info pull-right" type="sumit">Update Ads</button>
                      </form>
                    </div>
                  </div>
              </div>
              </div>
              <?php else:?>
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-4 col-sm-12 col-xs-12 leftbar-stick blog-sidebar">
               <?php  foreach ($info as $users): ?>
                  <?php include_once('menu.php'); ?>
               <?php endforeach; ?>
                  </div>
                  <div class="col-md-8 col-sm-12 col-xs-12">
                     <!-- Row -->
                     <div class="row">
                        <!-- Sorting Filters -->
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 heading">
                           <!-- Advertisement Panel -->
                           <div class="panel panel-default">
                              <div class="panel-heading" >
                                 <div class="col-md-4 col-sm-4 col-xs-4">
                                    <h4 class="panel-title">
                                       <a>
                                       <?php echo (isset($title)) ? $title : 'My Ads' ;?>
                                       </a>
                                    </h4>
                                 </div>
                                 <div class="col-md-8 col-sm-4 col-xs-4">
                                    <!--<div class="search-widget pull-right">
                                       <input placeholder="search" type="text">
                                       <button type="submit"><i class="fa fa-search"></i></button>
                                    </div>-->
                                 </div>
                              </div>
                           </div>
                           <!-- Advertisement Panel End -->
                        </div>
                        <!-- Sorting Filters End-->
                        <div class="clearfix"></div>
                        <!-- Ads Archive -->
                        <div class="posts-masonry">
                           <div class="col-md-12 col-xs-12 col-sm-12 user-archives">
                              <!-- Ads Listing -->
                            <?php echo (count($results) == 0) ? '<h3>No Ads Found.</h3>' : '';?>
                           <?php  foreach ($results as $ads): ?>
                              <div class="ads-list-archive">
                                 <!-- Image Block -->
                                 <div class="col-lg-3 col-md-3 col-sm-3 no-padding">
                                    <!-- Img Block -->
                                    <div class="ad-archive-img" style="margin-top: 5%">
                                      <?php if(is_null($ads->image_url)):?>
                                        <p style="margin-top: 30px; margin-bottom: 50%"><b style="padding: 10px;">Upload Image Banner.</b></p>
                                      <?php else:?>
                                        <?php if(file_exists($ads->image_full_path)):?>
                                          <a href="<?php echo $ads->link_url; ?>">
                                            <img src="/<?php echo $ads->image_url; ?>" alt="Ads Image Banner" style="margin: 5%;">
                                          </a>
                                        <?php else:?>
                                          <a href="<?php echo $ads->link_url; ?>" style="margin: 5%;">
                                            <img src="<?php echo ADMINURL.$ads->image_url; ?>" alt="Ads Image Banner">
                                          </a>
                                        <?php endif;?>
                                      <?php endif;?>
                                    </div>
                                    <!-- Img Block -->   
                                 </div>
                                 <!-- Ads Listing -->    
                                 <div class="clearfix visible-xs-block"></div>

                                 <!-- Content Block -->     
                                 <div class="col-lg-9 col-md-9 col-sm-9 no-padding">
                                    <!-- Ad Desc -->     
                                    <div class="ad-archive-desc">
                                       <!-- Price -->    
                                       <div class="wrap_adPrice">
                                        <label><?php echo ($ads->price != NULL)? '$'.number_format($ads->price) :''; ?></label>
                                        
                                        </div>
                                       <!-- Title -->    
                                       <h3><?php echo ucwords(strtolower($ads->title)); ?></h3>
                                       <!-- Category -->
                                       <div class="category-title">
                                          <label><?php echo ($ads->status =='A')? "ACTIVE":"PENDING"; ?></label>
                                          <span>
                                            <?php echo ($ads->status == 'A') ? 'Updated' : 'Added'; ?>: <?php echo $this->common->timetoago($ads->updated_at); ?> 
                                          </span>
                                        </div>
                                       <!-- Short Description -->
                                       <div class="clearfix visible-xs-block"></div>
                                       <!-- Ad Features -->

                                       <!-- Ad History -->
                                        <div class="wrap_btnChangeImg">
                                          <a class="btn btn-success" href="?update_ads=true&id=<?php echo $ads->id;?>&redirect_to=<?php echo (isset($title)) ? 'pending-ads' : 'my-ads' ;?>">
                                              <i class="fa fa-upload"></i> Change Photo
                                             </a>
                                        </div>
                                          
                                    </div>
                                    <!-- Ad Desc End -->     
                                 </div>


                                 <!-- Content Block End --> 
                              </div>
                           <?php endforeach; ?>
                              <!-- Ads Listing -->
                           </div>
                        </div>
                        <!-- Ads Archive End -->  
                        <div class="clearfix"></div>
                        <!-- Pagination -->  
                        <div class="col-md-12 col-xs-12 col-sm-12">
                           <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <!-- Pagination End -->   
                     </div>
                     <!-- Row End -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
              <?php endif;?>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
    </body>
</html>