<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">Update a deal</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <section class="section-padding gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row End -->
               <div class="row">
                  <!-- Middle Content Area -->
                  
                  <div class="col-md-12 col-xs-12 col-sm-12">
                     <div class="heading-panel">
                           <h3 class="main-title text-left">
                              Manage a Deal
                           </h3>
                     </div>
                     <!-- Row -->
                     <div class="profile-section margin-bottom-20">
                        <div class="profile-tabs">
                           <ul class="nav nav-justified nav-tabs">
                              <li class="active"><a href="#profile" data-toggle="tab">Information</a></li>
                              <li><a href="#edit" data-toggle="tab">Images</a></li>
                              <li><a href="#payment" data-toggle="tab">Comments</a></li>
                              <li><a href="#settings" data-toggle="tab">Ratings</a></li>
                              <li><a href="#issues" data-toggle="tab">Reported Issues</a></li>
                           </ul>
                           <div class="tab-content">
                              <div class="profile-edit tab-pane fade in active" id="profile">
                              <?php
                                    echo $this->session->flashdata('message'); 
                                    echo validation_errors(); 
                              ?>
                                 <h2 class="heading-md">Deal Information.</h2>
                                 <dl class="dl-horizontal">
                                  <form class="submit-form" action="<?php echo base_url('dealer/postdeal/updateinformation'); ?>" method="post">
                                  <?php  foreach ($deals as $deal): ?>
                                       <!-- Title  -->
                                       <div class="row">
                                          <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                             <label class="control-label">Title</label>
                                             <input name="code" type="hidden" value="<?php echo $code; ?>">
                                             <input name="dealid" type="hidden" value="<?php echo $deal->id; ?>">
                                             <input name="oldprice" type="hidden" value="<?php echo $deal->price; ?>">
                                             <input name="title" class="form-control" type="text" value="<?php echo $deal->title; ?>">
                                          </div>
                                       </div>
                                       <div class="row">
                                            <!-- Code  -->
                                          <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                             <label class="control-label">Code</label>
                                             <input name="itemcode" class="form-control" type="text"  value="<?php echo $deal->code; ?>">
                                          </div>
                                          <!-- Category  -->
                                          <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                             <label class="control-label">Brand</label>
                                             <select name="brand" class="brand form-control">
                                                <option label="Select Option"></option>
                                             <?php  foreach ($brands as $brand): ?>
                                                <option value="<?php echo $brand->id; ?>" <?php echo ($brand->id == $deal->brand_id)? "selected='selected'":''; ?>><?php echo $brand->name; ?></option>
                                             <?php endforeach; ?>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <!-- Category  -->
                                          <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                             <label class="control-label">Category</label>
                                             <select name="category" class="category form-control" id="category">
                                                <option label="Select Category"></option>
                                             <?php  foreach ($category as $cat): ?>
                                                <option value="<?php echo $cat->category_code; ?>" <?php echo ($cat->category_code == $deal->category_code)? "selected='selected'":''; ?>><?php echo $cat->name; ?></option>
                                              <?php  endforeach; ?> 
                                             </select>
                                          </div>
                                          <!-- Subcategory  -->
                                          <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                             <label class="control-label">Subcategory</label>
                                             <?php
                                             $subcategory = $this->common->getSubcategoryByCode($deal->category_code); 
                                             ?>
                                             <select name="subcategory" class="category form-control" id="subcategory">
                                             <?php  foreach ($subcategory as $subcat): ?>
                                                <option value="<?php echo $subcat->sub_category_code; ?>" <?php echo ($subcat->sub_category_code == $deal->sub_category_code)? "selected='selected'":''; ?>><?php echo $subcat->list_name; ?></option>
                                             <?php  endforeach; ?> 
                                             </select>
                                          </div>
                                       </div>
                                       <!-- end row -->
                                       <!-- end row -->
                                       <div class="row">
                                          <!-- Category  -->
                                          <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                             <label class="control-label">UPC</label>
                                             <input name="upc" class="form-control" type="text" value="<?php echo $deal->upc; ?>">
                                          </div>
                                          <!-- SKU  -->
                                          <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                             <label class="control-label">SKU</label>
                                             <input name="sku" class="form-control" type="text" value="<?php echo $deal->sku; ?>">
                                          </div>
                                       </div>
                                       <div class="row">
                                          <!-- Category  -->
                                          <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                             <label class="control-label">Price</label>
                                             <input name="price" class="form-control" type="text" value="<?php echo $deal->price; ?>">
                                          </div>
                                          <!-- Price  -->
                                          <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                             <label class="control-label">Shipping Rate</label>
                                             <input name="shipping" class="form-control" type="text"  value="<?php echo $deal->shipping_fee; ?>">
                                          </div>
                                       </div>

                                       <div class="row">
                                          <!-- Category  -->
                                          <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                             <label class="control-label">Item URL</label>
                                             <input name="item_url" class="form-control" type="text" value="<?php echo $deal->item_url; ?>">
                                          </div>
                                          <!-- Price  -->
                                          <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                                             <label class="control-label">Status</label>
                                             <select name="status" class="status form-control">
                                                <option value="A" <?php echo ($deal->status == 'A')?'selected="selected"':'' ?>>In Stock</option>
                                                <option value="I" <?php echo ($deal->status == 'I')?'selected="selected"':'' ?>>Out of Stock</option>
                                                <option value="E" <?php echo ($deal->status == 'E')?'selected="selected"':'' ?>>Expired</option>
                                             </select>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                              <div class="skin-minimal">
                                                <ul class="list">
                                                   <li>
                                                      <input name="notify" type="checkbox" id="minimal-checkbox-1" value="1" <?php echo ($deal->notify_dealer == '1')? 'checked':''; ?>>
                                                      <label for="minimal-checkbox-1">Subscribe to Customer Comments</label>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>

                                    <?php endforeach; ?>
                                    <?php  foreach ($dealDescriptions as $desc): ?>
                                       <div class="row">
                                          <div class="col-md-12 col-lg-12 col-xs-12  col-sm-12">
                                             <label class="control-label">Descriptions</label>
                                             <textarea name="editor1" id="editor1" rows="12" class="form-control" placeholder=""><?php echo $desc->descriptions; ?></textarea>
                                          </div>
                                       </div>
                                    <?php endforeach; ?>
                                    <?php  foreach ($dealOtherinfo as $info): ?>
                                       <!-- end row -->
                                       <div class="row">
                                          <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                             <label class="control-label">Caliber</label>
                                             <input name="caliber" class="form-control" type="text"  value="<?php echo $info->caliber; ?>">
                                          </div>
                                          <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                             <label class="control-label">Frame Description</label>
                                             <input name="framedesc" class="form-control" type="text"  value="<?php echo $info->frame_description; ?>">
                                          </div>
                                          <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                             <label class="control-label">Stock Description</label>
                                             <input class="form-control" name="stock" type="text" value="<?php echo $info->stock_description; ?>">
                                          </div>
                                       </div>
                                       <!-- end row -->
                                        <!-- end row -->
                                       <div class="row">
                                          <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                                             <label class="control-label">Type</label>
                                             <input name="type" class="form-control" type="text" value="<?php echo $info->type; ?>">
                                          </div>
                                          <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                                             <label class="control-label">Barrel</label>
                                             <input name="barrel" class="form-control" type="text" value="<?php echo $info->barrel; ?>">
                                          </div>
                                          <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                                             <label class="control-label">Capacity</label>
                                             <input class="form-control" name="capacity" type="text" value="<?php echo $info->capacity; ?>">
                                          </div>
                                          <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                                             <label class="control-label">Weight</label>
                                             <input class="form-control" name="weight" type="text" value="<?php echo $info->weight; ?>">
                                          </div>
                                       </div>
                                        <?php endforeach; ?>
                                       <!-- end row -->
                                       <button type="submit" class="btn btn-theme pull-right">Update</button>
                                       <a class="btn btn-theme pull-right" href="<?php echo base_url('dealer/postdeal/deletedeal/'.$deal->id);?>" onclick="return confirm('Delete this deal?')" style="margin-right:5px;">Delete</a> 
                                       <br>
                                    </form>
                              </div>
                              <div class="profile-edit tab-pane fade" id="edit">
                                 <h2 class="heading-md">Images</h2>
                                                                        <!-- Image Upload  -->
                                 <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                       <label class="control-label">Photos for your deal <small>Please add images of your deal. (313x234)</small></label>
                                       <div id="dropzone" class="dropzone"></div>
                                    </div>
                                 </div>

                                 <br>
                              </div>
                              <div class="profile-edit tab-pane fade" id="payment">
                                 <h2 class="heading-md">Deal Comments</h2>
                                 <div class="comment-section" id="commentBox">
                                    <?php
                                        $currentUser = $this->session->userdata('id');
                                    ?>
                                    <ol class="comment-list" id ="commentList" >
                                    <?php  foreach ($comments as $comment): ?>
                                       <li class="comment" id="comment<?php echo $comment->id ?>">
                                          <div class="comment-info">
                                             <img class="pull-left hidden-xs img-circle" src="
                                                <?php echo base_url(); ?>assets/images/blog/c1.png" alt="author">
                                                <div class="author-desc">
                                                    <div class="author-title">
                                                        <strong> <?=isset($comment->name) ?$comment->name :'' ?> </strong>
                                                        <ul class="list-inline pull-right">
                                                            <li>
                                                               <a href="#">
                                                                  <?= $this->common->timetoago($comment->created_at);?>
                                                               </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <span id="message<?php echo $comment->id ?>"><?php echo $comment->message ?></span>
                                                    <span style="display:none;" id="edit<?php echo $comment->id ?>">
                                                        <div class="input-group">
                                                            <textarea class="form-control custom-control" rows="3" style="resize:none" id="textMsg<?php echo $id ?>"><?php echo $comment->message ?></textarea>     
                                                            <span class="input-group-addon btn btn-primary" onClick="updateComment(<?php echo $comment->id ?>)"><i class="fa fa-save"></i></span>
                                                        </div>
                                                    </span>
                                                </div>
                                            </div>
                                       </li>
                                    <?php endforeach; ?>
                                    </ol>
                                 </div>
                              </div>
                              <div class="profile-edit tab-pane fade" id="settings">
                                 <h2 class="heading-md">Deal Ratings</h2><br/>
                                 <table class="table" id="ratingstbl">
                                     <thead >
                                       <tr>
                                         <th>Post Date</th>
                                         <th>Rate</th>
                                         <th>Remarks</th>
                                         <th>User</th>
                                       </tr>
                                     </thead>
                                    <tbody>
                                          <?php  foreach ($ratings as $rating): ?>
                                          <tr>

                                             <td><?php echo $rating->post_date;?></td>
                                             <td>
                                                <div class="rating">
                                                   <?php
                                                      for($x = 1; $x <= $rating->rate; $x++){
                                                         echo '<i class="fa fa-star"></i>';
                                                      }
                                                   ?>
                                                </div>
                                             </td>
                                             <td><?php echo $rating->remarks; ?></td>
                                             <td><?php echo $rating->name; ?></td>
                                          </tr>
                                          <?php endforeach; ?>
                                    </tbody>
                                 </table>
                              </div>

                              <div class="profile-edit tab-pane fade" id="issues">
                                 <h2 class="heading-md">Reported Issues</h2><br/>
                                 <table class="table" id="ratingstbl">
                                     <thead >
                                       <tr>
                                         <th>Details</th>
                                         <th>Report Type</th>
                                         <th>User Type</th>
                                         <th>User</th>
                                         <th>Date Reported</th>
                                       </tr>
                                     </thead>
                                    <tbody>
                                          <?php  foreach ($issues as $issue): ?>
                                          <tr>

                                             <td><?php echo $issue->details;?></td>
                                             <td><?php echo $issue->report_type; ?></td>
                                             <td><?php echo $issue->user_type; ?></td>
                                             <td><?php echo $issue->name; ?></td>
                                             <td><?php echo $issue->date_reported; ?></td>
                                          </tr>
                                          <?php endforeach; ?>
                                    </tbody>
                                 </table>
                              </div>

                           </div>
                        </div>
                     </div>
                     <!-- Row End -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
            
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
    </body>
</html>
<!-- Ckeditor  -->
<script src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js" ></script>
<!-- Ad Tags  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.tagsinput.min.js"></script>
<!-- DROPZONE JS  -->
<script src="<?php echo base_url(); ?>assets/js/dropzone.js" ></script>
<script src="<?php echo base_url(); ?>assets/js/form-dropzone.js" ></script>
<script type="text/javascript">
      "use strict";
      
      
      /*--------- Textarea Ck Editor --------*/
        // CKEDITOR.replace( 'editor1' );
       
      /*--------- Ad Tags --------*/ 
       $('#tags').tagsInput({
            'width':'100%'
       });
      
         /*--------- create remove function in dropzone --------*/
      Dropzone.autoDiscover = false;
      var acceptedFileTypes = "image/*"; //dropzone requires this param be a comma separated list
      var fileList = new Array;
      var i = 0;

      // Create the mock file:
      var existingUploads = $.post( "<?php echo base_url('dealer/postdeal/getdealsupload/'.$id); ?>");
      
         $("#dropzone").dropzone({
           addRemoveLinks: true,
           maxFiles: 3, //change limit as per your requirements
         acceptedFiles: '.jpeg,.jpg,.png,.gif,.JPEG,.GIF',
           dictMaxFilesExceeded: "Maximum upload limit reached",
           acceptedFiles: acceptedFileTypes,
         url: "<?php echo base_url('dealer/postdeal/managedealsupload/'.$id.'/'.$code); ?>",
           dictInvalidFileType: "upload only JPG/PNG",
           init: function () {
               var images;
               var drop = this;
               existingUploads.done(function( data ) {
                  images = jQuery.parseJSON( data );
                  $.each(images,function(key,image){
                     drop.emit("addedfile", image);
                     drop.emit("thumbnail", image, image.url);
                     drop.emit("complete", image);
                     drop.options.maxFiles = drop.options.maxFiles - 1;
                  });
                  
               });
               // Hack: Add the dropzone class to the element
               $(this.element).addClass("dropzone");

               this.on("maxfilesexceeded", function(file){
                   alert("Maximum of 3 files only!");
                   this.removeFile(file);
                   drop.options.maxFiles = 3;
               });
           },
           removedfile: function(file) {   
              $.ajax({
                   type: 'POST',
                   url: "<?php echo base_url('dealer/postdeal/deletedealsupload/'.$id); ?>",
                   data: "file_name="+file.name,
                   dataType: 'html'
               });
              this.options.maxFiles = this.options.maxFiles + 1;
              var _ref;
                  return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
              
          }
         });
       (jQuery);
</script>
<script type="text/javascript">
   $(document).ready(function(){ 
      $('#category').change(function(){
         $("#subcategory > option").remove(); 
         var category = $('#category').val(); 
         //All Category
         var op = $('<option />');
         op.val("0");
         op.text("All Category");
         $('#subcategory').append(op);
         
         $.post("<?php echo base_url('dealer/postdeal/getsubcategory'); ?>",{category: category})
         .done(function(data){

            $.each(data,function(code,desc) 
            {
               console.log(desc);
               var opt = $('<option />');
               opt.val(code);
               opt.text(desc);
               $('#subcategory').append(opt); 
            });
         });
      });

   });
</script>
 