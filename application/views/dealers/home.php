<?php $this->load->view("layouts/header"); ?>
<!-- =-=-=-=-=-=-= How It Work =-=-=-=-=-=-= -->
<section class="section-padding white">





   <!-- Main Container -->
   <div class="container-fluid">
      <!-- Row -->
      <div class="row">


         <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
            <div class="wrap_ads text-center">
               <a href="#"><img src="https://admin.gunsalefinder.com/images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif"></a>
            </div>
         </div>


         <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
            <!-- Heading Area -->
            <div class="heading-panel">
               <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                  <!-- Main Title -->
                  <h1>How It <span class="heading-color"> Works</span></h1>
                  <!-- Short Description -->
                  <p class="heading-text long-heading">
                     This is an online marketplace that strives to offer meaningful content to Sportsmen and Firearm enthusiasts. Gun Sale Finder offers each visitor a way to compare prices and item availability in real time resulting in the best deal possible for both buyers and sellers. 
                     Sellers can post deals and see where they are rated among their peers. Buyers can see deals and rate them as per their preference. In this way, an open and transparent communication is available for all users.
                     </br></br>
                     We do not endorse the companies listed here. Rather it reflects the customers’ opinion on what they think and feel about the prices offered for items and the deals presented.
                  </p>
               </div>
            </div>

            <!-- Middle Content Box -->
            <div class="col-xs-12 col-md-12 col-sm-12 ">
               <div class="row">
                  <div class="how-it-work text-center">
                     <div class="how-it-work-icon"> <i class="flaticon-people"></i> </div>
                     <h4>Create Your Account</h4>
                     <p>
                        By having a dealer account you can Post Ads, advertise your products and get Detailed Analysis on where you are in the market. Registered users have the ability to provide their comments by rating each product or deal, participating in the forum, and thereby affecting the status of the marketplace.
                     </p>
                  </div>
                  <div class="how-it-work text-center ">
                     <div class="how-it-work-icon"> <i class="flaticon-people-2"></i> </div>
                     <h4>Stream Your Product</h4>
                     <p>
                        Get more sales by streaming your catalogue on our marketplace. Stream your entire online store. Make your item searchable where motivated buyers gather.
                     </p>
                     <p>
                        To learn on how stream your products on our website-<a href="<?php echo base_url('api-guide'); ?>">Click here. </a>
                     </p>
                  </div>
                  <div class="how-it-work text-center">
                     <div class="how-it-work-icon "> <i class="flaticon-heart-1"></i> </div>
                     <h4>Participate in the Marketplace</h4>
                     <p>
                        Sit back and reap the benefit of increased traffic. While we do the advertising, gathering all interested buyers in one place, you can focus on providing your services in the best possible ways.
                     </p>
                  </div>
               </div>
            </div>
            <!-- Middle Content Box End -->
         </div>


         <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
            <div class="wrap_ads text-center">
               <a href="#"><img src="https://admin.gunsalefinder.com/images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif"></a>
            </div>
         </div>
         

      </div>
   </div>


      </br>
      <div class="row">
         <center>
               <a href="<?php echo base_url('dealer/subscribe'); ?>" class="btn btn-theme">Subscribe Now <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
         </center>
      </div>
      <!-- Row End -->
   </div>
   <!-- Main Container End -->
</section>
<!-- =-=-=-=-=-=-= How It Work End =-=-=-=-=-=-= -->

      <?php $this->load->view("layouts/footer"); ?>
    </body>
</html>
<?php $this->load->view("registration/script");?>