<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">My Deals</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-md-4 col-sm-12 col-xs-12 leftbar-stick blog-sidebar">
               <?php  foreach ($info as $users): ?>
                  <?php include_once('menu.php'); ?>
               <?php endforeach; ?>
                  </div>
                  <div class="col-md-8 col-sm-12 col-xs-12">
                     <!-- Row -->
                     <div class="row">
                        <!-- Sorting Filters -->
                        <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 heading">
                        <?php
                           echo $this->session->flashdata('message');  
                        ?>
                           <!-- Advertisement Panel -->
                           <div class="panel panel-default">
                              <div class="panel-heading" >
                                 <div class="col-md-4 col-sm-4 col-xs-4">
                                    <h4 class="panel-title">
                                       <a>
                                       My Deals
                                       </a>
                                    </h4>
                                 </div>
                                 <div class="col-md-8 col-sm-4 col-xs-4">
                                    <!--<div class="search-widget pull-right">
                                       <input placeholder="search" type="text">
                                       <button type="submit"><i class="fa fa-search"></i></button>
                                    </div>-->
                                 </div>
                              </div>
                           </div>
                           <!-- Advertisement Panel End -->
                        </div>
                        <!-- Sorting Filters End-->
                        <div class="clearfix"></div>
                        <!-- Ads Archive -->
                        <div class="posts-masonry">
                           <div class="col-md-12 col-xs-12 col-sm-12 user-archives">
                              <?php echo (count($results) == 0) ? '<h3>No deals Found.</h3>' : ''; ?>
                              <!-- Ads Listing -->
                           <?php  foreach ($results as $product): ?>
                              <div class="ads-list-archive">
                                 <!-- Image Block -->
                                 <div class="col-lg-3 col-md-3 col-sm-3 no-padding">
                                    <!-- Img Block -->
                                    <div class="ad-archive-img">
                                       <a href="#">
                                          <?php 
                                             switch ($product->status) {
                                                 case "I":
                                                     $status = "Out of Stock";
                                                     break;
                                                 case "E":
                                                     $status = "Expired";
                                                     break;
                                                 default:
                                                     $status = "In Stock";
                                             }
                                          ?>
                                          <div class="ribbon expired">
                                             <small style="font-size: 65%;"><?php echo $status; ?></small>
                                          </div>
                                             <img src="<?php echo ($product->product_image == null) ? $this->common->update_image($product->id, $product->upc, $product->api_created) : $product->product_image; ?>" alt="">
                                       </a>
                                    </div>
                                    <!-- Img Block -->   
                                 </div>
                                 <!-- Ads Listing -->    
                                 <div class="clearfix visible-xs-block"></div>
                                 <!-- Content Block -->     
                                 <div class="col-lg-9 col-md-9 col-sm-9 no-padding">
                                    <!-- Ad Desc -->     
                                    <div class="ad-archive-desc">
                                       <!-- Price -->    
                                       <div class="wrap_adPrice">
                                          <label><?php echo ($product->price != NULL)? '$'.number_format($product->price) :''; ?></label>
                                       </div>
                                       <!-- Title -->    
                                       <h3><?php echo ucwords(strtolower($product->title)); ?></h3>
                                       <!-- Category -->
                                       <div class="category-title">
                                          <label><?php echo $product->category_code; ?></label>
                                          <span> Updated: <?php echo $this->common->timetoago($product->created_at); ?></span>
                                       </div>
                                       <!-- Short Description -->
                                       <div class="clearfix visible-xs-block"></div>
                                       <!-- Ad Features -->
                                       <!-- Ad History -->
                                       <div class="clearfix archive-history">
                                          <div class="wrap_btnMyDeals">
                                              <a class="btn btn-success" target="blank" href="/product/viewProduct?id=<?php echo $product->id; ?>"><i class="fa fa-eye"></i> View Details
                                              </a>
                                             <!-- <a class="btn btn-success viewProduct" data-id="<?php echo $product->id; ?>"><i class="fa fa-eye"></i> View Details
                                             </a> -->
                                          </div>
                                       </div>
                                    </div>
                                    <!-- Ad Desc End -->     
                                 </div>
                                 <!-- Content Block End --> 
                              </div>
                           <?php endforeach; ?>
                              <!-- Ads Listing -->
                           </div>
                        </div>
                        <!-- Ads Archive End -->  
                        <div class="clearfix"></div>
                        <!-- Pagination -->  
                        <div class="col-md-12 col-xs-12 col-sm-12">
                           <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <!-- Pagination End -->   
                     </div>
                     <!-- Row End -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
    </body>
</html>