<div class="user-profile">
<?php 
   if($users->picture == ''){
      $images = base_url().'assets/images/user-profile.png';
   } else{
      $images = base_url().'assets/pictures/'.$users->picture;
   }
?>
   <div class="wrap_imgUserProfile">
      <a href="<?php base_url('dealer/profile'); ?>"><img src="<?php echo $images; ?>" alt="Profile Picture"></a>
   </div>
   

   <div class="profile-detail">
      <h6><?php echo $users->first_name; ?></h6>
      <ul class="contact-details">
         <li>
            <i class="fa fa-map-marker"></i> USA
         </li>
         <li>
            <i class="fa fa-envelope"></i>
            <?php echo $users->email; ?>
         </li>
         <li>
            <i class="fa fa-phone"></i> <?php echo $users->contact; ?>
         </li>
      </ul>
   </div>
   <ul>
        <li class="<?php echo ($this->uri->segment(2) == 'profile') ? 'active' : ''; ?>"><a href="<?php echo base_url('dealer/profile'); ?>">Profile</a></li>
        <li class="<?php echo ($this->uri->segment(1) == 'dealer-deals') ? 'active' : ''; ?>">
            <a href="<?php echo base_url('dealer-deals'); ?>">My Deals 
                <span class="badge">
                    <?php echo $this->common->countmyProduct(); ?>
                </span>
            </a>
        </li>

        <li class="<?php echo ($this->uri->segment(1) == 'dealer-wishlist') ? 'active' : ''; ?>">
            <a href="<?php echo base_url('dealer-wishlist'); ?>">My Wishlist
                <span class="badge">
                    <?php echo $this->common->countmyWishlist(); ?>
                </span>
            </a>
        </li>

        <li class="<?php echo ($this->uri->segment(1) == 'my-ads') ? 'active' : ''; ?>">
            <a href="<?php echo base_url('my-ads'); ?>">My Ads
                <span class="badge">
                    <?php echo $this->common->countmyAds(); ?>
                </span>
            </a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'pending-ads') ? 'active' : ''; ?>">
            <a href="<?php echo base_url('pending-ads'); ?>">
                Pending Ads
                <span class="badge">
                    <?php echo $this->common->countmyPendingAds(); ?>
                </span>
            </a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'reported-ads') ? 'active' : ''; ?>">
            <a href="<?php echo base_url('reported-ads'); ?>">
                Reported Deals
                <span class="badge">
                    <?php echo $this->common->countmyReportDeals(); ?>
                </span>
            </a>
        </li>
        <li><a href="<?php echo base_url('login/logout'); ?>">Logout</a></li>
   </ul>
</div>