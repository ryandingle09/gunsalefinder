﻿<?php $this->load->view("layouts/header"); ?>
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">Price Comparison</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="gray">
            <!-- Main Container -->


            <div class="row">

              <div class="margin-top-30"></div>
              
              <div class="col-md-2 hidden-sm hidden-xs">
                  <div class="wrap_ads text-center">
                     <a href="#"><img src="https://admin.gunsalefinder.com/images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif"></a>
                  </div>
               </div>


              <div class="col-md-8 col-sm-12 col-xs-12">
               <div class="alert-box-container">
                  <div class="well">
                     <h2>Search results for "<?=$upc;?>"</h2>
                     <p>Listing <?=count($results);?> deals total</p>
                  </div>
               </div>
               <div class="alert-box-container  margin-top-30">
                  <div class="well">
                  <?php 
                  $i= 0;
                    foreach ($results as $detail): 
                      if($i == 1){
                        break;
                      }
                  ?>
                        <div class="row">
                           <div class="col-lg-3 col-md-4 col-xs-12 col-sm-12">
                              <div class="wrap_imgCompare">
                                <img src="<?php echo $detail->product_image; ?>" alt="" onerror="this.src='<?=base_url('assets/pictures/default.jpg')?>'" width="auto">
                              </div>
                           </div>
                           <div class="col-lg-9 col-md-8 col-xs-12 col-sm-12" style="height:155px;">
                              <h2 class="main-title" style="top:50%;font-size: 18px;">
                                <a class="black-text" href="<?php echo base_url('product/viewProduct?id='.$detail->id); ?>" target="_blank">
                                  <?php echo $detail->title; ?>
                                </a>
                              </h2>
                           </div>
                           <!-- <div class="col-md-3 col-xs-12 col-sm-12">
                            <button class="btn btn-default btn-block">
                              <i class="fa fa-bell"></i> Follow product sitewide
                            </button>
                           </div> -->
                        </div>
                  <?php 
                    $i = $i+1;
                    endforeach; 
                  ?>
                  </div>
               </div>

               <!-- Row -->
               <div class="row">
                  <div class="col-md-12 col-xs-12 col-sm-12">
                  <section class="search-result-item rmv_padding_table">
                   <div class="heading-panel">
                      <dl class="legend-dl pull-right">
                         <dt style="background-color: #232323;">&nbsp;              &nbsp;              &nbsp;</dt>
                          <dd> In Stock </dd>

                          <dt style="background-color: red;">&nbsp;              &nbsp;              &nbsp;</dt>
                          <dd> Out of Stock </dd>
                      </dl>
                     <h3 class="text-left add_padding_title">
                        Price Compare
                     </h3>
                     <!-- <div class="alert-box-container  margin-top-30">
                         <div class="well">
                              <div class="row">
                                 <div class="col-md-3">
                                 </div>
                                 <div class="col-md-6 col-xs-12 col-sm-12"  align="center">
                                    <div class="col-md-4">
                                      Temporary ZIP Code:
                                    </div>
                                    <div class="col-md-5">
                                      <input placeholder="" type="text" class="form-control"> 
                                    </div>
                                    <div class="col-md-3">
                                      <input class="btn btn-theme btn-block" value="Apply" type="submit"> 
                                      
                                    </div>
                                 </div>
                                 <div class="col-md-3">
                                 </div>
                                 
                              </div>
                              <p class="text-center">Set temporary zip code for calculating store tax.<br/>
                              For permanent setup, go to your account edit page and type in your zip code.</p> 
                        </div>
                     </div> -->
                   </div>




                    <div class="compare-prices">
                        <div class="table-responsive">
                          <table class="table table-action">
                            <thead class="black-white-text">
                              <tr>
                                <th>Retailer</th>
                                <th>Price</th>
                                <th>Shipping rate</th>
                                <th>Retailer website</th>
                              </tr>
                            </thead>
                            <tbody class="">
                            <?php  foreach ($results as $product): ?>
                            <?php 
                                $class = ($product->qty > 0) ? 'color: #232323;' : 'color: red;';
                                $dealer = is_null($product->dealer_info) ? null : json_decode($product->dealer_info, true);
                            ?>
                              <tr>
                                <td class="setTDwidth">
                                  <a href="<?php echo isset($dealer)? $dealer[0]['web_url'] : $this->common->getDealerCompanyUrl($product->dealer_id); ?>" target="_blank" ><?php echo isset($dealer)? $dealer[0]['company_name'] : $this->common->getDealerCompanyName($product->dealer_id); ?>
                                  </a>
                                </td>
                                <td>
                                  <label class="hidden-lg hidden-md hidden-sm lbl_tableList">Price:</label><span style="<?php echo $class; ?>"><?php echo $product->price; ?></span>
                                </td>
                                <td>
                                  <label class="hidden-lg hidden-md hidden-sm lbl_tableList">Shipping Rate:</label>
                                  <span style="<?php echo $class; ?>"><?php echo $product->shipping_fee; ?></span>
                                </td>
                                <td>
                                    
                                    <a href="<?php echo $product->item_url; ?>" class="label label-danger" target="_blank">Click to View</a>
                                 </td>
                              </tr>
                            <?php endforeach; ?>
                            </tbody>
                          </table>
                        </div>
                    </div>


                  </section>
                  </div>
               </div>
               <!-- Row End -->
            </div>

            <div class="col-md-2 hidden-sm hidden-xs">
                <div class="wrap_ads text-center">
                   <a href="#"><img src="https://admin.gunsalefinder.com/images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif"></a>
                </div>
             </div>

            </div>

            <section class="advertizing custom-padding gray" style="margin-top: -10px;">
                <!-- Main Container -->
                <div class="container">
                   <!-- Row -->
                   <div class="row">
                      <div class="col-md-12 col-xs-12 col-sm-12">
                         <div class="banner">
                            <a href="#" id="footerLink" target="_blank">
                              <img src="<?php echo ADMINURL; ?>images/sliders/banner-1.jpg" alt="Slider" id="footerImage" style="width : 680px; height: 90px"></a>
                         </div>
                      </div>
                   </div>
                   <!-- Row End -->
                </div>
                <!-- Main Container End -->
            </section>
         </section>
         
      <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
    </body>
</html>
<?php $this->load->view("compare/script"); ?>