<?php
    $link_url = '';
    $apiCreated = isset($api_created) ? $api_created : 1; 
    if($apiCreated == 1)
    {
        $image_url = $this->common->getProductImagebyUPC($upc);
    } else {
        $image_url = $this->common->dealImagesLink($id);
    }
    
    $image_url = ($image_url != '') ? $image_url : '';
?>
<!-- <div class="item cursorIcon"> -->
    <div class="col-lg-3 col-md-4 col-xs-12 col-sm-12">
        <!-- Ad Box -->
        <div class="category-grid-box">
            <!-- Ad Img -->
            <div class="category-grid-img " onClick="gotoProduct(<?= $product_id ?>)">

               <div class="wrap_img_prod">
                  <img class="feature-img-size loadingImage" src="<?=base_url(). 'assets/pictures/loading.gif'?>"  data-image="<?=$images[0];?>" alt="<?=$title;?>" />

                  <!-- <img src="http://media.server.theshootingwarehouse.com/small/24048.jpg"> -->

               </div>

                
                

                <!-- Ad Status 
                <span class="ad-status"> Hot Deals </span>-->
                <!-- User Review -->
                <!-- <div class="user-preview"><a href="#"><img src="assets/images/users/1.jpg" class="avatar avatar-small" alt=""></a></div>-->
                <!-- View Details -->
                <a class="view-details " href="<?=$link_url?>">View Details</a>
                <!-- Additional Info -->
                <div class="additional-information">
                    <!-- <p> SKU#:  </p>
                    <p> UPC#:  </p>
                     <p> 100-006-987WB</p>
                    <p> 30-Round PMAG, 10-Pack, Black</p>
                    <p> 30-round capacity</p>
                    <p> Stainless steel spring</p> -->
                </div>
                <!-- Additional Info End-->
            </div>
            <!-- Ad Img End -->
            <div class="short-description">
                <!-- Ad Category -->
                <div class="category-title">
                    <span>
                        <a href="#"><?=$this->common->getCategoryname($cat_id)?></a>
                    </span>
                </div>
                <!-- Ad Title -->
                <p>
                  <a href="<?php echo base_url('product/viewProduct?id='.$product_id) ?>"><?= $title ?></a>
                </p>
                <!-- Price -->
                <div class="price">$<?=$price?>
                    <!-- <span class="negotiable">( 30% off )</span> -->
                </div>
            </div>
            <!-- Addition Info -->
            <div class="ad-info">
                <ul>
                    <li>
                        <?= $this->common->getDealerCompanyName($dealer_id) ?>
                    </li>
                    <li>
                      <a onClick="likeItem(this, <?php echo $product_id ?>)" title="Like This Deal">
                        <i class="fa fa-thumbs-o-up <?php echo $this->common->isProductLike($product_id) ?> like<?php echo $product_id ?>" aria-hidden="true"></i>
                        <span>10</span>
                      </a>
                      &nbsp;
                      <a onClick="dislikeItem(this, <?php echo $product_id ?>)" title="Dislike This Deal">
                        <i class="fa fa-thumbs-o-down <?php echo $this->common->isProductDislike($product_id) ?> dislike<?php echo $product_id ?>" aria-hidden="true"></i>
                        <span>11</span>
                      </a>
                      &nbsp;
                      <a href="<?php echo base_url('product/viewProduct?id='.$product_id) ?>#comments" title="Comments">
                        <i class="fa fa-comment-o" aria-hidden="true"></i>
                        <span>14</span>
                      </a>
                      &nbsp;
                      <a onClick="subsPDC(this, <?php echo $product_id ?>)" title="Subscribe for Price Drop and Comments">
                        <i class="fa fa-bell-o <?php echo $this->common->isProductSubs($product_id) ?>" aria-hidden="true"></i>
                        <span>16</span>
                      </a>
                      <a href="#">
                          <i class="fa fa-eye"></i>
                          <span>20</span>
                      </a>
                    </li>
                        <!-- ..TODO -->
                        <!-- <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <span class=  "rating-count">(2)</span>
                        </div> -->
                </ul>
            </div>
        </div>
        <!-- Ad Box End -->
    </div>
<!-- </div> -->
