<?php
    $link_url = '';
    $apiCreated = isset($api_created) ? $api_created : 1; 
    if($apiCreated == 1)
    {
        $image_url = $this->common->getProductImagebyUPC($upc);
    } else {
        $image_url = $this->common->dealImagesLink($id);
    }
    $image_url = ($image_url != '') ? $image_url : '';
?>
<div class="col-lg-3 col-md-4 col-xs-12 col-sm-12 wrap_itemsList">
    <div class="category-grid-box">
        <div class="category-grid-img" onClick="gotoProduct(<?= $product_id ?>)">
            <div class="wrap_img_prod">
                <img class="feature-img-size loadingImage" src="<?=base_url(). 'assets/pictures/loading.gif'?>"  data-image="<?=(isset($images[0])) ? $images[0] : '';?>" alt="<?=$title;?>" />
            </div>
            <a class="view-details" href="<?=$link_url?>">View Details</a>
            <div class="additional-information"></div>
        </div>
        <div class="short-description">
            <div class="category-title">
                <span> <a href="#"><?=$this->common->getCategoryname($cat_id)?></a></span>
            </div>
            <p><a href="<?php echo base_url('product/viewProduct?id='.$product_id) ?>"><?= $title ?></a></p>
            <div class="price">$<?=$price?></div>
        </div>
        <div class="ad-info">
            <ul>
                <li>
                   <?= $this->common->getDealerCompanyName($dealer_id) ?>
                </li>
                <li>
                    <a onClick="likeItem(this, <?php echo $product_id ?>)" title="Like This Deal">
                        <i class="fa fa-thumbs-o-up <?php echo $this->common->isProductLike($product_id) ?> like<?php echo $product_id ?>" aria-hidden="true"></i>
                        <span>11</span>
                    </a>
                    &nbsp;
                    <a onClick="dislikeItem(this, <?php echo $product_id ?>)" title="Dislike This Deal">
                        <i class="fa fa-thumbs-o-down <?php echo $this->common->isProductDislike($product_id) ?> dislike<?php echo $product_id ?>" aria-hidden="true"></i>
                        <span>15</span>
                    </a>
                    &nbsp;
                    <a href="<?php echo base_url('product/viewProduct?id='.$product_id) ?>#comments" title="Comments">
                        <i class="fa fa-comment-o" aria-hidden="true"></i>
                        <span>18</span>
                    </a>
                    &nbsp;
                    <a onClick="subsPDC(this, <?php echo $product_id ?>)" title="Subscribe for Price Drop and Comments">
                        <i class="fa fa-bell-o <?php echo $this->common->isProductSubs($product_id) ?>" aria-hidden="true"></i>
                        <span>20</span>
                    </a>
                    <a href="#">
                        <i class="fa fa-eye"></i>
                        <span>22</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>