<script type="text/javascript">
	$('.sortCategory').click(function () {
        var catCode = $(this).attr("data-catCode");
        var ifHaveChild = $(this).next('ul').length; 
        if (ifHaveChild == 0 ) window.location.href = '<?=base_url()?>' + 'search/index/created_at/asc/'+ catCode+'/0';
	});

    $('.sortSubCategory').click(function () {
        var catCode = $(this).attr("data-catCode");
        var subCatCode = $(this).attr("data-subCatCode");
        var ifHaveChild = $(this).next('ul').length; 
        if (ifHaveChild == 0 ) window.location.href = '<?=base_url()?>' + 'search/index/created_at/asc/'+ catCode +'/'+subCatCode+'/0';
    });

	/** DEALS */
	var expiredStorage = new ExpiredStorage();

	var feature = expiredStorage.getItem("feature");
	var banner_HTC = expiredStorage.getItem("banner_HTC");
	var banner_HMS = expiredStorage.getItem("banner_HMS");
	var banner_HLS = expiredStorage.getItem("banner_HLS");
	var banner_HRS = expiredStorage.getItem("banner_HRS");
	var banner_HF = expiredStorage.getItem("banner_HF");

	$.ajax({url : '/home/get_content/?page=popular_deals&offset=0', success : function(result){
		$('#popular_deals').html(result);
		manual_load();
	}});

	if(feature !== null)
	{
		featureSlider(feature);
	}
	else
	{
		$.ajax({url : '/home/get_content/?page=feature', success : function(result){
			featureSlider(result);
		}});
	}

	$.ajax({url : '/home/get_content/?page=today_deals&offset=0', success : function(result){
		$('#today_deals').html(result);
		manual_load();
	}});
/** END DEALS */

/** BANNERS */
	
	if(banner_HTC !== null)
	{
		rotate_banner(JSON.parse(banner_HTC));
	}
	else
	{
		$.ajax({url : '/home/getBanners/?location_code=HTC', success : function(result){
			rotate_banner(JSON.parse(result));
		}});
	}
	
	if(banner_HMS !== null)
	{
		rotate_banner(JSON.parse(banner_HMS), 1);
	}
	else
	{
		$.ajax({url : '/home/getBanners/?location_code=HMS', success : function(result){
			rotate_banner(JSON.parse(result), 1);
		}});
	}
	
	if(banner_HF !== null)
	{
		rotate_banner(JSON.parse(banner_HF), 2);
	}
	else
	{
		$.ajax({url : '/home/getBanners/?location_code=HF', success : function(result){
			rotate_banner(JSON.parse(result), 2);
		}});
	}
	
	if(banner_HLS !== null)
	{
		rotate_banner_with_style(JSON.parse(banner_HLS), 3);
	}
	else
	{
		$.ajax({url : '/home/getBanners/?location_code=HLS', success : function(result){
			rotate_banner_with_style(JSON.parse(result), 3);
		}});
	}

	if(banner_HRS !== null)
	{
		rotate_banner_with_style(JSON.parse(banner_HRS), 4);
	}
	else
	{
		$.ajax({url : '/home/getBanners/?location_code=HRS', success : function(result){
			rotate_banner_with_style(JSON.parse(result), 4);
		}});
	}

	/** END BANNERS */

	$(function(){

		$(document).on('click', '.loadmoredeals', function(e){
			e.preventDefault();

			var loader = '<div class="col-md-12" style="margin: 0px auto;padding: 0px auto;margin-top:5%">';
				loader += '<div class="lds-facebook" style="margin: 0px auto;padding: 0px auto"><div></div><div></div><div></div></div>';
				loader += '</div>';
				
			var dis = $(this)

			var url = dis.children().find('.loadmore').attr('href');
			var type = dis.children().find('.loadmore').attr('data-type');

			$.ajax({
				url : url, 
				beforeSend: function(){
					dis.html(loader);
				},
				success : function(result){
				
					dis.remove();

					if(type == 'popular')
					{
						$('.populars:last').after(result);
					}

					if(type == 'today')
					{
						$('.todays:last').after(result);
					}
	
					manual_load();
			}});
			
		});

	});

    function gotoProduct(product_id)
    {
        window.location = '<?php echo base_url('product/viewProduct?id=') ?>'+product_id;
    }

	function subsPDC(elem,id)
	{	
		var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
		
		if(login == '0')
		{
			alert('Please login to subscribe in product.');
		}
		else
		{
			$.ajax({
				url: '<?php echo base_url('product/subsPDC') ?>',
				data: {product_id: id},
				type: 'POST',
				beforeSend: function()
				{
					$.blockUI();
				},
				success: function(data)
				{
					if(data == 'subs')
					{
						var dl_count = parseInt($('.s_count_'+id).html());
						$('.s_count_'+id).html(dl_count + 2);
						$(elem).children().addClass('liked');
					}
					else
					{
						var dl_count = parseInt($('.s_count_'+id).html());
						if(dl_count != 0)
							$('.s_count_'+id).html(dl_count - 2);
						$(elem).children().removeClass('liked');
					}

					$.unblockUI();
				}
			});
			
		}
		
	}

	function likeItem(elem,id)
	{	
		var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
		
		if(login == '0')
		{
			alert('Please login to like product.');
		}
		else
		{
			$.ajax({
				url: '<?php echo base_url('product/likeProduct') ?>',
				data: {product_id: id},
				type: 'POST',
				beforeSend: function()
				{
					$.blockUI();
				},
				success: function(data)
				{
					if(data == 'like')
					{
						
						$(elem).children().addClass('liked');
						var count = parseInt($(elem).children('span').html()) + 1;
						$(elem).children('span').html(count);

						$('.dislike'+id).removeClass('liked');
						var dl_count = parseInt($('.dl_count_'+id).html());

						if(dl_count != 0)
							$('.dl_count_'+id).html(dl_count - 1);
					}
					else
					{
						$(elem).children().removeClass('liked');
						var count = parseInt($(elem).children('span').html()) - 1;
						$(elem).children('span').html(count);
					}
					$.unblockUI();
				}
			});
			
		}
		
	}

	function dislikeItem(elem,id)
	{	
		var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
		
		if(login == '0')
		{
			alert('Please login to dislike product.');
		}
		else
		{
			$.ajax({
				url: '<?php echo base_url('product/checkifdislikeexist') ?>',
				data: {product_id : id},
				type: 'POST',
				beforeSend: function()
				{
					$.blockUI();
				},
				success: function(data)
				{
					if(data == 'dislike')
					{
						$('.like'+id+', .dislike'+id).removeClass('liked');

						var count = parseInt($('.dl_count_'+id).html());
						var count_dislike = count - 1;

						if(count !== 0)
							$('.dl_count_'+id).html(count_dislike);
							
						$.unblockUI();
					}
					else{
						$.unblockUI();
						$('#product_id').val(id);
						$('.dislike-product').modal('show');
					}
				}
			});
			
		}
	}

	function dislikeSubmit()
	{
		var id = $('#product_id').val();
		var dislike_type = $('input[name=disLikeType]:checked').val();
		var dislike_details = $('#dislikeDetails').val();

		if(dislike_type != '' && dislike_details != '')
		{
			$.ajax({
				url: '<?php echo base_url('product/dislikeProduct') ?>',
				data: {product_id: id, dislike_type: dislike_type, dislike_details: dislike_details},
				type: 'POST',
				beforeSend: function()
				{
					
					$('.dislike-product').modal('hide');
					$.blockUI();
				},
				success: function(data)
				{	
					if(data == 'dislike')
					{
						$('.dislike'+id).addClass('liked');
						$('.like'+id).removeClass('liked');

						var count_like = ($('.l_count_'+id+'').html() !== '0') ? parseInt($('.l_count_'+id+'').html()) - 1 : '0';
						var count_dislike = parseInt($('.dl_count_'+id+'').html()) + 1;

						$('.l_count_'+id).html(count_like);
						$('.dl_count_'+id).html(count_dislike);
					}
					else
					{
						$('.like'+id).removeClass('liked');
						$('.dislike'+id).removeClass('liked');
					}
					$.unblockUI();
					$('#dislikeDetails').val('');
				}
			});
		}
		else
		{
			alert('Please fill up the fields.');
		}
	}

	function featureSlider(result)
	{
		var list = '<div class="featured-sliders owl-carousel owl-theme">';
		$.each(JSON.parse(result), function(key, val){
			list += '<div class="item">';
			list += '	<div class="col-md-12 col-xs-12 col-sm-12 clearfix">';
			list += '		<div class="category-grid-box">';
			list += '			<div class="category-grid-img">';
			list += '				<div class="wrap_img_ads">';
			list += '					<a href="'+val.link_url+'">';
			list += '						<img class="loadingImage" data-image="'+val.image_url+'" src="'+val.image_url+'" alt="'+val.title+'">';
			list += '					</a>';
			list += '				</div>';
			list += '				<span class="ad-status"> Featured </span>';
			list += '				<a class="view-details " href="'+val.link_url+'" target="_blank">View Details</a>';
			list += '				<div class="additional-information"></div>';
			list += '			</div>';
			list += '			<div class="short-description">';
			list += '				<div class="category-title">';
			list += '					<span><a href="#">'+val.category_name+'</a></span>';
			list += '				</div>';
			list += '				<h3><a title="" href="'+val.link_url+'" target="_blank">'+val.title+'</a></h3>';
			list += '				<div class="price">$'+val.price+'</div>';
			list += '			</div>';
			list += '			<div class="ad-info">';
			list += '				<ul>';
			list += '					<li>';
			list += '						<i class="fa fa-building"></i>'+val.dealer+'';
			list += '					</li>';
			list += '				</ul>';
			list += '			</div>';
			list += '		</div>';
			list += '	</div>';
			list += '</div>';
		});
		list += '</div>';
		$('.feature').html(list);
		$(".featured-sliders").owlCarousel({loop:true,autoplay: true,dots:!1,margin:-10,responsiveClass:!0,navText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],responsive:{0:{items:1,nav:!0},1800:{items:2,nav:!0},1e3:{items:3,nav:!0,loop:true}}}).trigger('refresh.owl.carousel');
	}

	function rotate_banner_with_style(result, location = '')
	{
		var i = 1;
		var duration = 5000;

		if(result.length !== 0)
		{
			duration = result[0]['duration'];
			$('#bannerImage'+location+'').attr('style', 'background-image: url('+result[0]['image_url']+');height: 600px;background-repeat: no-repeat center;background-size: 100% 100%;');
			$('#bannerLink'+location+'').attr('href', result[0]['link_url']);
			var renew = setInterval(function(){
				if(result.length == i){
					i = 0;
				}
				else {
					$('#bannerImage'+location+'').attr('style', 'background-image: url('+result[i]['image_url']+');height: 600px;background-repeat: no-repeat center;background-size: 100% 100%;');
					$('#bannerLink'+location+'').attr('href', result[i]['link_url']);
					duration = result[i]['duration'];
					i++;

				}
			},duration);
		}
	}

	function rotate_banner(result, location = '')
	{
		var i = 1;
		var duration = 5000;

		if(result.length !== 0)
		{
			document.getElementById("bannerImage"+location+"").src = result[0]['image_url']; 
			document.getElementById("bannerLink"+location+"").href = result[0]['link_url']; 
			duration = result[0]['duration'];
			var renew = setInterval(function(){
				if(result.length == i){
					i = 0;
				}
				else {
					document.getElementById("bannerImage"+location+"").src = result[i]['image_url']; 
					document.getElementById("bannerLink"+location+"").href = result[i]['link_url']; 
					duration = result[i]['duration'];
					i++;

				}
			},duration);
		}
	}

	function manual_load()
	{
		$(".loadingImage").each(function() {
			var  image_url =  $(this).attr("data-image");
			console.log('GET LINK');
			$(this).attr('src', $(this).attr("data-image")); 
			console.log($(this).attr("data-image"));
			$(".loadingImage").error(function(err){
				$(this).attr('src',  '<?=base_url()?>' + 'assets/pictures/default.jpg' );
			});

			var source = $(this);

			$(this).parent('div, a, td').zoom({
				url: image_url,
				callback: function(){
					var toBeHeight = Number(this.height) > Number(source.height()) ?  this.height : source.height();
					var toBeWidth = Number(this.width) > Number(source.width()) ?  this.width : source.width();

					console.log('toBeHeight.height()');
					console.log(toBeHeight);

					$(this).css({
						'width' : toBeWidth+'px',
						'height' : toBeHeight+'px',
					});
				},
				magnify: '0.1%',
				onZoomOut: true,
				onZoomIn: true,
			});
		});
	}
      
</script>