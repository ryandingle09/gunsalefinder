<?php $this->load->view("layouts/header"); ?>
         <section class="advertizing">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <?php echo $this->session->flashdata('message'); ?>
               <div class="row">
                  <div class="col-md-12 col-xs-12 col-sm-12">
                     <div class="banner">
                        <a href="#" id="bannerLink" target="_blank">
                          <img id="bannerImage" src="<?php echo ADMINURL; ?>images/sliders/banner-1.jpg"  alt="Slider" style="width : 728px; height: 90px"></a>
                     </div>
                  </div>
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=- -->
          <section class="custom-padding gray">
            <!-- Main Container -->
            <div class="container full-width"">
               <!-- Row -->
               <div class="row">
                  <!-- Heading Area -->
                  <div class="heading-panel">
                     <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                        <!-- Main Title -->
                        <h2><span class="heading-color"> Popular</span> Deals</h2>
                     </div>
                  </div>
                  <!-- Middle Content Box -->

                  <div class="col-md-1 no-padding banner-side-container">
                    <a id="bannerLink3" href="<?php echo base_url('dealer-post'); ?>" target="_blank">
                      <div id="bannerImage3" style="background-image: url(<?php echo ADMINURL; ?>images/banners/1539445993ThompsonLehAnimatedBanner160x600.gif); 
                        height: 600px;
                        background-repeat: no-repeat center;
                        background-size: 100% 100%;">  
                      </div>
                    </a>
                  </div>
  
                  <!-- Middle Content Box -->
                  <div class="col-md-10 col-xs-12 col-sm-12">
                    <div class="row" id="popular_deals">
                      <div class="col-md-12" style="margin: 0px auto;padding: auto">
                        <div class="lds-facebook" style="margin: 0px auto;padding: 0px auto"><div></div><div></div><div></div></div>
                      </div>
                    </div>
                  </div>
                  <!-- Middle Content Box End -->

                  <div class="col-md-1 no-padding banner-side-container">
                    <a id="bannerLink4" href="<?php echo base_url('dealer-post'); ?>" target="_blank">
                      <div id="bannerImage4" style="background-image: url(<?php echo ADMINURL; ?>images/banners/1539445993ThompsonLehAnimatedBanner160x600.gif); 
                        height: 600px;
                        background-repeat: no-repeat center;
                        background-size: 100% 100%;">  
                      </div>
                    </a>
                  </div>
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>



         <!-- Main Section -->
         <!-- =-=-=-=-=-=-= Featured Ads =-=-=-=-=-=-= -->
         <section class="custom-padding">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Heading Area -->
                  <div class="heading-panel">
                     <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                        <!-- Main Title -->
                        <h2><span class="heading-color"> Featured</span> Deals</h2>
                     </div>
                  </div>
                  <!-- Middle Content Box -->
                  <div class="col-md-12 col-xs-12 col-sm-12">
                     <div class="row">
                        <div class="feature"></div>
                     </div>
                  </div>
                  <!-- Middle Content Box End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>



         <!-- =-=-=-=-=-=-= Featured Ads End =-=-=-=-=-=-= -->
          <section class="advertizing custom-padding gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <div class="col-md-12 col-xs-12 col-sm-12">
                     <div class="banner">
                        <a href="#" id="bannerLink1" target="_blank">
                          <img id="bannerImage1" src="<?php echo ADMINURL; ?>images/sliders/banner-1.jpg" alt="Slider" style="width : 680px; height: 90px"></a>
                     </div>
                  </div>
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>



         <!-- =-=-=-=-=-=-= Trending Ads =-=-=-=-=-=-= -->
         <section>
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Heading Area -->
                  <div class="heading-panel">
                     <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                        <!-- Main Title -->
                        <br>
                        <h2><span class="heading-color">  Today's</span> Deals</h2>
                        <!-- Short Description -->
                        <!--<p class="heading-text">Latest Item On Sale You Won’t Want to Miss.</p>-->
                     </div>
                  </div>
                  <!-- Middle Content Box -->
                  <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="row" id="today_deals">
                      <div class="col-md-12" style="margin: 0px auto;padding: auto">
                        <div class="lds-facebook" style="margin: 0px auto;padding: 0px auto"><div></div><div></div><div></div></div>
                      </div>
                     </div>
                  </div>
                  <!-- Middle Content Box End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>


         <div class="clearfix_row_2"></div>


         <section class="advertizing custom-padding gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <div class="col-md-12 col-xs-12 col-sm-12">
                     <div class="banner">
                        <a href="#" id="bannerLink2" target="_blank">
                          <img id="bannerImage2" src="<?php echo ADMINURL; ?>images/sliders/banner-1.jpg" alt="Slider" style="width : 680px; height: 90px"></a>
                     </div>
                  </div>
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
</html>
<?php $this->load->view("layouts/footer"); ?>
<?php $this->load->view("home/script"); ?>
