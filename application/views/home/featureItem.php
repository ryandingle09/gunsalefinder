<div class="item">
    <div class="col-md-12 col-xs-12 col-sm-12 clearfix">
        <!-- Ad Box -->
        <div class="category-grid-box">
            <!-- Ad Img -->
            <div class="category-grid-img">

                <div class="wrap_img_ads">
                    <?php if(is_null($image_url)):?>
                        <img src="<?php echo ADMINURL.'images/banners/1539619369GSFOnlineMarketplaceBlack313x234.jpg'; ?>" alt="No image uploaded">
                    <?php else:?>
                        <?php if(file_exists($image_full_path)):?>
                            <a href="<?php echo (!is_null($link_url)) ? $link_url : '#'; ?>">
                                <img class="loadingImage" data-image="/<?php echo $image_url; ?>" src="/<?php echo $image_url; ?>" alt="<?php echo $title; ?>">
                            </a>
                        <?php else:?>
                            <a href="<?php echo (!is_null($link_url)) ? $link_url : '#'; ?>">
                                <img class="loadingImage" data-image="<?php echo ADMINURL.$image_url; ?>" src="<?php echo ADMINURL.$image_url; ?>" alt="<?php echo $title; ?>">
                            </a>
                        <?php endif;?>
                    <?php endif;?>
                </div>
                

                <!-- Ad Status -->
                <span class="ad-status"> Featured </span>
                <!-- User Review -->
                <!-- <div class="user-preview"><a href="#"><img src="assets/images/users/1.jpg" class="avatar avatar-small" alt=""></a></div>-->
                <!-- View Details -->
                <a class="view-details " href="<?php echo (!is_null($link_url)) ? $link_url : '#'; ?>" target="_blank">View Details</a>
                <!-- Additional Info -->
                <div class="additional-information">
                    <!-- <p> SKU#:  </p>
                    <p> UPC#:  </p>
                     <p> 100-006-987WB</p>
                    <p> 30-Round PMAG, 10-Pack, Black</p>
                    <p> 30-round capacity</p>
                    <p> Stainless steel spring</p> -->
                </div>
                <!-- Additional Info End-->
            </div>
            <!-- Ad Img End -->


            <div class="short-description">
                <!-- Ad Category -->
                <div class="category-title">
                    <span>
                        <a href="#"><?=$this->common->getCategoryname($category)?></a>
                    </span>
                </div>
                <!-- Ad Title -->
                <h3>
                    <a title="" href="<?=$link_url?>" target="_blank"><?=$title?></a>
                </h3>
                <!-- Price -->
                <div class="price">$<?=$price?>
                    <!-- <span class="negotiable">( 30% off )</span> -->
                </div>
            </div>
            <!-- Addition Info -->
            <div class="ad-info">
                <ul>
                    <li>
                       <!--  <i class="fa fa-building"></i> -->  <?= $this->common->getDealerCompanyName($dealer_id) ?>
                    </li>
                    <li>
                      <a onClick="likeItem(this, <?php echo $product_id ?>)" title="Like This Deal">
                        <i class="fa fa-thumbs-o-up <?php echo $this->common->isProductLike($product_id) ?> like<?php echo $product_id ?>" aria-hidden="true"></i>
                      </a>  
                      &nbsp;
                      <a onClick="dislikeItem(this, <?php echo $product_id ?>)" title="Dislike This Deal">
                        <i class="fa fa-thumbs-o-down <?php echo $this->common->isProductDislike($product_id) ?> dislike<?php echo $product_id ?>" aria-hidden="true"></i>
                      </a>
                      &nbsp;
                      <a href="<?php echo base_url('product/viewProduct?id='.$product_id) ?>#comments" title="Comments">
                        <i class="fa fa-comment-o" aria-hidden="true"></i>
                      </a>
                      &nbsp;
                      <a onClick="subsPDC(this, <?php echo $product_id ?>)" title="Subscribe for Price Drop and Comments">
                        <i class="fa fa-bell-o <?php echo $this->common->isProductSubs($product_id) ?>" aria-hidden="true"></i>
                      </a>
                    </li>
                        <!-- ..TODO -->
                        <!-- <div class="rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <span class=  "rating-count">(2)</span>
                        </div> -->
                </ul>
            </div>
        </div>
        <!-- Ad Box End -->
    </div>
</div>
