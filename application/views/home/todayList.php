<?php

foreach($data as $row):

$dealer = ($row['dealer'] != null) ? $row['dealer'] : '';
$dealer = (strlen($row['dealer']) > 31) ? substr($row['dealer'], 0 , 28).'...' : $row['dealer'];
$title  = (strlen($row['title']) > 70) ? substr($row['title'], 0, 70).'...' : $row['title'];

?>
<div class="col-lg-3 col-md-4 col-xs-12 col-sm-12 wrap_itemsList todays" onclick="gotoProduct(<?php echo $row['product_id'];?>)">
	<div class="category-grid-box">
		<div class="category-grid-img">
			<div class="wrap_img_prod">
				<img onclick="gotoProduct(<?php echo $row['product_id'];?>)" class="feature-img-size loadingImage" src="<?=base_url().'assets/pictures/loading.gif'?>" data-image="<?php echo $row['images'];?>" alt="<?php echo $row['title'];?>" />
			</div>
			<a class="view-details" href="<?php echo base_url('product/viewProduct?id=').$row['product_id']; ?>">View Details</a>
			<div class="additional-information"></div>
		</div>
		<div class="short-description">
			<div class="category-title">
				<span><a href="javascript:;"><?php echo $row['category'];?></a></span>
			</div>
			<p>
				<a href="<?php echo base_url('product/viewProduct?id='); ?><?php echo $row['product_id'];?>">
					<?php echo $title;?>
				</a>
			</p>
			<div class="price">$<?php echo $row['price'];?></div>
		</div>
		<div class="ad-info">
			<ul>
				<li title="<?php echo $row['dealer'];?>">
					<i class="fa fa-building"></i><?php echo $dealer;?>
				</li>
				<li>
					<li>
					<a onClick="likeItem(this, <?php echo $row['product_id'];?>)" title="Like This Deal">
						<i class="fa fa-thumbs-o-up <?php echo $row['isProductLike'];?> like<?php echo $row['product_id'];?>" aria-hidden="true"></i>
						<span class="l_count_<?php echo $row['product_id'];?>"><?php echo $row['like_count'];?></span>
					</a>
					&nbsp;
					<a onClick="dislikeItem(this, <?php echo $row['product_id'];?>)" title="Dislike This Deal">
						<i class="fa fa-thumbs-o-down <?php echo $row['isProductDisLike'];?> dislike<?php echo $row['product_id'];?>" aria-hidden="true"></i>
						<span class="dl_count_<?php echo $row['product_id'];?>"><?php echo $row['dislike_count'];?></span>
					</a>
					&nbsp;
					<a href="<?php echo base_url('product/viewProduct?id='); ?><?php echo $row['product_id'];?>#comments" title="Comments">
						<i class="fa fa-comment-o" aria-hidden="true"></i>
						<span class="c_count_<?php echo $row['product_id'];?>"><?php echo $row['comment_count'];?></span>
					</a>
					&nbsp;
					<a onClick="subsPDC(this, <?php echo $row['product_id'];?>)" title="Subscribe for Price Drop and Comments">
						<i class="fa fa-bell-o <?php echo $row['isProductSubs'];?>" aria-hidden="true"></i>
						<span class="s_count_<?php echo $row['product_id'];?>"><?php echo $row['subscriber_count'];?></span>
					</a>
					<a href="#" title="Views Count">
						<i class="fa fa-eye" aria-hidden="true"></i>
						<span class="v_count_<?php echo $row['product_id'];?>"><?php echo $row['view_count'];?></span>
					</a>
					</li>
				</li>
			</ul>
		</div>
	</div>
</div>
<?php
endforeach;

if(count($data) == 0): 
?>
<div class="row">
    <div class="col-md-12 text-center" style="margin: 0px auto;padding: auto">
        <a href="javascript:;" class="btn btn-default" style="margin-top:5%">No More Recent Deals</a>
    </div>
</div>
<?php else:?>
<div class="col-xs-12 col-sm-12 wrap_itemsList loadmoredeals">
    <div class="col-md-12 text-center" style="margin: 0px auto;padding: auto">
        <a style="margin-top:5%" href="<?=base_url();?>home/get_content/?page=today_deals&offset=<?=$offset + 12;?>" class="btn btn-default loadmore" data-type="today"><i class="fa fa-plus" aria-hidden="true"></i> Show More Recent Deals</a>
    </div>
</div>
<?php endif;?>