<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">How To Post A Deal</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page pattern-bg ">


            <!-- Main Container -->
            <div class="container-fluid">
               <!-- Row -->
               <div class="row">


                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink">
                          <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                  </div>


                  <!-- Middle Content Area -->
                  
                  <div class="col-lg-8 col-md-6 col-xs-12 col-sm-12">

                     <label class="lbl_faqTitlle">How To Post A Deal</label>

                     <div class="">
                          <p>
                             Only registered dealers are allowed to post deals. Depending on the subscription, dealers can post a deal once every 3 days, every 2 days or every day. 
                          </p>
                          <br>
                          <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <h4>Here’s how to post a deal:</h4>
                              <br>
                              <p>
                                1. Dealers must be logged in to be able to post their deals. Login to your account by clicking the <strong>Login</strong> button at the upper right corner of the website. 
                              </p>
                              <p>
                                2. Click on the tab for Dealer Login, input your username and password, and then click <strong>Login</strong>.
                              </p>
                              <p>
                                3. Click the <strong>Post A Deal</strong> button in the upper right hand corner of the website to start posting your deal.
                              </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord">
                                <img src="<?=base_url(). 'assets/images/register5.jpg'?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <br>
                              <p>
                                4. Provide the information for your deal in the required field (an image of these fields are shown below):
                              </p>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-lg-offset-1 col-sm-12 col-xs-12">
                              <br>
                              <ul class="listDeal">
                                <li><strong>Title:</strong> A short description or name of your deal.</li>
                                <li>
                                  <strong>Code:</strong> An alphanumeric character you will use to identify your deal. Code must be unique for each deal. 
                                </li>
                                <li>
                                  <strong>Brand, Category</strong> and <strong>Sub Category</strong>: Select the brand/manufacturer, category and sub category of your product from the drop down values.
                                </li>
                                <li><strong>UPC:</strong> A 12-digit numeric character used to identify each product.</li>
                                <li>
                                  <strong>SKU:</strong>An alphanumeric character used by the manufacturer to identify their product
                                </li>
                                <li>Provide your <strong>Deal Price</strong> and <strong>Shipping Rate</strong>.</li>
                                <li>
                                  You can attach an image or photo of your deal. For best result, kindly upload a 313 x 234 image.
                                </li>
                                <li>You can type in a more detailed description of your deal.</li>
                                <li>
                                  You can also provide additional specifications such as caliber, barrel length, capacity, weight, etc. of the product you are offering.
                                </li>
                              </ul>
                              <br>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-lg-offset-1 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord2">
                                <img src="<?=base_url(). 'assets/images/register6.jpg'?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <br>
                              <p>
                                5. You can opt-in to receive an email notification every time a customer comments on your deal.
                              </p>
                              <p>6. Click <strong>Publish My Deal</strong> to submit the form.</p>
                              <p>
                                7. If all required information is correct and your subscription is valid, your deal will be published on the website immediately.
                              </p>
                            </div>
                          </div>
                          <div class="row">
                            <br>
                            <div class="col-lg-10 col-lg-offset-1 col-md-offset-1 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord">
                                <img src="<?=base_url(). 'assets/images/register7.jpg'?>">
                              </div>
                            </div>
                          </div>
                       </div>
                      
                  </div>
 







                    <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink1">
                          <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                    </div>


               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>




         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>