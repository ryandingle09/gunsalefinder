﻿<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="<?php echo base_url('contact'); ?>">Contact Us</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section_padding_srch ">




            <!-- Main Container -->
            <div class="container-fluid">
               <!-- Row -->
               <div class="row">


                    <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                        <div class="wrap_ads text-center">
                            <a href="#" target="_blank" id="bannerLink">
                            <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                            </a>
                        </div>
                    </div>


                  <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12 commentForm">
                     <div class="row">
                        <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12">
                           <h2 >Send a Message</h2>
                           <?php echo $this->session->flashdata('message');  ?>
                           <div class="row">
                              <form action="<?php echo base_url('contact/savecontact'); ?>" method="post">
                              <div class="col-lg-6 col-md-6 col-xs-12">
                                 <div class="form-group">
                                    <input type="hidden"  name="usertype" value="<?php echo $usertype; ?>" required>
                                    <input type="text" placeholder="Name" id="name" name="name" class="form-control" value="<?php echo $name; ?>" required>
                                 </div>
                                 <div class="form-group">
                                    <input type="email" placeholder="Email" id="email" name="email" class="form-control" value="<?php echo $email; ?>" required>
                                 </div>
                                 <div class="form-group">
                                    <input type="text" placeholder="Subject" id="subject" name="subject" class="form-control" required>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-xs-12">
                                 <div class="form-group">
                                    <textarea cols="12" rows="7" placeholder="Message..." id="message" name="message" class="form-control" required></textarea>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <button class="btn btn-theme" type="submit">Send Message</button>
                              </div>
                              </form>
                           </div>                           
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12">
                           <div class="contactInfo">
                              <h2>Contact Info</h2>
                              <!--<div class="singleContadds">
                                 <i class="fa fa-map-marker"></i>
                                 <p>
                                    Model Town Link Road Lahore, 60 Street. Pakistan 54770
                                 </p>
                              </div>-->
                              <div class="singleContadds phone">
                                 <i class="fa fa-envelope"></i>
                                 <p>
                                    sales@gunsalefinder.com
                                 </p>
                                 <p>
                                    support@gunsalefinder.com
                                 </p>
                              </div>
                              <!-- <div class="singleContadds">
                                 <i class="fa fa-envelope"></i>
                                 <a href="mailto:contact@gunsalefinder.com">sales@gunsalefinder.com</a>
                              </div> -->
                           </div>
                        </div>
                     </div>
                  </div>
                 


                <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="#" target="_blank" id="bannerLink1">
                        <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                        </a>
                    </div>
                </div>

                


               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->

            <div class="clearfix_row_2"></div>

         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>