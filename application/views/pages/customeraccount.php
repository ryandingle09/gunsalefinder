<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">How To Create A User/Constumer Account</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page pattern-bg ">


            <!-- Main Container -->
            <div class="container-fluid">
               <!-- Row -->
               <div class="row">


                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink">
                          <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                    </div>


                  <!-- Middle Content Area -->
                  
                  <div class="col-lg-8 col-md-6 col-xs-12 col-sm-12">

                     <label class="lbl_faqTitlle">How To Create A User/Consumer Account</label>

                    <div class="">
                              <p>
                                 “Everyone can use our website to find deals they want, but only registered members can utilize the services and functionality it offers.  Registered users/consumers can create item wish lists, comment on and rate deals, and participate in contests to win free guns, ammo, optics and more!
                              </p>
                              <br>
                              <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                  <h4>Here’s how to create or register as a user/consumer:</h4>
                                  <br>
                                  <p>
                                    1. Click the <strong>Register</strong> button at the upper right corner of the website to go to the registration page and select <strong>Register as a user</strong>.
                                  </p>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                  <div class="wrap_imgaccord">
                                    <img src="<?=base_url(). 'assets/images/register3.jpg'?>">
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br><br></div>
                              </div>
                              <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                  <div class="wrap_imgaccord">
                                    <img src="<?=base_url(). 'assets/images/register4.jpg'?>">
                                  </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                  <p>
                                    2. Input your first and last name, phone number and email address.
                                  </p>
                                  <p>
                                    3. Enter the username and password that you will use to login to your account.
                                  </p>
                                  <p>
                                    4. Read and review the <strong>Terms and Conditions</strong> by clicking the link provided.
                                  </p>
                                  <p>
                                    5. After reading and agreeing to the <strong>Terms and Conditions</strong>, click REGISTER to submit your form.
                                  </p>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                  <br>
                                  <p>
                                    6. An acknowledgement email will be sent immediately to the email address you provided from <a href="#">support@gunsalefinder.com</a>. Please be sure to check your spam or junk folders for this email as it may go there. Activate your account by clicking the link provided in the email. We recommend adding the Gun Sale Finder email, <a href="#">sales@gunsalefinder.com</a>, to your safe senders list to make sure you receive all of our future emails. Please note that by clicking the agree box, you are opting in to our emails. 
                                  </p>
                                  <br>
                                </div>
                              </div>
                           </div>

                  </div>

                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="#" target="_blank" id="bannerLink1">
                        <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                        </a>
                    </div>
                  </div>

               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>




         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>