<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li>
                     <a class="active" href="<?php echo base_url('privacy'); ?>">Comments Policy</a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page content-section">



            <!-- Main Container -->
            <div class="container-fluid">
               <div class="row">
                    <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink">
                          <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                    </div>


               <!-- Row -->
               <div class="col-lg-8 col-md-6 col-xs-12 col-sm-12">
                  <div class="heading-panel"><h3 class="main-title text-left">Comments Policy</h3></div>
                  <div class="row">
                     <!-- Middle Content Area -->
                        <p class="content-paragraph">
                           Gun Sale Finder is created for the Firearm Enthusiasts and Sportsmen where they easily find deals that suit their needs, exchange ideas and benefit from the information, provided in the internet. The following rules and guidelines will be implemented to encourage quality communication and mutual respect. 
                        </p>
                        <div class="content-container">
                           <ul class="content-margin">
                              <li> No discussion promoting illegal activity.</li>
                              <li> No forms of harassment, vulgar language or intimidating behavior will be tolerated.</li>
                              <li> Bullying and disrespectful attitude are not permitted and may result in a permanent ban.</li>
                              <li> Offensive language will result in ban. Anti-Semitic,racist,sexual orientation comments will be removed and will result in a permanent ban.</li>
                              <li> User cannot create more than one account. This will result in a permanent ban.</li>
                              <li> Any attempt to disrupt or hijack a dealer’s posting will not be tolerated.</li>
                              <li> All postings should be posted in the English language.</li>
                              <li> Reposting concerning Gun Sale Finder directive or decision will be removed and poster maybe banned.</li>
                              <li> We do not permit post containing copyright material without appropriate approval.</li>
                              <li> All deals must be posted in the appropriate area.</li>
                              <li> Private message should not be used for unsolicited advance or a form to circumvent any rule listed Gun Sale Finder website.</li>
                              <li> No posting of irrelevant items that are not related to the firearm industry. No Spamming.</li>
                              <li> It is not acceptable to post another person’s private information without written consent.</li>

                              <li> Do not post inaccurate or dangerous information, or misquote other posts.</li>
                           </ul>
                        </div>
                        <p class="content-footer">
                           While warnings maybe given, single or repeated violation of any of these rules may result in permanent ban. Final decision will be given by Gun Sale Finder.
                        </p>
                     </div>
                  </div>
               <!-- Row End -->


               <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="#" target="_blank" id="bannerLink1">
                        <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                        </a>
                    </div>
                </div>
            </div>
            <!-- Main Container End -->



         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>