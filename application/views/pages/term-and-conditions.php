<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li>
                     <a class="active" href="<?php echo base_url('terms-and-conditions'); ?>">Terms & Conditions</a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
         <section class="section-padding error-page content-section">




            <!-- Main Container -->
            <div class="container-fluid">
               <!-- Row -->
               <div class="row">
                  

                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="#" target="_blank" id="bannerLink">
                        <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                        </a>
                    </div>
                  </div>



                  <div class="col-lg-8 col-md-6 col-xs-12 col-sm-12">
                     <div class="heading-panel"><h3 class="main-title text-left">Terms & Conditions</h3></div>
                     <!-- Middle Content Area -->

                        <p class="content-paragraph">
                           <h3>AGREEMENT, POLICIES AND TERMS OF USE</h3>
                           Welcome to GunSaleFinder.com  and thank you for visiting! GunSaleFinder.com is an online search engine and marketplace designed to provide real time information regarding product and pricing on firearms, ammo, archery, knives and accessories. GunSaleFinder.com also provides advertising via its website. The following statements and guidelines constitute our policies and terms of use or rules in using the website. GunSaleFinder.com does not sell any of the items listed and all sales and deals are made between the listed retailer and the customer. By visiting the website or by registering as a user you hereby agree to the terms and conditions listed below. You also affirm that you are either more than 18 years of age and are fully able and competent to enter into the terms, conditions, obligations, affirmations, representations, and warranties set forth in GunSaleFinder.com’s site policies and terms, and to abide by and comply with these terms of service. If you are under 18 years of age, please do not use the website or our services. 
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>SITE POLICIES, MODIFICATION, AND SERVICEABILITY</h3>
                           Please review all policies and terms of use posted on this site. These govern your visit to GunSaleFinder.com. We reserve the right to make changes to our site, policies, and any conditions at any time. If any of these conditions shall be deemed invalid, void, or for any reason unenforceable, that condition shall be deemed severable and shall not affect the validity and enforceability of any remaining condition. This agreement contains a mandatory individual arbitration and class action / jury trial waiver provision that requires the use of arbitration on an individual basis to resolve disputes, rather than jury trails or class actions. 
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>COPYRIGHT</h3>
                           Contents on this site is the property of GunSaleFinder.com and its affiliates. It is protected by United States and international copyright laws. We hereby give you the limited right to use our logo and our website link to promote your page on our site. All contents of the Service are copyrighted © 2018 GunSaleFinder.com. All rights reserved. Other product and company names may be trademarks or service marks of their respective owners.
                           <br>
                           <br>
                           If any content found on the GunSaleFinder.com violates a copyright, please contact us at Support@GunSaleFinder.com for further instruction and information.actions. 
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>USERS RESPONSIBILITY </h3>
                           All users agree that they will not use GunSaleFinder.com for any illegal purposes or activities. All users agree GunSaleFinder.com is not liable for any misuse of any item or information found on the website. All users agree and understand that prices and information are constantly changing and GunSaleFinder.com is not responsible for any changes or mistakes in the data. All users agree and understand that GunSaleFinder.com will and does not facilitate any sales. All users agree sales are between the listed dealer user and the customer user. All users agree and understand that they are solely responsible for all interactions with visitors to GunSaleFinder.com and GunSaleFinder.com reserves the right, but has no obligation, to monitor disputes between users and visitors. GunSaleFinder.com may also suspend accounts, delete post or data for any reason or for no reason based on its sole discretion. All users agree not to post any obscenities that could be found offensive. This includes but is not limited to anything vulgar, unlawful, pornographic, abusive, threatening, detestable, harassing, defamatory, damaging, or discriminatory. All users agree not to post anything that is invasive of another person’s privacy or exploitive of others in a violent or sexual manner. All users agree they will not post personal ads, auctions, personal affiliate links, giveaways or raffles. All users agree to never impersonate, stalk, or harass another person. All users agree to never post, upload or transmit unauthorized unsolicited advertisements, promotional materials, spam, junk mail, or chain letters. All users agree to never promote unauthorized copies of another person’s copyrighted work. All users agree to never post, upload or transmit material that contains viruses or any other type of software code. All users agree to never automatically create user identities. All users agree to use their registered account to post a deal. All users agree when posting a deal that they are posting the best deal online. This can be verified by searching GunSaleFinder.com using either product’s UPC (universal product code) or MPN (manufacturer product Number). All users agree when posting an ad or deal they will include a title with the product name, make, model, cost, and shipping charge. All users agree when selling ammo the title will include make, type, quantity, cost, and shipping charge. All prices must be listed, if there are rebates the price should be listed before the rebate and a note can be added mentioning the savings after rebate. All users agree to list States that an item cannot be shipped. Users agree to post ads or deals that always have accurate descriptions, UPC’s, MPN’s, Specifications, and accurate photos. All users agree that any information posted will not be inaccurate or misleading in anyway. All users agree they will not violate any applicable local, state, national or international law. All users hereby represent and warrant that they have all necessary authority, rights and permissions for submission of any materials and the materials or use of service does not infringe or misappropriate any copyright, trademark, trade secret, patent, or other intellectual property right of any third party or violate any other rights of third party, including, without limitation, any rights of privacy or publicity or any contractual rights. Users agree not to use GunSaleFinder.com to violate any agreements between them and a third party. All users agree to grant GunSaleFinder.com a non-exclusive, use, transferable, royalty free, international license to distribute, host, modify, copy, run, display, interpret, or create derivative works of the content they upload or post. All users also understand that if they choose to make any suggestions to improve GunSaleFinder.com that it is on a voluntary basis and no renumeration will be paid if GunSaleFinder.com chooses to include any suggestions. All users agree and understand that not all user content uploaded or posted to GunSaleFinder.com expresses GunSaleFinders.com opinions. All users understand that any violation of the policies and terms of use can lead to a temporary or permanent ban from GunSaleFinder.com and any violation is solely determined by GunSaleFinder.com. 
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>ACCOUNTS</h3>
                           Users must be at least 18 years of age to create an account on GunSaleFinder.com. All users agree that it is their responsibility to maintain the confidentiality of their account. Users agree to accept responsibility for all of the activities that occur under their account and password. GunSaleFinder.com and its affiliates reserve the right to suspend accounts, terminate accounts, cancel orders, refuse service, and remove or edit account information. GunSaleFinder.com is not liable for any loss or damage arising from users failure to keep passwords secure. Users agree to immediately notify GunSaleFinder.com of any unauthorized use of passwords or any breach of security. 
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>LIABILITY LIMITATION</h3>
                           The following limitations apply only to the maximum extent permitted by applicable law. Under no conditions, without limitation, including disregard, will GunSaleFinder.com, or its partners, affiliates, directors, employees, officers, agents, retailers, relatives, distributors, or suppliers be liable for any direct, incidental, indirect, distinct, or consequential damages resulting from inability of use or use of any services offered by GunSaleFinder.com, or for any cost of procurement for substituted goods and services, or results from goods and services purchased and obtained, or messages  received and transactions agreed upon offered within any service provided by GunSaleFinder.com, or transactions or transmissions of data resulting from unauthorized access to users accounts, or any other information that is not sent or received or sent, including but not limited to, damages for use, data, loss of profits, or any other tangibles, even if GunSaleFinder.com has been advised of possible such damages. GunSaleFinder.com is also not liable or responsible to any user, or to anyone, for the statements of behavior of any third party on or from any use of any service offered. All users agree that if dissatisfied with any service offered by GunSaleFinder.com a discontinue use of the services without refund of any kind will be the remedy. 
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>WARRANTY</h3>
                           GunSaleFinder.com’s services are provided on an “as is” and “as available” basis. All users and visitors agree that the use of any service offered by GunSaleFinder.com is at their own risk. GunSaleFinder.com disclaims all warranties of any kind, including expressed or implied. GunSaleFinder.com makes no warranty or guarantee on any of the services or products offered. Users agree all material and data downloaded or obtained from GunSaleFinder.com is up to the users discretion and at their own risk. Users are responsible for any damage or loss of their computer system in the result of any download of data from GunSaleFinder.com. GunSaleFinder.com does not warrant any goods or services purchased or published or offered on GunSaleFinder.com.
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>CONTRACTUAL OBLIGATIONS</h3>
                           All users guarantee not to use information provided by GunSaleFinderc.om to violate any Federal, State, or International law. All users agree to protect, defend and hold GunSaleFinder.com harmless, as well as its partners, affiliates, directors, employees, officers, agents, retailers, relatives, distributors, and suppliers, and their respective affiliates, directors, employees, officers, agents, retailers, relatives, distributors, and suppliers, from any action, demand, claim, or damage, including attorney's fees, made by any third party or governmental agency that arise from relation from use of any service provided by GunSaleFinder.com or the users violation of this Agreement, including without limitation, suits or claims of defamation, intrusion of property rights, violation of rights of publicity or privacy, trespassing violations, violations of State, Federal, or International Laws plagiarism, or patent infringement. It is  GunSaleFinder.com’s sole discretion to assume the defense or control of any matter subject to indemnification by any user. The assumption of such defense or control by GunSaleFinder.com shall not excuse any users indemnity responsibilities. 
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>OTHER LINKS</h3>
                           All links provided are for convenience and reference only. GunSaleFinder.com provides links to other various third party sites, affiliated companies, and many other businesses. GunSaleFinder.com is not responsible for evaluating or examining these other sites, affiliated companies, and many other businesses products or practices. GunSaleFinder.com does not warrant any content or offers from any other sites, affiliated companies, or other businesses. GunSaleFinder.com will not take responsibility or be liable for any products, actions, or content provided by any other sites, affiliated companies, or other businesses. 
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>ERRORS, DELAYS AND UNFORESEEABLE CIRCUMSTANCES</h3>
                           GunSaleFinder.com is not responsible for any errors or delays caused by incorrect information or e-mail addresses provided by users. GunSaleFinder.com is also not responsible for any technical problems beyond their reasonable control. Gunsalefinder.com is not responsible or liable for any issue that are beyond our control. This includes but is not limited to delays, outages, or availability.  
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>ASSIGNMENT AND AGREEMENT</h3>
                           No reassignments of rights or obligations can take place under this agreement. The agreement can only be modified by writing and executed by both parties, the terms and conditions described are the only representations, warranties, and understandings between the parties with respect to the products and services described. The headings of this agreement are for users convenience only and shall not be used to interpret the meaning of this agreement. We reserve the right to revise this agreement and policy anytime without prior notice. All changes will be published and can be viewed here.  
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>DISPUTE RESOLUTION</h3>
                           In the event of a dispute users agree to attempt to resolve the dispute by contacting GunSaleFinder.com at Support@GunSaleFinder.com before taking any other action. Failure to contact GunSaleFinder.com to attempt to resolve the dispute prior to taking any other action will result in a breach of this agreement by the user. Any dispute arising out of or relating to the use of GunSaleFinder.com’s provided goods or service, or to any acts or omissions for which you may contend GunSaleFinder.com is liable, including but not limited to and dispute, shall be finally, and exclusively, settled by arbitration in Fort Wayne, Indiana, from which arbitration there shall be no appeal. The arbitration shall be held before one arbitrator under the Commercial Arbitration rules of the American Arbitration Association in force at that time. Arbitrators must be selected by following American Arbitration Association rules. The arbitrator shall apply the substantive law of the State of Indiana unless the interpretation and enforcement of this arbitration provision shall be governed by the Federal Arbitration Act. Any arbitration process must begin with written demand from either party. Each party will be fully responsible to cover their own incurred cost and attorney fees. Any judgment by the arbitrators may be entered in any court of competent jurisdiction in Indiana. The arbitrator shall not have the power to award damages in connection with any dispute more than the actual compensation of damages. The arbitrator shall not increase actual damages, award consequential, punitive or exemplary damages, and each party irrevocably waives any claim to that. The agreement to arbitrate shall not be construed as an agreement to the joinder or consolidation of arbitration under this agreement with arbitration of disputes or claims of any non-party, regardless of the nature of the issues or disputes involved. This agreement provides that all disputes between users and GunSaleFinder.com will be resolved by binding arbitration. This means users give up the right to go to court to assert or defend rights. Users also give up the right to participate in or bring class actions. Users agree to waive the right to participate as a plaintiff or class member in any purported class action or representative proceeding. Users agree they cannot consolidate more than one person’s claims. Users rights will be determined by a neutral arbitrator and not a judge or jury. Users agree that if any provision or portion of this agreement is found invalid under any applicable statue or rule of law, then such invalidity shall not affect the validity of the remaining portions of this agreement. Users agree to substitute the invalid provision a valid provision which most closely approximates the intent and economic effect of the invalid provision. Users agree by entering this agreement we are waiving the right to trial by jury or participate in a class action, private attorney general action, collective action, or other representative proceedings of any kind.  
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3>APPLICABLE LAW</h3>
                           All users agree service shall be solely based in Indiana and this service shall be deemed a passive web site and service that does not give rise to personal jurisdiction over company, either specific or general, in jurisdictions other than Indiana. All terms of Service shall be governed by the internal substantive laws of the State of Indiana, without respect to its conflict of laws principles. Users and GunSaleFinder.com agree that they will submit to the personal jurisdiction of a state court located in Fort Wayne, Indiana or the United States District Court for the District of Indiana, for any actions for which either party retains the right to seek injunctive or other equitable relief. Everything found on these pages constitutes the entire agreement between all users and GunSaleFinder.com and it supersedes any prior written or oral representations. All users agree that any cause of action arising out of GunSaleFinder.com’s services must begin within one year of the cause of the action. Otherwise any such cause of action will be permanently barred. The terms of service may not be transferred or assigned by users, but may be assigned by GunSaleFinder.com without restriction. All attempted transfers or assignments in violation hereof shall be null and void. Pursuant to any applicable laws, rules or regulations, including without limitation the US Electronic Signatures in Global and National Commerce Act,P.L. 106-229 or other similar statutes, users hereby agree to the use of electronic signatures, contracts, orders and other records and to electronic delivery of policies, notices, and records of transactions. Users waive any rights or requirements under any rules, regulations, statutes, ordinances or other laws in any jurisdiction which require an original signature or delivery or retention of non-electronic records, or to payments or credits by other than electronic means. A printed version of these terms of service and of any notice given in electronic form shall be admissible in administrative or judicial proceedings based upon or relating to this agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. All rights not expressly granted herein are reserved to GunSaleFinder.com.  
                        </p>
                        <br>
                        <p class="content-paragraph">
                           <h3></h3>
                           VISITORS AND USERS IF YOU DO NOT AGREE TO THE TERMS STATED ABOVE OR TO ANY CHANGES MADE IN THESE TERMS, PLEASE EXIT THIS WEBSITE AND SERVICE IMMEDIATELY.  
                        </p>
                        
                  </div>


                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="#" target="_blank" id="bannerLink1">
                        <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                        </a>
                    </div>
                  </div>

                  

               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->






         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>