﻿<?php $this->load->view("layouts/header"); ?>
 <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">Pricing</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Pricing =-=-=-=-=-=-= -->
         <section class="custom-padding">
            <!-- Main Container -->
            <div class="container-fluid">
               <!-- Row -->
               <div class="row">


                <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="#" target="_blank" id="bannerLink">
                        <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                        </a>
                    </div>
                </div>


                  <!-- Middle Content Box -->
                  <div class="col-lg-8 col-md-6 col-xs-12 col-sm-12">
                     <div class="row">
                        <!-- Pricing -->
                     <?php  foreach ($subscriptions as $subscription): ?>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                           <div class="pricing-item fix_height">
                              <label class="lbl_monthly_subs">Dealer</label>
                              <label class="price"><span>$</span><span><?php echo abs($subscription->price); ?></span></label><br><br>
                              <strong><?php echo $subscription->name; ?> <span class="hidden-sm"><?php echo (abs($subscription->price) > 0) ? '' : '(Free)'; ?></span></strong>
                              <p><?php echo $subscription->description; ?></p>
                              
                              <a href="<?php echo base_url('dealer/subscription/payment_form?tab=subscription&subscription='.$subscription->id.'&user_id='.base64_encode($this->session->userdata('id'))); ?>"><button class="btn btn-theme btn_selectPlan">Select Plan</button></a>

                              <?php if(abs($subscription->monthly) == 1): ?>
                                <label class="lbl_monthly_subs2">Monthly Subscription</label>
                             <?php endif; ?>

                           </div>
                        </div>
                     <?php endforeach; ?>
                     </div>
                  </div>


                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="#" target="_blank" id="bannerLink1">
                        <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                        </a>
                    </div>
                </div>



               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Pricing End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>