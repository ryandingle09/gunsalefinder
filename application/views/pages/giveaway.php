<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">Giveaway</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page">


            <!-- Main Container -->
            <div class="container-fluid">
               <!-- Row -->
               <div class="row">

                <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="<?php echo base_url('contact'); ?>" target="_blank" id="bannerLink">
                        <img style="height: 600;width: 160" id="bannerImage" src="https://admin.gunsalefinder.com/images/banners/1539621035AdvertiseYourBusiness160x600.jpeg">
                        </a>
                    </div>
                  </div>

                <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
                  <div class="wrap_giveawayBanner banner text-centers">
                    <img src="https://gunsalefinder.com/assets/uploads/ads/bc9f92849d797e3ae740924af95d7b63.jpg">
                  </div>
                  <br>
                  <div class="row">
                    <div class=" wrap_givevaway">
                      <!-- <label class="lbl_giveawayTitle">Win with Burris BUR 200621</label> -->
                      <br/>
                        <a class="e-widget no-button" href="https://gleam.io/2arRi/gun-sale-finder-sig-p365-giveaway" rel="nofollow">Gun Sale Finder Sig P365 GiveAway</a>
                    </div>
                  </div>
  
                </div>


                <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="<?php echo base_url('contact'); ?>" target="_blank" id="bannerLink">
                        <img style="height: 600;width: 160" id="bannerImage" src="https://admin.gunsalefinder.com/images/banners/1539621035AdvertiseYourBusiness160x600.jpeg">
                        </a>
                    </div>
                  </div>

               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
       </div>
       <script type="text/javascript" src="https://js.gleam.io/e.js" async="true"></script>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>