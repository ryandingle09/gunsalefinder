<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">FAQS</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page pattern-bg ">


            <!-- Main Container -->
            <div class="container-fluid">
               <!-- Row -->
               <div class="row">


                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink">
                          <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                  </div>


                  <!-- Middle Content Area -->
                  
                  <div class="col-lg-8 col-md-6 col-xs-12 col-sm-12">

                    <label class="lbl_faqTitlle">Frequently Ask Questions</label>
                     <ul class="accordion">
                        <li class="">
                           <h3 class="accordion-title"><a href="#">How can I register?</a></h3>
                           <div class="accordion-content">
                              <p>
                                 <ul>
                                    <li>
                                       You can register on our website as either a dealer or a user.  Click <a href="<?php echo base_url('registration'); ?>" target="_blank">here</a> to go to the Registration Page. 
                                    </li>
                                    <li>
                                       <div class="top-space"></div>
                                       To register as a user, provide your full name, phone, email address, username and password. Please read and agree to the terms of service. Activate your account via acknowledgement email will be sent to the email provided on the account you create. Please be sure to check your spam and junk folders for this email as it may go there. 
                                    </li>
                                    <li>                                       
                                       <div class="top-space"></div>
                                       To register as a dealer, provide your full name, phone number, email address, company name, company address & website. Please be sure to upload your FFL and SOT documents if you hold an FFL Or SOT. Please provide your username and password, and be sure to read and agree to the terms of service. Activate your account via the acknowledgement email that will be sent to the email provided for the account you created. Please be sure to check your spam and junk folders for this email as it may go there.
                                    </li>
                                 </ul>
                              </p>
                           </div>
                        </li>
                        <li class="">
                           <h3 class="accordion-title"><a href="#">What is the difference between a dealer and a user account?</a></h3>
                           <div class="accordion-content">
                              <p>
                                 <ul>
                                    <li>
                                      Registered dealers can post and advertise their products, receive feedback from users/consumers about their products and receive detailed market analysis. Dealers can receive notifications when users/consumers leave comments on deals that they post. Dealers are required to hold a valid FFL to register as a Firearms dealer. Class III NFA Dealers must hold an SOT.
                                    </li>
                                    <li>
                                       <div class="top-space"></div>
                                       Registered users/consumers can create item wish lists, comment on and rate deals, and participate in contests to win free guns, ammo, optics and more! Users/consumers may opt in to receive email notifications on price drops and comments left on deals that they are interested in. 
                                    </li>
                                 </ul>
                              </p>
                           </div>
                        </li>
                        <li class="">
                           <h3 class="accordion-title"><a href="#">Can I purchase an item from you?</a></h3>
                           <div class="accordion-content">
                              <p>
                                GunSaleFinder does not facilitate any sales.  All sales are between the listed dealer and the consumer. 
                              </p>
                           </div>
                        </li>
                        <li class="">
                           <h3 class="accordion-title"><a href="#">How can I contact you?</a></h3>
                           <div class="accordion-content">
                              <p>
                                 You can email your questions to support@gunsalefinder.com or send a message on our Contact US page.
                              </p>
                           </div>
                        </li>
                        <li class="">
                           <h3 class="accordion-title"><a href="#">I’m a dealer, how can I upload my products to you?</a></h3>
                           <div class="accordion-content">
                              <p>
                                 You can stream your top 50 items or your entire catalog to us through our API Integration. Click <a href="<?php echo base_url('api-guide'); ?>" target="_blank">here</a> to go to our API Integration Guide. 
                              </p>
                           </div>
                        </li>
                        <li class="">
                           <h3 class="accordion-title"><a href="#">What items can I sell?</a></h3>
                           <div class="accordion-content">
                              <p>
                                 You can sell all legal items related to firearms, ammo, archery, knives, optics and accessories. You must hold a valid FFL to sell items on this website and you must hold a SOT to sell class III NFA items.
                              </p>
                           </div>
                        </li>
                        <li class="">
                           <h3 class="accordion-title"><a href="#">Do you charge commission / Portion Of Sale to Dealers when selling products?</a></h3>
                           <div class="accordion-content">
                              <p>
                                 No, we do not charge a commission.  All transactions to purchase items are between the dealer and the consumer.
                              </p>
                           </div>
                        </li>
                        <li class="">
                           <h3 class="accordion-title"><a href="#">As a dealer, how can I advertise my products?</a></h3>
                           <div class="accordion-content">
                              <p>
                                <ul>
                                    <li>
                                      Only Dealers with active dealer accounts may advertise on Gun Sale Finder. Users (Private Party Transactions) are not permitted on Gun Sale Finder.  We offer Banner Advertising where dealers may advertise in certain spots on our website. Dealers May Also purchase packages to post deals to our marketplace. Click <a href="<?php echo base_url('dealer-post'); ?>" target="_blank">here </a> to select the package where your ads will be posted.
                                    </li>
                                    
                                 </ul>
                              </p>
                           </div>
                        </li>
                        <li class="">
                           <h3 class="accordion-title"><a href="#">As a dealer, how do I post my deals?</a></h3>
                           <div class="accordion-content">
                              <p>
                                You must be a registered dealer to post a deal. Depending on your subscription, you can post a deal once every 3 days, every 2 days or every day. <a href="<?php echo base_url('login'); ?>" target="_blank">Login</a> to your account and click <a href="<?php echo base_url('post-deal'); ?>" >Post A Deal</a>. Provide the information for your deal such as: its Title, Code, Product Category & Sub Category, Brand, UPC, SKU, Price, Shipping Rate, a 313x234 Image, and a short description of the product.
                              </p>
                           </div>
                        </li>
                        <li>
                           <h3 class="accordion-title"><a href="#">Do you have subscription fees to post product deals?</a></h3>
                           <div class="accordion-content">
                              <p>
                                 Yes, please go to our <a href="<?php echo base_url('pricing'); ?>" target="_bl">Pricing page</a>  and select a plan that fits your needs.
                              </p>
                           </div>
                        </li>
                        <li>
                           <h3 class="accordion-title"><a href="#">What method of payments do you accept?</a></h3>
                           <div class="accordion-content">
                              <p>We accept all major credit cards.</p>
                           </div>
                        </li>
                         <li>
                           <h3 class="accordion-title"><a href="#">Popular deals Calculation</a></h3>
                           <div class="accordion-content">
                              <p>
                                We display deals under Popular deals base on the following criteria : <br>
                                Total number of views multiply to 50% percent <br>
                                Total number of likes multiply to 30% percent<br>
                                Total number of comments  multiply to 20% percent<br>
                                The highest average will be display on the Popular deals section.
                              </p>
                           </div>
                        </li>
                     </ul>

                    <div class="clearfix_row_2"></div>  
                    <label class="lbl_faqTitlle">Standard Work Instruction</label>
                    <ul class="standard_list">
                      <li>
                        <h3 class="lbl_standardTitle">
                          <a href="<?php echo base_url('Dealeraccount'); ?>" target="_blank">How To Create A Dealer Account</a>
                        </h3>
                      </li>
                      <li>
                        <h3 class="lbl_standardTitle">
                          <a href="<?php echo base_url('Customeraccount'); ?>" target="_blank">How To Create A User/Constumer Account</a>
                        </h3>
                      </li>
                      <li>
                        <h3 class="lbl_standardTitle">
                          <a href="<?php echo base_url('Postdeal'); ?>" target="_blank">How To Post A Deal</a>
                        </h3>
                      </li>
                      <li>
                        <h3 class="lbl_standardTitle">
                          <a href="<?php echo base_url('Usersinstruc'); ?>" target="_blank">Standard Instruction - Users</a>
                        </h3>
                      </li>
                    </ul>
                  </div>

                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink1">
                          <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                  </div>

               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>




         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>