<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">How To Create A Dealer Account</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page pattern-bg ">


            <!-- Main Container -->
            <div class="container-fluid">
               <!-- Row -->
               <div class="row">


                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink">
                          <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                    </div>


                  <!-- Middle Content Area -->
                  
                  <div class="col-lg-8 col-md-6 col-xs-12 col-sm-12">

                     <label class="lbl_faqTitlle">How To Create A Dealer Account</label>

                     <div class="">
                              <p>
                                 By having a dealer account you can post and advertise your products, receive feedback from the consumers about your products and receive detailed market analysis.
                              </p>
                              <br>
                              <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                  <h4>Here’s how to create or register as a dealer:</h4>
                                  <br>
                                  <p>
                                    1. Click the <strong>Register</strong> button at the upper right corner of the website to go to the registration selection page and select <strong> Register as a dealer</strong>. Please note that we accept only one account per dealer.
                                  </p>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                  <div class="wrap_imgaccord">
                                    <img src="<?=base_url(). 'assets/images/register.jpg'?>">
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                  <br>
                                  <p>
                                    2. Input the full name of a dealer representative, dealer phone number, dealer or representative email address, dealer name, dealer premise or mailing address, & dealer website in the required fields. Upload a copy of your FFL and SOT documents. <br><br>
                                    <i><strong>Note:</strong> All fields marked with an <span class="asterisk">**</span> are required information.</i> 
                                  </p>
                                  <br>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                  <div class="wrap_imgaccord">
                                    <img src="<?=base_url(). 'assets/images/register2.jpg'?>">
                                  </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                  <p>
                                    3. Enter a username and password that you will use to login to your account.
                                  </p>
                                  <p>
                                    4. Read and review the <strong>Terms and Conditions</strong> by clicking the link provided.
                                  </p>
                                  <p>
                                    5. After reading and agreeing to the <strong>Terms and Conditions</strong>, click REGISTER to submit your form.
                                  </p>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                  <br>
                                  <p>
                                    6. The account information you entered and documents you submitted will be reviewed.  We will then send you an email confirmation when your registration is approved.
                                  </p>
                                  <br>
                                </div>
                              </div>
                           </div>

                  </div>

                    <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink1">
                          <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                    </div>


               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>




         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>