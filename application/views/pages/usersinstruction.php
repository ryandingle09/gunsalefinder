<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">Standard Instruction - Users</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page pattern-bg ">


            <!-- Main Container -->
            <div class="container-fluid">
               <!-- Row -->
               <div class="row">


                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink">
                          <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                    </div>


                  <!-- Middle Content Area -->
                  
                  <div class="col-lg-8 col-md-6 col-xs-12 col-sm-12">

                     <label class="lbl_faqTitlle">Standard Instruction - Users</label>


                       <div class="">
                          <p>
                             Gun Sale Finder is an online search engine & marketplace designed to provide real time information regarding product and pricing on firearms, ammo, archery, knives and accessories.
                          </p>
                          <p>
                            Users have the ability to provide their comments by rating each product or deal, participating in the forum, and thereby affecting the status of the marketplace.
                          </p>
                          <br><br>
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <h4>A. How to Utilize the Price Comparison Feature:</h4>
                              <br>
                              <p>
                                1. Type-in the UPC number, Manufacturer’s ID, or Key Words of the item you want to compare.
                              </p>
                              <br>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord2">
                                <img src="<?=base_url(). 'assets/images/register8.jpg'?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <br>
                              <p>
                                2. Hit the Enter Key on your keyboard or click the desired item from the suggestion to proceed.
                              </p>
                              <p>
                                3. If the search is successful, the product price comparison will be displayed. You can proceed with your desired dealer by clicking the <strong>Click to View</strong> button corresponding to their name.  When you click the link, you will be taken to the dealer’s website to make your purchase.
                              </p>
                              <br>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord2">
                                <img src="<?=base_url(). 'assets/images/register9.jpg'?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <br><br>
                              <h4>B. How to Add a Product to Your Wishlist:</h4>
                              <br>
                              <p>
                                Do you want to get notified if a certain deal or product has a price drop? By adding the item to your wishlist, you will receive an email notification every time there is a change on that deal.
                              </p>
                              <br>
                              <p>
                                1. To add the product to your wishlist, click the <strong>Add to Wishlist</strong> button while viewing the product. An acknowledgment message will appear if successful.
                              </p>
                              <br>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord2">
                                <img src="<?=base_url(). 'assets/images/register10.jpg'?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br><br></div>
                          </div>
                          <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <br>
                              <p>
                                2. To view your wishlist, click the drop-down arrow beside <strong>My Account</strong> located at the upper right corner of the website.
                              </p>
                              <p>
                                3. Select <strong>My Wishlist</strong>.
                              </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord">
                                <img src="<?=base_url(). 'assets/images/register11.jpg'?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <br>
                              <p>
                                4. At the User Profile page, a summary of your wishlist items will be displayed.
                              </p>
                              <br>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord2">
                                <img src="<?=base_url(). 'assets/images/register12.jpg'?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                              <br><br>
                              <h4>C. How to Report an Issue with a Deal or Inappropriate Comments:</h4>
                              <br>
                              <p>
                                Deals posted on the website may contain errors like broken links,  wrong images or incomplete descriptions. Registered users can provide comments to any deal they see or like provided that they follow the website’s terms of service and privacy policy. A violation to this policy will be deemed inappropriate. 
                              </p>
                              <br>
                              <p>
                               If you find an issue,  you have the option to report it so that the responsible dealer will be notified.
                              </p>
                              <br>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <br>
                              <p>
                               1. While viewing the product, click <strong>Report Issue</strong>, a new window will popup where you can select the <strong>type of problem</strong> and provide a <strong>detailed description/report</strong> of the issue.
                              </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord">
                                <img src="<?=base_url(). 'assets/images/register13.jpg'?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br><br></div>
                          </div>
                          <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <br>
                              <p>
                               2. Hit <strong>Submit</strong> to continue.
                              </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord">
                                <img src="<?=base_url(). 'assets/images/register14.jpg'?>">
                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><br><br></div>
                          </div>
                          <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <br>
                              <p>
                               3. An acknowledgment screen will be displayed indicating that your report is successful.
                              </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                              <div class="wrap_imgaccord">
                                <img src="<?=base_url(). 'assets/images/register15.jpg'?>">
                              </div>
                            </div>
                          </div>
                       </div>


                  </div>
 







                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="#" target="_blank" id="bannerLink1">
                        <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                        </a>
                    </div>
                  </div>


               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>




         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>