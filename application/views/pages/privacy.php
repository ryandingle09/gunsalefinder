<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li>
                     <a class="active" href="<?php echo base_url('privacy'); ?>">Privacy Policy</a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page content-section">



            <!-- Main Container -->
            <div class="container-fluid">
               <div class="row">
               

                  <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink">
                          <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                    </div>


                  <div class="col-lg-8 col-md-6 col-xs-12 col-sm-12">
                     <div class="heading-panel"><h3 class="main-title text-left">Privacy Policy</h3></div>
                     <div class="row api-content-header">
                        <!-- Middle Content Area -->
                           <p class="content-paragraph">
                             When using GunSaleFinder.com, the servers may collect certain non-personal information like IP addresses, operating systems, or browser information. Any personal information GunSaleFinder.com collects and stores is the information that users voluntarily disclose. This information includes but is not limited to; any information that identifies, relates to, describes, or is capable of being associated with, a particular individual name or company name; a home, store, or other physical address, including street name, city, town or zip code; email addresses; phone numbers; government issued ID or licenses; any other identifier or license that permits the physical or online contacting of a specific individual or dealer. Personal Information does not include publicly available information that is lawfully made available to the general public from state, federal, or local government records. GunSaleFinder.com will never intentionally collect personal information from children who are younger than the age of majority in your jurisdiction. GunSaleFinder.com may use personal or business information for any legal purpose in its sole discretion. This includes but is not limited: Registration or Membership, Log-IN Data, Order or Fulfillment Information. You hereby agree that GunSaleFinder.com may also use users or businesses information to contact by phone, mail or email. GunSaleFinder.com may transfer personal information to third parties for order fulfillment, to respond to legal processes or governmental requests for information, or for any legal purpose. GunSaleFinder.com may use cookies to store or retrieve information. You hereby authorize the use of cookies by GunSaleFinder.com. GunSaleFinder.com contains links and displays advertising from third parties. Personal information that users voluntarily provide to third parties through links on GunSaleFinder.com may be shared with GunSaleFinder.com by the advertisers and other third parties. GunSaleFinder.com may receive personal information from partners/advertisers when you have voluntary agreed to allow a third party to share the data. GunSaleFinder.com has no control over the privacy policies of these third parties. Users always should be sure to read the privacy policy on any website visited before accepting the terms. GunSaleFinder.com stores user’s personal information on its computers, and believes reasonable security measures are in place to protect that data. However, GunSaleFinder.com cannot guarantee that all security measures will prevent its computers from being accessed without authorization. If users stored personal information is stolen, deleted, or changed, GunSaleFinder.com assumes no responsibility for such unauthorized actions. GunSaleFinder.com’s system may not be set up to cover all browser’s request so GunsSaleFinder.com makes no guarantee that “do not track” browser options will work on GunSaleFinder.com or on third party websites users can chose to be transferred to from GunSaleFinder.com. GunSaleFinder.com reserves the right to revise its privacy policy anytime without prior notice.

                           </p>
                           
                        </div>
                     </div>


                     <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink1">
                          <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                    </div>

               </div>
            </div>



            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>