<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">About Us</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding pattern_dots">



            <div class="container-fluid">
               <div class="row">


                    <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                        <div class="wrap_ads text-center">
                            <a href="#" target="_blank" id="bannerLink">
                            <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                            </a>
                        </div>
                    </div>
                    
                  <div class="col-lg-8 col-md-6 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-lg-6 col-md-7 col-sm-7 col-xs-12">
                           <div class="about-us-content">
                              <div class="heading-panel">
                                 <h3 class="main-title text-left">
                                    About GunSaleFinder.com
                                 </h3>
                              </div>
                              <h2></h2>
                              <p>GunSaleFinder.com is an online search engine to marketplace designed to provide real time information regarding product and pricing on firearms, ammo, archery, knives and accessories. We strive to be a resource to our customers so that they can make informed decisions when conducting purchases. GunSaleFinder.com does not sell any of the items listed and all sales and deals are made between the listed retailer and the customer. GunSaleFinder.com does offer marketing services to retailers, manufacturers and other vendors related to the shooting and firearms industry. </p>
                           </div>
                        </div>
                        <div class="col-lg-6 col-md-5 col-sm-5 col-xs-12">
                           <div class="about-page-featured-image">
                              <a href="#"><img class="img-about-us" src="<?php echo base_url(); ?>assets/images/pages/aboutus.jpeg" alt=""></a>
                           </div>
                        </div>
                     </div>
                     <div class="clearfix_row"></div>
                  </div>


                <div class="col-lg-2 col-md-3 hidden-sm hidden-xs">
                    <div class="wrap_ads text-center">
                        <a href="#" target="_blank" id="bannerLink1">
                        <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                        </a>
                    </div>
                </div>


               </div>
               
            </div>

      </section>
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("pages/script"); ?>
    </body>
</html>