<!DOCTYPE html>
<?php 
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>    
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">  
    <link href="<?php echo ASSET_URL; ?>assets/images/header/gunpro-favicon.png" rel="icon" type="image/x-icon">
    <meta property="og:image" content="<?php echo ASSET_URL; ?>assets/images/header/gunpro-header.png" /> 
    <title> Gunsalefinder <?php echo (isset($pagetitle)) ? ' - '.$pagetitle  :''; ?> </title>
    <!-- Facebook Meta -->
    <meta property="og:url" content="<?=$actual_link?>" /> 
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo (isset($pagetitle)) ? $pagetitle.' - Gunsalefinder' :'Gunsalefinder'; ?>" />
    <meta property="og:description" content="<?=isset($descriptions[0]['descriptions']) ?$descriptions[0]['descriptions'] : '' ?>" />
    <meta property="og:image" content="<?=isset($image_url) ?$image_url :''?>" />
    <meta property="og:image:width" content="119" />
    <meta property="og:image:height" content="73" />
    <meta property="fb:app_id" content="<?=FB_ID?>" />

    <link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/css/bootstrap.css">

    <!-- =-=-=-=-=-=-= Template CSS ssStyle =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/css/style.css">
    <!-- =-=-=-=-=-=-= Font Awesome =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/css/font-awesome.css" type="text/css">
    <!-- =-=-=-=-=-=-= Flat Icon =-=-=-=-=-=-= -->
    <link href="<?php echo ASSET_URL; ?>assets/css/flaticon.css" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Et Line Fonts =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/css/et-line-fonts.css" type="text/css">
    <!-- =-=-=-=-=-=-= Menu Drop Down =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/css/forest-menu.css" type="text/css">
    <!-- =-=-=-=-=-=-= Animation =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/css/animate.min.css" type="text/css">
    <!-- =-=-=-=-=-=-= Select Options =-=-=-=-=-=-= -->
    <link href="<?php echo ASSET_URL; ?>assets/css/select2.min.css" rel="stylesheet" >
    <!-- =-=-=-=-=-=-= noUiSlider =-=-=-=-=-=-= -->
    <link href="<?php echo ASSET_URL; ?>assets/css/nouislider.min.css" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Listing Slider =-=-=-=-=-=-= -->
    <link href="<?php echo ASSET_URL; ?>assets/css/slider.css" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Owl carousel =-=-=-=-=-=-= -->
    <link rel="stylesheet" type="text/css" href="<?php echo ASSET_URL; ?>assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ASSET_URL; ?>assets/css/owl.theme.css">
    <!-- =-=-=-=-=-=-= Check boxes =-=-=-=-=-=-= -->
    <link href="<?php echo ASSET_URL; ?>assets/skins/minimal/minimal.css" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Responsive Media =-=-=-=-=-=-= -->
    <link href="<?php echo ASSET_URL; ?>assets/css/responsive-media.css" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Template Color =-=-=-=-=-=-= -->
    <link rel="stylesheet" id="color" href="<?php echo ASSET_URL; ?>assets/css/colors/sea-green.css">
    <!-- =-=-=-=-=-=-= For Style Switcher =-=-=-=-=-=-= -->
    <link rel="stylesheet" id="theme-color" type="text/css" href="<?php echo ASSET_URL; ?>assets/css/style.css" />
    <!-- =-=-=-=-=-=-= Check boxes =-=-=-=-=-=-= -->
    <link href="<?php echo ASSET_URL; ?>assets/skins/minimal/minimal.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/css/dropzone.css">
    <link href="<?php echo ASSET_URL; ?>assets/css/jquery.tagsinput.min.css" rel="stylesheet">

    <script src="<?php echo ASSET_URL; ?>assets/js/modernizr.js"></script>

    <!-- =-=-=-=-=-=-= Easy Autocomplete =-=-=-=-=-=-= -->
    <link href="<?php echo ASSET_URL; ?>node_modules/easy-autocomplete/dist/easy-autocomplete.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/css/override.css">
    <!--<link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">-->

    <!-- Hotfix Assets -->
    <link rel="stylesheet" href="<?php echo ASSET_URL; ?>assets/css/hotfix.css">
    <link href="https://fonts.googleapis.com/css?family=Black+Ops+One" rel="stylesheet">

    <script type="text/javascript" src="https://js.gleam.io/e.js" async="true"></script> 

    
    <style>
      /* styles unrelated to zoom */
      .zoom {
        display:inline-block;
        position: relative;
      }
      
      /* magnifying glass icon */
      .zoom:after {
        content:'';
        display:block; 
        width:33px; 
        height:33px; 
        position:absolute; 
        top:0;
        right:0;
        background:url(icon.png);
      }

      /* .zoom img {
        display: block;
      } */
      .zoom img::selection { background-color: transparent; }
      /*landingpage stylesheet overwrite*/
      .easy-autocomplete{
        width: auto !important;
      }

      .banner-side-container{
        margin-top: 30px;
      }
      /*end*/

    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-129598728-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-129598728-1');
    </script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WJZBV64');</script>
<!-- End Google Tag Manager -->
</head>

<body>
<!-- Additionally, paste this code immediately after the opening <body> tag: -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WJZBV64"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div class="colored-header">
     <!-- Top Bar -->
     <div class="header-top">
      <div class="container">
       <div class="row">
        <!-- Header Top Left -->
        <div class="header-top-left col-md-8 col-sm-6 col-xs-12 hidden-xs">
         <ul class="listnone">
          <!--<li>
              <a href="tel:+1800229933"><i class="fa fa-phone" aria-hidden="true"></i> +6380222111</a>
          </li>-->
          <li>
            <a href="mailto:support@gunsalefinder.com"><i class="fa fa-envelope-o" aria-hidden="true"></i> support@gunsalefinder.com</a>
          </li>
        </ul>
      </div>
      <!-- Header Top Right Social -->
      <div class="header-right col-md-4 col-sm-6 col-xs-12 ">
       <div class="pull-right">
        <ul class="listnone">
        <?php 
            if($this->common->checkSession()):
        ?>
          <li><a href="<?php echo base_url('login'); ?>"><i class="fa fa-sign-in"></i> Log in</a></li>
          <!-- <li><a href="registration"><i class="fa fa-unlock" aria-hidden="true"></i> Register</a></li> -->
          <li class="dropdown">
            <a href="<?php echo base_url('registration'); ?>"><i class="fa fa-unlock" aria-hidden="true"></i>&nbsp;Register&nbsp;</a>
          </li>
        <?php else: ?>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="icon-profile-male" aria-hidden="true"></i><?php  echo "MY ACCOUNT";//$this->session->userdata('name'); ?> <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <?php $url = ($this->session->userdata('dealer_login')) ? base_url('dealer/profile') : base_url('profile'); ?>
             <li><a href="<?php echo $url; ?>">User Profile</a></li>
             <?php if($this->session->userdata('dealer_login')): ?>
                <li><a href="<?php echo base_url('dealer-deals'); ?>">My Deals</a></li>
                 <li><a href="<?php echo base_url('dealer-wishlist'); ?>">My Wishlist</a></li>
                <li><a href="<?php echo base_url('my-ads'); ?>">My Ads</a></li>
                <li><a href="<?php echo base_url('reported-ads'); ?>">Reported Deals</a></li>
              <?php endif; ?>
              <?php if($this->session->userdata('logged_in')): ?>
                <li><a href="<?php echo base_url('wishlist'); ?>">My Wishlist</a></li>
              <?php endif; ?>
           </ul>
         </li>
         <li><a href="<?php echo base_url('login/logout'); ?>"><i class="fa fa-sign-out"></i> Log Out</a></li>
       <?php endif; ?>
      </ul>
    </div>
    </div>
    </div>
    </div>
    </div>


    <!-- Top Bar End -->
    <div class="header">
      <div class="container">


        <!-- <div class="row">
          <div class="col-md-5 col-md-offset-4"><label class="lbl_PComparison">Price Comparison</label></div>
        </div> -->


        <div class="row">
        <!-- Logo -->
        <!-- <div class="col-lg-12 col-md-12 hidden-sm hidden-xs text-center">
          <label class="lbl_PComparison"><span class="lbl_Price"><span>P</span>rice</span>&nbsp;<span class="lbl_Price2"><span>C</span>omparison</span></label>
        </div> -->
        <div class="clearfix_row_2 hidden-sm hidden-xs"></div>
        <div class="col-md-2 col-sm-12 col-xs-12 text-center">
            <div class="logo">
              <a href="<?php echo ASSET_URL; ?>"><img alt="logo" src="<?php echo ASSET_URL; ?>assets/images/header/gunpro-header.svg"> </a>
            </div>
         </div>

         <!-- <div class="col-sm-12 col-xs-12 hidden-lg hidden-md text-center">
          <label class="lbl_PComparison"><span class="lbl_Price"><span>P</span>rice</span>&nbsp;<span class="lbl_Price2"><span>C</span>omparison</span></label>
        </div> -->
        <!-- Category -->
          <div class="col-md-8 col-sm-12 col-xs-12">
            <form action="<?php echo ASSET_URL; ?>search" id="searchForm" method="get">
              <!-- <div class="col-md-3 col-sm-4 no-padding src_marginTop">
              <?php
                $states = $this->common->getallStates();
              ?>
                <select class="category form-control">
                  <option>State</option>
                  <?php  foreach ($states as $state): ?>
                  <option value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>
                  <?php endforeach; ?>
                </select>
              </div> -->
              <!-- <input type="hidden" name="itemId" id="itemId" /> -->

                <div class="text-center">
                  <input id="txtSearch" name="search" type="text" value="<?php echo isset($postData['txtSearch']) ?  $postData['txtSearch'] :'' ?>" placeholder="Search UPC, Model # Or Key Words Here To Find and Compare The Best Prices" class="form-control input_srch">
                  
                  <button class="src_btn" type="submit" id="btnSearch" >Search</button>
                  <!-- <span class="input-group-btn"></span> -->
                </div>
            
            </form>
          </div>
        
          <!-- Post Button -->
          <div class="col-md-2 col-sm-12 col-xs-12">
            <a href="<?php echo base_url('post-deal'); ?>" class="btn btn-orange btn-block">Post a Deal </a> <span class="free-flag visible-lg"></span>
          </div>

        </div>
      </div>
    </div>




    <div class="main-menu">
     <!-- Navigation Bar -->
     <nav id="menu-1" class="mega-menu">
      <!-- menu list items container -->
      <section class="menu-list-items">
        <div class="container">
         <div class="row">
          <div class="col-lg-12 col-md-12">
           <!-- menu logo -->
           <ul class="menu-logo">
            <li>
             <a href="<?php echo ASSET_URL; ?>"><img src="<?php echo ASSET_URL; ?>assets/images/logo-1.png" alt="logo"> </a>
           </li>
         </ul>
         <!-- menu links -->
         <ul class="menu-links">
          <!-- active class -->
          <li>
           <a href="<?php echo base_url(''); ?>"> Home </i></a>
         </li>
         <li>
           <a href="javascript:void(0)">Deals <i class="fa fa-angle-down fa-indicator"></i></a>
           <!-- drop down multilevel  -->
           <ul class="drop-down-multilevel">
            <li><a class="deal-page" data-link="date" href="<?php echo base_url('search/index/?sort=date'); ?>">Latest Deals</a></li>
            <li><a class="deal-page" data-link="rating" href="<?php echo base_url('search/index/?sort=rating'); ?>">Highest Rated Deals</a></li>
            <li><a class="deal-page" data-link="like" href="<?php echo base_url('search/index/?sort=like'); ?>">Most Liked Deals</a></li>
            <li><a class="deal-page" data-link="views_count" href="<?php echo base_url('search/index/?sort=views_count'); ?>">Most Viewed Deals</a></li>
            <li><a class="deal-page" data-link="commented" href="<?php echo base_url('search/index/?sort=commented'); ?>">Most Commented Deals</a></li>
          </ul>
        </li>
        <li><a class="deal-page" data-link="category" href="<?=base_url('search'); ?>"> Categories</a></li>
        <li><a href="<?php echo base_url('promotion/learnmore'); ?>">Promotion </a></li>
        <li><a href="<?php echo base_url('giveaway'); ?>">Giveaway</a></li>
        <li><a href="<?php echo base_url('about'); ?>">About </a></li>
        <li><a href="<?php echo base_url('pricing'); ?>">Pricing</a></li>
        <li><a href="<?php echo base_url('dealer-post'); ?>">Advertise </a></li>
        <li><a href="<?php echo base_url('contact'); ?>">Contact </a></li>
      </ul>
      <ul class="menu-search-bar hidden">
        <li>
         <a href="<?php echo base_url('post-deal'); ?>" class="btn btn-light"><i class="fa fa-plus" aria-hidden="true"></i> Post a Deal</a>
       </li>
     </ul>
    </div>
    </div>
    </div>
    </section>
    </nav>
    </div>
    <div class="clearfix"></div>
    <!-- =-=-=-=-=-=-= Dark Header End =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix"></div>

