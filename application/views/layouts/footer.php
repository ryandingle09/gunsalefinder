   <footer>
      <!-- Footer Content -->
      <div class="footer-top">
         <div class="container">
            <div class="row">
               <div class="col-md-3  col-sm-6 col-xs-12">
                  <!-- Info Widget -->
                  <div class="widget footer-logo">
                     <div class="logo"> <img alt="" src="<?php echo ASSET_URL; ?>assets/images/header/gunpro-header-black.svg"> </div>
                     <p class="brand-text">Gun Sale Finder is an online search engine & marketplace designed to provide real time information regarding product and pricing on firearms, ammo, archery, knives and accessories.</p>
                     <ul>
                        <li>
                          <div class="social-links-two clearfix"> 
                              <a class="facebook img-circle" href="https://web.facebook.com/sales.gunsalefinder.3?_rdc=1&_rdr"><span class="fa fa-facebook-f"></span></a>
                              <a class="twitter img-circle" href=" https://twitter.com/GunSaleFinder" target="_blank"><span class="fa fa-twitter"></span></a>
                              <a class="google-plus img-circle" href="https://plus.google.com/u/4/103781460892423544008" target="_blank"><span class="fa fa-google-plus"></span></a>
                              <a class="linkedin img-circle" href="https://www.linkedin.com/feed/?trk=onboarding-landing" target="_blank"><span class="fa fa-linkedin"></span></a>
                              <a class="linkedin img-circle" href="mailto:support@gunsalefinder.com?subject=Contact%20Us&body=Email%20Message%20here" target="_blank"><span class="fa fa-envelope"></span></a>
                            </div>
                        </li>
                     </ul>
                  </div>
                  <!-- Info Widget Exit -->
               </div>
               <div class="col-md-2  col-sm-6 col-xs-12">
                  <!-- Follow Us -->
                  <div class="widget socail-icons">
                     <h5 class="information">Information</h5>
                     <ul class="menu-link">
                        <li><a href="<?php echo base_url('about'); ?>">About Us</a></li>
                        <li><a href="<?php echo base_url('pricing'); ?>">Pricing</a></li>
                        <li><a href="<?php echo base_url('terms-and-conditions'); ?>">Terms & Conditions</a></li>
                        <li><a href="<?php echo base_url('privacy-policy'); ?>">Privacy Policy</a></li>
                        <li><a href="<?php echo base_url('commentspolicy'); ?>">Comments Policy</a></li>
                     </ul>
                  </div>
                  <!-- Follow Us End -->
               </div>

               <div class="col-md-2  col-sm-6 col-xs-12">
                  <!-- Follow Us -->
                  <div class="widget socail-icons">
                     <h5 class="help">Help</h5>
                     <ul class="menu-link">
                        <li><a href="<?php echo base_url('contact'); ?>">Contact Us</a></li>
                        <li><a href="<?php echo base_url('api-guide'); ?>">API Guide</a></li>
                        <li><a href="<?php echo base_url('faqs'); ?>">FAQS</a></li>
                        <li><a href="<?php echo base_url('dealer/dashboard'); ?>">Subscribe</a></li>
                        <li><a href="<?php echo base_url('promotion/learnmore'); ?>">Promotion</a></li>
                     </ul>
                  </div>
                  <!-- Follow Us End -->
               </div>

               <div class="col-md-5  col-sm-6 col-xs-12">
                  <!-- Newslatter -->
                  <div class="widget widget-newsletter">
                     <h5>Sign Up for Weekly Newsletter</h5>
                     <div class="fieldset">
                        <p>We may send you information regarding product and pricing on firearms, ammo, archery, knives and accessories.</p>
                        <div id="newslettermsg"></div>
                        <form>
                           <input name="email" id="emailaddress" placeholder="Enter your email address" type="text">
                           <input class="submit-btn" name="submit" value="Submit" type="button" id="subscribeemail"> 
                        </form>
                     </div>
                  </div>
                  <!-- Newslatter -->
               </div>
            </div>
         </div>
      </div>
      <!-- Copyrights -->
      <div class="copyrights">
         <div class="container">
            <div class="copyright-content">
               <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                     <p>© 2018 Gunsalefinder. All rights reserved. </p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </footer>

<div tabindex="-1" class="modal fade dislike-product in" role="dialog" aria-hidden="true" style="padding-right: 16px; display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title">Why Dislike Product.</h3>
            </div>
            <div class="modal-body">
                <!-- content goes here -->
                <form>
                    <div class="form-group col-md-3">
                        <label>Type of problem:</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio1" name="disLikeType" class="custom-control-input" value="Unsatisfactory">
                            <label class="custom-control-label" for="customRadio1">Unsatisfactory</label>
                        </div>
                        
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio2" name="disLikeType" class="custom-control-input" value="Bad Dealer">
                            <label class="custom-control-label" for="customRadio2">Bad Dealer</label>
                        </div>
                        
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio3" name="disLikeType" class="custom-control-input" value="Spam">
                            <label class="custom-control-label" for="customRadio3">Spam</label>
                        </div>
                        
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio4" name="disLikeType" class="custom-control-input" value="Defective">
                            <label class="custom-control-label" for="customRadio4">Defective</label>
                        </div>
                        
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio5" name="disLikeType" class="custom-control-input" value="Poor Product">
                            <label class="custom-control-label" for="customRadio5">Poor Product</label>
                        </div>
                    </div>
                    <div class="form-group  col-md-9 col-sm-9">
                        <label>Dislike Details</label>
                        <textarea id="dislikeDetails" placeholder="This message will be send to the dealer." rows="3" class="form-control " required></textarea>
                    </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 col-sm-12 margin-bottom-20 margin-top-20">
                            <input type="hidden" id="product_id">
                            <button onClick="dislikeSubmit()" type="button" class="btn btn-theme btn-block">Submit</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
<?php include_once('scripts.php'); ?>
