<?php 
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>  
<?php 
  /*switch ($images) {
    case '':
    $image_url =  base_url() .'assets/pictures/default.jpg';
    break;
    default:
    $image_url = $images ;

  }*/
$image_url = isset($images) ? $images : base_url() .'assets/pictures/default.jpg' ;
?>


<div tabindex="-1" class="modal fade share-ad" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title">Share</h3>
            </div>
            <div class="modal-body">
                <div class="recent-ads">
                    <div class="recent-ads-list">
                        <div class="recent-ads-container">

                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                                    <div class="wrap_imgModal">
                                        <img alt="Product Image" src="<?=$image_url?>" title="Product Image">
                                    </div>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                    <div class="recent-ads-list-content">
                                        <h3 class="recent-ads-list-title">
                                            <a href="#"><?=$title?></a>
                                        </h3>
                                        <ul class="recent-ads-list-location">
                                            <li><a href="#">New York</a></li>
                                            <li><a href="#">Brooklyn</a></li>
                                        </ul>
                                        <div class="recent-ads-list-price price">
                                            <ul>
                                                <li>
                                                    <label class="lbl_modal1">Price:&nbsp;$<?=$price?></label>
                                                </li>
                                                <li>
                                                    <span class="lbl_modal">Shipping Fee:</span><span class=""><?=isset($shipping_fee) ? $shipping_fee : '0.00' ?></span>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.recent-ads-list-price -->
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                        <!-- /.recent-ads-container -->
                    </div>
                </div>
                <div class="wrap_modalContent">
                    <h3>Descriptions</h3>
                    <p> <?=is_array(($pdescriptions)) ? $pdescriptions[0]['descriptions'].'...' : '' ?> </p>
                    <h3>Link</h3>
                    <a class="lbl_modalLink" href="<?=$actual_link?>"><?=$actual_link?></a>
                    <p>
                        <!-- <a href="#" data-target="#share-email" data-toggle="modal" data-keyboard="false">Share via email</a> -->
                        <button class="btn_show" id="show"><i class="fa fa-share-alt"></i>&nbsp;Share via email</button>
                        <div class="wrap_inputShare">
                            <input type="email" class="input_email" id="modal_shareemail" placeholder="Email Address">
                            <button class="btnShare" id="btnShareEmailProduct">Share</button>
                        </div>
                    </p>
                </div>    
                </div>
                <div class="modal-footer">
	            <!-- <a class="btn btn-fb btn-md" href="https://www.facebook.com/sharer/sharer.php?app_id=<?=FB_ID?>&sdk=joey&u=<?=$image_url?>&display=popup&ref=plugin&src=share_button" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
			<i class="fa fa-facebook"></i>
		    </a> -->
		    <input type="hidden" id="itemDesc" value="<?=is_array(($pdescriptions)) ? $pdescriptions[0]['descriptions'] . '...' : '' ?>" />
                    <input type="hidden" id="itemTitle" value="<?=$title?>" />
                    <input type="hidden" id="itemImage" value="<?=$image_url?>" />
                    <input type="hidden" id="itemUrl" value="<?=$actual_link?>" />
                    <a class="btn btn-fb btn-md btnFbShare" >
                        <i class="fa fa-facebook btnFbShare"></i>
                    </a>
                    <a class="btn btn-twitter btn-md"  >
                        <i class="fa fa-twitter"></i>
                    </a>
                    <!-- <button id="twitter"></button> -->
                    <a class="btn btn-gplus btn-md" >
                        <i class="fa fa-google-plus"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>




<!-- 

<div tabindex="-1" class="modal fade" id="share-email" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title">Share</h3>
            </div>
            <div class="modal-body">
                <input type="email" id="modal_shareemail" placeholder="Email Address">
                <div class="modal-footer">
                    <a class="btn btn-md" id="btnShareEmailProduct">
                        Share
                    </a> 
                </div>
            </div>
        </div>
    </div>
 -->