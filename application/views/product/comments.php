<?php
    $currentUser = $this->session->userdata('id');
?>
<li class="comment" id="comment<?php echo $id ?>">
    <div class="comment-info">
        <img class="pull-left hidden-xs img-circle" src="
            <?php echo base_url(); ?>assets/pictures/userprofile90x90.png" alt="author">
            <div class="author-desc">
                <div class="author-title">
                    <strong> <?=isset($USER_USERNAME) ? $USER_USERNAME : $DEALER_USERNAME ?></strong>
                    <ul class="list-inline pull-right">
                        <li>
                            <a href="#"><?= $this->common->timetoago($created_at);?></a>
                        </li>
                    </ul>
                </div>
                <span id="message<?php echo $id ?>"><?php echo $message ?></span>
                <span style="display:none;" id="edit<?php echo $id ?>">
                    <div class="input-group">
                        <textarea class="form-control custom-control" rows="3" style="resize:none" id="textMsg<?php echo $id ?>"><?php echo $message ?></textarea>     
                        <span class="input-group-addon btn btn-primary" onClick="updateComment(<?php echo $id ?>)"><i class="fa fa-save"></i></span>
                    </div>
                </span>
                
                <ul class="list-inline pull-right" id="util<?php echo $id ?>">
                    <li>
                    <?php
                        if($user_id == $currentUser)
                        {
                    ?>
                        <a id="editBtn<?php echo $id ?>" onClick="toggleEdit(<?php echo $id ?>)">
                            <i class="fa fa-pencil"></i>
                        </a>    
                        <a id="deleteBtn<?php echo $id ?>" onClick="deleteComment(<?php echo $id ?>)">
                            <i class="fa fa-trash"></i>
                        </a>
                    <?php
                        }
                        else
                        {
                    ?>  
                        <a onClick="reportComment(<?php echo $id ?>)">
                            <i class="fa fa-exclamation-circle"></i>
                        </a>
                    <?php
                        }
                    ?>
                    </li>
                </ul>
                
            </div>
        </div>
    </li>