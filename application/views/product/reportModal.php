<div tabindex="-1" class="modal fade report-product in" role="dialog" aria-hidden="true" style="padding-right: 16px; display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title">Report Product.</h3>
            </div>
            <div class="modal-body">
                <!-- content goes here -->
                <form>
                    <div class="form-group col-md-3">
                        <label>Type of problem:</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio1" name="reportType" class="custom-control-input" value="Unsatisfactory">
                            <label class="custom-control-label" for="customRadio1">Unsatisfactory</label>
                        </div>
                        
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio2" name="reportType" class="custom-control-input" value="Bad Dealer">
                            <label class="custom-control-label" for="customRadio2">Bad Dealer</label>
                        </div>
                        
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio3" name="reportType" class="custom-control-input" value="Spam">
                            <label class="custom-control-label" for="customRadio3">Spam</label>
                        </div>
                        
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio4" name="reportType" class="custom-control-input" value="Defective">
                            <label class="custom-control-label" for="customRadio4">Defective</label>
                        </div>
                        
                        <div class="custom-control custom-radio">
                            <input type="radio" id="customRadio5" name="reportType" class="custom-control-input" value="Poor Product">
                            <label class="custom-control-label" for="customRadio5">Poor Product</label>
                        </div>
                    </div>
                    <div class="form-group  col-md-9 col-sm-9">
                        <label>Report Details</label>
                        <textarea id="reportDetails" placeholder="This message will be send to the user." rows="3" class="form-control " required></textarea>
                    </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 col-sm-12 margin-bottom-20 margin-top-20">
                            <button id="reportProduct" type="button" class="btn btn-theme btn-block">Submit</button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
<div tabindex="-1" class="modal fade report-comment in" role="dialog" aria-hidden="true" style="padding-right: 16px; display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title">Report Comment.</h3>
            </div>
            <div class="modal-body">
                    <div class="form-group  col-md-12 col-sm-12">
                        <label>Report Details</label>
                        <textarea id="reportCommentDetails" placeholder="This message will be send to the admin for review." rows="3" class="form-control " required></textarea>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 col-sm-12 margin-bottom-20 margin-top-20">
                        <button id="reportCommentBtn" type="button" class="btn btn-theme btn-block">Submit</button>
                    </div>
                    <input type="hidden" id="reportCommentID">
            </div>
        </div>
    </div>
</div>