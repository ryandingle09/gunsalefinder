<?php 
  $this->load->view("layouts/header");
  $a = is_null($dealer_info) ? null : json_decode($dealer_info, true); 
  $b = is_null($category_info) ? null : json_decode($category_info, true);
  $c = is_null($sub_category_info) ? null : json_decode($sub_category_info, true);
  $d = is_null($brand_info) ? null : json_decode($brand_info, true);
?>


 <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">Details</a></li>
               </ul>
            </div>
         </div>
      </div>

      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Transparent Breadcrumb End =-=-=-=-=-=-= -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page pattern-bgs gray ">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <?php echo $this->session->flashdata('message'); ?>

                  <!-- Middle Content Area -->
                  <div class="col-md-8 col-xs-12 col-sm-12">
                     <!-- Single Ad -->
                     <div class="single-ad">
                        <!-- Ad Slider -->

                        <div class="row">
                          
                          <div class="col-md-6 col-sm-12 col-xs-12">

                            <div class="flexslider single-page-slider">
                              <div class="flex-viewport">
                                 <ul class="slides slide-main">
                                    <?php if(count($thumbnails) > 0): ?>
                                         <?php
                                         $no = 1;  
                                         foreach ($thumbnails as $thumb): 
                                            if(($no == 1)){
                                         ?>
                                            <li class="wrap_img_slider_view_prod flex-active-slide">
                                              <img alt="Product Image" src="<?=$product_image?>" title="Product Image <?php echo $no; ?>">
                                            </li>
                                          <?php } else { ?>
                                            <li class="wrap_img_slider_view_prod">
                                              <img alt="Product Image" src="<?php echo $thumb->image_url; ?>" title="Product Image <?php echo $no; ?>">
                                            </li>
                                          <?php
                                          }
                                          $no = $no+1;
                                          endforeach;
                                          ?>
                                      <?php else: ?>
                                            <li class="wrap_img_slider_view_prod">
                                              <img class="loadingImage alt="Product Image" src="<?php echo ($product_image !== NULL or $product_image !== '') ? $product_image : base_url() .'assets/pictures/default.jpg'; ?>" title="Product Image">
                                            </li>
                                      <?php endif; ?>
                                 </ul>
                              </div>
                           </div>
                           <div class="flexslider wrap_thumbnailSlider" id="carousels">
                              <div class="flex-viewport">
                              <?php if(count($thumbnails) > 0): ?>
                                 <ul class="slides slide-thumbnail reoveBGslider">
                                  <?php
                                   $tno = 1;  
                                   foreach ($thumbnails as $thumb): 
                                    if(($tno == 1)){
                                  ?>
                                    <li class="list_thumbnailsSlider flex-active-slide">
                                      <img alt="Deal Thumnail <?php echo $tno; ?>" draggable="false" src="<?=$product_image?>">
                                    </li>
                                  <?php } else { ?>
                                    <li class="list_thumbnailsSlider">
                                      <img alt="Deal Thumnail <?php echo $tno; ?>" draggable="false" src="<?php echo $thumb->image_url; ?>">
                                    </li>
                                  <?php
                                    }
                                    $tno = $tno+1;
                                    endforeach; 
                                  ?>
                                 </ul>
                              <?php endif; ?>
                              </div>
                           </div>
                          </div>

                            <div class="clearfix_3 col-sm-12 col-xs-12 hidden-md hidden-lg"></div>
                            <div class="hidden-lg hidden-md">
                              <div class="contact white-bg">
                                 <!-- Email Button trigger modal -->
                              <?php
                              $isDealer = $this->session->userdata('dealer_login');
                              $isDealer = false;
                              if(isset($isDealer) && $a[0]['id'] == $this->session->userdata('id'))
                              { 
                                $isDealer = true;
                              }
                              if($isDealer):
                              ?>
                                 <button class="btn-block btn-contact contactUpdate" data-toggle="modal" onclick="location.href='<?php echo base_url('dealer/postdeal/managedeal/'.$id); ?>';" >Manage Deal</button>
                                 <button data-upc="<?=$upc?>" class="btn-block btn-contact contactMoney compareUpc" data-toggle="modal" data-target=".price-quote" >Price Comparison</button>
                              <?php else: ?>
                                 <button data-upc="<?=$upc?>" class="btn-block btn-contact contactMoney compareUpc" data-toggle="modal" data-target=".price-quote" >Price Comparison</button>
                                 <button class="btn-block btn-contact contactCart" data-toggle="modal" data-target=".price-quote" onclick="window.open('<?=isset($item_url) ? $item_url : '' ?>')">Get Deal</button>
                              <?php endif; ?>
                              </div>
                              <!-- Price info block -->   
                              <div class="ad-listing-price">
                                 <p>$ <?=$price?></p>
                              </div>
                              <!-- User Info -->
                              <div class="white-bg user-contact-info">
                                 <div class="user-info-card">
                                    <div class="user-photo col-md-4 col-sm-3  col-xs-4">
                                       <img src="<?php echo base_url(); ?>assets/pictures/<?=$a[0]['picture']?>" alt="User Photo">
                                    </div>
                                    <div class="user-information no-padding col-md-8 col-sm-9 col-xs-8">
                                       <span class="user-name"><a class="hover-color" href="#"><?=$a[0]['company_name']?> </a></span>
                                       <div class="item-date">
                                          <span class="ad-pub">Published on: <?php echo $this->common->formatdate($created_at); ?></span><br>
                                          <a href="<?=base_url('search?dealer='.$a[0]['id'])?>" class="link">More Deals from this Dealer</a>
                                       </div>
                                    </div>
                                    <div class="clearfix"></div>
                                 </div>

                                 <div class="ad-listing-meta">
                                    <ul>
                                       <li>Contact No.: <span class="color"><?=$a[0]['contact']?></span></li>
                                       <li>Location: <span class="color"><?=$this->common->getStatebyDealer($a[0]['id']);?>, <?=$a[0]['city']?></span></li>
                                       <?php 
                                          $dealersites = $a[0]['web_url'];
                                       ?>
                                       <li>Dealer’s Websites: 
                                        <span> <a href="<?php echo $dealersites; ?>" class="label label-danger" target="_blank" >Link</a> </span>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                            </div>


                          <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="heading-panel remove_margin_RateTitle"><h3 class="main-title text-left">Rating</h3></div>
                              <div class="row rating-title-container">
                                <div class="col-xs-3">
                                <?php if(($rateScale['ROUND'])): ?>
                                  <label class="lbl_rateCount">
                                    <span><?= ($rateScale['ROUND']) ? $rateScale['ROUND'] : '0' ?></span>/<span>5</span>
                                  </label>
                                  <label><?= ($rateScale['TOTAL_RATES'] != null ) ? $rateScale['TOTAL_RATES'] : '0' ?>&nbsp;Ratings</label>
                                <?php else: ?>
                                      <span class="content-center">Not yet rated</span>
                                <?php endif; ?>
                                </div>

                                <div class="col-xs-9">
                                    <ul class="list rate-product-modal" style="cursor: pointer;">
                                      <li>
                                          <div class="rating">
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star"></i> 
                                              <span class="rating-count">(<?=$rateScale['FIVE']?>)</span> 
                                         </div>
                                      </li>
                                      <li>
                                          <div class="rating">
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star-o"></i> 
                                              <span class="rating-count">(<?=$rateScale['FOUR']?>)</span>
                                         </div>
                                      </li>
                                      <li>
                                          <div class="rating">
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star-o"></i> 
                                              <i class="fa fa-star-o"></i> 
                                              <span class="rating-count">(<?=$rateScale['THREE']?>)</span>
                                         </div>
                                      </li>
                                      <li>
                                          <div class="rating">
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star-o"></i> 
                                              <i class="fa fa-star-o"></i> 
                                              <i class="fa fa-star-o"></i> 
                                              <span class="rating-count">(<?=$rateScale['TWO']?>)</span>
                                         </div>
                                      </li>
                                      <li >
                                        <div class="rating">
                                              <i class="fa fa-star"></i> 
                                              <i class="fa fa-star-o"></i> 
                                              <i class="fa fa-star-o"></i> 
                                              <i class="fa fa-star-o"></i> 
                                              <i class="fa fa-star-o"></i> 
                                              <span class="rating-count">(<?=$rateScale['ONE']?>)</span>
                                         </div>
                                      </li>
                                    </ul>
                                  </div>
                                </div>



                                <div class="">
                                  <!-- Share Ad  --> 
                                  <div class="ad-share text-center">
                                     <div data-toggle="modal" data-target=".share-ad" class="ad-box ad-box2 col-md-4 col-sm-4 col-xs-12">
                                        <i class="fa fa-share-alt"></i> <label class="lbl_share">Share <br></label>
                                     </div>
                                     <div id="rate-product-modal" class="ad-box ad-box2 col-md-4 col-sm-4 col-xs-12 rate-product-modal">
                                        <i class="fa fa-star active"></i> <label class="lbl_share">Rate Product</label>
                                     </div>
                                     <div id="addtowishlist" class="ad-box ad-box2 col-md-4 col-sm-4 col-xs-12">
                                        <i class="fa fa-plus"></i> <label class="lbl_share">Add to Wishlist
                                        </label>
                                     </div>
                                     <div  id="report-issue" class="ad-box ad-box2 col-md-4 col-sm-4 col-xs-12">
                                        <i class="fa fa-exclamation-triangle"></i> <label class="lbl_share">Report Issue
                                        </label>
                                     </div>
                                     <div class="ad-box ad-box2 col-md-4 col-sm-4 col-xs-12" onClick="subsComment(this, <?php echo $id ?>)">
                                        <i class="fa <?php echo ($this->common->isProductSubsComment($id) == true) ? 'fa-bell' : 'fa-bell-o'?>"></i> <label class="lbl_share">Subscribe Comments
                                        </label>
                                     </div>
                                     <div class="ad-box ad-box2 col-md-4 col-sm-4 col-xs-12" onClick="subsPriceDrop(this, <?php echo $id ?>)">
                                        <i class="fa <?php echo ($this->common->isProductSubsPricedrop($id) == true) ? 'fa-bell' : 'fa-bell-o' ?>"></i> <label class="lbl_share">Subscribe Price Drop 
                                        </label>
                                     </div>
                                  </div>
                                </div>

                                
                          </div>
                        </div>



                        

                        <!-- Short Description  --> 
                        <div class="ad-box">
                          <div class="ad-title clearfix">
                              <h2><a href="#"> <?//$details[0]['title']?> </a> </h2>
                              <h2><a href="#"> <?=isset($title) ? $title : '' ?> </a></h2>
                              <div class="short-history">
                                <ul>
                                    <li>
                                        <i class="fa fa-eye" aria-hidden="true"></i> <b> <?php echo abs($view_count) + 1 ?> views</b>
                                    </li>
                                    <li>
                                    <a onClick="likeItem(this, <?php echo $id ?>)" title="Like This Deal">
                                      <b class="l_count_<?php echo $id ?>"><?php echo abs($like_count) ?></b> <i class="fa <?php echo ($this->common->thisProductLike($id)) ? 'fa-thumbs-up' : 'fa-thumbs-o-up' ?> like<?php echo $id ?>" aria-hidden="true"></i>
                                    </a>
                                    &nbsp;
                                    <a onClick="dislikeItem(this, <?php echo $id ?>)" title="Dislike This Deal">
                                      <b class="dl_count_<?php echo $id ?>"><?php echo abs($dislike_count) ?></b> <i class="fa <?php echo ($this->common->thisProductDislike($id)) ? 'fa-thumbs-down' : 'fa-thumbs-o-down' ?> dislike<?php echo $id ?>" aria-hidden="true"></i>
                                    </a>
                                        &nbsp;
                                      <a id="commentBtnBox" title="Comments">
                                        <b class="c_count_<?php echo $id ?>"><?php echo $comments['count']; ?></b> <i class="fa fa-comment-o" aria-hidden="true"></i>
                                      </a>
                                      &nbsp;
                                      <a onClick="subsPDC(this, <?php echo $id ?>)" title="Subscribe for Price Drop and Comments">
                                        <i class="fa <?php echo ($this->common->isProductSubs($id)) ? 'fa-bell' : 'fa-bell-o' ?>" aria-hidden="true"></i>
                                      </a>
                                    </li>
                                    
                                </ul>
                              </div>
                          </div>

                           <div class="short-features">
                              <!-- Heading Area -->
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Code</strong> :</span> <?=$code?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Category</strong> :</span>
                                 <?php
                                    echo ($b == null) ? 'N/A' : $b[0]['name'];
                                 ?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>UPC</strong> :</span> <?=$upc?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Type</strong> :</span>
                                 <?php
                                    echo ($c == null) ? 'N/A' : $c[0]['list_name'];
                                 ?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Brand</strong> :</span> 
                                 <?php
                                    echo ($d == null) ? 'NA' : $d[0]['name'];
                                 ?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>SKU</strong> :</span> <?=$sku?>
                              </div>
                             
                              <!-- <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <a data-upc="<?=$upc?>" class="compareUpc">
                                    <span><strong class="compareUpc" >UPC</strong>:</span> <?=$upc?> </a><br>
                                    <span class="label label-info"> Price Comparison </span>
                                  </a>
                              </div> -->
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Price</strong> :</span> USD <?=$price?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Shipping Rate</strong> :</span> <?=$shipping_fee?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Date</strong> :</span> <?= $this->common->timetoago($created_at);?>
                              </div>
                           </div>
                          <?php if(isset($productinfo)) : ?>
                           <!-- Short Features  --> 
                           <div class="short-features">
                              <!-- Heading Area -->
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Caliber</strong> :</span> 
                                 <?php echo (isset($productinfo['caliber'])) ? $productinfo['caliber'] : '' ?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Stock description</strong> :</span> <?php echo (isset($productinfo['stock_description'])) ? $productinfo['stock_description'] : '' ?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Frame description</strong> :</span> <?php echo (isset($productinfo['frame_description'])) ? $productinfo['frame_description'] : '' ?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Barrel Length</strong>:</span> 
                                 <?php echo (isset($productinfo['barrel'])) ? $productinfo['barrel'] : '' ?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Capacity</strong> :</span> <?php echo (isset($productinfo['capacity'])) ? $productinfo['capacity'] : '' ?>
                              </div>
                              <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                 <span><strong>Weight</strong> :</span> 
                                 <?php echo (isset($productinfo['weight'])) ? $productinfo['weight'] : '' ?>
                              </div>
                           </div>
                           <!-- Short Features  --> 
                           <?php endif; ?>
                           <!-- Description of product -->
                           <div class="desc-points" >
                             <div class="heading-panel">
                                 <h3 class="main-title text-left">
                                    Description 
                                 </h3>
                              </div>
                              <ul id="descriptionDiv">
                                 <?php
                                    if ( is_array($pdescriptions) && count($pdescriptions) > 0 )  {
                                       foreach ($pdescriptions as $key => $value) {
                                          echo '<li>'.$value['descriptions'].'</li>';
                                       }
                                    }
                                 ?>
                              </ul>
                           </div>
                           <!-- Ad Specifications --> 
                           <div class="specification">
                                <!-- Main Container -->
                                   <!-- Row -->
                                   <!-- <?php echo $this->session->flashdata('message'); ?> -->
                                   <div class="row">
                                      <div class="col-md-12 col-xs-12 col-sm-12">
                                         <div class="banner_viewProd">
                                            <a href="#" id="bannerLink" target="_blank">
                                              <img id="bannerImage" src="https://admin.gunsalefinder.com/images/banners/1540012640YourOnlineMarketplace615x320.jpeg" style="max-width : 615px; height: 320px; object-fit: scale-down; "></a>
                                         </div>
                                      </div>
                                   </div>
                                   <!-- Row End -->
                              <!-- Heading Area -->
                           </div>
                           <div class="clearfix"></div>
                        </div>


                        
                        <div class="clearfix"></div>
                        <!-- =-=-=-=-=-=-= Comments Section =-=-=-=-=-=-= -->
                        <div class="comment-section ad-box" id="commentBox">

                            <div class="heading-panel">
                              <h3 class="main-title text-left">
                                 leave your comment 
                              </h3>
                           </div>
                           <?php
                                if($this->session->userdata('logged_in') || $this->session->userdata('dealer_login') )
                                {
                           ?>
                           <div class="commentform">
                              <form>
                                 <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                       <div class="form-group">
                                          <label>Comment <span class="required">*</span>
                                          </label>
                                          <textarea id="txtComment" class="form-control" placeholder="" rows="2" cols="6"></textarea>
                                       </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 margin-top-20 clearfix">
                                       <button id='addComment' type="button" class="btn btn-theme">Post Your Comment</button>
                                    </div>
                                 </div>
                              </form>
                           </div>
     
                           <?php
                                }
                                else
                                {
                           ?>
                            <div class="alert alert-info">
                                            Please <a href="<?php echo base_url('login') ?>" style="color:black;">LOGIN</a> to submit comment.
                            </div>
                           
                           <?php
                                }
                           ?>
                           <div class="heading-panel">
                              <h3 class="main-title text-left">
                                 Comments (<span id="commentCount"><?php echo $comments['count']; ?></span>)
                              </h3>
                           </div>
                           <ol class="comment-list" id ="commentList" >
                              <!-- comment-list    -->
                                 <?php 
                                    
                                    if ( is_array($comments['data']) && count($comments['data']) > 0 )  {
                                       foreach ($comments['data'] as $key => $value) {
                                           $this->load->view('product/comments', $value);
                                       }
                                    }
                                 ?>
                              <!-- comment-list    -->
                           </ol>
                           <?php if ($comments['next'] == 'Y') :?>
                              <div align='center'><input type='button' value='Load More' id='loadMore' class='btn btn-theme btn-lg btn-block' /></div>
                           <?php endif ?>

                           <input type='hidden' id='product_id' value='<?=isset($id) ? $id : '' ?>' />
                           <input type='hidden' id='start' value='<?=isset($comments["start"]) ? $comments["start"] : '' ?>' />
                           
                           
                           
                           
                        </div>
                        <!-- =-=-=-=-=-=-= Comments Section End =-=-=-=-=-=-= -->
                     </div>
                     <!-- Single Ad End --> 
                  </div>



                  <!-- Right Sidebar -->
                  <div class="col-md-4 col-xs-12 col-sm-12">
                     <!-- Sidebar Widgets -->
                     <div class="sidebar">
                        <!-- Contact info -->
                        <div class="hidden-sm hidden-xs">
                          <div class="contact white-bg">
                             <!-- Email Button trigger modal -->
                          <?php
                          $isDealer = $this->session->userdata('dealer_login');
                          $isDealer = false;
                          if(isset($isDealer) && $a[0]['id'] == $this->session->userdata('id'))
                          { 
                            $isDealer = true;
                          }
                          if($isDealer):
                          ?>
                             <button class="btn-block btn-contact contactUpdate" data-toggle="modal" onclick="location.href='<?php echo base_url('dealer/postdeal/managedeal/'.$id); ?>';" >Manage Deal</button>
                             <button data-upc="<?=$upc?>" class="btn-block btn-contact contactMoney compareUpc" data-toggle="modal" data-target=".price-quote" >Price Comparison</button>
                          <?php else: ?>
                             <button data-upc="<?=$upc?>" class="btn-block btn-contact contactMoney compareUpc" data-toggle="modal" data-target=".price-quote" >Price Comparison</button>
                             
                             <button class="btn-block btn-contact contactCart" data-toggle="modal" data-target=".price-quote" onclick="window.open('<?=isset($item_url) ? $item_url : '' ?>')">Get Deal</button>
                          <?php endif; ?>
                          </div>
                          <!-- Price info block -->   
                          <div class="ad-listing-price">
                             <p>$<?=$price?></p>
                          </div>
                          <!-- User Info -->
                          <div class="white-bg user-contact-info">
                             <div class="user-info-card">
                                <div class="user-photo col-md-4 col-sm-3  col-xs-4">
                                   <img src="<?php echo base_url(); ?>assets/pictures/<?=$a[0]['picture']?>" alt="">
                                </div>
                                <div class="user-information no-padding col-md-8 col-sm-9 col-xs-8">
                                   <span class="user-name"><a class="hover-color" href="profile.html">
                                    <?php
                                        echo is_null($a) ? 'N/A' : $a[0]['company_name'];
                                    ?>
                                    </a></span>
                                   <div class="item-date">
                                      <span class="ad-pub">Published on: <?php echo $this->common->formatdate($created_at); ?></span><br>
                                       <a href="<?=base_url('search?dealer='.$a[0]['id'])?>" class="link">More Deals from this Dealer</a>
                                   </div>
                                </div>
                                <div class="clearfix"></div>
                             </div>

                             <div class="ad-listing-meta">
                                <ul>
                                   <!-- <li>Ad Id: <span class="color">4143</span></li> -->
                                   <!-- <li>Categories: <span class="color">Used Cars</span></li> -->
                                   <!-- <li>Visits: <span class="color">9</span></li> -->
                                   <li>Contact No.: <span class="color"><?=$a[0]['contact']?></span></li>
                                   <li>Location: <span class="color"><?=$this->common->getStatebyDealer($a[0]['id'])?>, <?=$a[0]['city'];?></span></li>
                                   <li>Dealer’s Websites: 
                                    <span> <a href="<?php echo $a[0]['web_url']; ?>" class="label label-danger" target="_blank" >Link</a> </span>
                                   </li>
                                </ul>
                             </div>
                          </div>
                        </div>
                        <!-- Featured Ads --> 
                        <?php if(count($this->homedb->getBannersByLocationCode('VPRMF')) !== 0): ?>
                        <div class="widget">
                           <div class="widget-heading">
                              <h4 class="panel-title"><a>Featured Products</a></h4>
                           </div>
                           <div class="widget-content">
                              <div class="featured-slider-3">
                                 <!-- Featured Ads -->
                                 <?php 
                                    $this->load->view('search/feature', ['feature' => $this->homedb->getBannersByLocationCode('VPRMF') ]);
                                  ?>
                                  <!-- Featured Ads -->
                              </div>
                           </div>
                        </div>
                        <?php endif; ?>
                        <!-- Recent Ads --> 
                        <div class="widget">
                           <div class="widget-heading">
                              <h4 class="panel-title"><a>Related Products</a></h4>
                           </div>
                           <div class="widget-content recent-ads">
                              
                           <!-- Ads -->
                           <?php echo (count($related) == 0) ? 'No Related Products' : ''; ?>
                           <?php  foreach ($related as $rel): 
                              $dealerInfo = is_null($rel->dealer_info) ? null : json_decode($rel->dealer_info, true); 
                            ?>
                              <div class="recent-ads-list">
                                 <div class="recent-ads-container">
                                    <div class="recent-ads-list-image">
                                       <a href="#" class="recent-ads-list-image-inner">
                                       <img src="<?=base_url().'assets/pictures/loading.gif' ?>" class="loadingImage" data-image="<?php echo ($rel->product_image == null) ? $this->product->update_image($rel->id, $rel->upc, $rel->api_created) : $rel->product_image; ?>"  alt="Deal Image">
                                       </a><!-- /.recent-ads-list-image-inner -->
                                    </div>
                                    <!-- /.recent-ads-list-image -->
                                    <div class="recent-ads-list-content">
                                       <h3 class="recent-ads-list-title">
                                          <a href="#" class="viewProduct" data-id="<?php echo $rel->id; ?>">
                                             <?php echo $rel->title; ?>
                                          
                                          </a>
                                       </h3>
                                       <ul class="recent-ads-list-location">
                                          <li><a href="#">
                                            <?php echo isset($dealerInfo)? $dealerInfo[0]['company_name'] : $this->common->getDealerCompanyName($rel->dealer_id); ?>
                                          </a>
                                          </li>
                                       </ul>
                                       <div class="recent-ads-list-price">
                                          $ <?php echo $rel->price; ?>
                                       </div>
                                       <!-- /.recent-ads-list-price -->
                                    </div>
                                    <!-- /.recent-ads-list-content -->
                                 </div>
                                 <!-- /.recent-ads-container -->
                              </div>
                           <?php endforeach; ?>

                           </div>
                        </div>
                     <!-- Sidebar Widgets End -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
    </body>
</html>
