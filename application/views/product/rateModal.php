<div tabindex="-1" class="modal fade rate-product in" id="rate-modal" role="dialog" aria-hidden="true" style="padding-right: 16px; display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                    <span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title">Product Rate</h3>
            </div>
            <div class="modal-body">
                <!-- content goes here -->
                <?php if ( !empty($this->session->userdata("id") ) ) { ?>

                <form>
                    <div class="skin-minimal">
                        <div class="form-group col-md-6 col-sm-6">
                            <ul class="list">
                                <li >
                                  <div class="rating">
                                        <input type="radio" value="1" name="rateRadio" required /> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star-o"></i> 
                                        <i class="fa fa-star-o"></i> 
                                        <i class="fa fa-star-o"></i> 
                                        <i class="fa fa-star-o"></i> 
                                        Poor
                                        <span class="rating-count">(1)</span>
                                   </div>
                                </li>
                                <li>
                                    <div class="rating">
                                        <input type="radio" value="2" name="rateRadio" /> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star-o"></i> 
                                        <i class="fa fa-star-o"></i> 
                                        <i class="fa fa-star-o"></i> 
                                        Fair
                                        <span class="rating-count">(2)</span>
                                   </div>
                                </li>
                                <li>
                                    <div class="rating">
                                        <input type="radio" value="3" name="rateRadio" /> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star-o"></i> 
                                        <i class="fa fa-star-o"></i> 
                                        Average
                                        <span class="rating-count">(3)</span>
                                   </div>
                                </li>
                                <li>
                                    <div class="rating">
                                        <input type="radio" value="4" name="rateRadio" /> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star-o"></i> 
                                        Good
                                        <span class="rating-count">(4)</span>
                                   </div>
                                </li>
                                <li>
                                    <div class="rating">
                                        <input type="radio" value="5" name="rateRadio" /> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star"></i> 
                                        <i class="fa fa-star"></i> 
                                        Excelent
                                        <span class="rating-count">(5)</span> 

                                   </div>
                                </li>
                            </ul>
                        </div>
                    </div>



                    <div class="form-group  col-md-12 col-sm-12">
                        <label>Remarks</label>
                        <textarea id="remarks" placeholder="Enter your remark(s) here." rows="3" class="form-control " required></textarea>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 col-sm-12 margin-bottom-20 margin-top-20">
                        <button id="submitRate" type="button" class="btn btn-theme btn-block">Submit</button>
                    </div>

                </form>

                <?php }else{ ?>
                    <div class="alert alert-info">
                        Please <a href="<?=base_url('login')?>" style="color:black;">LOGIN</a> to submit rate.
                    </div>

                <?php } ?>

                <div class="clearfix"> </div>
                <div>
                    <?php
                            if ( !empty( $comment_rates ) ) :

                                $comment_html = '<div class="panel panel-white post panel-shadow comment-panel">';
                                $comment_html .= '<div class="post-heading">';

                                foreach ($comment_rates as $comments){

                                    $comment_html .= '<div class="row">';
                                    $comment_html .= '<div class=" col-md-6 title h5 text-left">';
                                    $username = ($comments['user']) ? $comments['user'] : $comments['dealer'];
                                    $comment_html .= '<a href="#"><b>'.$username.'</b> <span class="text-muted time">'.$comments['post_date'].' - '.$comments['post_time'].'</span></a>';
                                    $comment_html .= ' </div>';

                                    $rate = $comments['rate'];

                                    $comment_html .= ' <div class=" col-md-6 title h5 rating  text-right">';
                                    $star_1 =  ($rate >= 1)? 'fa-star' : 'fa-star-o';
                                    $comment_html .= '     <i class="fa '.$star_1.'"></i>';

                                    $star_2 =  ($rate >= 2 )? 'fa-star' : 'fa-star-o';
                                    $comment_html .= '     <i class="fa '.$star_2.'"></i>';

                                    $star_3 =  ($rate >= 3 )? 'fa-star' : 'fa-star-o';
                                    $comment_html .= '     <i class="fa '.$star_3.'"></i>';

                                    $star_4 =  ($rate >= 4 )? 'fa-star' : 'fa-star-o';
                                    $comment_html .= '     <i class="fa '.$star_4.'"></i>';

                                    $star_5 =  ($rate >= 5 )? 'fa-star' : 'fa-star-o';
                                    $comment_html .= '     <i class="fa '.$star_5.'"></i>';
                                    $comment_html .= ' </div>';


                                    $comment_html .= '<div class="post-description">';
                                    $comment_html .= ' <p>'.$comments['remarks'].'</p>';
                                    $comment_html .= ' </div>';

                                }

                                $comment_html .= '</div>';
                                $comment_html .= '</div>';
                                echo  $comment_html;
                            endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<style type="text/css">
    .comment-panel{
        padding: 10px 20px 0 20px;
    }
    .comment-panel .post-description p{
        padding: 0 14px;
        font-size: 15px;
        color: #616161;
    }
</style>