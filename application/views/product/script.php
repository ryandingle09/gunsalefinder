<?php 
    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>

<script type="text/javascript">


	var  hash = window.location.hash;
	if(hash != "" && hash == '#comments')
	{
		$('html, body').animate({
			scrollTop: $("#commentBox").offset().top
		}, 2000);		
	}
	// // Pass value to varaible
	// var itemDescription = '$description';
	// // String to JSON format
	// itemDescription = JSON.parse(itemDescription) || '';

	// // Sort and append descriptiop
	// _.forEach( _.sortBy(itemDescription) , function(description) {
	//   	$("#descriptionDiv").append('<li>'+description+'</li>');
	// });

	/* Facebook */
	window.fbAsyncInit = function() {
	console.log('START CONNECT');
	    FB.init({
	      appId            : '<?=FB_ID?>',
	      //cookie: true,
	      //status: true,
	      autoLogAppEvents : true,
	      xfbml            : true,
	      version          : 'v3.1'
	    });
	  };
	
	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "https://connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	
	   $.ajax({url : '/home/getBanners/?location_code=VPMF', dataType : 'json', success : function(result){
			var i = 1;

			if(result.length !== 0)
			{
				document.getElementById("bannerImage").src = result[0]['image_url']; 
				document.getElementById("bannerLink").href = result[0]['link_url']; 
				var renew = setInterval(function(){
					if(result.length == i){
						i = 0;
					}
					else {
						document.getElementById("bannerImage").src = result[i]['image_url']; 
						document.getElementById("bannerLink").href = result[i]['link_url']; 
						i++;
					}
				},5000);
			}
		}});

	// $.ajax({url : 'getTopBanners/', dataType : 'json', success : function(result){

    //     var i = 1;
    //     document.getElementById("bannerImage").src = "<?php echo LOCALADMINURL ?>"+result[0]['image_url']; 
    //     document.getElementById("bannerLink").href = result[0]['link_url']; 
    //     var renew = setInterval(function(){
    //         if(result.length == i){
    //             i = 0;
    //         }
    //         else {
    //             document.getElementById("bannerImage").src = "<?php echo LOCALADMINURL ?>"+result[i]['image_url']; 
    //             document.getElementById("bannerLink").href = result[i]['link_url']; 
    //             i++;

    //         }
    //     },5000);
    // }});

	// function postToFeed(title, desc, url, image){
	// 	var obj = {method: 'feed',link: url, picture: image,name: title,description: desc};
 //        	FB.getLoginStatus(function(response) {
 //        	        console.log(response.status);
 //        	        //if((response.status === 'connected')) {
 //        	                FB.ui( obj, function(response) {
 //        	                    return  console.log(response);
 //        	                });
 //        	        //}
 //        	})
	// }

	function postToFeed(title, desc, url, image){
		var isLogin = <?=(!empty($this->session->userdata("id"))) ? 'true' : 'false';?>;
		if(isLogin){
        	location.href = "<?=base_url('Promotion/facebook/' . $this->session->userdata("id"). '?redirect=' . current_url() . '?' . $_SERVER['QUERY_STRING'])?>" 
			 
			var obj = {method: 'feed',link: url, picture: image,name: title,description: desc};
        	FB.getLoginStatus(function(response) {
        	});
		}
		else
        	location.href = "<?=base_url('promotion/redirecttologin');?>"; 
	}

	$(document).ready(function(){

		$(".btn-twitter").click(function(e){
			var isLogin = <?=(!empty($this->session->userdata("id"))) ? 'true' : 'false';?>;
			if(isLogin){
				window.open("https://twitter.com/intent/tweet?url=http://www.gunsalefinder.com/product/viewProduct?id=<?=$id?>&text=Check%20out,%20this%20product.%20Tweet%20to%20earn%20a%20point%20every%20click.&via=gunsalefinder", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
				location.href = "<?=base_url('Promotion/twitter/' . $this->session->userdata("id") . '?redirect=' . current_url() . '?' . $_SERVER['QUERY_STRING'])?>" 
			}
			else{
				e.preventDefault();
				location.href = "<?=base_url('promotion/redirecttologin');?>"; 
			}

		});

		$('#report-issue').click(function(){

			var isLogin = <?=(!empty($this->session->userdata("id"))) ? 'true' : 'false';?>;
			if(isLogin){
				$('.report-product').modal('show');
			}else{
				alert('Please login to continue.');
			}
		});

		$('.rate-product-modal').click(function(){

			//var isLogin = <?//=(!empty($this->session->userdata("id"))) ? 'true' : 'false';?>//;
			//if(isLogin){
				$('.rate-product').modal('show');
			// }else{
			//     alert('Please login to continue.');
			//  }
		});

		$(".btn-gplus").click(function(e){
			var isLogin = <?=(!empty($this->session->userdata("id"))) ? 'true' : 'false';?>;
			if(isLogin){
				window.open("https://plus.google.com/share?url=<?php echo rawurlencode('http://www.gunsalefinder.com/product/viewProduct?id='.$id); ?>", "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
				
				location.href = "<?=base_url('Promotion/googleplus/' . $this->session->userdata("id") . '?redirect=' . current_url() . '?' . $_SERVER['QUERY_STRING'])?>" 
			}
			else{
				e.preventDefault();
				location.href = "<?=base_url('promotion/redirecttologin');?>"; 
			}

		});

		$("#btnShareEmailProduct").click(function(e){
			var isLogin = <?=(!empty($this->session->userdata("id"))) ? 'true' : 'false';?>;
			if(isLogin){
				
				location.href = "<?=base_url('Promotion/shareemailwithproduct/')?>" + encodeURI($("#modal_shareemail").val())  +  "/" + <?=$id?>
			}
			else{
				e.preventDefault();
				location.href = "<?=base_url('promotion/redirecttologin');?>"; 
			}

		});
	
	});


	/* Edit Comment v2 */
	
	function toggleEdit(id)
	{
		$('#util'+id).hide();
		$('#message'+id).hide();
		$('#edit'+id).show();
	}

	function updateComment(id)
	{
		var newComment = $('#textMsg'+id).val();
		if(newComment == '')
		{
			$.blockUI({
				message: '<h3>Comment message is required.</h3>',
				timeout: 1000	
			});
		}
		else{
			$.ajax({
				url: '<?php echo base_url('product/update_comments') ?>',
				data: {new_comment: newComment, comment_id : id},
				type: 'POST',
				beforeSend: function()
				{
					$.blockUI({
						message: '<h3>Updating comment...'
					});
				},
				success: function(data)
				{
					console.log(data);
					if(data == 1)
					{
						$('#edit'+id).hide();
						$('#message'+id).html(newComment).show();
						$('#util'+id).show();
						$.unblockUI();
					}	
					
				}
			});
		}
	}

	function deleteComment(id)
	{
		if(confirm('Are you sure to delete this comment?')){
			$.ajax({
				url: '<?php echo base_url('product/delete_comment') ?>',
				data: {comment_id : id},
				type: 'POST',
				beforeSend: function()
				{
					$.blockUI({
						message: '<h3>Deleting comment..'
					});
				},
				success: function(data)
				{
					$('#commentCount').html(data);
					$('#comment'+id).fadeOut().remove();
					$.unblockUI();
				}
			});
		}
	}

	function reportComment(id)
	{
		$('#reportCommentID').val(id)
		$('.report-comment').modal('show');
	}

	$('#reportCommentBtn').click(function(){
		var comment_id = $('#reportCommentID').val();
		var reportDetails = $('#reportCommentDetails').val();
		if(reportDetails == '')
		{
			alert('Please input your details.');
		}
		else
		{
			$.ajax({
				url: '<?php echo base_url('product/submit_commentreport') ?>',
				data: {comment_id: comment_id, details: reportDetails},
				type: 'POST',
				beforeSend: function()
				{
					$('.report-comment').modal('hide');
					$.blockUI({
						message: '<h3>Submitting your report..</h3>'
					});
				},
				success: function(data)
				{
					if(data)
					{
						$.unblockUI();
						alert('Submitted for review. Thank you!');
					}
				}
			});
		}
	});

	$("#commentBtnBox").click(function() {
		$('html, body').animate({
			scrollTop: $("#commentBox").offset().top
		}, 2000);
	});

	$('.btnFbShare').click(function() {
		postToFeed($('#itemTitle').val(), $('#itemDesc').val(), $('#itemUrl').val(), $('#itemImage').val());	
		return false;
	});

	/* Add Comment V2 */

	$('#addComment').click(function(){
		var comment = $('#txtComment').val();
		var product_id = $('#product_id').val();
		if(comment == '')
		{
			$.blockUI({
				message: '<h3>Comment message is required.</h3>',
				timeout: 1000	
			});
		}
		else
		{
			$.ajax({
				url: '<?php echo base_url('product/insert_commentv2') ?>',
				data: {product_id: product_id, message: comment},
				type: 'POST',
				dataType: 'JSON',
				beforeSend: function()
				{
					$.blockUI({
						message: '<h2>Posting your comment..</h2>'
					});
				},
				success: function(data)
				{
					$('#txtComment').val('');
					$('#commentCount').html(data.total);
					$('#commentList').fadeIn().prepend(data.html);
					$.unblockUI();
				}
			});
		}
	});

	/* Add comment */
	$('#bntComments').click(function() {
		$('#myLoading').modal({backdrop: 'static', keyboard: false,})
		$('#myLoading').modal('show');
		$.ajax({
		    type: "POST",
		    url: "<?=base_url()?>product/insert_comments",
		    data: { 
		    	txtName : $('#txtName').val()
				,txtEmail : $('#txtEmail').val()
				,txtComment : $('#txtComment').val()
				,product_id : $('#product_id').val()
			},
		    success: function(_data) { 
				$('#myLoading').modal('hide');
		    	var dataParsed = JSON.parse(_data);
		    	if (dataParsed.errCode == 'R00002' ) {
		    		$('#myModal').modal('show');
		    		$('#myModalMessage').html(dataParsed.errMsg + '<br>Please login to submit your comment.');
		    		$('#myModalBtn').off();
		    		$('#myModalBtn').click(function() {
		    			window.location = '<?=base_url()?>login';
		    		});
		    	}
		    	else {
		    		$('#myModal').modal('show');
		    		$('#myModalMessage').html(dataParsed.errMsg);
		    		$('#myModalBtn').off();
		    		$('#myModalBtn').click(function() {
		    			window.location = window.location;
		    		});
		    	}
	        },
	        error: function(x, t, m) {       
	        }
		});
	});

	/* Load More comments (Offset) */
	$('#loadMore').click(function() {
		$('#myLoading').modal({backdrop: 'static', keyboard: false,})
		$('#myLoading').modal('show');
		$.ajax({
		    type: "POST",
		    url: "<?=base_url()?>product/loadMoreComments",
		    data: { 
		    	product_id : $('#product_id').val()
				,start : $('#start').val()
			},
		    success: function(_data) { 
				$('#myLoading').modal('hide');
		    	var dataParsed = JSON.parse(_data);
		    	$('#commentList').append(dataParsed.views);
		    	$('#start').val(dataParsed.start);
		    	if (dataParsed.next == 'N') $('#loadMore').hide();
	        },
	        error: function(x, t, m) {       
	        }
		});
	});

	/* Add Rate product */
	$('#submitRate').click(function() {
		$('#myLoading').modal({backdrop: 'static', keyboard: false,})
		$('#myLoading').modal('show');
		$.ajax({
		    type: "POST",
		    url: "<?=base_url()?>product/rateProduct",
		    data: { 
		    	product_id : $('#product_id').val()
				,rate : $('input[name=rateRadio]:checked').val()
				,remarks : $('#remarks').val()
			},
		    success: function(_data) { 
				$('#myLoading').modal('hide');
		    	var dataParsed = JSON.parse(_data);
		    	if (dataParsed.errCode == 'R00002' ) {
		    		$('#myModal').modal('show');
		    		$('#myModalMessage').html(dataParsed.errMsg + '<br>Please login to submit your rate.');
		    		$('#myModalBtn').off();
		    		$('#myModalBtn').click(function() {
		    			window.location = '<?=base_url()?>login';
		    		});
		    	}
		    	else {
		    		$('#myModal').modal('show');
		    		$('#myModalMessage').html(dataParsed.errMsg);
		    		$('#myModalBtn').off();
		    		$('#myModalBtn').click(function() {
		    			window.location = window.location;
		    		});
		    	}
	        },
	        error: function(x, t, m) {       
	        }
		});
	});

	/* Add To wishlist */
	$('#addtowishlist').click(function() {

		var id = $('#product_id').val();
		
		if (id != '') { 
			//POST
			$.post( "<?php echo base_url(); ?>/wishlist/addtowishlist", { id: id })
			  .done(function( data ) {
				 if(data == '1'){
				 	alert('Product added to your wishlist');
				 }else if(data == '2'){
				 	alert('Error in adding wishlist. Please report to us!');
				 }else {
				 	alert('Login to continue adding wishlist');
				 }
			});
			//POST
		}
		
	});

	$('#reportProduct').click(function(){
		var id = $('#product_id').val();
		var reportDetails = $('#reportDetails').val();
		var reportType = $('input[name=reportType]:checked').val();
		if (id != '') { 
			$.ajax({
				url: '<?php echo base_url('reportproduct/reportItem') ?>',
				type: 'POST',
				data: { id: id, details: reportDetails, type: reportType },
				beforeSend: function()
				{
					$('.report-product').modal('hide');
					$.blockUI({message: '<h1>Submitting. Please Wait!</h1>'});
				},	
				success: function(data)
				{
					console.log(data);
					$.unblockUI();
					if(data == '1')
					{
						alert('Product successfully reported!');
					}
					else if(data == '2')
					{
						alert('Error in reporting the product!');
					}
					else
					{
						alert('Login to continue reporting this product.');
					}
				}
			});
			//POST
		}
	});

	function subsPDC(elem,id)
	{	
		var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
		
		if(login == '0')
		{
			alert('Please login to subscribe in product.');
		}
		else
		{
			$.ajax({
				url: '<?php echo base_url('product/subsPDC') ?>',
				data: {product_id: id},
				type: 'POST',
				beforeSend: function()
				{
					$.blockUI();
				},
				success: function(data)
				{
					if(data == 'subs')
					{
						$(elem).children().removeClass('fa-bell-o').addClass('fa-bell');
						var dl_count = parseInt($('.s_count_'+id).html());
						$('.s_count_'+id).html(dl_count + 2);
					}
					else
					{
						$(elem).children().removeClass('fa-bell').addClass('fa-bell-o');
						if(dl_count != 0)
							$('.s_count_'+id).html(dl_count - 2);
						
					}
					$.unblockUI();
				}
			});
			
		}
		
	}

	function subsComment(elem,id)
	{	
		var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
		
		if(login == '0')
		{
			alert('Please login to subscribe in product.');
		}
		else
		{
			$.ajax({
				url: '<?php echo base_url('product/subsComment') ?>',
				data: {product_id: id},
				type: 'POST',
				beforeSend: function()
				{
					$.blockUI();
				},
				success: function(data)
				{
					if(data == 'subs')
					{
						
						$(elem).children().removeClass('fa-bell-o').addClass('fa-bell');
					}
					else
					{
						$(elem).children().removeClass('fa-bell').addClass('fa-bell-o');
					}
					$.unblockUI();
				}
			});
			
		}
		
	}

	function subsPriceDrop(elem,id)
	{	
		var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
		
		if(login == '0')
		{
			alert('Please login to subscribe in product.');
		}
		else
		{
			$.ajax({
				url: '<?php echo base_url('product/subsPriceDrop') ?>',
				data: {product_id: id},
				type: 'POST',
				beforeSend: function()
				{
					$.blockUI();
				},
				success: function(data)
				{
					if(data == 'subs')
					{
						
						$(elem).children().removeClass('fa-bell-o').addClass('fa-bell');
					}
					else
					{
						$(elem).children().removeClass('fa-bell').addClass('fa-bell-o');
					}
					$.unblockUI();
				}
			});
			
		}
		
	}

	function likeItem(elem,id)
	{	
		var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
		
		if(login == '0')
		{
			alert('Please login to like product.');
		}
		else
		{
			$.ajax({
				url: '<?php echo base_url('product/likeProduct') ?>',
				data: {product_id: id},
				type: 'POST',
				beforeSend: function()
				{
					$.blockUI();
				},
				success: function(data)
				{
					if(data == 'like')
					{
						
						$(elem).children().removeClass('fa-thumbs-o-up').addClass('fa-thumbs-up');
						$('.dislike'+id).removeClass('fa-thumbs-down').addClass('fa-thumbs-o-down');

						var count = parseInt($('.l_count_'+id).html());
						$('.l_count_'+id).html(count + 1)
						
						var dl_count = parseInt($('.dl_count_'+id).html());

						if(dl_count != 0)
							$('.dl_count_'+id).html(dl_count - 1);
					}
					else
					{
						$(elem).children().removeClass('fa-thumbs-up').addClass('fa-thumbs-o-up');
						var count = parseInt($('.dl_count_'+id).html());

						if(count != 0)
							$('.dl_count_'+id).html(count - 1)

						var l_count = parseInt($('.l_count_'+id).html());
						if(l_count != 0)
							$('.l_count_'+id).html(l_count - 1)
					}
					$.unblockUI();
				}
			});
			
		}
		
	}

	function dislikeItem(elem,id)
	{	
		var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
		
		if(login == '0')
		{
			alert('Please login to dislike product.');
		}
		else
		{
			$.ajax({
				url: '<?php echo base_url('product/checkifdislikeexist') ?>',
				data: {product_id : id},
				type: 'POST',
				beforeSend: function()
				{
					$.blockUI();
				},
				success: function(data)
				{
					if(data == 'dislike')
					{
						$('.dislike'+id).removeClass('fa-thumbs-down').addClass('fa-thumbs-o-down');
						
						var count = parseInt($('.dl_count_'+id).html());
						var count_dislike = count - 1;

						if(count !== 0)
							$('.dl_count_'+id).html(count_dislike);

						$.unblockUI();
					}
					else{
						$.unblockUI();
						$('#product_id').val(id);
						$('.dislike-product').modal('show');
					}
				}
			});
			
		}
	}

	function dislikeSubmit()
	{
		var id = $('#product_id').val();
		var dislike_type = $('input[name=disLikeType]:checked').val();
		var dislike_details = $('#dislikeDetails').val();

		if(dislike_type != '' && dislike_details != '')
		{
			$.ajax({
				url: '<?php echo base_url('product/dislikeProduct') ?>',
				data: {product_id: id, dislike_type: dislike_type, dislike_details: dislike_details},
				type: 'POST',
				beforeSend: function()
				{
					
					$('.dislike-product').modal('hide');
					$.blockUI();
				},
				success: function(data)
				{	
					if(data == 'dislike')
					{
						$('.dislike'+id).removeClass('fa-thumbs-o-down').addClass('fa-thumbs-down');
						$('.like'+id).removeClass('fa-thumbs-up').addClass('fa-thumbs-o-up');

						var count_like = ($('.l_count_'+id+'').html() !== '0') ? parseInt($('.l_count_'+id+'').html()) - 1 : '0';
						var count_dislike = parseInt($('.dl_count_'+id+'').html()) + 1;

						$('.l_count_'+id).html(count_like);
						$('.dl_count_'+id).html(count_dislike);
					}
					else
					{
						$('.dislike'+id).removeClass('fa-thumbs-down').addClass('fa-thumbs-o-down');
						$('.like'+id).removeClass('fa-thumbs-up').addClass('fa-thumbs-o-up');
					}
					$.unblockUI();
					$('#dislikeDetails').val('');
				}
			});
		}
		else
		{
			alert('Please fill up the fields.');
		}
	}

	/* If user already done for rating product, details of rate proudct will be displayed. */
	var detailsOfRate = '<?=$rateDetails?>' || null;
	detailsOfRate = typeof detailsOfRate == 'object'? detailsOfRate: JSON.parse(detailsOfRate);

	if (detailsOfRate != null ) {
		$('#remarks').val(detailsOfRate.remarks).attr("disabled", "disabled"); 
		$("input[name=rateRadio][value='"+detailsOfRate.rate+"']").trigger( "click" ); 
		$("input[name=rateRadio] , #submitRate").attr("disabled", "disabled");
	}
	

	
	
</script>
