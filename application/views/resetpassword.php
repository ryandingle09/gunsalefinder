<?php
  ob_start();
  echo "<!DOCTYPE html>\n";
    include ("layouts/header.php");
  ob_end_flush();
?>
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">Reset Password</a></li>
               </ul>
            </div>
         </div>
      </div>



      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding error-page pattern-bg ">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <!-- Middle Content Area -->
                  <div class="col-lg-5 col-lg-offset-4 col-md-5 col-md-offset-7 col-sm-6 col-sm-offset-3 col-xs-12">
                     <!--  Form -->
                     <div class="form-grid">
						<?php
							echo $this->session->flashdata('message'); 
							echo validation_errors(); 
                     $formsubmit = base_url('login/saverecoverpassword');
                     if($usertype == 'dealer'){
                        $formsubmit = base_url('dealer/login/saverecoverpassword');
                     }
						?>
                        <form action="<?php echo $formsubmit; ?>" method="post">
                           <div class="form-group">
                              <label>New Password</label>
                              <input name="userid" value="<?php echo $userid; ?>" type="hidden">
                              <input placeholder="New Password" name="newpassword" class="form-control" type="password">
                           </div>
                           <div class="form-group">
                              <label>Reenter Password</label>
                              <input placeholder="Reenter Password" name="repeat" class="form-control" type="password">
                           </div>
                           <button class="btn btn-theme btn-lg btn-block">Reset my Password</button>
                        </form>
                     </div>
                     <!-- Form -->
                  </div>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>




         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
    </body>
</html>
    