 <div class="user-profile">
      <?php 
         if($users->picture == ''){
            $images = base_url().'assets/images/user-profile.png';
         } else{
            $images = base_url().'assets/pictures/'.$users->picture;
         }
      ?>
         <div class="wrap_imgUserProfile">
            <a href="<?php base_url('profile'); ?>"><img src="<?php echo $images; ?>" alt="Profile Picture"></a>
         </div>
         <div class="profile-detail">
            <h6><?php echo $users->name; ?></h6>
            <ul class="contact-details">
               <li>
                  <i class="fa fa-map-marker"></i> USA
               </li>
               <li>
                  <i class="fa fa-envelope"></i>
                  <?php echo $users->email; ?>
               </li>
               <li>
                  <i class="fa fa-phone"></i> <?php echo $users->contact; ?>
               </li>
            </ul>
         </div>
         <ul>
            <li class="<?php echo ($this->uri->segment(1) == 'profile') ? 'active' : ''; ?>"><a href="<?php echo base_url('profile'); ?>">Profile</a></li>
            <li class="<?php echo ($this->uri->segment(1) == 'wishlist') ? 'active' : ''; ?>">
                  <a href="<?php echo base_url('wishlist'); ?>">My Wishlist 
                  <span class="badge">
                        <?php echo $this->common->countMyWishlist(); ?>
                  </span>
                  </a>
            </li>
            <!--<li ><a href="messages.html">Messages</a></li>-->
            <li><a href="<?php echo base_url('login/logout'); ?>">Logout</a></li>
         </ul>
   </div>
