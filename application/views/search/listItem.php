
<input type="hidden" class="currLink" name="currLink" value="<?php echo $currLink;?>">
<!-- Sorting Filters -->
<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
    <!-- Sorting Filters Breadcrumb -->
    <div class="filter-brudcrums">
    <?php 
            if (is_array($dealer)) 
                echo "Dealer: (<b>". $dealer['company_name']."</b>)";
        ?>
    <span> 
        <br>Showing 
        <span class="showed"> <?= ($paginate["count"] > 0) ?  $paginate["offset"] : '0' ?> - <?=$paginate["limit"]?></span> of 
        <span class="showed"><?=$paginate["count"]?></span> results 
        
    </span>
    <div class="filter-brudcrums-sort">
        <ul>
            <li>
                <select id="sortItem" class="form-controlx not-select2">
                    <option value="default">Sort By</option>
                    <option value="date" <?php echo ($sortCode == 'title') ? 'selected' : '' ?>>Date</option>
                    <option value="price" <?php echo ($sortCode == 'price') ? 'selected' : '' ?>>Price</option>
                    <option value="categories" <?php echo ($sortCode == 'categories') ? 'selected' : '' ?>>Category</option>
                    <option value="rating" <?php echo ($sortCode == 'rating') ? 'selected' : '' ?>>Rating</option>
                    <option value="views_count" <?php echo ($sortCode == 'views_count') ? 'selected' : '' ?>>Views</option>
                    <option value="relevance" <?php echo ($sortCode == 'relevance') ? 'selected' : '' ?>>Relevance</option>
                </select>
            </li>
            <li>&nbsp;</li>
            <li>
                <button id="sortAsc" href="" class="btn btn-default"> <i class="fa fa-chevron-up"></i>  </button>
                <button id="sortDesc" href="" class="btn btn-default"> <i class="fa fa-chevron-down"></i>  </button>
            </li>
        </ul>
    </div>
    </div>
    <!-- Sorting Filters Breadcrumb End -->
</div>
<!-- Sorting Filters End-->
<div class="clearfix"></div>
<!-- Ads Archive -->

<div class="posts-masonry">
    <div class="col-md-12 col-xs-12 col-sm-12 user-archives">
        <div class="row">
        <?php 
            foreach ($product as $key => $value):
            ?>
            <!-- Ads Listing -->
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <!-- Image Block -->
                <div class="wrap_listitem">

                    <!-- Img Block -->
                    <div class="images-search">
                        <img src="<?=base_url(). 'assets/pictures/loading.gif'?>" class="loadingImage" data-image="<?php echo $value['product_image']; ?>" alt="<?=$value['title']?>">
                    </div>
                    <!-- Img Block -->
                    <label class="last-updated"> <?= $this->common->timetoago($value['created_at']);?> </label>
                    <!-- Price -->
                    <div class="price-search">
                        <label class="lblPrice">Price:&nbsp <?=$value['price']?></label>
                        <span class="lblShipingFee"><br>Shipping Fee:&nbsp <?=isset($value['shipping_fee']) ? $value['shipping_fee'] : '0.00' ?></span>
                    </div>


                    <!-- Content Block -->
                    <div class="wrap_listItemInfo">
                        
                        <div class="wrap_titleItem">
                            <a href="<?=base_url('product/viewProduct?id='.$value['id'])?>">
                                <label class="lbl_ProdName" data-id="<?=$value['id']?>"><?=$value['title']?>
                                    <br>
                                    <?php
                                        $dealer = is_null($value['dealer_info']) ? null : json_decode($value['dealer_info'], true);
                                        echo is_null($dealer) ? '' : $dealer[0]['company_name']
                                    ?>
                                </label>
                            </a>
                        </div>
                                    
                        <!-- Category -->
                        <div class="wrap_lbl_Cat"> 
                            <ul>
                                <li>
                                    <a href="<?=base_url('search/index/?category='.$value['category_code'])?>">
                                        <label>Category:</label>
                                        <?=$value['category_code'] ? : 'N/A'?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=base_url('search/index/?brand='.$value['brand_id'])?>">
                                        <label>Brand:</label>
                                        <?php
                                            $brand = ($value['brand_info'] == null) ? null : json_decode($value['brand_info'], true);
                                            echo ($brand == null) ? 'N/A' : $brand[0]['name'];
                                        ?>
                                    </a>
                                </li>
                            </ul>
                        </div> 

                        <div class="ad-details">
                            <ul>
                                <li>
                                    <a data-upc="<?=$value['upc']?>" class="compareUpc">
                                        UPC: 
                                        <span class="compareUpc" ><?=$value['upc']?></span> 
                                        <a href="<?php echo base_url('compare/prices/'.$value['upc']) ?>">
                                        <span class="label label-info lbl_Pcomp"> 
                                            Price Comparison
                                        </span>
                                        </a>
                                    </a>
                                </li>
                                <li>
                                    <i class="fa fa-stream"></i> SKU: <span class=""><?=$value['sku']?></span>    
                                </li>
                            </ul>
                        </div>
                        


                        <!-- Short Description -->
                        <div class="clearfix visible-xs-block"></div>




                        <div class="row add_paddingRT">
                            <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="rating">
                                    <?php 
                                        for ($x = 1; $x <= 5; $x++) {
                                            if ( $value['rating'] >= $x )
                                                echo '<i class="fa fa-star"></i>';
                                            else if ( isset($rate['rating']) && explode('.', $rate['rating'])[1] >= '50') 
                                                echo '<i class="fa fa-star-half-o"></i>';
                                            else
                                                echo '<i class="fa fa-star-o"></i>';
                                        } 
                                    ?>
                                    <span class="rating-count">(<?=$value['total_rates']?>)</span>
                                </div>
                            </div> -->
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="ad-info2">
                                    <ul>
                                        <li>
                                            <a onClick="likeItem(this, <?php echo $value['id'] ?>)" title="Like This Deal">
                                                <i class="fa <?php echo ($this->common->thisProductLike($value['id'])) ? 'fa-thumbs-up' : 'fa-thumbs-o-up' ?> like<?php echo $value['id'] ?>" aria-hidden="true"></i>
                                                <!-- <span class="l_count"><?php echo $this->product->get_count('like', $value['id']);?></span> -->
                                                <span class="l_count_<?=$value['id'];?>"><?php echo $value['like_count'];?></span>
                                            </a>
                                            &nbsp;
                                            <a onClick="dislikeItem(this, <?php echo $value['id'] ?>)" title="Dislike This Deal">
                                                <i class="fa <?php echo ($this->common->thisProductDislike($value['id'])) ? 'fa-thumbs-down' : 'fa-thumbs-o-down' ?> dislike<?php echo $value['id'] ?>" aria-hidden="true"></i>
                                                <!-- <span class="dl_count"><?php echo $this->product->get_count('dislike', $value['id']);?></span> -->
                                                <span class="dl_count_<?=$value['id'];?>"><?php echo $value['dislike_count'];?></span>
                                            </a>
                                            &nbsp;
                                            <a href="<?php echo base_url('product/viewProduct?id='.$value['id']) ?>#comments"  title="Comments">
                                                <i class="fa fa-comment-o" aria-hidden="true"></i>
                                                <!-- <span class="c_count"><?php echo $this->product->get_count('comment', $value['id']);?></span> -->
                                                <span class="c_count_<?=$value['id'];?>"><?php echo $value['comment_count'];?></span>
                                            </a>
                                            &nbsp;
                                            <a onClick="subsPDC(this, <?php echo $value['id'] ?>)" title="Subscribe for Price Drop and Comments">
                                            <i class="fa <?php echo ($this->common->isProductSubs($value['id'])) ? 'fa-bell' : 'fa-bell-o' ?>" aria-hidden="true"></i>
                                                <!-- <span class="s_count"><?php echo $this->product->get_count('subscriber', $value['id']);?></span> -->
                                                <span class="s_count_<?=$value['id'];?>"><?php echo $value['subscriber_count'];?></span>
                                            </a>
                                            <a href="#">
                                                <i class="fa fa-eye"></i>
                                                <span><?php echo $value['view_count'] ;?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Content Block End -->


                    <div class="wrap_buttonsItems">
                        <div class="ad-meta">
                            <ul>
                                <li>
                                    <button class="save-ad btnProd btn addToCompare" data-id="<?=$value['id']?>"><!-- <i class="fa fa-plus-circle"></i> -->Add to Compare</button>
                                </li>
                                <li>
                                    <a href="<?=base_url('product/viewProduct?id='.$value['id'])?>" class="viewProduct btnProd btn btn-success" data-id="<?=$value['id']?>"><!-- <i class="fa fa-eye"></i> -->View Details</a>
                                </li>
                            </ul>
                            
                        </div>
                    </div>
                </div>

            </div>
            <?php 
            endforeach;
            ?>
            <div id="paginationDiv" class="col-md-12 col-xs-12 col-sm-12">
                <?php 
                    echo ($paginate["pageLinks"]);
                ?>
            </div>
            <?php if(count($product) == 0):?>
                <div class="col-md-12">  
                    <h4 class="text-center">No results found.</h4>
                </div>
            <?php endif;?>
        </div>                  
    </div>
</div>

<!-- Ads Archive End -->  
<div class="clearfix"></div> 
<!-- <div id="paginationDiv" class="col-md-12 col-xs-12 col-sm-12">
    <?php 
        echo ($paginate["pageLinks"]);
    ?>
</div> -->
