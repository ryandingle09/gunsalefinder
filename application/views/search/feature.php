<?php foreach ($feature as $ads):?>
<div class="item">
    <div class="col-md-12 col-xs-12 col-sm-12 no-padding">
        <!-- Ad Box -->
        <div class="category-grid-box">
        <!-- Ad Img -->
        <div class="category-grid-img">

            <div class="wrap_imgSlider_serch">
            <?php if(is_null($ads['image_url'])):?>
                <img src="<?php echo ADMINURL.'images/banners/1539619369GSFOnlineMarketplaceBlack313x234.jpg'; ?>" alt="No image uploaded">
            <?php else:?>
                <?php if(file_exists($ads['image_full_path'])):?>
                    <a href="<?php echo (!is_null($ads['link_url'])) ? $ads['link_url'] : '#'; ?>">
                        <img class="loadingImage" data-image="/<?php echo $ads['image_url']; ?>" src="/<?php echo $ads['image_url']; ?>" alt="<?php echo $ads['title']; ?>">
                    </a>
                <?php else:?>
                    <a href="<?php echo (!is_null($ads['link_url'])) ? $ads['link_url'] : '#'; ?>">
                        <img class="loadingImage" data-image="<?php echo ADMINURL.$ads['image_url']; ?>" src="<?php echo ADMINURL.$ads['image_url']; ?>" alt="<?php echo $ads['title']; ?>">
                    </a>
                <?php endif;?>
            <?php endif;?>
                
            </div>
            
            <!-- Ad Status -->


            <span class="ad-status"> Featured </span>
            <!-- User Review -->
            <!-- <div class="user-preview">
                <a href="#"> <img src="<?php echo base_url(); ?>assets/images/users/2.jpg" class="avatar avatar-small" alt=""> </a>
            </div> -->
            <a class="view-details " href="<?php echo (!is_null($ads['link_url'])) ? $ads['link_url'] : '#'; ?>">View Details</a>
        </div>
        <!-- Ad Img End -->
        <div class="short-description">
            <!-- Ad Category -->
            <div class="category-title"> <span><a href="#"> <a href="#"><?=$this->common->getCategoryname($ads['category'])?></a> </a></span> </div>
            <!-- Ad Title -->
            <h3><a title="" href="<?php echo (!is_null($ads['link_url'])) ? $ads['link_url'] : '#'; ?>"> <?=$ads['title']?></a></h3>
            <!-- Price -->
            <div class="price"> <?=$ads['price']?> <span class="negotiable"></span></div>
        </div>
        <!-- Addition Info -->
        <div class="lbl_featured_info">
            <ul>
                <li><i class="fa fa-map-marker"></i> <?=$this->common->getStatename($ads['state'])?> </li>
                <li><i class="fa fa-clock-o"></i> <?= $this->common->timetoago($ads['created_at']);?> </li>
            </ul>
        </div>
        </div>
        <!-- Ad Box End -->
    </div>
</div>
<?php endforeach;?>