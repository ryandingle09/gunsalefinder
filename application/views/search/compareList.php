<div class="recent-ads-list">
<button class="btn btn-link margin-bottom-10 close comparison-close rmvCompare" type="button" data-id="<?=$id?>" >remove</button>
    <div class="recent-ads-container">
        <div class="recent-ads-list-image">
            <a href="#" class="recent-ads-list-image-inner">
                <img src="<?=($product_image == null) ? $this->product->update_image($id, $upc, $api_created) : $product_image;?>" alt="<?=$product_image;?>">
            </a>
        </div>
        <div class="recent-ads-list-content">
            <h3 class="recent-ads-list-title">
                <a href="#"><?=$title?></a>
            </h3>
            <ul class="recent-ads-list-location">
                <li>
                    <a href="#"> 
                        <?php
                            $dl = is_null($dealer_info) ? null : json_decode($dealer_info, true); 
                            echo $this->common->getStatebyDealer($dl[0]['id']);
                        ?>
                    </a>
                </li>
            </ul>
            <div class="recent-ads-list-price">
                <?=$price?>
            </div>
        </div>
    </div>
</div>