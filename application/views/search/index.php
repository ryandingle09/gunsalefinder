<?php
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', FALSE);
    header('Pragma: no-cache');
?>
<?php $this->load->view("layouts/header"); ?>
<style type="text/css">
    .wrap_titleItem{
        height: 55px !important;
    }
</style>
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section_padding_srch gray">




            <!-- Main Container -->
            <div class="container-fluid">
               <div class="row">






                        <!-- Left Sidebar -->
                        <div class="col-lg-3 col-md-3 col-sm-12 col-sx-12">
                           <!-- Sidebar Widgets -->
                           <div class="sidebar">
                              
                              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

                                 <!-- Categories Panel -->
                                 <form id="formFilter" method="get"><!-- Panel group -->
                                    <div class="panel panel-default">
                                       <!-- Heading -->
                                       <div class="panel-heading" role="tab" id="headingOne">
                                          <!-- Title -->
                                          <h4 class="panel-title">
                                             <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                             <i class="more-less glyphicon glyphicon-plus"></i>
                                             Categories
                                             </a>
                                          </h4>
                                          <!-- Title End -->
                                       </div>
                                       <!-- Content -->
                                       <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                          <div class="panel-body categories skin-minimal">
                                             <ul id="ulCategories" class="" >
                                                <li>
                                                   <!-- <a href="<?=base_url('search/index?category=all')?>"> -->

                                                   <!-- <input type="radio" class="category-checkbox "  <?=$category_code == false ? 'checked="checked"' : ''?> name="category" value=""> All -->
                                                   <!-- </a> -->
                                                   <input type="radio" class="category-checkbox catCode" name="category" value="all"> All
                                                </li>
                                                <?php
                                                    $cat_list = '';
                                                   foreach ($category as $key => $value) {

                                                       $checked = ($category_code === $value["category_code"])? 'checked="checked"' : '';

                                                       $cat_list .= "<li> <span>
                                                                        <input type='radio'class='category-checkbox catCode' name='category'
                                                                        value='".$value["category_code"]."'
                                                                        $checked
                                                                        data-catCode='".$value["category_code"]."'>
                                                                        {$value["name"]}
                                                                        <span>({$value["count_product"]})</span>
                                                             </span>
                                                        </li>";
                                                   }
                                                   echo $cat_list;
                                                ?>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- Categories Panel End --> 
   
                                    <!-- <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingThree">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                             <i class="more-less glyphicon glyphicon-plus"></i>
                                             Type
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                          <div class="panel-body categories skin-minimal">
                                             <ul id="ulCategories" class="" >
                                                <li>
                                                   <a> <input type="radio" class=" notIncluded" name="sub_category" value=""> All </a>
                                                </li>
                                                <?php 
                                                   foreach ($category as $cat) {
                                                      if (isset($cat['subMenu']) ) {
                                                         foreach ($cat['subMenu'] as $subCat) {
                                                            if ($subCat['count_product'] > 0) {
                                                               echo   "<li>
                                                               <a>
                                                                  <input type='radio'
                                                                  class='category-checkbox subCatCode'
                                                                  name='sub_category'
                                                                  value='".$subCat["sub_category_code"]."'
                                                                  data-catCode='".$cat["category_code"]."'
                                                                  data-catSubCode='".$subCat["sub_category_code"]."'>
                                                                  ".$subCat["list_name"]."
                                                                  <span>(".$subCat["count_product"].")</span>
                                                               </a>
                                                               </li>";
                                                            }
                                                         }
                                                      }

                                                   }
                                                ?>
                                             </ul>
                                          </div>
                                       </div>
                                    </div> -->
                                    <!-- Condition Panel End -->  

                                    <!-- Condition Panel -->    
                                    <div class="panel panel-default">
                                       <!-- Heading -->
                                       <div class="panel-heading" role="tab" id="headingFour">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                             <i class="more-less glyphicon glyphicon-plus"></i>
                                             Brand
                                             </a>
                                          </h4>
                                       </div>
                                       <!-- Content -->
                                       <div id="collapseFour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFour">
                                          <div class="panel-body categories skin-minimal">
                                             <div class="widget-content">
                                                 <select class=" form-control" name="brand" id="brand">
                                                     <option label="Select Option"></option>
                                                     <?php 
                                                         foreach ($brand as $parseBrand) {
                                                             if (isset($parseBrand['id']) )
                                                                 echo '<option value="'.$parseBrand["id"].'"> '.$parseBrand["name"].' </option>';
                                                         }
                                                     ?>
                                                 </select>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <!-- Condition Panel End --> 

                                    <!-- Pricing Panel -->
                                    <div class="panel panel-default">
                                       <!-- Heading -->
                                       <div class="panel-heading" role="tab" id="headingfour">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                             <i class="more-less glyphicon glyphicon-plus"></i>
                                             Price
                                             </a>
                                          </h4>
                                       </div>
                                       <!-- Content -->
                                       <div id="collapsefour" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingfour">
                                          <div class="panel-body">
                                             <span class="price-slider-value">Price ($)<span id="price-min"><?=$priceMin?></span> - <span id="price-max"><?=$priceMax?></span></span>
                                             <div id="price-slider"></div>
                                          </div>
                                       </div>
                                    </div>

                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="headingfive">
                                          <h4 class="panel-title">
                                             <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                             <i class="more-less glyphicon glyphicon-plus"></i>
                                             Filter
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapsefive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingfive">
                                          <div class="recent-ads-container">
                                             <center>
                                                <button id="applyFilter" class="btn btn-success btn-sm margin-bottom-10" type="submit">Apply</button>
                                             </center>
                                          </div>
                                       </div>
                                    </div>
                                 </form>
                                 <!-- Pricing Panel End -->

                                <?php if(count($features) !== 0): ?>
                                 <!-- Featured Ads Panel -->
                                 <div class="panel panel-default">
                                    <!-- Heading -->
                                    <div class="panel-heading" >
                                       <h4 class="panel-title">
                                          <a>
                                          Featured Products
                                          </a>
                                       </h4>
                                    </div>
                                    <!-- Content -->
                                    <div class="panel-collapse">
                                       <div class="panel-body recent-ads rmv_padding">
                                          <div class="featured-slider-3">
                                             <!-- Featured Ads -->
                                             <?php 
                                                $this->load->view('search/feature', ['feature' => $features ]);
                                             ?>
                                              <!-- Featured Ads -->
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Featured Ads Panel End -->
                                <?php endif; ?>


                                 <!-- Latest Ads Panel -->
                                 <div class="panel panel-default">
                                    <!-- Heading -->
                                    <div class="panel-heading" >
                                       <h4 class="panel-title">
                                          <a>
                                          Compare List 
                                          </a>
                                       </h4>
                                    </div>
                                    <!-- Content -->
                                    <div class="panel-collapse">
                                       <div class="panel-body recent-ads">
                                          <!-- Ads -->
                                          <?php 
                                             if (is_array($compareList)) {
                                                foreach ($compareList as $key => $value) {
                                                   $this->load->view('search/compareList', $value);
                                                }
                                             }
                                          ?>
                                          <!-- Ads -->
                                          <div class="recent-ads-list">
                                             <div class="recent-ads-container">
                                                <center>
                                                   <button id="compareNow" class="btn btn-success btn-sm margin-bottom-10" type="button">Compare now</button>
                                                </center>
                                             </div>
                                             <!-- /.recent-ads-container -->
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Latest Ads Panel End -->
                              </div>
                              <!-- panel-group end -->
                           </div>
                           <!-- Sidebar Widgets End -->
                        </div>
                        <!-- Left Sidebar End -->

                        <!-- Middle Content Area -->
                        <div class="col-lg-9 col-md-9 col-sm-12 col-sx-12">
                           <!-- Row -->
                            <div class="row" id="deal-list">
                                <div class="col-md-12" style="margin: 0px auto;padding: auto;margin-top: 10%">
                                    <div class="lds-facebook" style="margin: 0px auto;padding: 0px auto"><div></div><div></div><div></div></div>
                                </div>
                           </div>
                           <!-- Row End -->
                        </div>
                        <!-- Middle Content Area  End -->
               </div>
            </div>
            <!-- Main Container End -->
            <br>
         <br>
         </section>

         <section class="advertizing">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
               <div class="row">
                  <div class="col-md-12 col-xs-12 col-sm-12">
                     <div class="banner">
                        <a href="#" id="bannerLink">
                          <img src="<?php echo ADMINURL; ?>images/sliders/banner-1.jpg" alt="Slider" id="bannerImage" style="width : 680px; height: 90px"></a>
                     </div>
                  </div>
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
        <?php $this->load->view("layouts/footer"); ?>
        <?php $this->load->view("search/script");?>
    </body>
</html>
