
<script type="text/javascript">

var loader = '<div class="col-md-12" style="margin: 0px auto;padding: auto;margin-top: 10%">';
	loader += '<div class="lds-facebook" style="margin: 0px auto;padding: 0px auto"><div></div><div></div><div></div></div>';
	loader += '</div>';

var search_url = window.location.protocol+'//'+window.location.hostname+'/search/render_deals/';

var catCode = '<?=$catCode?>' || 0;
var sortCode = '<?=$sortCode?>' || 0;
var subCatCode = '<?=$subCatCode?>' || 0;
var dealerId = '<?=$dealerId?>' || 0;
var brandId = '<?=$brandId?>' || 0;
var currOffset = '<?=$currOffset?>' || 0;

$('#updatedDate').click(function() {
	var order = '<?=$currOrder?>' == 'desc' ? 'asc' : 'desc';
	window.location = '<?=$currLink?>' + 'created_at/' + order  +'/'+ catCode +'/'+ subCatCode +'/'+ dealerId +'/'+ brandId + '/' + currOffset;
});

$('#price').click(function() {
	var order = '<?=$currOrder?>' == 'desc' ? 'asc' : 'desc';
	window.location = '<?=$currLink?>' + 'price/' + order  +'/'+ catCode +'/'+ subCatCode +'/'+ dealerId +'/'+ brandId + '/' + currOffset;
});

/** Sorting Process */
$('body').on('change','#sortItem', function(){

	var sortby = this.value;
	search_url = $('.currLink').val();

	if(search_url.indexOf('?') != -1)
		search_url += '&sort='+sortby;
	else
		search_url += '?sort='+sortby;

	goUp();
	load_results(search_url);

});

$(document).on('click','#sortAsc', function(){

	search_url = search_url = $('.currLink').val();

	if(search_url.indexOf('?') != -1)
		search_url = search_url+'&sort_type=asc';
	else
		search_url = search_url+'?sort_type=asc';
	
	goUp();
	load_results(search_url);
});

$(document).on('click','#sortDesc', function(){

	search_url = search_url = $('.currLink').val();

	if(search_url.indexOf('?') != -1)
		search_url = search_url+'&sort_type=desc';
	else
		search_url = search_url+'?sort_type=desc';
	
	goUp();
	load_results(search_url);
});

/* Add to compare */
$(document).on('click', '.addToCompare', function() {
	var disableId = <?=json_encode($compareIdList)?>;
	var id = $(this).attr("data-id");

	// ..TODO
	if (_.includes(disableId, id)) {
		alert('Already in compare list');
		return;
	}

	if (disableId.length >= 3 ) return alert('Maximumn of three items only.');

	$('#myLoading').modal({backdrop: 'static', keyboard: false,})
	$('#myLoading').modal('show');
	$.ajax({
		type: "POST",
		url: "<?=base_url()?>compare/addCompare",
		data: { id: id },
		success: function(_data) { 
			// $('#myLoading').modal('hide');
			window.location = window.location;
		},
		error: function(x, t, m) {       
		}
	});
});

$('.catCode').change(function(){
	var catcode = $(this).val();

	$('#brand').val('').trigger('change');

	url = $('.currLink').val();

	if(url.indexOf('dealer') != -1)
		url = url+'&category='+catcode;
	else
		url = window.location.protocol+'//'+window.location.hostname+'/search/render_deals/?category='+catcode;

	goUp();
	load_results(url);
});

// Search Item
$(document).on('click','#applyFilter', function() {
	var catcode = $('input[name=category]:checked').attr('data-catcode') || 0;
	var catSubCode = $('input[name=sub_category]:checked').attr('data-catsubcode') || 0;
	var brandId = $('#brand').val() || 0;
	var minPrice = $('#price-min').html();
	var maxPrice = $('#price-max').html();

	$(this).append('<input type="hidden" name="pMin" value="'+minPrice+'" /> ');
	$(this).append('<input type="hidden" name="pMax" value="'+maxPrice+'" /> ');
});

$(document).on('submit','#formFilter', function(e){
	e.preventDefault();
	
	var catcode = $('input[name=category]:checked').attr('data-catcode') || '';
	var catSubCode = $('input[name=sub_category]:checked').attr('data-catsubcode') || '';
	var brandId = $('#brand').val() || '';
	var minPrice = $('#price-min').html();
	var maxPrice = $('#price-max').html();

	var url = $('.currLink').val();

	if(url.indexOf('dealer') != -1)
		url = url+'&category='+catcode+'&sub_category='+catSubCode+'&brand='+brandId+'&pMin='+minPrice+'&pMax='+maxPrice;
	else
		url = '/search/render_deals/?category='+catcode+'&sub_category='+catSubCode+'&brand='+brandId+'&pMin='+minPrice+'&pMax='+maxPrice;

	goUp();
	load_results(url);
});

$('#collapseThree').parent().attr('hidden','true');
$('.catCode').click(function() {
	var catcode = $(this).attr("data-catcode");
	/* Hide All*/

	/* Clear radio button after clickng categories */
	// $('input[name="subCategories"]').attr('checked', false);
	$('input[name="subCategories"]').prop('checked', false);

	/* Unhide the Type (SubCategories) */
	$('#collapseThree').parent().removeAttr('hidden');
	$('[name=subCategories]').parent().attr('hidden','true');
	/* Reveal */
	$('*[data-catcode="'+catcode+'"]').parent().removeAttr('hidden');
	$('.notIncluded').parent().removeAttr('hidden');

});

/* Compare now */
$('#compareNow').click(function() {
	window.location = '<?=base_url()?>' + 'compare/index';
});

$('.rmvCompare').click(function() {
	var id = $(this).attr("data-id");
	$('#myLoading').modal({backdrop: 'static', keyboard: false,})
	$('#myLoading').modal('show');
	$.ajax({
		type: "POST",
		url: "<?=base_url()?>compare/removeCompareItem",
		data: { id: id },
		success: function(_data) { 
			// $('#myLoading').modal('hide');
			window.location = window.location;
		},
		error: function(x, t, m) {       
		}
	});
});

var catCode = '<?=$catCode?>' || 0;
var catSubCode = '<?=$subCatCode?>' || 0;

$('input[name="categories"]').attr('checked', false);
if (catCode != 0 )  $("input[name=categories][data-catcode='"+catCode+"']").trigger( "click" );
if (catSubCode != 0 ) $("input[name=subCategories][data-catsubcode='"+catSubCode+"']").trigger( "click" );
if (brandId != 0 ) $('#brand').val(brandId).trigger('change');

var full_url = '<?php echo $currLink;?>';
var url = full_url.replace('index', 'render_deals');

if(url == '<?php echo base_url();?>search')
{
	url = '<?php echo base_url();?>search/render_deals/?search=';
}

var index = url.indexOf('<?php echo base_url();?>search?search='); // and get its index.
if (index != -1) 
{ 
	url = url.replace('<?php echo base_url();?>search?search=', '<?php echo base_url();?>search/render_deals/?search=');
}

var index2 = url.indexOf('<?php echo base_url();?>search?dealer='); // and get its index.
if (index2 != -1) 
{ 
	url = url.replace('<?php echo base_url();?>search?dealer=', '<?php echo base_url();?>search/render_deals/?dealer=');
}

var index3 = url.indexOf('<?php echo base_url();?>search?category='); // and get its index.
if (index3 != -1) 
{ 
	url = url.replace('<?php echo base_url();?>search?category=', '<?php echo base_url();?>search/render_deals/?category=');
}

var index4 = url.indexOf('<?php echo base_url();?>search?sort='); // and get its index.
if (index4 != -1) 
{ 
	url = url.replace('<?php echo base_url();?>search?sort=', '<?php echo base_url();?>search/render_deals/?sort=');
}

var index5 = url.indexOf('<?php echo base_url();?>search?sort_type='); // and get its index.
if (index5 != -1) 
{ 
	url = url.replace('<?php echo base_url();?>search?sort_type=', '<?php echo base_url();?>search/render_deals/?sort_type=');
}

$.get(url, function(result){
	$('#deal-list').html(result);
	$('.posts-masonry .user-archives').attr('style', 'left: 0px; top: 0px;');
	manual_load();
});

$.ajax({url : '/home/getBanners/?location_code=SFS', dataType : 'json', success : function(result){
	var i = 1;
	var duration = 5000;

	if(result.length !== 0)
	{
		duration = result[0]['duration'];
		document.getElementById("bannerImage").src = result[0]['image_url']; 
		document.getElementById("bannerLink").href = result[0]['link_url']; 
		var renew = setInterval(function(){
			if(result.length == i){
				i = 0;
			}
			else {
				document.getElementById("bannerImage").src = result[i]['image_url']; 
				document.getElementById("bannerLink").href = result[i]['link_url']; 
				duration = result[i]['duration'];
				i++;

			}
		}, duration);
	}
}});

//paginatiion ajax
$(document).on('click', '.pagination li a', function(e){
	e.preventDefault();
	goUp();
	var url = $(this).attr('href');
	load_results(url);
});

//deals nav no refresh
$(document).on('click', '.deal-page', function(e){
	e.preventDefault();
	goUp();
	
	var url = $(this).attr('href');
	var type = $(this).data('link');

	if(type == 'category')
		url = '<?php echo base_url();?>search/render_deals/?search=';
	else
		url = '<?php echo base_url();?>search/render_deals/?sort='+type;

	load_results(url);
});

function subsPDC(elem,id)
{	
	var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
	
	if(login == '0')
	{
		alert('Please login to subscribe in product.');
	}
	else
	{
		$.ajax({
			url: '<?php echo base_url('product/subsPDC') ?>',
			data: {product_id: id},
			type: 'POST',
			beforeSend: function()
			{
				$.blockUI();
			},
			success: function(data)
			{
				if(data == 'subs')
				{
					$(elem).children().removeClass('fa-bell-o').addClass('fa-bell');
					var dl_count = parseInt($('.s_count_'+id).html());
					$('.s_count_'+id).html(dl_count + 2);
				}
				else
				{
					$(elem).children().removeClass('fa-bell').addClass('fa-bell-o');
					if(dl_count != 0)
						$('.s_count_'+id).html(dl_count - 2);
				}
				$.unblockUI();
			}
		});
		
	}
	
}

function likeItem(elem,id)
{	
	var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
	
	if(login == '0')
	{
		alert('Please login to like product.');
	}
	else
	{
		$.ajax({
			url: '<?php echo base_url('product/likeProduct') ?>',
			data: {product_id: id},
			type: 'POST',
			beforeSend: function()
			{
				$.blockUI();
			},
			success: function(data)
			{
				if(data == 'like')
				{
					
					$(elem).children().removeClass('fa-thumbs-o-up').addClass('fa-thumbs-up');
					$('.dislike'+id).removeClass('fa-thumbs-down').addClass('fa-thumbs-o-down');
					
					var count = parseInt($('.l_count_'+id).html());
					$('.l_count_'+id).html(count + 1)
					
					var dl_count = parseInt($('.dl_count_'+id).html());

					if(dl_count != 0)
						$('.dl_count_'+id).html(dl_count - 1);
				}
				else
				{
					$(elem).children().removeClass('fa-thumbs-up').addClass('fa-thumbs-o-up');
					var count = parseInt($('.dl_count_'+id).html());

					if(count != 0)
						$('.dl_count_'+id).html(count - 1)

					var l_count = parseInt($('.l_count_'+id).html());
					if(l_count != 0)
						$('.l_count_'+id).html(l_count - 1)
				}
				$.unblockUI();
			}
		});
		
	}
	
}

function dislikeItem(elem,id)
{	
	var login = '<?php echo ($this->session->userdata('logged_in') || $this->session->userdata('dealer_login')) ? '1' : '0' ?>';
	
	if(login == '0')
	{
		alert('Please login to dislike product.');
	}
	else
	{
		$.ajax({
			url: '<?php echo base_url('product/checkifdislikeexist') ?>',
			data: {product_id : id},
			type: 'POST',
			beforeSend: function()
			{
				$.blockUI();
			},
			success: function(data)
			{
				if(data == 'dislike')
				{
					$('.dislike'+id).removeClass('fa-thumbs-down').addClass('fa-thumbs-o-down');

					var count = parseInt($('.dl_count_'+id).html());
					var count_dislike = count - 1;

					if(count !== 0)
						$('.dl_count_'+id).html(count_dislike);

					$.unblockUI();
				}
				else{
					$.unblockUI();
					$('#product_id').val(id);
					$('.dislike-product').modal('show');
				}
			}
		});
		
	}
}

function dislikeSubmit()
{
	var id = $('#product_id').val();
	var dislike_type = $('input[name=disLikeType]:checked').val();
	var dislike_details = $('#dislikeDetails').val();

	if(dislike_type != '' && dislike_details != '')
	{
		$.ajax({
			url: '<?php echo base_url('product/dislikeProduct') ?>',
			data: {product_id: id, dislike_type: dislike_type, dislike_details: dislike_details},
			type: 'POST',
			beforeSend: function()
			{
				$('.dislike-product').modal('hide');
				$.blockUI();
			},
			success: function(data)
			{	
				if(data == 'dislike')
				{
					$('.dislike'+id).removeClass('fa-thumbs-o-down').addClass('fa-thumbs-down');
					$('.like'+id).removeClass('fa-thumbs-up').addClass('fa-thumbs-o-up');

					var count_like = ($('.l_count_'+id+'').html() !== '0') ? parseInt($('.l_count_'+id+'').html()) - 1 : '0';
					var count_dislike = parseInt($('.dl_count_'+id+'').html()) + 1;

					$('.l_count_'+id).html(count_like);
					$('.dl_count_'+id).html(count_dislike);
				}
				else
				{
					$('.dislike'+id).removeClass('fa-thumbs-down').addClass('fa-thumbs-o-down');
					$('.like'+id).removeClass('fa-thumbs-up').addClass('fa-thumbs-o-up');
				}
				$.unblockUI();
				$('#dislikeDetails').val('');
			}
		});
	}
	else
	{
		alert('Please fill up the fields.');
	}
}

function load_results(url)
{
	$('#deal-list').html(loader);
	$.get(url, function(result){
		$('#deal-list').html(result);
		$('.posts-masonry .user-archives').attr('style', 'left: 0px; top: 0px;');
		manual_load();
	});
}

function manual_load()
{
	$(".loadingImage").each(function() {
		var  image_url =  $(this).attr("data-image");
		console.log('GET LINK');
		$(this).attr('src', $(this).attr("data-image")); 
		console.log($(this).attr("data-image"));
		$(".loadingImage").error(function(err){
			$(this).attr('src',  '<?=base_url()?>' + 'assets/pictures/default.jpg' );
		});

		var source = $(this);

		$(this).parent('div, a, td').zoom({
			url: image_url,
			callback: function(){
				var toBeHeight = Number(this.height) > Number(source.height()) ?  this.height : source.height();
				var toBeWidth = Number(this.width) > Number(source.width()) ?  this.width : source.width();

				console.log('toBeHeight.height()');
				console.log(toBeHeight);

				$(this).css({
					'width' : toBeWidth+'px',
					'height' : toBeHeight+'px',
				});
			},
			magnify: '0.1%',
			onZoomOut: true,
			onZoomIn: true,
		});
	});
}

function goUp()
{
	$("html, body, .deal-list").animate({ scrollTop: 100 }, "slow");
}
</script>