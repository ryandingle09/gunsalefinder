﻿<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url(); ?>">Home</a></li>
                  <li><a class="active" href="#">Post Ads</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding  gray content-section">


            <!-- Main Container -->
            <div class="container-fluid">
               <!-- Row -->
               <div class="row">



                  <div class="col-md-2 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink">
                          <img style="height: 600;width: 160" id="bannerImage" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                  </div>
                  

                  <div class="col-md-8 col-sm-12 col-xs-12">
                     <!-- end post-padding -->
                     <div class="post-ad-form postdetails">
                        <div class="heading-panel">
                           <h3 class="main-title text-left">
                              Post Your Ad
                           </h3>
                        </div>
                        <!--<p class="lead">Banner Store where dealers can purchase their ads placement:</p>-->
                        <form  class="submit-form">
                           <!-- Select Package  -->
                           <div class="select-package">
                              	<div class="no-padding col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                 <h3 class="margin-bottom-20">Select Package</h3>
                              <?php  foreach ($results as $ads): ?>
                                 <div class="pricing-list">
                                    <div class="row">
                                       <div class="col-md-9 col-sm-9 col-xs-12">
                                       <h3><?php echo $ads->title; ?>   
                                          <small>( <?php echo $ads->size; ?> )</small>
                                       </h3>
                                          <p>
                                             <?php echo $ads->description; ?>
                                          </p>
                                          <span class="label label-info">
                                          
                                             <?php echo ($ads->status == 1) ? "Available":"Sold"; ?>
                                          
                                          </span>
                                       </div>
                                       <!-- end col -->
                                       <div class="col-md-3 col-sm-3 col-xs-12">
                                          <div class="pricing-list-price text-center">
                                              <h4>$ <?php echo $ads->price; ?></h4>
                                              <?php if(in_array($ads->id, $myads) == TRUE):?>
                                                You already purchased this Package <i class="fa fa-check"></i>
                                              <?php else:?>
                                                <a href="<?php echo base_url('dealer/post/addpackage/'.$ads->id);?>" class="btn btn-theme btn-sm btn-block">Select</a>
                                                <a data-toggle="modal" data-target="#viewLocationModal" class="btn btn-theme btn-sm btn-block viewlocation" data-id="<?=$ads->id;?>">View Location</a>
                                              <?php endif;?>
                                          </div>
                                       </div>
                                       <!-- end col -->
                                    </div>
                                    <!-- end row -->
                                 </div>
                              <?php endforeach; ?>


                              </div>
                           </div>   
                           <!-- end row -->
                        </form>
                     </div>
                     <!-- end post-ad-form-->
                  </div>
                  <!-- end col -->


                  <div class="col-md-2 hidden-sm hidden-xs">
                      <div class="wrap_ads text-center">
                          <a href="#" target="_blank" id="bannerLink1">
                          <img style="height: 600;width: 160" id="bannerImage1" src="<?php echo ADMINURL;?>images/banners/1539445864ThompsonLehAnimatedBanner160x600.gif">
                          </a>
                      </div>
                  </div>


               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>


   <div class="modal fade" id="viewLocationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content"> 
          <div class="modal-body" align="center">
                  <img id="location_image" width="313px"  height="234px" style="object-fit: scale-down; "/>
          </div>
          <div class="modal-footer">
               <div>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
          </div>
        </div>
      </div>
    </div>
         <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
      <?php $this->load->view("layouts/footer"); ?>
      <?php $this->load->view("ads/scripts"); ?>
    </body>
</html>