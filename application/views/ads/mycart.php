﻿<?php $this->load->view("layouts/header"); ?>
      <!-- Small Breadcrumb -->
      <div class="small-breadcrumb">
         <div class="container">
            <div class=" breadcrumb-link">
               <ul>
                  <li><a href="<?php echo base_url('eshop'); ?>">Home</a></li>
                  <li><a href="<?php echo base_url('dealer-post'); ?>">Post Ads</a></li>
                  <li><a class="active" href="#">My Cart</a></li>
               </ul>
            </div>
         </div>
      </div>
      <!-- Small Breadcrumb -->
      <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
      <div class="main-content-area clearfix">
         <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
         <section class="section-padding gray">
            <!-- Main Container -->
            <div class="container">
               <!-- Row -->
            <form action="<?php echo base_url('dealer/post/purchasenow'); ?>" method="post">
               <div class="row">
              <?php 
               echo $this->session->flashdata('message');  
               echo validation_errors(); 
               echo (isset($error))?$error:'';
              ?>
                  <div class="col-md-8 col-sm-12 col-xs-12">
                     <!-- Row -->
                     <div class="profile-section">
                        <div class="profile-tabs">
                           <ul class="nav nav-justified nav-tabs">
                              <li class="active"><a href="#profile" data-toggle="tab">
                                 <span class="pull-left">My Cart</span>
                              </a>
                              </li>
                              <!--<li><a href="#settings" data-toggle="tab">Notification Settings</a></li>-->
                           </ul>

                           <div class="tab-pane fade active in" id="payment">
                           <?php $cart_check = $this->cart->contents(); 

                              $i =1 ;
                              $grand_total = 0;

                              if(empty($cart_check)) {
                                 echo 'Your cart is empty';
                              } 
                           ?>
                           <?php if ($cart = $this->cart->contents()): ?>
                              <div class="table-responsive">
                                 <table class="table table-action">
                                    <thead>
                                       <tr>
                                          <th scope="col">No.</th>
                                          <th scope="col">Location</th>
                                          <th scope="col">Size</th>
                                          <th scope="col">Price</th>
                                          <th scope="col">Qty</th>
                                          <th scope="col">Subtotal</th>
                                          <th scope="col"></th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                 <?php foreach ($cart as $item): ?>
                                     <tr>
                                       <td scope="row"><?php echo $i++; ?></td>
                                       <td><?php echo $item['name']; ?></td>
                                       <td><label class="hidden-lg hidden-md hidden-sm lbl_tableList">Size:</label><?php echo $item['size']; ?></td>
                                       <td><label class="hidden-lg hidden-md hidden-sm lbl_tableList">Price:</label><?php echo $item['price']; ?></td>
                                       <td><label class="hidden-lg hidden-md hidden-sm lbl_tableList">Qty:</label><?php echo $item['qty']; ?></td>
                                       <td><label class="hidden-lg hidden-md hidden-sm lbl_tableList">Sub Total:</label><?php echo $item['subtotal']; ?></td>
                                       <td>
                                          
                                          <a href="<?php echo base_url('dealer/post/remove/'.$item['rowid']);?>"> <i class="fa fa-close" style="font-size:24px"></i>  
                                          </a>
                                       </td>
                                     </tr>
                                     <?php $grand_total = $grand_total + $item['subtotal']; ?>
                                 <?php endforeach; ?>
                                 </tbody>
                              </table>
                           </div>
                           <?php endif; ?>
                        
                           <!-- Featured Ad  -->
                           <div class="row">
                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">

                           <div class="row">
                              <div class="widget-heading">
                                    <h4 class="panel-title"><a>Payment Information </a></h4>
                              </div>
                              <!-- Expiration Date  -->
                              <!-- <div class="col-md-8 col-lg-8 col-xs-12 col-sm-12">
                                 <label class="control-label">Name on Card </label>
                                 <span class="color-red">*</span>
                                 <input name="name" class="form-control" type="text" value="<?php echo set_value('name'); ?>">
                              </div> -->
                              <!-- Card Type  -->
                              <!-- <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                 <label class="control-label">Credit Card Type</label>
                                 <span class="color-red">*</span>
                                 <select name="type" class="category form-control" id="subcategory">
                                    <option value="visa">VISA</option>
                                    <option value="mc">Mastercard</option>
                                    <option value="disc">Discover</option>
                                    <option value="ax">AMEX</option>
                                 </select>
                              </div> -->
                           </div>

                           <div class="row">
                              <!-- Expiration Date  -->
                              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                 <label class="control-label">Credit Card Number </label>
                                 <span class="color-red">*</span>
                                 <input name="cardno" class="form-control" type="text" value="<?php echo set_value('cardno'); ?>">
                                 <div style="background-image: url(<?php echo base_url('assets/images/icons/cart.png'); ?>); height: 30px; width: 200px; background-repeat: no-repeat;">
                                 </div>
                              </div>
                              <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                                 
                              </div>
                           </div>

                           <div class="row">
                              <!-- Category  -->
                              <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12">
                                 <label class="control-label">Expiration Date </label>
                                 <select name="month" class="category form-control" id="category">
                                    <option value="">MM</option>
                                    <option value="01">01 - Jan</option>
                                    <option value="02">02 - Feb</option>
                                    <option value="03">03 - Mar</option>
                                    <option value="04">04 - Apr</option>
                                    <option value="05">05 - May</option>
                                    <option value="06">06 - Jun</option>
                                    <option value="07">07 - Jul</option>
                                    <option value="08">08 - Aug</option>
                                    <option value="09">09 - Sep</option>
                                    <option value="10">10 - Oct</option>
                                    <option value="11">11 - Nov</option>
                                    <option value="12">12 - Dec</option>
                                 </select>
                              </div>
                              <!-- Year  -->
                              <?php
                                   $startdate = date("Y");
                                   $years = range($startdate, $startdate+10);
                              ?>
                              <div class="col-md-3 col-lg-3 col-xs-12 col-sm-12">
                                 <label class="control-label">&nbsp; </label>
                                 <select name="year" class="category form-control" id="subcategory">
                                    <option value="">YYYY</option>
                                    <?php foreach($years as $year) { ?>
                                       <option value="<?php echo $year;?>"><?php echo $year; ?></option>
                                    <?php } ?>
                                 </select>
                              </div>
                              <!-- Subcategory  -->
                              <div class="col-md-5 col-lg-5 col-xs-12 col-sm-12">
                                  <label class="control-label">Card Verification No. <small>What is this ?</small></label>
                                 <input name="cvv" class="form-control" type="text" value="<?php echo set_value('cvv'); ?>">
                              </div>
                           </div>
                                
                              </div>
                           </div>

                           </div>
                           </div>
                        </div>
                     </div>

               <?php if(!empty($cart_check)) : ?>
                  <!-- Middle Content Area -->
                  <div class="col-md-4 col-sm-12 col-xs-12 leftbar-stick blog-sidebar">
                     <!-- Sidebar Widgets -->
                     <div class="widget">
                           <div class="widget-heading">
                              <h4 class="panel-title"><a>Order Summary </a></h4>
                           </div>

                           <div class="widget-content">
                              
                                 <dl class="dl-horizontal">
                                    <dt class="pull-summary"><strong>Sub Total: </strong></dt>
                                    <dd>
                                       <?php echo number_format($grand_total, 2); ?>
                                    </dd>
                                    <dt class="pull-summary"><strong>Total: </strong></dt>
                                    <dd>
                                       <?php 
                                       $total = number_format($grand_total + SERVICEFEE, 2);
                                       echo $total;
                                        ?>
                                       <input name="total" type="hidden" value="<?php echo $total; ?>">
                                    </dd>
                                 </dl>

                              <center>
                                    <!--<a href="<?php //echo base_url('eshop/cart/ordernow') ; ?>" class="btn btn-light btn-purchase-now"> Purchase  Now!  </a>-->
                                    <button type="submit" class="btn btn-light btn-purchase-now">Purchase  Now</button>
                                    <a href="<?php echo base_url('dealer-post') ; ?>" class="btn btn-light btn-purchase-now"> Add more!  </a>
                              </center>
                           </div>
                        </div>
                  </div>
               <?php endif; ?>
                     <!-- Row End -->
                  </div>
               </form>
                  <!-- Middle Content Area  End -->
               </div>
               <!-- Row End -->
            </div>
            <!-- Main Container End -->
         </section>
      <?php $this->load->view("layouts/footer"); ?>
    </body>
</html>
