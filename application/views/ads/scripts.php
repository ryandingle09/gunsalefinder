<script>
	
    $('.viewlocation').click(function(e){
        var img_id = $(this).data("id")
        $("#location_image").attr("src", null)
        $.post( "<?php echo base_url(); ?>dealer/postdeal/getimageofads", { id: img_id })
          .done(function( data ) {
            data = JSON.parse(data)
            data = data[0]
            if(data.location_photo)
                $("#location_image").attr("src", "<?=LOCALADMINURL?>" + data.location_photo)
            else
                $("#location_image").attr("src", "<?=base_url('assets/images/ads/ads1.jpg')?>")
        });
    })

    $.ajax({url : '/home/getBanners/?location_code=HRS', dataType : 'json', success : function(result){
		var i = 1;
		var duration = 5000;

		if(result.length !== 0)
		{
			document.getElementById("bannerImage").src = result[0]['image_url']; 
			document.getElementById("bannerLink").href = result[0]['link_url']; 
			duration = result[0]['duration'];

			var renew = setInterval(function(){
				if(result.length == i){
					i = 0;
				}
				else {
					document.getElementById("bannerImage").src = result[i]['image_url']; 
					document.getElementById("bannerLink").href = result[i]['link_url']; 
					duration = result[i]['duration'];
					i++;

				}
			},duration);
		}
	}});

    $.ajax({url : '/home/getBanners/?location_code=HLS', dataType : 'json', success : function(result){
		var i = 1;
		var duration = 5000;

		if(result.length !== 0)
		{
			document.getElementById("bannerImage1").src = result[0]['image_url']; 
			document.getElementById("bannerLink1").href = result[0]['link_url']; 
			duration = result[0]['duration'];

			var renew = setInterval(function(){
				if(result.length == i){
					i = 0;
				}
				else {
					document.getElementById("bannerImage1").src = result[i]['image_url']; 
					document.getElementById("bannerLink1").href = result[i]['link_url']; 
					duration = result[i]['duration'];
					i++;

				}
			},duration);
		}
	}});
</script>