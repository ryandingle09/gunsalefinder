/** 
    *
    *
    TABLE ADJUSTMENT RUN THIS IF YOU DONT HAVE LIVE DATABASE COPY
    - please put date and name, description(optional) for your alter command, modify or crud action in tables
    *
    *
**/

/** RYAN DINGLE on /11/26/2018 **/
ALTER TABLE `orders` ADD `payment_info` JSON NULL DEFAULT NULL AFTER `payment_method`;
ALTER TABLE `ads` ADD `image_full_path` TEXT NULL DEFAULT NULL AFTER `image_url`;
CREATE TABLE `subscription_payment` ( `id` INT NOT NULL AUTO_INCREMENT , `dealer_id` INT NOT NULL , `subscription_id` INT NOT NULL , `payment_info` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `dealer_subscriptions` ADD `is_paid` INT(1) NOT NULL DEFAULT '0' AFTER `subscription_id`;

/**
    RYAN DINGLE on 11/28/2018 
    adding location code for this banner locations for easy spefication rather than id
**/
ALTER TABLE `ad_locations` ADD `location_code` VARCHAR(50) NULL DEFAULT NULL AFTER `id`;
UPDATE `ad_locations` SET `location_code` = 'HTC' WHERE `ad_locations`.`id` = 1;
UPDATE `ad_locations` SET `location_code` = 'HLS' WHERE `ad_locations`.`id` = 2;
UPDATE `ad_locations` SET `location_code` = 'HRS' WHERE `ad_locations`.`id` = 3;
UPDATE `ad_locations` SET `location_code` = 'HMF' WHERE `ad_locations`.`id` = 4;
UPDATE `ad_locations` SET `location_code` = 'HBM' WHERE `ad_locations`.`id` = 5;
UPDATE `ad_locations` SET `location_code` = 'HF' WHERE `ad_locations`.`id` = 6;
UPDATE `ad_locations` SET `location_code` = 'SLMF' WHERE `ad_locations`.`id` = 30;
UPDATE `ad_locations` SET `location_code` = 'VPRMF' WHERE `ad_locations`.`id` = 31;
UPDATE `ad_locations` SET `location_code` = 'VPMF' WHERE `ad_locations`.`id` = 32;
UPDATE `ad_locations` SET `location_code` = 'HMS' WHERE `ad_locations`.`id` = 33;
UPDATE `ad_locations` SET `location_code` = 'SFS' WHERE `ad_locations`.`id` = 34;
UPDATE `ad_locations` SET `location_code` = 'CFS' WHERE `ad_locations`.`id` = 35;
UPDATE `ad_locations` SET `location_code`='FPM' WHERE `id`='37';

ALTER TABLE `ads` ADD `description` TEXT NULL AFTER `title`;

/** RYAN DINGLE - 11/29/2018 
    New add location
**/
INSERT INTO `ad_locations` (`id`, `location_code`, `title`, `size`, `price`, `description`, `location_photo`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES (NULL, 'IPLS', 'Inner Page- Left Side', '160*600', '100.00', 'Frame Width = 160; Frame Height = 600 ; Style = No Border; Location = Top Right of Home Page. Rotate with other ads.', NULL, 'I', '5', '2018-11-29 00:00:00', '5', '2018-11-29 00:00:00'), (NULL, 'IPRS', 'Inner Page- Right Side', '160*600', '100.00', 'Frame Width = 160; Frame Height = 600 ; Style = No Border; Location = Top Right of Home Page. Rotate with other ads.', NULL, 'A', '5', '2018-11-29 00:00:00', '5', '2018-11-29 00:00:00')

/** ELMER - 11/30/2018
	Alter table wishlists (add column - dealer_id)
**/
ALTER TABLE wishlists ADD dealer_id INT;

/** RYAN DINGLE - 12/04/2018 
    Flag for dealer in rate_product table 
**/
ALTER TABLE `rate_product` ADD `is_dealer` VARCHAR(20) NULL DEFAULT NULL AFTER `user_id`;

/** RYAN DINGLE - 01/04/2019 
    Remove on update created at
**/
SET SQL_MODE='ALLOW_INVALID_DATES'
UPDATE products SET updated_at = NULL WHERE updated_at = '0000-00-00 00:00:00'
ALTER TABLE `products` CHANGE `created_at` `created_at` DATETIME NULL DEFAULT NULL;

/** RYAN DINGLE - 01/05/2019 
    Creating new fields for optimize products table
    to be followed on other tables new structure. 
**/
ALTER TABLE `products` 
ADD `comment_count` INT NULL DEFAULT NULL AFTER `view_count`, 
ADD `like_count` INT NULL DEFAULT NULL AFTER `comment_count`, 
ADD `dislike_count` INT NULL DEFAULT NULL AFTER `like_count`, 
ADD `subscriber_count` INT NULL DEFAULT NULL AFTER `dislike_count`, 
ADD `product_image` LONGTEXT NULL DEFAULT NULL AFTER `subscriber_count`, 
ADD `dealer_info` LONGTEXT NULL DEFAULT NULL AFTER `product_images`, 
ADD `category_info` LONGTEXT NULL DEFAULT NULL AFTER `dealer_info`, 
ADD `sub_category_info` LONGTEXT NULL DEFAULT NULL AFTER `category_info`;

ALTER TABLE `products` 
ADD `brand_info` LONGTEXT NULL DEFAULT NULL AFTER `sub_category_info`;

ALTER TABLE `products` 
CHANGE `comment_count` `comment_count` INT(11) NULL DEFAULT '0', 
CHANGE `like_count` `like_count` INT(11) NULL DEFAULT '0', 
CHANGE `dislike_count` `dislike_count` INT(11) NULL DEFAULT '0', 
CHANGE `subscriber_count` `subscriber_count` INT(11) NULL DEFAULT '0';

ALTER TABLE `products` 
CHANGE `product_image` `product_image` LONGTEXT NULL DEFAULT NULL, 
CHANGE `dealer_info` `dealer_info` LONGTEXT NULL DEFAULT NULL, 
CHANGE `category_info` `category_info` LONGTEXT NULL DEFAULT NULL, 
CHANGE `sub_category_info` `sub_category_info` LONGTEXT NULL DEFAULT NULL;

update products set comment_count = 0 where comment_count is NULL;
update products set like_count = 0 where like_count is NULL;
update products set dislike_count = 0 where dislike_count is NULL;
update products set subscriber_count = 0 where subscriber_count is NULL;

UPDATE products 
SET like_count = (
SELECT COUNT(id) 
FROM product_likes 
WHERE product_likes.product_id = products.id AND type='like'
);
       
UPDATE products 
SET dislike_count = (
SELECT COUNT(id) 
FROM product_likes 
WHERE product_likes.product_id = products.id AND type='dislike'
);

UPDATE products 
SET comment_count = (
SELECT COUNT(id) 
FROM comments 
WHERE comments.product_id = products.id
);

-- UPDATE products 
-- SET subscriber_count = (
-- SELECT COUNT(id) 
-- FROM pricedrop_subscriptions 
-- WHERE pricedrop_subscriptions.product_id = products.id
-- );

ALTER TABLE `products` ADD `rate_count` INT NOT NULL DEFAULT '0' AFTER `subscriber_count`;